<?php

namespace App\Repository;

use App\Entity\UserPoint;
use App\Entity\UserPointsExcelencia;
use App\Service\NeoPharmaExcelenciaService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPoint[]    findAll()
 * @method UserPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPointRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPoint::class);
    }

    public function getUserRankByBadge($badge){

        if($badge->getSlug() == 'super-neo'){
            $sql = "SELECT 
                        x.position,
                        x.user_id,
                        x.user_name,
                        x.user_avatar,
                        x.user_is_pharmaceutical,
                        x.total_points
                    FROM (SELECT
                                z.user_id,
                                z.user_name,
                                z.user_avatar,
                                z.user_is_pharmaceutical,
                                @rownum := @rownum + 1 AS position,
                                z.total_points
                            FROM (
                                SELECT
                                    u.id as user_id,
                                    u.name as user_name,
                                    u.avatar as user_avatar,
                                    u.is_pharmaceutical as user_is_pharmaceutical,
                                    SUM(p.point) as total_points
                                FROM user_point p
                                JOIN user u on p.user_id = u.id
                                GROUP BY p.user_id
                                ORDER BY total_points DESC
                            ) z, (SELECT @rownum := 0) r 
                        ) x
                    ORDER BY x.position ASC
                    LIMIT 3";
        } else {
            $sql = "SELECT 
                        x.position,
                        x.user_id,
                        x.user_name,
                        x.user_avatar,
                        x.user_is_pharmaceutical,
                        x.total_points
                    FROM (SELECT
                                z.user_id,
                                z.user_name,
                                z.user_avatar,
                                z.user_is_pharmaceutical,
                                @rownum := @rownum + 1 AS position,
                                z.total_points
                            FROM (
                                SELECT
                                    u.id as user_id,
                                    u.name as user_name,
                                    u.avatar as user_avatar,
                                    u.is_pharmaceutical as user_is_pharmaceutical,
                                    SUM(p.point) as total_points
                                FROM user_point p
                                JOIN user u on p.user_id = u.id
                                WHERE p.badge_id = {$badge->getId()}
                                GROUP BY p.user_id
                                ORDER BY total_points DESC
                            ) z, (SELECT @rownum := 0) r 
                        ) x
                    ORDER BY x.position ASC
                    LIMIT 3";
        }
       
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $response = $stmt->fetchAll();

        return $response;
    }

    public function setPointsToBadge($user, $object, $slug, $type){

        $em = $this->getEntityManager();
        
        $badge = $em->getRepository('App:Badge')->findOneBySlug($slug);

        if(!$badge){
            return false;
        }

        $point = $em->getRepository('App:UserPoint')->findOneBy(['user' => $user, 'badge' => $badge, 'pointTypeId' => $object->getId(), 'isPharmaceutical' => $user->getIsPharmaceutical()]);

        if(!$point){
            $point = new UserPoint();
            $point->setCreatedAt(new \DateTime());
            $point->setIsValid(true);

            $point->setPointType($slug);
            $point->setPointTypeId($object->getId());
            $point->setPointTypeDescription($type);

            if($user->getIsPharmaceutical()){
                $point->setPoint($badge->getGivenPointPharmaceutical());
            } else {
                $point->setPoint($badge->getGivenPointClerk());
            }

            if($slug != 'quem-indica' && $slug != 'downloader'){
                if ($type != 'fazedor-do-novo-quiz') {
                    $point->setIsPharmaceuticalContent($object->getIsOnlyToPharmaceutical());
                }
            }
            
            $point->setIsPharmaceutical($user->getIsPharmaceutical());
            
            $point->setUser($user);
            $point->setBadge($badge);
            
            $em->persist($point);
            $em->flush();

            $em->getRepository('App:User')->setUserBadgeReceive($user, $badge);
        }

        return true;
    }

    // public function setUserPointsNeoExcelencia($user, $object, $slug, $type, $api_excelencia, $admin_user, $admin_pass)
    // {
    //     $em = $this->getEntityManager();
    //    // $points_type = $em->getRepository('App:PointsTypeExcelencia')->findOneBySlug($slug);
    //     $points_type = $em->getRepository('App:PointsTypeExcelencia')->findOneBy(['slug' => $slug, 'active' => true]);

    //     if(!$points_type){
    //         return array('status' => false, 'message' => 'Interação não encontrada.');
    //     }

    //     $user_points = new UserPointsExcelencia();
    //     $user_points->setUser($user);
    //     $user_points->setPointTypeExcelencia($points_type);
    //     $cod_interacao = '';

    //     switch($type) {
    //         case 'blog':
    //             $user_points->setBlogPost($object);
    //             //$cod_interacao = $object->getCodInteraction();
    //             break;
    //         case 'curso':
    //             $user_points->setCourse($object);
    //             $cod_interacao = $object->getCodInteraction();
    //             break;
    //         case 'quiz':
    //             $user_points->setQuiz($object);
    //             //$cod_interacao = $object->getCodInteraction();
    //             break;
    //     }
        
    //     if ($user->getIsPharmacy()) {
    //         $user_points->setIsGestor(true);
    //     } else if($user->getIsPharmaceutical()) {
    //         $user_points->setIsPharmaceutical(true);
    //     } else {
    //         $user_points->setIsClerk(true);
    //     }

    //     $user_points->setCreatedAt(new \DateTime());

    //     $em->persist($user_points);
    //     $em->flush();
     
    //     $excelencia_service = new NeoPharmaExcelenciaService( //API
    //         $api_excelencia
    //     );
       
    //     $cadastra_interacao = $excelencia_service->cadastraInteracao(
    //         $admin_user,
    //         $admin_pass, 
    //         $user->getCnpj(), $user->getCpf(), $user_points->getId(), $cod_interacao
    //     );
        
    //     if ($cadastra_interacao['status']) {
    //         $user_points->setCodReturnApi($cadastra_interacao['codigo']);
    //         $user_points->setDescriptionReturnApi($cadastra_interacao['descricao']);
    //     }
       
    //     $em->persist($user_points);
    //     $em->flush();

    //     if (!$cadastra_interacao['status']) {
    //         return $cadastra_interacao;
    //     }

    //     $em->persist($user_points);
    //     $em->flush();

    //     return array('status' => true, 'message' => 'success');
    // }
}
