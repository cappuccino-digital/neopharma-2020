<?php

namespace App\Repository;

use App\Entity\UserGoogleData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserGoogleData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGoogleData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGoogleData[]    findAll()
 * @method UserGoogleData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGoogleDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserGoogleData::class);
    }

    // /**
    //  * @return UserGoogleData[] Returns an array of UserGoogleData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserGoogleData
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
