<?php

namespace App\Repository;

use App\Entity\UserCorinthiansAlwaysReady;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserCorinthiansAlwaysReady|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCorinthiansAlwaysReady|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCorinthiansAlwaysReady[]    findAll()
 * @method UserCorinthiansAlwaysReady[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCorinthiansAlwaysReadyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCorinthiansAlwaysReady::class);
    }

    // /**
    //  * @return UserCorinthiansAlwaysReady[] Returns an array of UserCorinthiansAlwaysReady objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCorinthiansAlwaysReady
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
