<?php

namespace App\Repository;

use App\Entity\UserStoreManagerResponsibleChange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserStoreManagerResponsibleChange|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserStoreManagerResponsibleChange|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserStoreManagerResponsibleChange[]    findAll()
 * @method UserStoreManagerResponsibleChange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserStoreManagerResponsibleChangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserStoreManagerResponsibleChange::class);
    }

    // /**
    //  * @return UserStoreManagerResponsibleChange[] Returns an array of UserStoreManagerResponsibleChange objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserStoreManagerResponsibleChange
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
