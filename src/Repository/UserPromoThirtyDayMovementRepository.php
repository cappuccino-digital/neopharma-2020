<?php

namespace App\Repository;

use App\Entity\UserPromoThirtyDayMovement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserPromoThirtyDayMovement|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPromoThirtyDayMovement|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPromoThirtyDayMovement[]    findAll()
 * @method UserPromoThirtyDayMovement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPromoThirtyDayMovementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPromoThirtyDayMovement::class);
    }

    // /**
    //  * @return UserPromoThirtyDayMovement[] Returns an array of UserPromoThirtyDayMovement objects
    //  */
    
    public function findOrInstagram($instagram)
    {
        $result = $this->createQueryBuilder('u')
                        ->where('u.instagram = :instagram OR u.instagram = :instagram_')
                        ->setParameter('instagram', $instagram)
                        ->setParameter('instagram_', "@$instagram")
                        ->getQuery()
                        ->getResult();

        $user_promo = isset($result[0]) ? $result[0] : null;

        return $user_promo;
    }

    /*
    public function findOneBySomeField($value): ?UserPromoThirtyDayMovement
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
