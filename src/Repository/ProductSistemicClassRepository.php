<?php

namespace App\Repository;

use App\Entity\ProductSistemicClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductSistemicClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSistemicClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSistemicClass[]    findAll()
 * @method ProductSistemicClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSistemicClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSistemicClass::class);
    }

    // /**
    //  * @return ProductSistemicClass[] Returns an array of ProductSistemicClass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductSistemicClass
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
