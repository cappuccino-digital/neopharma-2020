<?php

namespace App\Repository;

use App\Entity\NeoTvVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NeoTvVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method NeoTvVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method NeoTvVideo[]    findAll()
 * @method NeoTvVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NeoTvVideoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NeoTvVideo::class);
    }

    // /**
    //  * @return NeoTvVideo[] Returns an array of NeoTvVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NeoTvVideo
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
