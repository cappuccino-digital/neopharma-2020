<?php

namespace App\Repository;

use App\Entity\CorinthiansFluminenseCompanion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CorinthiansFluminenseCompanion|null find($id, $lockMode = null, $lockVersion = null)
 * @method CorinthiansFluminenseCompanion|null findOneBy(array $criteria, array $orderBy = null)
 * @method CorinthiansFluminenseCompanion[]    findAll()
 * @method CorinthiansFluminenseCompanion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorinthiansFluminenseCompanionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CorinthiansFluminenseCompanion::class);
    }

    // /**
    //  * @return CorinthiansFluminenseCompanion[] Returns an array of CorinthiansFluminenseCompanion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CorinthiansFluminenseCompanion
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
