<?php

namespace App\Repository;

use App\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlogPost::class);
    }

    public function getNextAndPrevious($id){

        $previous = $id-1;
        $next = $id+1;
        
        $em = $this->getEntityManager();

        $sqlPrevious = "SELECT p.id, p.slug
            FROM blog_post p
            WHERE p.id = {$previous}";
        
        $stmtPrevious = $em->getConnection()->prepare($sqlPrevious);
        $stmtPrevious->execute();
        $previous = $stmtPrevious->fetchAll();
        $response['previous'] = isset($previous[0]) ? $previous[0] : null;

        $sqlNext = "SELECT p.id, p.slug
            FROM blog_post p
            WHERE p.id = {$next}";
        
        $stmtNext = $em->getConnection()->prepare($sqlNext);
        $stmtNext->execute();
        $next = $stmtNext->fetchAll();

        $response['next'] = isset($next[0]) ? $next[0] : null;

        return $response;
    }

    public function getPostByCategoryGraph(){
        
        $sql = "SELECT count(*) as total, c.name
                FROM blog_post p JOIN blog_post_category c ON p.category_id = c.id
                GROUP BY c.name
                ORDER BY total";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getPostByTypeGraph(){
        
        $sql = "SELECT count(*) as total, t.name
                FROM blog_post p JOIN blog_post_type t ON p.type_id = t.id
                GROUP BY t.name
                ORDER BY total";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getPostByMonthGraph(){
        $from = date('2019-06-01 00:00:00');

        $sql = "SELECT count(*) as total, MONTH(created_at) as mes, YEAR(created_at) as ano
                FROM blog_post
                WHERE created_at >= '{$from}'
                GROUP BY MONTH(created_at),YEAR(created_at)
                ORDER BY YEAR(created_at), MONTH(created_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getPostViewsGraph(){
        $sql = "SELECT title, views, created_at, reading_time
                FROM blog_post
                ORDER BY views DESC, created_at ASC
                LIMIT 10";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getPostScoreGraph(){
        $sql = "SELECT title, score, created_at, reading_time
                FROM blog_post
                ORDER BY score DESC, created_at ASC
                LIMIT 10";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}
