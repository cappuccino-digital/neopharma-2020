<?php

namespace App\Repository;

use App\Entity\UserEmblue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserEmblue|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserEmblue|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserEmblue[]    findAll()
 * @method UserEmblue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserEmblueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserEmblue::class);
    }

    // /**
    //  * @return UserEmblue[] Returns an array of UserEmblue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserEmblue
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
