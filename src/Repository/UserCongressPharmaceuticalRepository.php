<?php

namespace App\Repository;

use App\Entity\UserCongressPharmaceutical;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserCongressPharmaceutical|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCongressPharmaceutical|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCongressPharmaceutical[]    findAll()
 * @method UserCongressPharmaceutical[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCongressPharmaceuticalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCongressPharmaceutical::class);
    }

    // /**
    //  * @return UserCongressPharmaceutical[] Returns an array of UserCongressPharmaceutical objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCongressPharmaceutical
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
