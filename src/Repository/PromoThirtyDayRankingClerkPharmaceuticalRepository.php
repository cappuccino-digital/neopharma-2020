<?php

namespace App\Repository;

use App\Entity\PromoThirtyDayRankingClerkPharmaceutical;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromoThirtyDayRankingClerkPharmaceutical|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoThirtyDayRankingClerkPharmaceutical|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoThirtyDayRankingClerkPharmaceutical[]    findAll()
 * @method PromoThirtyDayRankingClerkPharmaceutical[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoThirtyDayRankingClerkPharmaceuticalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoThirtyDayRankingClerkPharmaceutical::class);
    }

    // /**
    //  * @return PromoThirtyDayRankingClerkPharmaceutical[] Returns an array of PromoThirtyDayRankingClerkPharmaceutical objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoThirtyDayRankingClerkPharmaceutical
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
