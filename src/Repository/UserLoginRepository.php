<?php

namespace App\Repository;

use App\Entity\UserLogin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserLogin|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLogin|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLogin[]    findAll()
 * @method UserLogin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLoginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLogin::class);
    }

    public function saveLastLogin($user)
    {
        if ($user) { 
            $user->setLastLoginAt(new \DateTime());

            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();

            $last_login = new UserLogin();
            $last_login->setUser($user);
            $last_login->setLastLoginAt(new \DateTime());
          
            $em->persist($last_login);
            $em->flush();

            $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'acessar-o-portal', 'login', null); //Gamificação interação
        }

        return true;
    }
           
    public function findByInactivity($start, $offset = null)
    {
        $em = $this->getEntityManager();
        if ($offset != null) {
            
            $sql = "select
                        u.id,
                        u.name,
                        u.email,
                        (select  STR_TO_DATE(last_login_at, '%Y-%m-%d') from user_login where user_id = ul.user_id order by last_login_at desc limit 1) as ultimo_login
                    from user_login as ul 
                        inner join user as u
                        on u.id = ul.user_id
                    where 
                        ul.user_id in (select distinct(user_id) from user_login where STR_TO_DATE(last_login_at, '%Y-%m-%d') = '{$start}') 
                        and (select STR_TO_DATE(last_login_at, '%Y-%m-%d') from user_login where user_id = ul.user_id order by last_login_at desc limit 1) = '{$start}'
                        and u.is_active = true
                    group by user_id

                    LIMIT 1 OFFSET {$offset};";
        } else {
           
            $sql = "select
                        u.id,
                        u.name,
                        u.email,
                        (select  STR_TO_DATE(last_login_at, '%Y-%m-%d') from user_login where user_id = ul.user_id order by last_login_at desc limit 1) as ultimo_login
                    from user_login as ul 
                        inner join user as u
                        on u.id = ul.user_id
                    where 
                        ul.user_id in (select distinct(user_id) from user_login where STR_TO_DATE(last_login_at, '%Y-%m-%d') = '{$start}') 
                        and (select STR_TO_DATE(last_login_at, '%Y-%m-%d') from user_login where user_id = ul.user_id order by last_login_at desc limit 1) = '{$start}'
                        and u.is_active = true 
                    group by user_id;";
        }
         //   dump($sql); die();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $user_inactivity = $stmt->fetchAll(); 
        
        return $user_inactivity;
    }

    // /**
    //  * @return UserLogin[] Returns an array of UserLogin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserLogin
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
