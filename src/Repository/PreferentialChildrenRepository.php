<?php

namespace App\Repository;

use App\Entity\PreferentialChildren;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PreferentialChildren|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreferentialChildren|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreferentialChildren[]    findAll()
 * @method PreferentialChildren[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreferentialChildrenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreferentialChildren::class);
    }

    // /**
    //  * @return PreferentialChildren[] Returns an array of PreferentialChildren objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreferentialChildren
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
