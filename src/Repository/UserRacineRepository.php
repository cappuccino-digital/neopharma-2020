<?php

namespace App\Repository;

use App\Entity\UserRacine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPiangers|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPiangers|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPiangers[]    findAll()
 * @method UserPiangers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRacineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserRacine::class);
    }

    // /**
    //  * @return UserPiangers[] Returns an array of UserPiangers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPiangers
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
