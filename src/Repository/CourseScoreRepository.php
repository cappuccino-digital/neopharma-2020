<?php

namespace App\Repository;

use App\Entity\CourseScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CourseScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseScore[]    findAll()
 * @method CourseScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseScoreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CourseScore::class);
    }


    public function getAvgScore($id){
        
        $sql = "SELECT AVG(score) as score
                FROM course_score 
                where course_id = {$id}";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $response = $stmt->fetchAll();

        if(!isset($response[0]['score'])){
            return 0;
        }

        return $response[0]['score'];

    }

    // /**
    //  * @return CourseScore[] Returns an array of CourseScore objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseScore
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
