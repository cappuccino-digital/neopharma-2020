<?php

namespace App\Repository;

use App\Entity\CentroContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CentroContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentroContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentroContact[]    findAll()
 * @method CentroContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentroContactRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CentroContact::class);
    }
    
    // /**
    //  * @return CentroContact[] Returns an array of CentroContact objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CentroContact
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
