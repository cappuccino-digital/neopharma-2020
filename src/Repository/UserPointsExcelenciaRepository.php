<?php

namespace App\Repository;

use App\Entity\UserPointsExcelencia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserPointsExcelencia|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPointsExcelencia|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPointsExcelencia[]    findAll()
 * @method UserPointsExcelencia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPointsExcelenciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPointsExcelencia::class);
    }

    // /**
    //  * @return UserPointsExcelencia[] Returns an array of UserPointsExcelencia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPointsExcelencia
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
