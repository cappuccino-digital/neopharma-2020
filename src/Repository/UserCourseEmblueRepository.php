<?php

namespace App\Repository;

use App\Entity\UserCourseEmblue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserCourseEmblue|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCourseEmblue|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCourseEmblue[]    findAll()
 * @method UserCourseEmblue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCourseEmblueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCourseEmblue::class);
    }

    // /**
    //  * @return UserCourseEmblue[] Returns an array of UserCourseEmblue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCourseEmblue
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
