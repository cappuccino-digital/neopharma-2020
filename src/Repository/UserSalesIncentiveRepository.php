<?php

namespace App\Repository;

use App\Entity\UserSalesIncentive;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSalesIncentive|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSalesIncentive|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSalesIncentive[]    findAll()
 * @method UserSalesIncentive[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSalesIncentiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSalesIncentive::class);
    }

    // /**
    //  * @return UserSalesIncentive[] Returns an array of UserSalesIncentive objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSalesIncentive
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
