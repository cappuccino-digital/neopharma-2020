<?php

namespace App\Repository;

use App\Entity\OrientMe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OrientMe|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrientMe|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrientMe[]    findAll()
 * @method OrientMe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrientMeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrientMe::class);
    }

    // /**
    //  * @return OrientMe[] Returns an array of OrientMe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrientMe
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
