<?php

namespace App\Repository;

use App\Entity\UserPointPromoThirtyDayMovement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserPointPromoThirtyDayMovement|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPointPromoThirtyDayMovement|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPointPromoThirtyDayMovement[]    findAll()
 * @method UserPointPromoThirtyDayMovement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPointPromoThirtyDayMovementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPointPromoThirtyDayMovement::class);
    }

    // /**
    //  * @return UserPointPromoThirtyDayMovement[] Returns an array of UserPointPromoThirtyDayMovement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPointPromoThirtyDayMovement
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
