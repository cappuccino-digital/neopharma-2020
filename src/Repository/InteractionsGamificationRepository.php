<?php

namespace App\Repository;

use App\Entity\InteractionsGamification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InteractionsGamification|null find($id, $lockMode = null, $lockVersion = null)
 * @method InteractionsGamification|null findOneBy(array $criteria, array $orderBy = null)
 * @method InteractionsGamification[]    findAll()
 * @method InteractionsGamification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InteractionsGamificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InteractionsGamification::class);
    }

    // /**
    //  * @return InteractionsGamification[] Returns an array of InteractionsGamification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InteractionsGamification
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
