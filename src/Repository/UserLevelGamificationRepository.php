<?php

namespace App\Repository;

use App\Entity\UserLevelGamification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserLevelGamification|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLevelGamification|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLevelGamification[]    findAll()
 * @method UserLevelGamification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLevelGamificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLevelGamification::class);
    }

    // /**
    //  * @return UserLevelGamification[] Returns an array of UserLevelGamification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserLevelGamification
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
