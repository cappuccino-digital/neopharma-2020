<?php

namespace App\Repository;

use App\Entity\UserCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCourse[]    findAll()
 * @method UserCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCourseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserCourse::class);
    }

    public function getCourseCompletedTotal(){
        $sql = "SELECT count(uc.id) as total, MONTH(uc.conclusion_at) as mes, YEAR(uc.conclusion_at) as ano, u.is_pharmaceutical
                FROM user_course uc JOIN course c ON c.id = uc.course_id JOIN user u ON u.id = uc.user_id
                WHERE uc.conclusion_at IS NOT NULL
                GROUP BY MONTH(uc.conclusion_at),YEAR(uc.conclusion_at), u.is_pharmaceutical
                ORDER BY YEAR(uc.conclusion_at), MONTH(uc.conclusion_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getCourseSubscribesTotal(){
        $sql = "SELECT count(uc.id) as total, MONTH(uc.subscribed_at) as mes, YEAR(uc.subscribed_at) as ano, u.is_pharmaceutical
                FROM user_course uc JOIN course c ON c.id = uc.course_id JOIN user u ON u.id = uc.user_id
                GROUP BY MONTH(uc.subscribed_at),YEAR(uc.subscribed_at), u.is_pharmaceutical
                ORDER BY YEAR(uc.subscribed_at), MONTH(uc.subscribed_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getAvgTimeToComplete(){
        $sql = "SELECT count(c.id) as total,  AVG(TIME_TO_SEC(TIMEDIFF(c.conclusion_at, c.subscribed_at))) timediff, cc.name, cc.classes
        FROM  user_course c inner join course cc on c.course_id = cc.id
        where c.conclusion_at is not null
        group by cc.name
        order by timediff DESC";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function findByCourseStart($date_start, $offset = null) 
    {
        $sql = "select 
                    u.id as user_id,
                    if(u.is_pharmacy, 'Gestor de Loja', if(u.is_pharmaceutical, 'Farmacêutico', 'Balconista')) as perfil, 
                    u.email,
                    c.name as course,
                    c.id as course_id
                from user_course as uc
                    inner join course as c
                    on c.id = uc.course_id
                    inner join user as u
                    on u.id = uc.user_id
                where STR_TO_DATE(uc.subscribed_at, '%Y-%m-%d') = '{$date_start}' and uc.conclusion_at is null and u.is_active = true ";

        if ($offset) {        
            $sql .= "LIMIT 1 OFFSET {$offset};";
        }
        //dump($sql); die();
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(); 
    }

    public function findByCourseFinish($date_start, $offset = null) 
    {
        $sql = "select 
                    u.id as user_id,
                    if(u.is_pharmacy, 'Gestor de Loja', if(u.is_pharmaceutical, 'Farmacêutico', 'Balconista')) as perfil, 
                    u.email,
                    c.name as course,
                    c.id as course_id
                from user_course as uc
                    inner join course as c
                    on c.id = uc.course_id
                    inner join user as u
                    on u.id = uc.user_id
                where STR_TO_DATE(uc.conclusion_at, '%Y-%m-%d') = '{$date_start}' and u.is_active = true ";

        if ($offset) {        
            $sql .= "LIMIT 1 OFFSET {$offset};";
        }

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(); 
    }
}
