<?php

namespace App\Repository;

use App\Entity\PromoThirtyDayMovement;
use App\Entity\PromoThirtyDayRankingClerkPharmaceutical;
use App\Entity\UserSalesIncentive;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method PromoThirtyDayMovement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoThirtyDayMovement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoThirtyDayMovement[]    findAll()
 * @method PromoThirtyDayMovement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoThirtyDayMovementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoThirtyDayMovement::class);
    }

    public function savePoints($user, $cod, $type, $object = null)
    {
        $em = $this->getEntityManager();
        $points_indication = $em->getRepository('App:PromoThirtyDayMovement')->findOneBy(['code' => $cod, 'is_incentivo_vendas' => true]);

        if (!$points_indication) { 
            $points_indication = new PromoThirtyDayMovement();
            $points_indication->setTeam('');
            $points_indication->setName('');
            $points_indication->setCode($cod);
            $points_indication->setPoints(0);
            $points_indication->setAmountClerk(0);
            $points_indication->setAmountPharmaceutical(0);
            $points_indication->setStoreManager(0);
            $points_indication->setIsIncentivoVendas(true);
            $points_indication->setUltimateCronAt(new DateTime());

            $em->persist($points_indication); //atualiza a pontuação total
            $em->flush();
        }
        
        $points_indication = $em->getRepository('App:PromoThirtyDayMovement')->findOneBy(['code' => $cod, 'is_incentivo_vendas' => true]);

        if ($points_indication) {
            $point = $points_indication->getPoints();

            $user_sales_incentive = $em->getRepository('App:UserSalesIncentive')->findOneBy(['user' => $user, 'type' => $type, 'type_id' => $object]);

            $gestor_loja = $points_indication->getStoreManager();
            $farmaceutico = $points_indication->getAmountPharmaceutical();
            $balconista = $points_indication->getAmountClerk();

            $is_gestor_loja = false;
            $is_farmaceutico = false;
            $is_balconista = false;

            if ($user->getIsPharmacy()) {
                $is_gestor_loja = true;
                $gestor_loja += 1;
            } else if ($user->getIsPharmaceutical()) {
                $is_farmaceutico = true;
                $farmaceutico += 1;
            } else if (!$user->getIsPharmaceutical()) {
                $is_balconista = true;
                $balconista += 1;
            }

            switch ($type) {
                case 'curso':
                    // usuário só pode pontuar uma vez por curso
                    if (!$user_sales_incentive) {
                        $point += 1;   
                    }

                    break;
                case 'cadastro':
                    $point += 1;
                    break;
                default:
                    $point = 0;
                    break;
            }

            if (!$user_sales_incentive) {

                $user_sales_incentive = new UserSalesIncentive();
                $user_sales_incentive->setUser($user);
                $user_sales_incentive->setType($type);
                if ($object) {
                    $user_sales_incentive->setTypeId($object->getId());
                }
                $user_sales_incentive->setIsAmountClerk($is_balconista);
                $user_sales_incentive->setIsAmountPharmaceutical($is_farmaceutico);
                $user_sales_incentive->setIsStoreManager($is_gestor_loja);
                $user_sales_incentive->setCode($cod);
                $user_sales_incentive->setCreatedAt(new DateTime());
    
                $em->persist($user_sales_incentive); //atualiza a pontuação total
                $em->flush();
                
                $points_indication->setPoints($point);
                $points_indication->setAmountClerk($balconista);
                $points_indication->setAmountPharmaceutical($farmaceutico);
                $points_indication->setStoreManager($gestor_loja);
                $points_indication->setUltimateCronAt(new DateTime());
    
                $em->persist($points_indication); //atualiza a pontuação total
                $em->flush();
            }

            return true;
        }

        return false;
    }

    public function buscarVendedoresCron()
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.ultimate_cron_at < :dataEnvio')
            ->setParameter('dataEnvio', date('Y-m-d'))
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

    public function vendedoresAtualizados()
    {           
        $now = date('Y-m-d');
        $sql = "SELECT count(id) as qtd FROM promo_thirty_day_movement 
                    where date(ultimate_cron_at) = '$now'";
      
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        if ($row[0]) {
            $row = $row[0]['qtd'];
        }

        return $row;
    }

    public function findRank()
    {
        set_time_limit(360);

        $em = $this->getEntityManager();
       // $business = $em->getRepository('App:PromoThirtyDayMovement')->findAll();

        $business = $this->buscarVendedoresCron();
       
        $array = [];

        foreach ($business as $key => $b) {
            $point = $em->getRepository('App:UserInvite')->findBy(['shareCode'=>$b->getCode()]);
        
            $farmaceutico = 0;
            $balconista = 0;

            if ($point) { // dividindo pontuação entre farmaceutico e balconista 
                foreach ($point as $key_point => $p) { 
                    if ($p->getUser()->getIsPharmaceutical()) {
                        $farmaceutico += 1;
                    } else {
                        $balconista += 1;
                    }
                }
            }
            
            $array[] = ['name'=> $b->getName(), 'team' => $b->getTeam(), 'code'=> $b->getCode(), 'points' => count($point), 'amount_pharmaceutical'=> $farmaceutico, 'amount_clerk'=> $balconista];

           $b->setPoints(count($point));
           $b->setAmountClerk($balconista);
           $b->setAmountPharmaceutical($farmaceutico);
           $b->setUltimateCronAt(new DateTime());

           $em->persist($b); //atualiza a pontuação total
           $em->flush();
        }
        
        return true;
    }

    public function findRankBalFarma()
    {
        set_time_limit(360);
        
        $sql = "SELECT u.name as nome, u.email, p.* FROM user_promo_thirty_day_movement as p
                inner join user as u
                on u.id = p.user_id";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
    
        $promos = $stmt->fetchAll();
        $array = [];

        foreach ($promos as $key => $promo) {
            $points = $em->getRepository('App:UserPointPromoThirtyDayMovement')->findBy(['user_promo_thirty_day_movement'=> $promo['id']]);
            $post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $promo['id']]);

            $qtd_point = 0;
            $point_post_instagram = 0;
            $perfil = '';

            if ($points) {
                foreach ($points as $key_point => $point) {
               
                    $qtd_point += $point->getPoint();
                    
                    $perfil = $point->getIsPharmaceutical();

                    if ($point->getIsPharmaceutical() == true){
                        $perfil = 'Farmacêutico';
                    } else if ($point->getIsPharmaceuticalContent() == true) {
                        $perfil = 'Balconista';
                    } else {
                        $perfil = 'Não identificado';
                    }
                }
            }

            foreach ($post_instagram as $key => $post) { //somando ponto instagram
                $point_post_instagram += $post->getPoint() + $post->getPointExtra();
            }

            $point_type = $this->pointType($promo['user_id']);
            
            if ($qtd_point > 15 || $point_post_instagram > 0) {
                $total_points = $qtd_point + $point_post_instagram;
            } else {
                if ($qtd_point > 10 && $qtd_point != 15) {
                    $total_points = $qtd_point + $point_post_instagram;
                } else {
                    $total_points = 0; 
                } 
            }
            
            if ($total_points > 0) {
                $array[] = [
                            'nome'=> $promo['nome'],
                            'email'=> $promo['email'],
                            'perfil' => $perfil, 
                            'whatsapp' => $promo['whatsapp'], 
                            'instagram'=> $promo['instagram'], 
                            'points' => $total_points, 
                            'n_cadastro' => $point_type['n_cadastro'] ? $point_type['n_cadastro'] : 0, 
                            'n_marco' => $point_type['n_marco'] ? $point_type['n_marco'] : 0, 
                            'n_indicacao' => $point_type['n_indicacao'] ? $point_type['n_indicacao'] : 0, 
                            'n_video' => $point_type['n_video'] ? $point_type['n_video'] : 0,
                            'n_instagram' => $point_post_instagram,
                            'data_cadastro_en' => $promo['created_at'],
                            'data_cadastro' => date('d/m/Y H:i:s', strtotime($promo['created_at']))
                        ];
            }
        }
       
        // Obtain a list of columns
        foreach ($array as $key => $row) {
            $points[$key]  = $row['points'];
            $data[$key] = $row['data_cadastro_en'];
        }
        //dump($points); die(); 
        array_multisort($points, SORT_DESC, $data, SORT_ASC, $array); // ordenando por pontos e data cadastro

        return $array;
    }

    public function pointType($user_id)
    {
       $sql = "
       SELECT
        *
        FROM 
            (SELECT sum(point) as n_cadastro FROM neoquimica.user_point_promo_thirty_day_movement where point_type = 'cadastro-promo-30-dias-de-movimento' and user_id = {$user_id}) as c, 
            (SELECT sum(point) as n_marco FROM neoquimica.user_point_promo_thirty_day_movement where point_type = 'inscricao-antes-de-01-marco-2020' and user_id = {$user_id}) as m,
            (SELECT sum(point) as n_indicacao FROM neoquimica.user_point_promo_thirty_day_movement where point_type = 'indicacao-de-amigo' and user_id = {$user_id}) as i,
            (SELECT sum(point) as n_video FROM neoquimica.user_point_promo_thirty_day_movement where point_type = 'assistiu-video' and user_id = {$user_id}) as v";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        if ($row[0]) {
            $row = $row[0];
        }

        return $row;

    }

    public function findRankingFaramaceuticoBalconista()
    {
        set_time_limit(3600);
        
        $em = $this->getEntityManager();
        $promos = $em->getRepository('App:UserPromoThirtyDayMovement')->findAll();
  
        $array = [];
        
        foreach ($promos as $key => $promo) {
            $points = $em->getRepository('App:UserPointPromoThirtyDayMovement')->findBy(['user_promo_thirty_day_movement'=> $promo->getId()]); //query de todos os pontos do usuario
            $post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $promo->getId()]);
           
            $qtd_point = 0;
            $point_post_instagram = 0;
                         
            foreach ($points as $key_point => $point) { //somando os pontos geral
                
                   // die('teste');
                    $qtd_point += $point->getPoint();
            
            }
            
            foreach ($post_instagram as $key => $post) { //somando ponto instagram
                $point_post_instagram += $post->getPoint() + $post->getPointExtra();
            }
            
            if ($qtd_point > 15 || $point_post_instagram > 0) {
                $total_points = $qtd_point + $point_post_instagram;
            } else {
                if ($qtd_point > 10 && $qtd_point != 15) {
                    $total_points = $qtd_point + $point_post_instagram;
                } else {
                    $total_points = 0; 
                } 
            }
         
            $ranking = $em->getRepository('App:PromoThirtyDayRankingClerkPharmaceutical')->findOneBy(['user_promo' => $promo->getId()]); // ranking

            if (!$ranking) {
                $ranking = new PromoThirtyDayRankingClerkPharmaceutical();
            }
            
            $ranking->setUser($promo->getUser());
            $ranking->setUserPromo($promo);
            $ranking->setPoints($total_points);
            $ranking->setCreatedAt($promo->getCreatedAt());
            $ranking->setUpdatedAt(new DateTime());

            $em->persist($ranking); //atualiza a pontuação total
            $em->flush();
        }

        //die();
        return true;
    }

    function array_sort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}
