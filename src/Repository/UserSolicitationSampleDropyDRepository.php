<?php

namespace App\Repository;

use App\Entity\UserSolicitationSampleDropyD;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSolicitationSampleDropyD|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSolicitationSampleDropyD|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSolicitationSampleDropyD[]    findAll()
 * @method UserSolicitationSampleDropyD[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSolicitationSampleDropyDRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSolicitationSampleDropyD::class);
    }

    // /**
    //  * @return UserSolicitationSampleDropyD[] Returns an array of UserSolicitationSampleDropyD objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSolicitationSampleDropyD
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
