<?php

namespace App\Repository;

use App\Entity\ReferrerContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerContent[]    findAll()
 * @method ReferrerContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerContent::class);
    }

    public function getReferrerContent($id, $type)
    {
        $sql = "SELECT id, referrer_content_id, referrer_content_type
                FROM referrer_content
                WHERE main_content_id = '{$id}' AND main_content_type = '{$type}'
                ORDER BY main_content_type";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $content = $stmt->fetchAll();

        $response = [];

        foreach($content as $c){
            switch($c['referrer_content_type']){
                case 'produto' : {
                    $result = $em->getRepository('App:Product')->find($c['referrer_content_id']);
                    $data = [
                        'id' => $c['id'],
                        'type' => $c['referrer_content_type'],
                        'title' => $result->getName()
                    ];
                    $response[] = $data;
                    break;
                }
                case 'post' : {
                    $result = $em->getRepository('App:BlogPost')->find($c['referrer_content_id']);
                    $data = [
                        'id' => $c['id'],
                        'type' => $c['referrer_content_type'],
                        'title' => $result->getTitle()
                    ];
                    $response[] = $data;
                    break;
                }
                case 'curso' : {
                    $result = $em->getRepository('App:Course')->find($c['referrer_content_id']);
                    $data = [
                        'id' => $c['id'],
                        'type' => $c['referrer_content_type'],
                        'title' => $result->getName()
                    ];
                    $response[] = $data;
                    break;
                }
                case 'quiz' : {
                    $result = $em->getRepository('App:Quiz')->find($c['referrer_content_id']);
                    $data = [
                        'id' => $c['id'],
                        'type' => $c['referrer_content_type'],
                        'title' => $result->getTitle()
                    ];
                    $response[] = $data;
                    break;
                }
            }
        }

        return $response;
    }

    public function getReferrerContentView($id, $type)
    {
        $sql = "SELECT id, referrer_content_id, referrer_content_type
                FROM referrer_content
                WHERE main_content_id = '{$id}' AND main_content_type = '{$type}'
                ORDER BY main_content_type";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $content = $stmt->fetchAll();

        $response = [];
        
        foreach($content as $key=>$c){
            switch($c['referrer_content_type']){
                case 'produto' : {
                    $result = $em->getRepository('App:Product')->find($c['referrer_content_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '5';
                    $response[$key]['class'] = 'product';
                    break;
                }
                case 'post' : {
                    $result = $em->getRepository('App:BlogPost')->find($c['referrer_content_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '1';
                    $response[$key]['class'] = 'blog';
                    break;
                }
                case 'curso' : {
                    $result = $em->getRepository('App:Course')->find($c['referrer_content_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '3';
                    $response[$key]['class'] = 'course';
                    break;
                }
                case 'quiz' : {
                    $result = $em->getRepository('App:Quiz')->find($c['referrer_content_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '2';
                    $response[$key]['class'] = 'quiz';
                    break;
                }
            }
        }

        return $response;
    }
}
