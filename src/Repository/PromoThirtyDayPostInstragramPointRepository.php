<?php

namespace App\Repository;

use App\Entity\PromoThirtyDayPostInstragramPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromoThirtyDayPostInstragramPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoThirtyDayPostInstragramPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoThirtyDayPostInstragramPoint[]    findAll()
 * @method PromoThirtyDayPostInstragramPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoThirtyDayPostInstragramPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoThirtyDayPostInstragramPoint::class);
    }

    public function findPostDatePublic($date_public, $user_promo)
    {    
        $date_public = $date_public->format('Y-m-d');

        $sql = "SELECT * FROM promo_thirty_day_post_instragram_point where date(date_public_at) = '$date_public' and user_promo_id = $user_promo";
    
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    // /**
    //  * @return PromoThirtyDayPostInstragramPoint[] Returns an array of PromoThirtyDayPostInstragramPoint objects
    //  */
    
    public function findUserPostInstagram()
    {
        return $this->createQueryBuilder('p')
            ->select('distinct(p.user_promo) as userPromo')
            ->getQuery()
            ->getResult();
    }
    

    /*
    public function findOneBySomeField($value): ?PromoThirtyDayPostInstragramPoint
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
