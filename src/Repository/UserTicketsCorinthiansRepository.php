<?php

namespace App\Repository;

use App\Entity\UserTicketsCorinthians;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTicketsCorinthians|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTicketsCorinthians|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTicketsCorinthians[]    findAll()
 * @method UserTicketsCorinthians[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTicketsCorinthiansRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTicketsCorinthians::class);
    }

    // /**
    //  * @return UserTicketsCorinthians[] Returns an array of UserTicketsCorinthians objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTicketsCorinthians
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
