<?php

namespace App\Repository;

use App\Entity\UserFacebookData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserFacebookData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFacebookData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFacebookData[]    findAll()
 * @method UserFacebookData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFacebookDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserFacebookData::class);
    }

    // /**
    //  * @return UserFacebookData[] Returns an array of UserFacebookData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserFacebookData
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
