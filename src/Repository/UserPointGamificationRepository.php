<?php

namespace App\Repository;

use App\Entity\UserLevelGamification;
use App\Entity\UserMedalsGamification;
use App\Entity\UserPointGamification;
use App\Entity\UserPointTotalGamification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserPointGamification|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPointGamification|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPointGamification[]    findAll()
 * @method UserPointGamification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPointGamificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPointGamification::class);
    }

    public function setInterationGamification($user, $interation_slug, $content, $content_id)
    {
        $em = $this->getEntityManager();
        $interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => $interation_slug, 'is_active' => true, 'is_mission' => false]);
        
        if (!$interation) {
            return false;
        }

        $array_interacoes_cadastro = ['cadastrar-se-na-plataforma', 'acessar-o-portal'];
        if (in_array($interation_slug, $array_interacoes_cadastro)) {

            //cadastrando os pontos
            $this->registerPoint($user, $interation, $content, null, false);
            
        } else {
            $user_point = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'interation' => $interation, 'content' => $content, 'content_type_id' => $content_id, 'is_mission' => false]);
            
            if (!$user_point) {
                //cadastrando os pontos
                $this->registerPoint($user, $interation, $content, $content_id, false);
            }
        }

        //Bonificação de pontuação 
        $user_contents = $em->getRepository('App:UserPointGamification')->findBy(['user' => $user, 'interation' => $interation, 'content' => $content]);
        if ($user_contents) {

            switch ($interation->getSlug()) {
                case 'ler-uma-materia': //ID interação 3

                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'acessar-10-conteudo-da-pagina-de-materias-diferentes', 'is_mission' => true]); #ID interação 13

                    if (count($user_contents) == 10) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'vocacao-em-cuidar');
                    }
                break;

                case 'consultar-um-produto-da-marca': //ID interação 6

                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'consultar-10-paginas-de-produtos-diferentes', 'is_mission' => true]); #ID interação 16

                    if (count($user_contents) == 10) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'neo-pedia');
                    }
                break;

                case 'concluir-um-curso': //ID interação 8
                    
                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'concluir-5-cursos', 'is_mission' => true]); #ID interação 17 
                   
                    if (count($user_contents) == 5) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'nphd');
                    }

                break;

                case 'ouvir-podcast': //ID interação 9
                  
                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'ouvir-2-podcasts-diferentes', 'is_mission' => true]); #ID interação 18 
                   
                    if (count($user_contents) == 2) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'maos-livres');
                    }

                break;

                case 'acessar-o-portal': //ID interação 02
                  
                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'retornar-ao-portal-10-vezes', 'is_mission' => true]); #ID interação 14
                   
                    if (count($user_contents) == 10) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'medalha-de-casa');
                    }

                    //verificando as regras de cadastro completo
                    $this->checkRegisterComplete($user);

                break;

                case 'cadastrar-se-na-plataforma': 

                    $userInvide = null;
                    $share_code = $user->getInvite() ? $user->getInvite()->getShareCode() : null;

                    if ($share_code) {
                        $userInvide = $em->getRepository('App:User')->findOneby(['shareCode' => $share_code]);
                    }

                    if ($userInvide) {
                        $interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'indicar-amigo', 'is_mission' => false]); #ID interação 14
                        //cadastrando os pontos
                        $this->registerPoint($userInvide, $interation, 'indicacao-amigo', null, false);

                        $user_point_gamefication = $em->getRepository('App:UserPointGamification')->findBy(['user' => $userInvide->getId(), 'interation' => $interation]);
                        
                        if (count($user_point_gamefication) == 3) {
                            $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'indicar-3-amigos-primeiros', 'is_mission' => true]); #ID interação 14
                            $this->registerPoint($userInvide, $mission_interation, 'indicar-3-missao-concluida', null, true);
                            $this->setMedals($userInvide, 'popular');
                        }

                    }
                    
                    //leitura do regulamento
                    $user_point_interation = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
        
                    $user_point_interation->setIsRegulation(true);
                    $user_point_interation->setDateRegulation(new \DateTime());
                    
                    $em->persist($user_point_interation);
                    $em->flush();
                    
                break;

                case 'acessar-uma-landing-page-especial': //ID interação 7
                  
                    $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'acessar-3-landing-pages-especiais-diferentes', 'is_mission' => true]); #ID interação 14
                   
                    if (count($user_contents) == 3) { 
                        //registrar na tabela do ranking
                        $this->registerPoint($user, $mission_interation, $content . '-missao-concluida', null, true);
                        $this->setMedals($user, 'neo-expert');
                    }

                break;
                   
            }
        }
       
        return true;
    }

    public function registerPoint($user, $interation, $content, $content_id, $is_mission)
    {
        $em = $this->getEntityManager();
        
        //verificando se a missão já foi registrada
        if ($is_mission) {
            $is_exist_mission_content = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'interation' => $interation, 'content' => $content, 'is_mission' => true]);
            if ($is_exist_mission_content) {
                return true;
            }
        }
              
        //save user_point
        $user_point = new UserPointGamification();
        $user_point->setUser($user);
        $user_point->setInteration($interation);
        $user_point->setContent($content);
        //$user_point->setContentType();
        $user_point->setContentTypeId($content_id);
        $user_point->setIsMission($is_mission);

        if ($user->getIsPharmacy()) { //Gestor de Loja
            $user_point->setProfileStoreManager(true);
        } else if ($user->getIsPharmaceutical()) { //Farmacêutico
            $user_point->setProfilePharmaceutical(true);
        } else if (!$user->getIsPharmaceutical()) { //Balconista
            $user_point->setProfileClerk(true);
        }      

        $user_point->setIsShowPopup(false);
        $user_point->setCreatedAt(new \DateTime());
        $user_point->setUpdatedAt(new \DateTime());

        $em->persist($user_point);
        $em->flush();

        //registrar na tabela do ranking
        $this->updateRanking($user, $interation->getPoint());

        return $user_point;
    }

    public function updateRanking($user, $point_interation)
    {
        $em = $this->getEntityManager();
        //tabela do ranking
        $user_ranking = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);

        if (!$user_ranking) {
            //registrar na tabela do ranking
            $user_ranking = new UserPointTotalGamification();
            $user_ranking->setUser($user);
            $user_ranking->setCreatedAt(new \DateTime());
        }
        
        if ($user->getIsPharmacy()) { // 'Gestor de Loja'
            $user_ranking->setProfileStoreManager(true);
        } else if ($user->getIsPharmaceutical()) { //Farmacêutico
            $user_ranking->setProfilePharmaceutical(true);
        } else if (!$user->getIsPharmaceutical()) { //Balconista
            $user_ranking->setProfileClerk(true);
        }      

        //somando os ponto para registrar na tabela do ranking
        $point_interation = (int) $user_ranking->getPoint() + (int) $point_interation;
        $user_ranking->setPoint($point_interation);
        
        $user_ranking->setUpdatedAt(new \DateTime());
        // $user_ranking->setIsRegulation(false);
        //$user_ranking->setDateRegulation(new \DateTime());
        
        $em->persist($user_ranking);
        $em->flush();

        return $user_ranking;
    }

    public function setMedals($user, $medal_slug)
    {
        $em = $this->getEntityManager();
        $medal = $em->getRepository('App:MedalsGamification')->findOneBy(['slug' => $medal_slug]);

        if (!$medal) {
            return false;
        }

        $user_medal = $em->getRepository('App:UserMedalsGamification')->findOneBy(['user' => $user, 'medal' => $medal]);

        if (!$user_medal) {
            $user_medal = new UserMedalsGamification();

            $user_medal->setUser($user);
            $user_medal->setMedal($medal);
            $user_medal->setCreatedAt(new \DateTime());
            $user_medal->setUpdatedAt(new \DateTime());
        
            $em->persist($user_medal);
            $em->flush();

            //Registrando o nivel do usuário a cada nova medalha
            $this->setLevel($user);
        }

        $user_medals = $em->getRepository('App:UserMedalsGamification')->findBy(['user' => $user]);
        //Se usuário conquistar todas as medalhas de 1 a 8 ele recebe a medalha master ("Neopharmer")
        if (count($user_medals) == 8) {
            $medal_master = $em->getRepository('App:MedalsGamification')->findOneBy(['slug' => 'neopharmer']);

            $user_medal = new UserMedalsGamification();

            $user_medal->setUser($user);
            $user_medal->setMedal($medal_master);
            $user_medal->setCreatedAt(new \DateTime());
            $user_medal->setUpdatedAt(new \DateTime());
        
            $em->persist($user_medal);
            $em->flush();
            
            $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'ganhou-todos-as-medalhas', 'is_mission' => true]); #ID interação 14
            //cadastrando os pontos
            $this->registerPoint($user, $mission_interation, 'ganhou-todos-as-medalhas-missao-concluida', null, true);
        }

        return $user_medal;
    }

    public function setLevel($user)
    {
        $em = $this->getEntityManager();
        $user_medals = $em->getRepository('App:UserMedalsGamification')->findBy(['user' => $user]);
        
        $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user]);

        if (!$user_level) {
            $user_level = new UserLevelGamification();
            $user_level->setUser($user);
            $user_level->setCreatedAt(new \DateTime());
        }

        if (count($user_medals) <= 3) { //Bronze
            $level = $em->getRepository('App:LevelGamification')->findOneBy(['slug' => 'bronze', 'is_active' => true]);
        } else if ((count($user_medals) >= 4) && (count($user_medals) <= 7)) { //Prata
            $level = $em->getRepository('App:LevelGamification')->findOneBy(['slug' => 'prata', 'is_active' => true]);
        } else if (count($user_medals) >= 7) { //Ouro
            $level = $em->getRepository('App:LevelGamification')->findOneBy(['slug' => 'ouro', 'is_active' => true]);
        }
       
        $user_level->setLevel($level);
        $user_level->setIsShowPopup(false); 
        $user_level->setUpdatedAt(new \DateTime());
        
        $em->persist($user_level);
        $em->flush();

        return $user_level;
    }

    public function userPositionGamification($search_key, $search_value, $user)
    {
        $where = "";
        if ($user->getIsPharmacy()) { // 'Gestor de Loja'
            $where = 'where u.is_pharmacy = true and u.is_active = true';
        } else if ($user->getIsPharmaceutical()) { //Farmacêutico
            $where = 'where u.is_pharmaceutical = true and u.is_active = true';
        } else if (!$user->getIsPharmaceutical()) { //Balconista
            $where = 'where u.is_pharmaceutical = false and u.is_active = true';
        }    

        $this->_em->getConnection()->exec("SET @row_number = 0;");
        $str = "SELECT 
                    (@row_number:=@row_number + 1) AS position,
                    user_id, 
                    point 
                FROM user_point_total_gamification as ut
                inner join user as u
                on u.id = ut.user_id
                {$where}
            ORDER BY CONVERT(point, SIGNED) DESC
            ";

        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare($str);
        $query->execute([]);
        $rows = $query->fetchAll();

        $user_search = array_search($search_value, array_column($rows, $search_key));

        if ($user_search === false) {
            $result = [];
        } else {
            $result = $rows[$user_search];
        }

        return $result;
    }

    public function checkRegisterComplete($user) 
    {
        $em = $this->getEntityManager();
        
        //cadastro preferecias 
        if ($this->preferentialRegisterComplete($user)) {
            $preferential_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'preencher-preferencias-no-cadastro']); #ID interação 11
            
            $user_point = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'interation' => $preferential_interation, 'content' =>  'cadastro-preferencias', 'is_mission' => false]);
            if (!$user_point) {
                $this->registerPoint($user, $preferential_interation, 'cadastro-preferencias', null, false);
            }
        }
        
        //cadastro completo
        if ($this->userRegisterComplete($user)) {
            $cadastro_complete_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'completar-cadastro-no-portal', 'is_mission' => true]); #ID interação 12
            
            $user_point = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'interation' => $cadastro_complete_interation, 'content' =>  'cadastro-completo-missao-concluida', 'is_mission' => true]);
            if (!$user_point) {
                //registrar na tabela do ranking
                $this->registerPoint($user, $cadastro_complete_interation, 'cadastro-completo-missao-concluida', null, true);
            }
        }

        if ($this->preferentialRegisterComplete($user) && $this->userRegisterComplete($user)) { 
            $this->setMedals($user, 'sempre-atualizado');
        }

        return true;
    }

    public function userRegisterComplete($user)
    {

        if ($user->getIsPharmacy()) { // 'Gestor de Loja'
            return $this->gestorRegisterComplete($user);
        } else if ($user->getIsPharmaceutical()) { //Farmacêutico
            return $this->farmaceuticoRegisterComplete($user);
        } else if (!$user->getIsPharmaceutical()) { //Balconista
            return $this->balconistaRegisterComplete($user);
        }  

        return false;
    }

    public function gestorRegisterComplete($user)
    {
        $errors = [];
        if (!$user->getName()) {
            $errors['name'] = 'O nome é obrigatório';
        }

        if (!$user->getBirthday()) {
            $errors['birthday'] = 'A data de nascimento é obrigatório';
        }

        if (!$user->getEmail()) {
            $errors['email'] = 'O e-mail é obrigatório';
        }

        if (!$user->getState()) {
            $errors['state'] = 'O estado é obrigatório';
        }

        if (!$user->getCity()) {
            $errors['city'] = 'A cidade é obrigatório';
        }

        if (!$user->getCpf()) {
            $errors['cpf'] = 'O CPF é obrigatório';
        }
        
        if (!$user->getCnpj()) {
            $errors['cnpj'] = 'O CNPJ é obrigatório';
        }

        if (!$user->getPassword()) {
            $errors['password'] = 'A senha é obrigatório';
        }

        if (!$user->getNewsletter()) {
            $errors['newsletter'] = 'A newsletter é obrigatório';
        }

        if (!$user->getZipCode()) {
            $errors['zip_code'] = 'O CEP é obrigatório';
        }

        if (!$user->getAddress()) {
            $errors['address'] = 'O endereço é obrigatório';
        }

        if (!$user->getNumber()) {
            $errors['number'] = 'O número é obrigatório';
        }

        if (!$user->getComplement()) {
            $errors['complement'] = 'O complemento é obrigatório';
        }

        if (!$user->getNeighborhood()) {
            $errors['neighborhood'] = 'O bairro é obrigatório';
        }

        if (!$user->getFantasyName()) {
            $errors['fantasy_name'] = 'O nome fantasia é obrigatório';
        }

        if (!$user->getCompanyName()) {
            $errors['company_name'] = 'O razão Social é obrigatório';
        }

        if (!$user->getDddCellphone()) {
            $errors['ddd_cellphone'] = 'O DDD é obrigatório';
        }

        if (!$user->getCellphone()) {
            $errors['cellphone'] = 'O celular é obrigatório';
        }
        
        if (!$user->getIsNotificationWhatsapp()) {
            $errors['is_whats'] = 'Receber mensagens de Whatsapp e SMS é obrigatório';
        }

        if (!$user->getIsRegulation()) {
            $errors['is_regulation'] = 'A leitura do regulamento é obrigatório';
        }
        
        if (count($errors) > 0) {
            return false;
        }

        return true;
    }

    public function balconistaRegisterComplete($user)
    {
        $errors = [];
        if (!$user->getName()) {
            $errors['name'] = 'O nome é obrigatório';
        }

        if (!$user->getGender()) {
            $errors['gender'] = 'O sexo é obrigatório';
        }

        if (!$user->getBirthday()) {
            $errors['birthday'] = 'A data de nascimento é obrigatório';
        }

        if (!$user->getEmail()) {
            $errors['email'] = 'O e-mail é obrigatório';
        }

        if (!$user->getEstateAddress()) {
            $errors['state'] = 'O estado é obrigatório';
        }

        if (!$user->getCity()) {
            $errors['city'] = 'A cidade é obrigatório';
        }

        if (!$user->getEducationalLevel()) {
            $errors['educational_level'] = 'O grau de escolaridade é obrigatório';
        }

        if (!$user->getCpf()) {
            $errors['cpf'] = 'O CPF é obrigatório';
        }
        
        if (!$user->getCnpj()) {
            $errors['cnpj'] = 'O CNPJ é obrigatório';
        }

        if (!$user->getPassword()) {
            $errors['password'] = 'A senha é obrigatório';
        }

        // if (!$user->getNewsletter()) {
        //     $errors['newsletter'] = 'A newsletter é obrigatório';
        // }

        if (!$user->getZipCode()) {
            $errors['zip_code'] = 'O CEP é obrigatório';
        }

        if (!$user->getAddress()) {
            $errors['address'] = 'O endereço é obrigatório';
        }

        if (!$user->getNumber()) {
            $errors['number'] = 'O número é obrigatório';
        }

        if (!$user->getComplement()) {
            $errors['complement'] = 'O complemento é obrigatório';
        }

        if (!$user->getNeighborhood()) {
            $errors['neighborhood'] = 'O bairro é obrigatório';
        }
        
        if (count($errors) > 0) {
            return false;
        }

        return true;
    }

    public function farmaceuticoRegisterComplete($user)
    {
        $errors = [];
        if (!$user->getName()) {
            $errors['name'] = 'O nome é obrigatório';
        }

        if (!$user->getGender()) {
            $errors['gender'] = 'O sexo é obrigatório';
        }

        if (!$user->getBirthday()) {
            $errors['birthday'] = 'A data de nascimento é obrigatório';
        }

        if (!$user->getEmail()) {
            $errors['email'] = 'O e-mail é obrigatório';
        }

        if (!$user->getEstateAddress()) {
            $errors['state'] = 'O estado é obrigatório';
        }

        if (!$user->getCity()) {
            $errors['city'] = 'A cidade é obrigatório';
        }

        if (!$user->getEducationalLevel()) {
            $errors['educational_level'] = 'O grau de escolaridade é obrigatório';
        }

        if (!$user->getCpf()) {
            $errors['cpf'] = 'O CPF é obrigatório';
        }
        
        // if (!$user->getCnpj()) {
        //     $errors['cnpj'] = 'O CNPJ é obrigatório';
        // }

        if (!$user->getPassword()) {
            $errors['password'] = 'A senha é obrigatório';
        }

        // if (!$user->getNewsletter()) {
        //     $errors['newsletter'] = 'A newsletter é obrigatório';
        // }

        if (!$user->getZipCode()) {
            $errors['zip_code'] = 'O CEP é obrigatório';
        }

        if (!$user->getAddress()) {
            $errors['address'] = 'O endereço é obrigatório';
        }

        if (!$user->getNumber()) {
            $errors['number'] = 'O número é obrigatório';
        }

        if (!$user->getComplement()) {
            $errors['complement'] = 'O complemento é obrigatório';
        }

        if (!$user->getNeighborhood()) {
            $errors['neighborhood'] = 'O bairro é obrigatório';
        }

        if (!$user->getCrf()) {
            $errors['crf'] = 'CRF é obrigatório';
        }
        
        if (!$user->getState()) {
            $errors['state'] = 'O estado é obrigatório';
        }
        
        if (count($errors) > 0) {
            return false;
        }

        return true;
    }


    public function preferentialRegisterComplete($user)
    {
        $em = $this->getEntityManager();
        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);
         
        $errors_preferential = [];
        if ($user_preferential) {          
            if (!$user_preferential->getHobby()) { 
                $errors_preferential['hobby'] = 'O time é obrigatório';
            }

            if (!$user_preferential->getSport()) { 
                $errors_preferential['sport'] = 'O time é obrigatório';
            }

            if (!$user_preferential->getTeamFootball()) {
                $errors_preferential['team_football'] = 'O time é obrigatório';
            }

            if (count($errors_preferential) > 0) {
                return false;
            }
            
            return true;
           
        } else {
            return false;
        }

    }


    public function setInterationLandingPage($user, $content, $content_id)
    {
        $em = $this->getEntityManager();
        $builder = $em->getRepository('App:SpecialCampaign')->createQueryBuilder('p');

        switch ($content) {
            case 'post':
                $builder->innerJoin('p.blog_post', 'b')
                        ->where('b.id = :post')
                        ->setParameter('post', $content_id);
            break;

            case 'course':
                $builder->innerJoin('p.course', 'c')
                        ->where('c.id = :courseId')
                        ->setParameter('courseId', $content_id);
            break;

            case 'quiz':
                $builder->innerJoin('p.quiz_all', 'q')
                        ->where('q.id = :quizId')
                        ->setParameter('quizId', $content_id);
            break;

            case 'product':
                $builder->innerJoin('p.product', 'prod')
                        ->where('prod.id = :prodId')
                        ->setParameter('prodId', $content_id);
            break;
        
        }

        $user_point = $builder->getQuery()->getResult();
        

        if (count($user_point) > 0) {
            $landing_page = isset($user_point[0]) ? $user_point[0] : null;   

            if ($landing_page) {
                $mission_interation = $em->getRepository('App:InteractionsGamification')->findOneBy(['slug' => 'consumir-conteudo-de-campanha-nova', 'is_active' => true, 'is_mission' => true]);
                
                $user_point_interation = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'content' =>  $content, 'content_type_id' => $content_id]);
                if (!$user_point_interation) {
                    $this->registerPoint($user, $mission_interation, $content, $content_id, false);
                }
                
                if ($landing_page->getMedals()) {
                    if ($landing_page->getMedals()->getSlug()) {
                        $this->setMedals($user, $landing_page->getMedals()->getSlug());
                    }
                }
            }            
        }

        return true;
    }



    // /**
    //  * @return UserPointGamification[] Returns an array of UserPointGamification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPointGamification
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
