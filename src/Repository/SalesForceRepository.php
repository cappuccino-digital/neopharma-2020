<?php

namespace App\Repository;

use App\Entity\SalesForce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SalesForce|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesForce|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesForce[]    findAll()
 * @method SalesForce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesForceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesForce::class);
    }

    // /**
    //  * @return SalesForce[] Returns an array of SalesForce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalesForce
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
