<?php

namespace App\Repository;

use App\Entity\QuizAllAlternatives;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuizAllAlternatives|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizAllAlternatives|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizAllAlternatives[]    findAll()
 * @method QuizAllAlternatives[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizAllAlternativesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuizAllAlternatives::class);
    }

    // /**
    //  * @return QuizAllAlternatives[] Returns an array of QuizAllAlternatives objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizAllAlternatives
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
