<?php

namespace App\Repository;

use App\Entity\UserPreferential;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserPreferential|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPreferential|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPreferential[]    findAll()
 * @method UserPreferential[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPreferentialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPreferential::class);
    }

    // /**
    //  * @return UserPreferential[] Returns an array of UserPreferential objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPreferential
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
