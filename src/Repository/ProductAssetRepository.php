<?php

namespace App\Repository;

use App\Entity\ProductAsset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductAsset|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductAsset|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductAsset[]    findAll()
 * @method ProductAsset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductAssetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductAsset::class);
    }

    // /**
    //  * @return ProductAsset[] Returns an array of ProductAsset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductAsset
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
