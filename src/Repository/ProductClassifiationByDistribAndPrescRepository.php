<?php

namespace App\Repository;

use App\Entity\ProductClassifiationByDistribAndPresc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductClassifiationByDistribAndPresc|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductClassifiationByDistribAndPresc|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductClassifiationByDistribAndPresc[]    findAll()
 * @method ProductClassifiationByDistribAndPresc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductClassifiationByDistribAndPrescRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductClassifiationByDistribAndPresc::class);
    }

    // /**
    //  * @return ProductClassifiationByDistribAndPresc[] Returns an array of ProductClassifiationByDistribAndPresc objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductClassifiationByDistribAndPresc
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
