<?php

namespace App\Repository;

use App\Entity\UserInactivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserInactivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInactivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInactivity[]    findAll()
 * @method UserInactivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserInactivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserInactivity::class);
    }

    // /**
    //  * @return UserInactivity[] Returns an array of UserInactivity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserInactivity
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
