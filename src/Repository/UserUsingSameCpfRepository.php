<?php

namespace App\Repository;

use App\Entity\UserUsingSameCpf;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserUsingSameCpf|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserUsingSameCpf|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserUsingSameCpf[]    findAll()
 * @method UserUsingSameCpf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserUsingSameCpfRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserUsingSameCpf::class);
    }

    // /**
    //  * @return UserUsingSameCpf[] Returns an array of UserUsingSameCpf objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserUsingSameCpf
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
