<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserBadge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    // public function getPointThertyDay()
    // {
    //     $em = $this->getEntityManager();

    // }

    public function setUserBadgeReceive($user, $badge){
        $em = $this->getEntityManager();
        $points = $em->getRepository('App:UserPoint')->findBy(['user'=> $user, 'badge' => $badge]);

        $totalPoints = 0;

        foreach($points as $p){
            if($user->getIsPharmaceutical()){
                $totalPoints += $p->getBadge()->getGivenPointPharmaceutical();
            } else {
                $totalPoints += $p->getBadge()->getGivenPointClerk();
            }
        }

        $finalLevel = null;
        $minPoints = 0;

        foreach($badge->getLevels() as $level){
            if($user->getIsPharmaceutical()){
                if($totalPoints >= $level->getMinPointPharmaceutical() && $minPoints > $level->getMinPointPharmaceutical()){
                    $finalLevel = $level;
                    $minPoints = $level->getMinPointPharmaceutical();
                }
            } else {
                if($totalPoints >= $level->getMinPointClerk() && $minPoints > $level->getMinPointClerk()){
                    $finalLevel = $level;
                    $minPoints = $level->getMinPointClerk();
                }
            }
        }

        if($finalLevel){
            $userHasBadge = $em->getRepository('App:UserBadge')->findBy(['user'=> $user, 'badgeLevel' => $finalLevel, 'BadgeId' => $badge->getId()]);
            
            if(!$userHasBadge){
                $userHasBadge = new UserBadge();
                $userHasBadge->setBadgeId($badge->getId());
                $userHasBadge->setCreatedAt(new \DateTime());
                $userHasBadge->setUpdatedAt(new \DateTime());
                $userHasBadge->setUser($user);
                $userHasBadge->setBadgeLevel($finalLevel);
                $em->persist($userHasBadge);
                $em->flush();
            }
        }
        return true;
    }

    public function getAllPointsBadges($user){
        $em = $this->getEntityManager();
        $builder = $em->getRepository('App:UserPoint')->createQueryBuilder('p')->innerJoin('App:Badge', 'b', 'WITH', 'p.badge = b.id');
        $builder->where('p.user = :user')
                ->setParameter('user', $user)
                ->groupBy('p.badge')
                ->orderBy('b.name', 'ASC');
        return $builder->getQuery()->getResult();
    }

    public function getUserRankByBadge($user, $badge){
        if($badge->getSlug() == 'super-neo'){
            $sql = "SELECT 
                        x.position,
                        x.user_id,
                        x.user_name,
                        x.user_avatar,
                        x.user_is_pharmaceutical,
                        x.total_points
                    FROM (SELECT
                                z.user_id,
                                z.user_name,
                                z.user_avatar,
                                z.user_is_pharmaceutical,
                                @rownum := @rownum + 1 AS position,
                                z.total_points
                            FROM (
                                SELECT
                                    u.id as user_id,
                                    u.name as user_name,
                                    u.avatar as user_avatar,
                                    u.is_pharmaceutical as user_is_pharmaceutical,
                                    SUM(p.point) as total_points
                                FROM user_point p
                                JOIN user u on p.user_id = u.id
                                GROUP BY p.user_id
                                ORDER BY total_points DESC
                            ) z, (SELECT @rownum := 0) r 
                        ) x
                    WHERE x.user_id = {$user->getId()}";

        } else {
            $sql = "SELECT 
                        x.position,
                        x.user_id,
                        x.user_name,
                        x.user_avatar,
                        x.user_is_pharmaceutical,
                        x.total_points
                    FROM (SELECT
                                z.user_id,
                                z.user_name,
                                z.user_avatar,
                                z.user_is_pharmaceutical,
                                @rownum := @rownum + 1 AS position,
                                z.total_points
                            FROM (
                                SELECT
                                    u.id as user_id,
                                    u.name as user_name,
                                    u.avatar as user_avatar,
                                    u.is_pharmaceutical as user_is_pharmaceutical,
                                    SUM(p.point) as total_points
                                FROM user_point p
                                JOIN user u on p.user_id = u.id
                                WHERE p.badge_id = {$badge->getId()}
                                GROUP BY p.user_id
                                ORDER BY total_points DESC
                            ) z, (SELECT @rownum := 0) r 
                        ) x
                    WHERE x.user_id = {$user->getId()}";
        }

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $response = $stmt->fetchAll();

        return isset($response[0]) ? $response[0] : null;
    }

    public function findOneByFacebookEmail($email){
        $em = $this->getEntityManager();
        $data = $em->getRepository('App:UserFacebookData')->findOneByEmail($email);
        if($data){
            return $data->getUser();
        }
        return null;
    }

    public function findOneByGoogleEmail($email){
        $em = $this->getEntityManager();
        $data = $em->getRepository('App:UserGoogleData')->findOneByEmail($email);
        if($data){
            return $data->getUser();
        }

        return null;
    }

    public function getViewByOthers($user, $content, $contentType, $count){
        
        $em = $this->getEntityManager();

        $sql = "SELECT count(id) as total_views, point_type_id FROM user_point";

        if($user){
            if($user->getIsPharmaceutical()){
                $sql .= " WHERE user_id != {$user->getId()} AND point_type_description = '{$contentType}' AND point_type_id != {$content->getId()}";
            } else {
                $sql .= " WHERE user_id != {$user->getId()} AND point_type_description = '{$contentType}' AND point_type_id != {$content->getId()} AND is_pharmaceutical = false";
            }
        } else {
            $sql .= " WHERE point_type_description = '{$contentType}' AND point_type_id != {$content->getId()}";
        }
            
        $sql .= " GROUP BY point_type_id ORDER BY total_views LIMIT {$count}";
        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $response = [];

        foreach($result as $c){
            switch($contentType){
                case 'produto' : {
                    $result = $em->getRepository('App:Product')->find($c['point_type_id']);
                    $response[] = $result;
                    break;
                }
                case 'post' : {
                    $result = $em->getRepository('App:BlogPost')->find($c['point_type_id']);
                    $response[] = $result;
                    break;
                }
                case 'curso' : {
                    $result = $em->getRepository('App:Course')->find($c['point_type_id']);
                    $response[] = $result;
                    break;
                }
                case 'quiz' : {
                    $result = $em->getRepository('App:Quiz')->find($c['point_type_id']);
                    $response[] = $result;
                    break;
                }
            }
        }
        
        if(!count($response)){
            switch($contentType){
                case 'produto' : {
                    $builder = $em->getRepository('App:Product')->createQueryBuilder('q');
                    $builder->where('q.id != :id')
                    ->setParameter('id',$content->getId())
                    ->orderBy('q.views','DESC');
                    return $builder->setMaxResults($count)->getQuery()->getResult();
                    break;
                }
                case 'post' : {
                    $builder = $em->getRepository('App:BlogPost')->createQueryBuilder('q');
                    $builder->where('q.id != :id')
                    ->setParameter('id',$content->getId())
                    ->orderBy('q.views','DESC');
                    return $builder->setMaxResults($count)->getQuery()->getResult();
                    break;
                }
                case 'curso' : {
                    $builder = $em->getRepository('App:Course')->createQueryBuilder('q');
                    $builder->where('q.id != :id')
                    ->setParameter('id',$content->getId())
                    ->orderBy('q.views','DESC');
                    return $builder->setMaxResults($count)->getQuery()->getResult();
                    break;
                }
                case 'quiz' : {
                    $builder = $em->getRepository('App:Quiz')->createQueryBuilder('q');
                    $builder->where('q.id != :id')
                    ->setParameter('id',$content->getId())
                    ->orderBy('q.views','DESC');
                    return $builder->setMaxResults($count)->getQuery()->getResult();
                    break;
                }
            }
        }

        return $response;
    }

    public function getByMonthGraph(){
        $from = date('2019-06-01 00:00:00');

        $sql = "SELECT count(*) as total, MONTH(u.created_at) as mes, YEAR(u.created_at) as ano, u.is_pharmaceutical
                FROM user u
                WHERE created_at >= '{$from}'
                GROUP BY MONTH(u.created_at),YEAR(u.created_at), u.is_pharmaceutical
                ORDER BY YEAR(u.created_at), MONTH(u.created_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getByStateMonthGraph(){
        $from = date('2019-06-01 00:00:00');

        $sql = "SELECT count(*) as total, u.state,  u.is_pharmaceutical, MONTH(u.created_at) as mes, YEAR(u.created_at) as ano
                FROM user u
                GROUP BY MONTH(u.created_at), YEAR(u.created_at), u.state, u.is_pharmaceutical
                ORDER BY YEAR(u.created_at), MONTH(u.created_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getByGenderMonthGraph(){
        $from = date('2019-06-01 00:00:00');

        $sql = "SELECT count(*) as total, u.gender,  u.is_pharmaceutical, MONTH(u.created_at) as mes, YEAR(u.created_at) as ano
                FROM user u
                GROUP by MONTH(u.created_at),YEAR(u.created_at), u.gender, u.is_pharmaceutical
                ORDER BY YEAR(u.created_at), MONTH(u.created_at)";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getByGenderLifetime(){
        $sql = "SELECT count(*) as total, u.gender,  u.is_pharmaceutical
        FROM user u
        GROUP BY u.gender, u.is_pharmaceutical";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getByStateLifetime(){
        $from = date('2019-06-01 00:00:00');

        $sql = "SELECT count(*) as total, u.state,  u.is_pharmaceutical
                FROM user u
                GROUP BY u.state, u.is_pharmaceutical";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getConsultUserExcelencia($user_id, $role_id)
    {
        $sql = "select user.* from user_role
                inner join user
                on user.id = user_role.user_id
                inner join role
                on role.id = user_role.role_id
                where user.id = {$user_id} and role.id = {$role_id}";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function relatorioBaseQuizenal($date_start, $date_end, $offset = null, $limit)
    {
        $query_filter_date = '';
        $query_offset = "";
      
        if ($date_start && $date_end) {
            $query_filter_date = " and u.created_at >= '{$date_start} 00:00:59' and u.created_at <= '{$date_end} 23:59:59'";
        } 

        if ($offset) {
            $query_offset = "OFFSET {$offset}";
        }
        //dump($date_end, $query_filter_date); die();
        $sql = "select
                    u.id, 
                    if(u.is_facebook_connected, 'SIM', 'NÃO') as facebook, 
                    if(u.is_google_connected, 'SIM', 'NÃO') as google,
                    u.name,
                    u.email,
                    if(u.is_pharmacy, 'Gestor de Loja', if(u.is_pharmaceutical, 'Farmacêutico', 'Balconista')) as perfil,
                    if(u.is_pharmacy, u.state, u.estate_address) as estado,
                    u.city,
                    CONVERT(u.cpf, char) as cpf,
                    CONVERT(REPLACE(u.cnpj, '/', ''), char) as cnpj,
                    CONVERT(u.crf, char) as crf,
                    if(u.newsletter, 'SIM', 'NÃO') as newsletter,
                    if(u.is_pharmacy, concat('(', u.ddd_cellphone, ')',' ' , u.cellphone), u.cellphone) as celular,
                    up.whatsapp,
                    u.created_at,
                    upre.hobby, upre.sport, upre.team_football, upre.how_many_children as qtd_filhos, if(upre.is_children, 'SIM', if(upre.how_many_children >= 0,'NÃO', '')) as tem_filhos, 
                    (select
                        if(fa.age <> '', GROUP_CONCAT(fa.age  separator ', '), '')
                            from preferential_children as fa where fa.user_preferential_id = upre.id)
                    as Filhos_Idade,
                    (select
                        if(fs.sport <> '', GROUP_CONCAT(fs.sport  separator ', '), '')
                        from preferential_children as fs where fs.user_preferential_id = upre.id)
                    as Filhos_esportes,
                    (select
                        if(ft.team_football <> '', GROUP_CONCAT(ft.team_football  separator ', '), '')
                            from preferential_children as ft where ft.user_preferential_id = upre.id)
                    as Filhos_Time_Futebol,
                    upre.created_at as data_preferencial
                  
                    from user as u
                        
                        left join user_promo_thirty_day_movement as up
                        on up.user_id = u.id
                        
                        left join user_preferential as upre
                        on upre.user_id = u.id
                       
                    where u.is_active = true {$query_filter_date} ORDER BY ID ASC LIMIT {$limit} $query_offset";

        //echo $sql; die();
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function relatorioBaseQuizenaTotal($date_start, $date_end)
    {
        $sql = "select count(u.id) as qtd from user as u where u.is_active = true and u.created_at >= '{$date_start} 00:00:59' and u.created_at <= '{$date_end} 23:59:59'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $total = $stmt->fetchAll();
        //dump($total[0]['qtd']); die();
        $total = isset($total[0]['qtd']) ? $total[0]['qtd'] : null; 
        return $total;
    }
}
