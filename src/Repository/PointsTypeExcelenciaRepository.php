<?php

namespace App\Repository;

use App\Entity\PointsTypeExcelencia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PointsTypeExcelencia|null find($id, $lockMode = null, $lockVersion = null)
 * @method PointsTypeExcelencia|null findOneBy(array $criteria, array $orderBy = null)
 * @method PointsTypeExcelencia[]    findAll()
 * @method PointsTypeExcelencia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PointsTypeExcelenciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PointsTypeExcelencia::class);
    }

    // /**
    //  * @return PointsTypeExcelencia[] Returns an array of PointsTypeExcelencia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PointsTypeExcelencia
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
