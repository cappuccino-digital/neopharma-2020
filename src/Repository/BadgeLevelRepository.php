<?php

namespace App\Repository;

use App\Entity\BadgeLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BadgeLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method BadgeLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method BadgeLevel[]    findAll()
 * @method BadgeLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BadgeLevelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BadgeLevel::class);
    }

    // /**
    //  * @return BadgeLevel[] Returns an array of BadgeLevel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BadgeLevel
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
