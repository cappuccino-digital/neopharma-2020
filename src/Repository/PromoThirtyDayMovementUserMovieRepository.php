<?php

namespace App\Repository;

use App\Entity\PromoThirtyDayMovementUserMovie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromoThirtyDayMovementUserMovie|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoThirtyDayMovementUserMovie|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoThirtyDayMovementUserMovie[]    findAll()
 * @method PromoThirtyDayMovementUserMovie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoThirtyDayMovementUserMovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoThirtyDayMovementUserMovie::class);
    }

    // /**
    //  * @return PromoThirtyDayMovementUserMovie[] Returns an array of PromoThirtyDayMovementUserMovie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoThirtyDayMovementUserMovie
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
