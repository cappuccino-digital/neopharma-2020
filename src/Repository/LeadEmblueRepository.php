<?php

namespace App\Repository;

use App\Entity\LeadEmblue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LeadEmblue|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeadEmblue|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeadEmblue[]    findAll()
 * @method LeadEmblue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadEmblueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeadEmblue::class);
    }

    // /**
    //  * @return LeadEmblue[] Returns an array of LeadEmblue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeadEmblue
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
