<?php

namespace App\Repository;

use App\Entity\UserVoucherUber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserVoucherUber|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserVoucherUber|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserVoucherUber[]    findAll()
 * @method UserVoucherUber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserVoucherUberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserVoucherUber::class);
    }

    // /**
    //  * @return UserVoucherUber[] Returns an array of UserVoucherUber objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserVoucherUber
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
