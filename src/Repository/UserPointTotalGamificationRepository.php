<?php

namespace App\Repository;

use App\Entity\UserPointTotalGamification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserPointTotalGamification|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPointTotalGamification|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPointTotalGamification[]    findAll()
 * @method UserPointTotalGamification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPointTotalGamificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPointTotalGamification::class);
    }

    // /**
    //  * @return UserPointTotalGamification[] Returns an array of UserPointTotalGamification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPointTotalGamification
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
