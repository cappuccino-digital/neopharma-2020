<?php

namespace App\Repository;

use App\Entity\UserCorinthiansFluminense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserCorinthiansFluminense|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCorinthiansFluminense|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCorinthiansFluminense[]    findAll()
 * @method UserCorinthiansFluminense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCorinthiansFluminenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCorinthiansFluminense::class);
    }

    // /**
    //  * @return UserCorinthiansFluminense[] Returns an array of UserCorinthiansFluminense objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCorinthiansFluminense
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
