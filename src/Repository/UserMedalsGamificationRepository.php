<?php

namespace App\Repository;

use App\Entity\UserMedalsGamification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserMedalsGamification|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserMedalsGamification|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserMedalsGamification[]    findAll()
 * @method UserMedalsGamification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserMedalsGamificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserMedalsGamification::class);
    }

    // /**
    //  * @return UserMedalsGamification[] Returns an array of UserMedalsGamification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserMedalsGamification
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
