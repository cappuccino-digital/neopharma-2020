<?php

namespace App\Repository;

use App\Entity\TicketsCorinthiansCompanion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicketsCorinthiansCompanion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicketsCorinthiansCompanion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicketsCorinthiansCompanion[]    findAll()
 * @method TicketsCorinthiansCompanion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketsCorinthiansCompanionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicketsCorinthiansCompanion::class);
    }

    // /**
    //  * @return TicketsCorinthiansCompanion[] Returns an array of TicketsCorinthiansCompanion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicketsCorinthiansCompanion
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
