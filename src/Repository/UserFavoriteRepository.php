<?php

namespace App\Repository;

use App\Entity\UserFavorite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserFavorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFavorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFavorite[]    findAll()
 * @method UserFavorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFavoriteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserFavorite::class);
    }


    public function getUserFavoriteContent($id){
        $sql = "SELECT id, type, type_id
                FROM user_favorite
                WHERE user_id = '{$id}' AND is_active = true
                ORDER BY created_at DESC";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $content = $stmt->fetchAll();

        $response = [];

        foreach($content as $key=>$c){
            switch($c['type']){
                case 'product' : {
                    $result = $em->getRepository('App:Product')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '5';
                    $response[$key]['class'] = 'product';
                    break;
                }
                case 'blog' : {
                    $result = $em->getRepository('App:BlogPost')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '1';
                    $response[$key]['class'] = 'blog';
                    break;
                }
                case 'course' : {
                    $result = $em->getRepository('App:Course')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '3';
                    $response[$key]['class'] = 'course';
                    break;
                }
                case 'quiz' : {
                    $result = $em->getRepository('App:Quiz')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['cat'] = '2';
                    $response[$key]['class'] = 'quiz';
                    break;
                }
            }
        }

        return $response;
    }

    // /**
    //  * @return UserFavorite[] Returns an array of UserFavorite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserFavorite
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
