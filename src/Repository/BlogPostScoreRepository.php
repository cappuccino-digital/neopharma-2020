<?php

namespace App\Repository;

use App\Entity\BlogPostScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlogPostScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPostScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPostScore[]    findAll()
 * @method BlogPostScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostScoreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlogPostScore::class);
    }

    public function getAvgScore($id){
        
        $sql = "SELECT AVG(score) as score
                FROM blog_post_score
                where post_id = {$id}";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        
        $response = $stmt->fetchAll();

        if(!isset($response[0]['score'])){
            return 0;
        }

        return $response[0]['score'];

    }

    // /**
    //  * @return BlogPostScore[] Returns an array of BlogPostScore objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogPostScore
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
