<?php

namespace App\Repository;

use App\Entity\PromoThirtyDayPostInstagram;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromoThirtyDayPostInstagram|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoThirtyDayPostInstagram|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoThirtyDayPostInstagram[]    findAll()
 * @method PromoThirtyDayPostInstagram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoThirtyDayPostInstagramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoThirtyDayPostInstagram::class);
    }

    // /**
    //  * @return PromoThirtyDayPostInstagram[] Returns an array of PromoThirtyDayPostInstagram objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoThirtyDayPostInstagram
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
