<?php

namespace App\Repository;

use App\Entity\UserRegisterIncomplete;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserRegisterIncomplete|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRegisterIncomplete|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRegisterIncomplete[]    findAll()
 * @method UserRegisterIncomplete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRegisterIncompleteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRegisterIncomplete::class);
    }

    // /**
    //  * @return UserRegisterIncomplete[] Returns an array of UserRegisterIncomplete objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserRegisterIncomplete
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
