<?php

namespace App\Repository;

use App\Entity\QuizAllRuleResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuizAllRuleResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizAllRuleResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizAllRuleResult[]    findAll()
 * @method QuizAllRuleResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizAllRuleResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuizAllRuleResult::class);
    }

    // /**
    //  * @return QuizAllRuleResult[] Returns an array of QuizAllRuleResult objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizAllRuleResult
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
