<?php

namespace App\Repository;

use App\Entity\QuizAll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuizAll|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuizAll|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuizAll[]    findAll()
 * @method QuizAll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizAllRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuizAll::class);
    }

    // /**
    //  * @return QuizAll[] Returns an array of QuizAll objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuizAll
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
