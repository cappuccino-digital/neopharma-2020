<?php

namespace App\Repository;

use App\Entity\SpecialCampaign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpecialCampaign|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecialCampaign|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecialCampaign[]    findAll()
 * @method SpecialCampaign[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialCampaignRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecialCampaign::class);
    }

    // /**
    //  * @return SpecialCampaign[] Returns an array of SpecialCampaign objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpecialCampaign
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
