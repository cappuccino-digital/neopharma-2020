<?php

namespace App\Service;

class EmBlueService {

    private $http;
    private $url;    
  
    public function __construct()
    {
        $this->url = 'https://api.embluemail.com/Services/Emblue3Service.svc/json/';
    }
    
    public function authenticate() //método para buscar usuário
    {  
        $body = [
            // 'User' => 'ana.morales@cappuccinodigital.com.br',
            // 'Pass' => 'C@ppuccino2k21',
            'User' => 'wellington.santos@cappuccinodigital.com.br',
            'Pass' => 'Emblue$2k22',
            'Token' => 'zvWBJAIA-PAn4q-Heq5o-0awCap1XQc',
        ];
       
        try {
            $ch = curl_init($this->url . 'Authenticate');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($body));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);

        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true);   
            
            if (!isset($response['Token'])) {
                return ['status' => false, 'message' => 'not token'];
            }      
            
            return ['status' => true, 'message' => 'success', 'token' => $response['Token']];
        }
    
    }

    public function addContact($user, $last_login_at, $group_ID)
    {
        $authenticate = $this->authenticate();

        if (!$authenticate['status']) {
            return $authenticate;
        }

        $data = [
            "Emails" => [$user->getEmail()],
            "SelectGroups" => $group_ID, 
            "Token" => $authenticate['token']
        ];
    
        try {
     
            $ch = curl_init($this->url . 'NewContactAll');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
    
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage()];

        } finally {

            $response = json_decode($result, true);   
            //dump($user->getEmail(), $response); die();
            $contact_id = isset($response['Contacts'][0]) ? $response['Contacts'][0] : null;

            if (!$contact_id) {
                return ['status' => false, 'message' => 'Não foi possível cadastrar o contato'];
            }      

            //Enviando campos personalizados
            $sendFieldsCustomContact = $this->editContact($user, $contact_id, $last_login_at);

            if (isset($sendFieldsCustomContact['status']) && !$sendFieldsCustomContact['status']) {
                if ($contact_id) { 
                    return ['status' => false, 'message' => 'O e-mail foi cadastrado, porém os campos personalizado não foi enviado'];
                } else {
                    return ['status' => false, 'message' => 'Não foi possível alterar os campos personalizados']; //Não foi possível alterar o campos personalizados
                }
            }
                        
            return ['status' => true, 'message' => 'success', 'contactID' => $contact_id, 'updateFieldsCustom' => $sendFieldsCustomContact['is_register']];
        }
       
    }

    public function editContact($user, $contact_id, $last_login_at)
    {
        $authenticate = $this->authenticate();

        if (!$authenticate['status']) {
            return $authenticate;
        }

        $name = $user->getName();
        $profile = '';
        if ($user->getIsPharmacy()) {
            $profile = 'Gestor de Loja';
        } else if ($user->getIsPharmaceutical()) {
            $profile = 'Farmacêutico';
        } else if (!$user->getIsPharmaceutical()) {
            $profile = 'Balconista';
        } 

        $data = [
            "EmailId" => $contact_id,
            "EditedFields" => "tipo:|:$profile:|:1|||nombre:|:$name:|:1|||data_ultimo_login:|:$last_login_at:|:1|||id_contacto:|:$contact_id:|:1",
            "Token" => $authenticate['token']
        ];
      
        try {
     
            $ch = curl_init($this->url . 'EditCustomFieldsOneContact');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true); 
            
            $is_register = isset($response['Result']) ? $response['Result'] : false;
            if (!$is_register) {
                return ['status' => false, 'message' => 'Não foi possível salvar os campos personalizados'];
            }
            //dump($response); die();
            return ['status' => true, 'message' => 'success', 'is_register' => $is_register];
        }
       
    }

    public function eventInactivity($user, $event)
    {
        $name = $user->getName();
        $date = $user->getCreatedAt()->format('Y-m-d');

        $profile = '';
        if ($user->getIsPharmacy()) {
            $profile = 'Gestor de Loja';
        } else if ($user->getIsPharmaceutical()) {
            $profile = 'Farmacêutico';
        } else if (!$user->getIsPharmaceutical()) {
            $profile = 'Balconista';
        }       
        
        $data = [
            'email' => $user->getEmail(),
            'eventName' => $event,
            'attributes' => [
                'nome' => $name,
                //'sobrenome' => 'sobrenometest',
                //'cadastro' => $date,
                'perfil' => $profile,
            ]
        ];
    
        $header = [
            'Content-Type:application/json',
            'Authorization: Basic NGQ4ZWYxNWIzMjQyNGYzM2ExMTdmOTJhZDY5NWIyYTY=',
        ];
        
        try {
     
            $ch = curl_init('https://track.embluemail.com/contacts/event');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true); 
            //dump($response); die();
            return ['status' => true, 'message' => 'success', 'result' => $response['result']];
        }
       
    }


    public function eventWelcome($user)
    {
        $name = $user->getName();
        $date = $user->getCreatedAt()->format('Y-m-d');

        $profile = '';
        if ($user->getIsPharmacy()) {
            $profile = 'Gestor de Loja';
        } else if ($user->getIsPharmaceutical()) {
            $profile = 'Farmacêutico';
        } else if (!$user->getIsPharmaceutical()) {
            $profile = 'Balconista';
        }       
        
        $data = [
            'email' => $user->getEmail(),
            'eventName' => 'boas_vindas_prod',
            'attributes' => [
                'nome' => $name,
                //'sobrenome' => 'sobrenometest',
                'cadastro' => $date,
                'perfil' => $profile,
            ]
        ];
    
        $header = [
            'Content-Type:application/json',
            'Authorization: Basic NGQ4ZWYxNWIzMjQyNGYzM2ExMTdmOTJhZDY5NWIyYTY=',
        ];
        
        try {
     
            $ch = curl_init('https://track.embluemail.com/contacts/event');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true); 
            //dump($response); die();
            return ['status' => true, 'message' => 'success', 'result' => $response['result']];
        }
       
    }

    public function eventLeads($lead, $is_register)
    {
        $name    = $lead->getName();
        $profile = $lead->getRole()->getName();
               
        //avisando para sistema emblue que o lead virou um cadastro real (parâmetro cadastro)
        if ($is_register) {
            $data = [
                'email' => $lead->getEmail(),
                'eventName' => 'cadastros_de_lead_',
                'attributes' => [
                    'nome' => $name,
                    'cadastro' => 'Cadastrado',
                    'perfil' => $profile,
                ]
            ];
        } else { //enviando o lead para emblue, sem o parâmetro cadastro
            $data = [
                'email' => $lead->getEmail(),
                'eventName' => 'contatos_leads_hmg',
                'attributes' => [
                    'nome' => $name,
                    'perfil' => $profile,
                ]
            ];
        }

        $header = [
            'Content-Type:application/json',
            'Authorization: Basic NGQ4ZWYxNWIzMjQyNGYzM2ExMTdmOTJhZDY5NWIyYTY=',
        ];
        
        try {
     
            $ch = curl_init('https://track.embluemail.com/contacts/event');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true); 
            //dump($response); die();
            return ['status' => true, 'message' => 'success', 'result' => $response['result']];
        }
       
    }

    public function eventCourseStart($user, $course, $event)
    {
        $profile = '';
        if ($user->getIsPharmacy()) {
            $profile = 'Gestor de Loja';
        } else if ($user->getIsPharmaceutical()) {
            $profile = 'Farmacêutico';
        } else if (!$user->getIsPharmaceutical()) {
            $profile = 'Balconista';
        }       
        
        $data = [
            'email' => $user->getEmail(),
            'eventName' => $event,
            'attributes' => [
                'email' => $user->getEmail(),
                'perfil' => $profile,
                'Curso' => $course,                
            ]
        ];
    
        $header = [
            'Content-Type:application/json',
            'Authorization: Basic NGQ4ZWYxNWIzMjQyNGYzM2ExMTdmOTJhZDY5NWIyYTY=',
        ];
        
        try {
     
            $ch = curl_init('https://track.embluemail.com/contacts/event');
            curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $result = curl_exec($ch);
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {

            $response = json_decode($result, true); 
            //dump($response); die();
            return ['status' => true, 'message' => 'success', 'result' => $response['result']];
        }
       
    }

}