<?php

namespace App\Service;

use \GuzzleHttp\Client;

class OrienteMeService {

    private $http;
    private $url;    
    private $token;
  
    public function __construct($url, $token)
    {
        $this->http  = new Client();
        $this->url   = $url;
        $this->token = $token;
    }

    public function inserirPacientePagoComCPF($cpf)
    {
        if (empty($cpf)) {
            return array('status' => false, 'message' => 'CPF não identificado.');
        }
        
        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                        <InserirPacientePagoComCPF xmlns="http://tempuri.org/">
                            <token>'.$this->token.'</token>
                            <cpf>'.$cpf.'</cpf>
                            <titular>true</titular>
                            <status>Oriente Me - Cadastro Portal Neopharma</status>
                        </InserirPacientePagoComCPF>
                    </soap:Body>
                </soap:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=UTF8',
            ],
            'body' => $xml,
        ];

        $response = $this->http->post($this->url, $options);
        
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
    
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $result = $doc->getElementsByTagName('InserirPacientePagoComCPFResult')->item(0)->nodeValue;
            
        if ($result == 'ok') {
            return array('status' => true, 'message' => 'Cadastrado com sucesso!');
        }
        
        return array('status' => false, 'message' => 'error');
    }

}

