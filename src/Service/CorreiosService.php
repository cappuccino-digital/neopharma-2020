<?php

namespace App\Service;

use \GuzzleHttp\Client;

class CorreiosService {

    private $url = 'https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl';    
    private $http;

    public function __construct()
    {
        $this->http  = new Client();
    }

    public function consultaCEP($cep)
    {
        if (empty($cep)) {
            return array('status' => false, 'message' => 'CEP não identificado.');
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
                    <soapenv:Header />
                    <soapenv:Body>
                        <cli:consultaCEP>
                            <cep>'.$cep.'</cep>
                        </cli:consultaCEP>
                    </soapenv:Body>
                </soapenv:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=UTF8',
            ],
            'body' => $xml,
        ];

        try { //tratamento de erro de requisição
            $response = $this->http->post($this->url, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) { 
            $response = $e->getResponse();
        }

        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());

        if ($response->getStatusCode() == 500) {
            $faultstring = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
            return ['status' => false, 'message' => $faultstring];
        }

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }

        $bairro = $doc->getElementsByTagName('bairro')->item(0)->nodeValue;
        $cep = $doc->getElementsByTagName('cep')->item(0)->nodeValue;
        $cidade = $doc->getElementsByTagName('cidade')->item(0)->nodeValue;
        $complemento2 = $doc->getElementsByTagName('complemento2')->item(0)->nodeValue;
        $endereco = $doc->getElementsByTagName('end')->item(0)->nodeValue;
        $uf = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
       
        $info = array(
            'status'=> true, 
            'bairro' => $bairro, 
            'cep' => $cep, 
            'cidade' => $cidade, 
            'endereco' => $endereco, 
            'complemento2' => $complemento2,
            'uf'=> $uf
        );
        
        return $info;
    }

}

