<?php

namespace App\Service;

use \GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class NeoPharmaExcelenciaService 
{
    private $http;
    private $url;
    private $username;
    private $password;
 
    public function __construct($url)
    {
        $this->http = new Client();
        $this->url  = $url;  
    }
    
    public function requestToken($username, $password)
    {
        if (empty($username)) {
            return array('status' => false, 'message' => 'Username não identificado.');
        }

        if (empty($password)) {
            return array('status' => false, 'message' => 'Password não identificada.');
        }
        
        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                        <RequestToken xmlns="http://tempuri.org/">
                        <user>'.sha1($username).'</user>
                        <pass>'.sha1($password).'</pass>
                        </RequestToken>
                    </soap:Body>
                </soap:Envelope>';

        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];

        $response = $this->http->post($this->url, $options);
        
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }

        //dump($response->getBody()->getContents()); die();
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $token = $doc->getElementsByTagName('access_token')->item(0)->nodeValue;
        $message = $doc->getElementsByTagName('message')->item(0)->nodeValue;

        //dump($response->getBody()->getContents()); die();
        
        if (empty($token)) {
            return array('status' => false, 'message' => $message);
        }
        
        return array('status' => true, 'token' => $token);
    }

    public function consultaConsulmidor($username, $password, $cnpj)
    {   
        $requesToken = $this->requestToken($username, $password);
        //dump($requesToken); die();
        if (!$requesToken['status']) {
            return $requesToken;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <soap:Header>
                            <Authorization xmlns="http://tempuri.org/">
                                <access_token>'.$requesToken['token'].'</access_token>
                            </Authorization>
                        </soap:Header>
                        <soap:Body>
                            <ConsultaConsumidor xmlns="http://tempuri.org/" />
                        </soap:Body>
                    </soap:Envelope>';

        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];

        $response = $this->http->post($this->url, $options);
        
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
        //dump($response->getBody()->getContents()); die();
        $doc = new \DOMDocument();  
        $doc->loadXML($response->getBody()->getContents());

        $nome = $doc->getElementsByTagName('nome')->item(0)->nodeValue;
        $cnpj = $doc->getElementsByTagName('cnpj')->item(0)->nodeValue;
        $cpf  = $doc->getElementsByTagName('cpf')->item(0)->nodeValue;
        $email = $doc->getElementsByTagName('email')->item(0)->nodeValue;
        $ddd = $doc->getElementsByTagName('ddd_celular')->item(0)->nodeValue;
        $num_celular = $doc->getElementsByTagName('num_celular')->item(0)->nodeValue;
        /* news fields */
        $razao = $doc->getElementsByTagName('razao')->item(0)->nodeValue;
        $fantasia = $doc->getElementsByTagName('fantasia')->item(0)->nodeValue;
        $cep = $doc->getElementsByTagName('cep')->item(0)->nodeValue;
        $logradouro = $doc->getElementsByTagName('logradouro')->item(0)->nodeValue;
        $numero = $doc->getElementsByTagName('numero')->item(0)->nodeValue;
        $complemento = $doc->getElementsByTagName('complemento')->item(0)->nodeValue;
        $bairro = $doc->getElementsByTagName('bairro')->item(0)->nodeValue;
        $cidade = $doc->getElementsByTagName('cidade')->item(0)->nodeValue;
        $uf = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
        $referencia = $doc->getElementsByTagName('referencia')->item(0)->nodeValue;
        $is_cadastro = $doc->getElementsByTagName('cadastro')->item(0)->nodeValue;

        /* end */

        $info = array(
            'status' => true, 
            'cnpj' => $cnpj, 
            'nome' => $nome, 
            'cpf' => $cpf, 
            'email' => $email, 
            'ddd' => $ddd, 
            'num_celular' => $num_celular,
            'razao' => $razao,
            'fantasia' => $fantasia,
            'cep' => $cep,
            'logradouro' => $logradouro,
            'numero' => $numero,
            'complemento' => $complemento,
            'bairro' => $bairro,
            'cidade' => $cidade,
            'uf' => $uf,
            'referencia' => $referencia,
            'is_cadastro' => filter_var($is_cadastro, FILTER_VALIDATE_BOOLEAN)
        );
          
        return $info;
    }

    public function consultaAcesso($username, $password, $cnpj, $cpf)
    {
        $requesToken = $this->requestToken($username, $password);
        //dump($requesToken); die();
        if (!$requesToken['status']) {
            return $requesToken;
        } 
        // if (!$cnpj || !$cpf) {
        //     return array('status' => false, 'message' => 'O CNPJ e CPF são obrigatórios.');
        // }

        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Header>
                        <Authorization xmlns="http://tempuri.org/">
                        <access_token>'.$requesToken['token'].'</access_token>
                        </Authorization>
                    </soap:Header>
                    <soap:Body>
                        <ConsultaAcesso xmlns="http://tempuri.org/">
                            <cnpj>'.$cnpj.'</cnpj>
                            <cpf>'.$cpf.'</cpf>
                        </ConsultaAcesso>
                    </soap:Body>
                </soap:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];
        //dump($xml); die();
        $response = $this->http->post($this->url, $options);
      
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
        //dump($response->getBody()->getContents()); die();
        
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $elegivel = $doc->getElementsByTagName('elegivel')->item(0)->nodeValue;
        $cnpj = $doc->getElementsByTagName('cnpj')->item(0)->nodeValue;
        $url = $doc->getElementsByTagName('url')->item(0)->nodeValue;
        $cpf = $doc->getElementsByTagName('cpf')->item(0)->nodeValue;
        $codigo = intval($doc->getElementsByTagName('codigo')->item(0)->nodeValue);
        $descricao = $doc->getElementsByTagName('descricao')->item(0)->nodeValue;
               
        if ($codigo == 1) {
            return array('status' => false, 'message' => 'CNPJ não faz parte do Neopharma Excelência.');
        }

        if ($codigo == 9) {
            return array('status' => false, 'message' => 'CNPJ já cadastrado, vá para a tela de login.');
        }

        if (empty($elegivel) || !$elegivel) {
            return array('status' => false, 'message' => $descricao);
        }
                      
        return array('status' => true, 'elegivel' => filter_var($elegivel, FILTER_VALIDATE_BOOLEAN), 'cnpj' => $cnpj, 'cpf'=> $cpf, 'codigo'=> $codigo, 'url' => $url, 'descricao' => $descricao);
    }

    public function updateCnpj($username, $password, $cnpj, $cpf)
    {
        $requesToken = $this->requestToken($username, $password);
          
        if (!$requesToken['status']) {
            return $requesToken;
        }

        if (!$cnpj || !$cpf) {
            return array('status' => false, 'message' => 'O CNPJ e CPF são obrigatórios.');
        }
        
        $xml = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header>
                    <Authorization xmlns="http://tempuri.org/">
                    <access_token>'.$requesToken['token'].'</access_token>
                    </Authorization>
                </soap:Header>
                <soap:Body>
                    <AtualizaConsumidor xmlns="http://tempuri.org/">
                        <cnpj>'.$cnpj.'</cnpj>
                        <cpf>'.$cpf.'</cpf>
                    </AtualizaConsumidor>
                </soap:Body>
                </soap:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];
        //dump($xml); die();
        $response = $this->http->post($this->url, $options);
      
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
       //dump($response->getBody()->getContents()); die();
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $codigo = $doc->getElementsByTagName('codigo')->item(0)->nodeValue;
        $descricao = $doc->getElementsByTagName('descricao')->item(0)->nodeValue;
        
        return array('status' => true, 'codigo' => $codigo, 'descricao' => $descricao);
    }

    public function cadastraInteracao($username, $password, $cnpj, $cpf, $transacao_id, $cod_evento)
    {
        $requesToken = $this->requestToken($username, $password);
          

        if (!$requesToken['status']) {
            return $requesToken;
        }

        if (empty($cnpj)) {
            return array('status' => false, 'message' => 'O CNPJ é obrigatório.');
        }

        if (empty($cpf)) {
            return array('status' => false, 'message' => 'O CPF é obrigatório.');
        }

        if (empty($transacao_id)) {
            return array('status' => false, 'message' => 'O código de transação é obrigatório.');
        }

        if (empty($cod_evento)) {
            return array('status' => false, 'message' => 'O código de evento é obrigatório.');
        }
        
        $xml = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header>
                    <Authorization xmlns="http://tempuri.org/">
                    <access_token>'.$requesToken['token'].'</access_token>
                    </Authorization>
                </soap:Header>
                <soap:Body>
                    <CadastraInteracao xmlns="http://tempuri.org/">
                    <cnpj>'.$cnpj.'</cnpj>
                    <cpf>'.$cpf.'</cpf>
                    <unique_id>'.$transacao_id.'</unique_id>
                    <cod_evento>'.$cod_evento.'</cod_evento>
                    </CadastraInteracao>
                </soap:Body>
                </soap:Envelope>';
       
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];
       
        try { //tratamento de erro de requisição
            $response = $this->http->post($this->url, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) { 
            $response = $e->getResponse();
        }
      
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
       
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $codigo = $doc->getElementsByTagName('codigo')->item(0)->nodeValue;
        $descricao = $doc->getElementsByTagName('descricao')->item(0)->nodeValue;
        
        return array('status' => true, 'codigo' => $codigo, 'descricao' => $descricao);
    }

    public function consultaConsumidorCadastro($username, $password, $cnpj, $cpf)
    {   
        $requesToken = $this->requestToken($username, $password);
        //dump($requesToken); die();
        if (!$requesToken['status']) {
            return $requesToken;
        }

        if (!$cnpj) {
            return array('status' => false, 'message' => 'O CNPJ é obrigatório.');
        }

        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Header>
                        <Authorization xmlns="http://tempuri.org/">
                        <access_token>'.$requesToken['token'].'</access_token>
                        </Authorization>
                    </soap:Header>
                    <soap:Body>
                        <ConsultaConsumidorCadastro xmlns="http://tempuri.org/">
                        <cnpj>'.$cnpj.'</cnpj>
                        <cpf>'.$cpf.'</cpf>
                        </ConsultaConsumidorCadastro>
                    </soap:Body>
                </soap:Envelope>';

        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];

        try { //tratamento de erro de requisição
            $response = $this->http->post($this->url, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) { 
            $response = $e->getResponse();
        }
        
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
        //dump($response->getBody()->getContents()); die();
        $doc = new \DOMDocument();  
        $doc->loadXML($response->getBody()->getContents());

        $nome = $doc->getElementsByTagName('nome')->item(0)->nodeValue;
        $cnpj = $doc->getElementsByTagName('cnpj')->item(0)->nodeValue;
        $cpf  = $doc->getElementsByTagName('cpf')->item(0)->nodeValue;
        $email = $doc->getElementsByTagName('email')->item(0)->nodeValue;
        $ddd = $doc->getElementsByTagName('ddd_celular')->item(0)->nodeValue;
        $num_celular = $doc->getElementsByTagName('num_celular')->item(0)->nodeValue;
        /* news fields */
        $razao = $doc->getElementsByTagName('razao')->item(0)->nodeValue;
        $fantasia = $doc->getElementsByTagName('fantasia')->item(0)->nodeValue;
        $cep = $doc->getElementsByTagName('cep')->item(0)->nodeValue;
        $logradouro = $doc->getElementsByTagName('logradouro')->item(0)->nodeValue;
        $numero = $doc->getElementsByTagName('numero')->item(0)->nodeValue;
        $complemento = $doc->getElementsByTagName('complemento')->item(0)->nodeValue;
        $bairro = $doc->getElementsByTagName('bairro')->item(0)->nodeValue;
        $cidade = $doc->getElementsByTagName('cidade')->item(0)->nodeValue;
        $uf = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
        $referencia = $doc->getElementsByTagName('referencia')->item(0)->nodeValue;
        $is_cadastro = $doc->getElementsByTagName('cadastro')->item(0)->nodeValue;

        /* end */

        $info = array(
            'status' => true, 
            'cnpj' => $cnpj, 
            'nome' => $nome, 
            'cpf' => $cpf, 
            'email' => $email, 
            'ddd' => $ddd, 
            'num_celular' => $num_celular,
            'razao' => $razao,
            'fantasia' => $fantasia,
            'cep' => $cep,
            'logradouro' => $logradouro,
            'numero' => $numero,
            'complemento' => $complemento,
            'bairro' => $bairro,
            'cidade' => $cidade,
            'uf' => $uf,
            'referencia' => $referencia,
            'is_cadastro' => filter_var($is_cadastro, FILTER_VALIDATE_BOOLEAN)
        );
          
        return $info;
    }

    public function atualizaConsumidorCadastro($username, $password, $request)
    {
        $requesToken = $this->requestToken($username, $password);
          
        if (!$requesToken['status']) {
            return $requesToken;
        }

        $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
        $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf'));
        $cep = str_replace([' ', '-'], '', $request->request->get('cep'));
        $razao_social = str_replace('&', '&amp;', $request->request->get('razao_social'));
        $name_fantasia = str_replace('&', '&amp;', $request->request->get('name_fantasia'));

        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Header>
                        <Authorization xmlns="http://tempuri.org/">
                        <access_token>'.$requesToken['token'].'</access_token>
                        </Authorization>
                    </soap:Header>
                    <soap:Body>
                        <AtualizaConsumidorCadastro xmlns="http://tempuri.org/">
                        <cnpj>'.$cnpj.'</cnpj>
                        <cpf>'.$cpf.'</cpf>
                        <razao>'.$razao_social.'</razao>
                        <fantasia>'.$name_fantasia.'</fantasia>
                        <responsavel>'.$request->request->get('name').'</responsavel>
                        <email>'.$request->request->get('mail').'</email>
                        <ddd>'.$request->request->get('ddd_celular').'</ddd>
                        <celular>'.$request->request->get('celular').'</celular>
                        <cep>'.$cep.'</cep>
                        <logradouro>'.$request->request->get('endereco').'</logradouro>
                        <numero>'.$request->request->get('numero').'</numero>
                        <complemento>'.$request->request->get('complemento').'</complemento>
                        <bairro>'.$request->request->get('bairro').'</bairro>
                        <cidade>'.$request->request->get('cidade').'</cidade>
                        <uf>'.$request->request->get('uf').'</uf>
                        <referencia>'.$request->request->get('ponto_referencia').'</referencia>
                        <whatsApp>'.$request->request->get('whatsApp').'</whatsApp>
                        </AtualizaConsumidorCadastro>
                    </soap:Body>
                </soap:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=utf-8',
            ],
            'body' => $xml,
        ];

        //dump($xml); die();
        
        try { //tratamento de erro de requisição
            $response = $this->http->post($this->url, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) { 
            $response = $e->getResponse();
        }
      
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
       //dump($response->getBody()->getContents()); die();
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        $codigo = $doc->getElementsByTagName('codigo')->item(0)->nodeValue;
        $descricao = $doc->getElementsByTagName('descricao')->item(0)->nodeValue;
        
        return array('status' => true, 'codigo' => $codigo, 'descricao' => $descricao);
    }



}