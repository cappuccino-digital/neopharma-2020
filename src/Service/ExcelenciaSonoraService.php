<?php
namespace App\Service;

use AgenciaSonora\NeoPharma\Excelencia\SDK\Facades\NeoExcelenciaSDK;

class ExcelenciaSonoraService {
    
    private $client;

    public function __construct($secret, $environment)
    {
        $config = [
            'secret' => $secret,
            'environment' => $environment
        ];

        $sdk = new NeoExcelenciaSDK();
        $this->client = $sdk->client($config);
    }

    public function impersonate($user) 
    {
        $response = [];

        if (!$user->getCnpj()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (!$user->getCpf()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (!$user->getCompanyName()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (!$user->getFantasyName()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (!$user->getName()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }
        
        if (!$user->getEmail()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (!$user->getDddCellphone() || !$user->getCellphone()) {
            $response = ['status' => false, 'message' => 'Para prosseguir é necessário completar o cadastro ', 'error' => 'GJ001'];
        }

        if (count($response) > 0) {
            return $response;
        }

        $dd = preg_replace('/[^0-9]/', '', $user->getDddCellphone());
        $cellphone =  preg_replace('/[^0-9]/', '', $user->getCellphone());

        $cellphone = $dd . $cellphone;
       
        try {
            $birthday = $user->getBirthday();

            if ($birthday) {
                $birthday = $user->getBirthday()->format('Y-m-d');
            }

            $is_whats_and_sms = $user->getIsNotificationWhatsapp() ? $user->getIsNotificationWhatsapp() : false;
            $is_newsletter = $user->getNewsletter() ? $user->getNewsletter() : false;

            $fields =  [
                'cpf' => $user->getCpf(),
                'cnpj' => $user->getCnpj(),
                'corporate_name' => $user->getFantasyName(),
                'company_name' => $user->getCompanyName(),
                'name' => $user->getName(),
                'role_code' => 6,
                'role' => 'Gestor de Loja',
                'email' => $user->getEmail(),
                'mobile' => $cellphone,
                'sex' => null,
                'birth' => $birthday,
                'optin_whats_and_sms' => $is_whats_and_sms,
                'optin_email' => $is_newsletter,
            ];

            $response = $this->client->post('/impersonate', $fields);
            
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...

            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {
            
            if ($response->getStatusCode() != 200) {
                return ['status' => false, 'message' => 'Ops! error '. $response->getStatusCode(), 'error' => 'GJ002'];
            }

            $content = json_decode($response->getBody()->getContents());
         
            if (isset($content->url)) {
                return ['status' => true, 'message' => 'success', 'url' => $content->url, 'expire_at' => $content->expire_at];
            }

            return ['status' => false, 'message' => 'ERROR-05', 'error' => 'GJ002'];
        }
    }


}