<?php

namespace App\Service;

use \GuzzleHttp\Client;

class NeoPharmaService {

    private $http;
    private $url;    
    private $wstoken;
  
    public function __construct($url, $wstoken)
    {
        $this->http    = new Client();
        $this->url     = $url;
        $this->wstoken = $wstoken;
    }
    
    public function getUser($user, $courseId = null) //método para buscar usuário
    {  
        $parameters = [
            'wstoken' => $this->wstoken,
            'wsfunction' => 'core_user_get_users',
            'moodlewsrestformat' => 'json',
        ];
        
        if (!empty($user->getEmail())) {
            $parameters['criteria[0][key]'] = 'email';
            $parameters['criteria[0][value]'] = $user->getEmail();
        //} else if (!empty($user->getUserName())) {
        } else if (!empty($user->getEmail())) {
            $parameters['criteria[0][key]'] = 'username';
            $parameters['criteria[0][value]'] = $user->getEmail();
            //$parameters['criteria[0][value]'] = $user->getUserName();
        } else {
            return ['status' => false, 'message' => 'Opção de busca, não encontrada'];
        }

        $response = $this->http->post($this->url, ['form_params' => $parameters]);
       
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        $result = json_decode($response->getBody()->getContents());

        if (empty($courseId)) { // se não passar o courseId, sera retornado usuario do racine
            return $result;
        }

        $userRacine = isset($result->users[0]) ? $result->users[0] : null;
        
        if (empty($userRacine)) { 
            $createUser = $this->createUser($user); //criando usuário se não existir no racine
            $userId     = isset($createUser[0]->id) ? $createUser[0]->id : null;
           
            if (empty($userId)) {
                return ['status' => false, 'message' => $createUser['message']];
            }
           
        } else {
            $userId = $userRacine->id; //se o usuário existir 
        }
        
        $enrollCourse = $this->enrollCourse($userId, $courseId); //matricula
        
        if ($enrollCourse['status']) {
            $login = $this->login($user->getEmail(), $user->getEmail(), $courseId); //login
            
            if ($login['status'])  {
                return ['status' => true, 'userRacineId' => $userId, 'url' => $login['url']];
            }
            return ['status' => false, 'message' => $login['message']];
        } 

        return ['status' => false, 'message' => $enrollCourse['message']];        
    }

    public function createUser($user) //método para criar novo usuário 
    {    
        if (empty($user->getEmail())) {
            return ['status' => false, 'message' => 'Username é obrigatório.'];
        }

        if (empty($user->getName())) {
            return ['status' => false, 'message' => 'O nome é obrigatório.'];
        }

        if (empty($user->getEmail())) {
            return ['status' => false, 'message' => 'O e-mail é obrigatório.'];
        }
     
        $parameters = array(
            'wstoken' => $this->wstoken,
            'wsfunction' => 'core_user_create_users',
            'moodlewsrestformat' => 'json',
            'users[0][username]' => strtolower($user->getEmail()),
            'users[0][password]' => strtolower($user->getEmail()));

        if (strstr($user->getName(), ' ')) {
            list($firstName, $lastName) = explode(' ', $user->getName());
        } else {
            $firstName = $user->getName();
            $lastName = $user->getName();
        }

        $parameters['users[0][firstname]'] = $firstName;
        $parameters['users[0][lastname]']  = $lastName;
        $parameters['users[0][email]'] = $user->getEmail();
      
        $response = $this->http->post($this->url, ['form_params' => $parameters]);

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        $result = json_decode($response->getBody()->getContents());
       
        if (isset($result->errorcode)) {
            return ['status' => false, 'message' => 'Erro cadastro de usuário: ' . $result->message];
        }

        return ['status' => true, 'message' => 'Cadastro realizado com sucesso.'];
    }

    public function enrollCourse($userId, $courseId) //método para inscrever aluno no curso
    {
        if (empty($userId)) {
            return ['status' => false, 'message' => 'Usuário é obrigatório.'];
        } 

        if (empty($courseId)) {
            return ['status' => false, 'message' => 'Curso é obrigatório.'];
        }

        if (empty($this->getCourse($courseId)) || (isset($this->getCourse($courseId)[0]->id) && $this->getCourse($courseId)[0]->id == 1)) {
            return ['status' => false, 'message' => 'Curso não encontrado'];
        }
                
        $parameters = array(
            'wstoken' => $this->wstoken,
            'wsfunction' => 'enrol_manual_enrol_users',
            'moodlewsrestformat' => 'json',
            'enrolments[0][roleid]' => 5,
            'enrolments[0][userid]' => $userId,
            'enrolments[0][courseid]' => $courseId);
       
        $response = $this->http->post($this->url, ['form_params' => $parameters]); //salvar matricula do usuário

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        $result = json_decode($response->getBody()->getContents());
       
        if (isset($result->errorcode)) {
            return ['status' => false, 'message' => 'Erro ao tentar fazer matrícula.'];
        }

        return ['status' => true, 'message' => 'Cadastro realizado com sucesso.'];   
    }

    public function checkUserInCurso($userId) //método para consultar a matricula do usuário
    {
        if (empty($courseId)) {
            return ['status' => false, 'message' => 'Curso é obrigatório.'];
        }

        $parameters = array(
            'wstoken' => $this->wstoken,
            'wsfunction' => 'core_enrol_get_users_courses',
            'moodlewsrestformat' => 'json');

        $parameters['userid'] = $userId;

        $response = $this->http->post($this->url, ['form_params' => $parameters]);

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getCourse($courseId) 
    {
        if (empty($courseId)) {
            return ['status' => false, 'message' => 'Curso é obrigatório.'];
        }

        $parameters = array(
            'wstoken' => $this->wstoken,
            'wsfunction' => 'core_course_get_courses',
            'moodlewsrestformat' => 'json',
            'options[ids][0]' => $courseId);

        $response = $this->http->post($this->url, ['form_params' => $parameters]);

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getListCourse()
    {
        $response = $this->http->post($this->url, ['form_params' => [
                                                   'wstoken' => $this->wstoken,
                                                   'wsfunction' => 'core_course_get_courses',
                                                   'moodlewsrestformat' => 'json']]);
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }

        return json_decode($response->getBody()->getContents());
    }

    public function login($username, $password, $courseId) 
    {
        $response = $this->http->get("http://cursos.portalneopharma.com.br/ws/auth.php?wstoken={$this->wstoken}&username={$username}&password={$password}&courseid={$courseId}");
     
        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }
        
        $result = json_decode($response->getBody()->getContents());
       
        if ((isset($result->error)) && ($result->error == 'invalidparameter')) {
            return ['status' => false, 'message' => 'Curso não encontrado'];
        } else if ((isset($result->error)) && ($result->error == 'invalidtoken')) {
            return ['status' => false, 'message' => 'Token inválido'];
        } else if ((isset($result->error)) && ($result->error== 'nvalidlogin')) {
            return ['status' => false, 'message' => 'Login ou senha inválida'];
        } else if ((isset($result->error)) && ($result->error == 'noenrolled')) {
            return ['status' => false, 'message' => 'Aluno não matriculado no curso'];
        } 

        return ['status' => true, 'url' => $result->url];
    }

    public function updatedUser($user, $id)
    {
        if (empty($user->getEmail())) {
            return ['status' => false, 'message' => 'Username é obrigatório.'];
        }

        if (empty($user->getName())) {
            return ['status' => false, 'message' => 'O nome é obrigatório.'];
        }

        if (empty($user->getEmail())) {
            return ['status' => false, 'message' => 'O e-mail é obrigatório.'];
        }
     
        $parameters = array(
            'wstoken' => $this->wstoken,
            'wsfunction' => 'core_user_update_users',
            'moodlewsrestformat' => 'json',
            'users[0][id]' => $id,
            'users[0][username]' => $user->getEmail(),
            'users[0][password]' => $user->getEmail());

        if (strstr($user->getName(), ' ')) {
            list($firstName, $lastName) = explode(' ', $user->getName());
        } else {
            $firstName = $user->getName();
            $lastName = $user->getName();
        }

        $parameters['users[0][firstname]'] = $firstName;
        $parameters['users[0][lastname]']  = $lastName;
        $parameters['users[0][email]'] = $user->getEmail();
      
        $response = $this->http->post($this->url, ['form_params' => $parameters]);

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! erro na requisição.'];
        }

        return json_decode($response->getBody()->getContents());
    }

    
}