<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $directory)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = explode(' ', $originalFilename);

        $original_name = isset($safeFilename[0]) ? strtolower($safeFilename[0]) : $directory; 
        $fileName = $original_name.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory() . '/' . $directory . '/', $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        $address_file = '/uploads/' . $directory . '/'.$fileName;
        return $address_file;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}