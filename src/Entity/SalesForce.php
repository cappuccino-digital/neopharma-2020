<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalesForceRepository")
 */
class SalesForce
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nm_regional;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nm_divisao;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sede_setor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $colaborador;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNmRegional(): ?string
    {
        return $this->nm_regional;
    }

    public function setNmRegional(string $nm_regional): self
    {
        $this->nm_regional = $nm_regional;

        return $this;
    }

    public function getNmDivisao(): ?string
    {
        return $this->nm_divisao;
    }

    public function setNmDivisao(string $nm_divisao): self
    {
        $this->nm_divisao = $nm_divisao;

        return $this;
    }

    public function getSedeSetor(): ?string
    {
        return $this->sede_setor;
    }

    public function setSedeSetor(string $sede_setor): self
    {
        $this->sede_setor = $sede_setor;

        return $this;
    }

    public function getColaborador(): ?string
    {
        return $this->colaborador;
    }

    public function setColaborador(string $colaborador): self
    {
        $this->colaborador = $colaborador;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }
}
