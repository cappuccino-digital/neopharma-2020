<?php

namespace App\Entity;

use App\Repository\PreferentialChildrenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PreferentialChildrenRepository::class)
 */
class PreferentialChildren
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserPreferential::class, inversedBy="preferentialChildrens")
     */
    private $user_preferential;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $team_football;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $children;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserPreferential(): ?UserPreferential
    {
        return $this->user_preferential;
    }

    public function setUserPreferential(?UserPreferential $user_preferential): self
    {
        $this->user_preferential = $user_preferential;

        return $this;
    }

    public function getTeamFootball(): ?string
    {
        return $this->team_football;
    }

    public function setTeamFootball(?string $team_football): self
    {
        $this->team_football = $team_football;

        return $this;
    }

    public function getSport(): ?string
    {
        return $this->sport;
    }

    public function setSport(?string $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getChildren(): ?string
    {
        return $this->children;
    }

    public function setChildren(?string $children): self
    {
        $this->children = $children;

        return $this;
    }
}
