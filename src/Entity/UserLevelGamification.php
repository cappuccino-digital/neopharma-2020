<?php

namespace App\Entity;

use App\Repository\UserLevelGamificationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserLevelGamificationRepository::class)
 */
class UserLevelGamification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userLevelGamifications")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=LevelGamification::class, inversedBy="userLevelGamifications")
     */
    private $level;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_show_popup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLevel(): ?LevelGamification
    {
        return $this->level;
    }

    public function setLevel(?LevelGamification $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsShowPopup(): ?bool
    {
        return $this->is_show_popup;
    }

    public function setIsShowPopup(?bool $is_show_popup): self
    {
        $this->is_show_popup = $is_show_popup;

        return $this;
    }
}
