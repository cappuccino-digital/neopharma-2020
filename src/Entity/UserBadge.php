<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserBadgeRepository")
 */
class UserBadge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $badgeId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="BadgeLevel")
     * @ORM\JoinColumn(name="badge_level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $badgeLevel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBadgeId(): ?int
    {
        return $this->badgeId;
    }

    public function setBadgeId(?int $badgeId): self
    {
        $this->badgeId = $badgeId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBadgeLevel(): ?BadgeLevel
    {
        return $this->badgeLevel;
    }

    public function setBadgeLevel(?BadgeLevel $badgeLevel): self
    {
        $this->badgeLevel = $badgeLevel;

        return $this;
    }
}
