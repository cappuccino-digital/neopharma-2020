<?php

namespace App\Entity;

use App\Repository\InteractionsGamificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InteractionsGamificationRepository::class)
 */
class InteractionsGamification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_mission;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=UserPointGamification::class, mappedBy="interation")
     */
    private $userPointGamifications;

    public function __construct()
    {
        $this->userPointGamifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPoint(): ?string
    {
        return $this->point;
    }

    public function setPoint(?string $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsMission(): ?bool
    {
        return $this->is_mission;
    }

    public function setIsMission(?bool $is_mission): self
    {
        $this->is_mission = $is_mission;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|UserPointGamification[]
     */
    public function getUserPointGamifications(): Collection
    {
        return $this->userPointGamifications;
    }

    public function addUserPointGamification(UserPointGamification $userPointGamification): self
    {
        if (!$this->userPointGamifications->contains($userPointGamification)) {
            $this->userPointGamifications[] = $userPointGamification;
            $userPointGamification->setInteration($this);
        }

        return $this;
    }

    public function removeUserPointGamification(UserPointGamification $userPointGamification): self
    {
        if ($this->userPointGamifications->removeElement($userPointGamification)) {
            // set the owning side to null (unless already changed)
            if ($userPointGamification->getInteration() === $this) {
                $userPointGamification->setInteration(null);
            }
        }

        return $this;
    }
}
