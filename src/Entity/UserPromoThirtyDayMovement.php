<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPromoThirtyDayMovementRepository")
 */
class UserPromoThirtyDayMovement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPromoThirtyDayMovements")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $whatsapp;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $instagram;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $isRegulament;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPointPromoThirtyDayMovement", mappedBy="user_promo_thirty_day_movement")
     */
    private $userPointPromoThirtyDayMovements;

    public function __construct()
    {
        $this->userPointPromoThirtyDayMovements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function setWhatsapp(string $whatsapp): self
    {
        $this->whatsapp = $whatsapp;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getIsRegulament(): ?bool
    {
        return $this->isRegulament;
    }

    public function setIsRegulament(bool $isRegulament): self
    {
        $this->isRegulament = $isRegulament;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|UserPointPromoThirtyDayMovement[]
     */
    public function getUserPointPromoThirtyDayMovements(): Collection
    {
        return $this->userPointPromoThirtyDayMovements;
    }

    public function addUserPointPromoThirtyDayMovement(UserPointPromoThirtyDayMovement $userPointPromoThirtyDayMovement): self
    {
        if (!$this->userPointPromoThirtyDayMovements->contains($userPointPromoThirtyDayMovement)) {
            $this->userPointPromoThirtyDayMovements[] = $userPointPromoThirtyDayMovement;
            $userPointPromoThirtyDayMovement->setUserPromoThirtyDayMovement($this);
        }

        return $this;
    }

    public function removeUserPointPromoThirtyDayMovement(UserPointPromoThirtyDayMovement $userPointPromoThirtyDayMovement): self
    {
        if ($this->userPointPromoThirtyDayMovements->contains($userPointPromoThirtyDayMovement)) {
            $this->userPointPromoThirtyDayMovements->removeElement($userPointPromoThirtyDayMovement);
            // set the owning side to null (unless already changed)
            if ($userPointPromoThirtyDayMovement->getUserPromoThirtyDayMovement() === $this) {
                $userPointPromoThirtyDayMovement->setUserPromoThirtyDayMovement(null);
            }
        }

        return $this;
    }
}
