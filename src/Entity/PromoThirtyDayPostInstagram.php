<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoThirtyDayPostInstagramRepository")
 */
class PromoThirtyDayPostInstagram
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $username;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_collect_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $post;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_public_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromoThirtyDayPostInstragramPoint", mappedBy="post_instagram")
     */
    private $promoThirtyDayPostInstragramPoints;

    public function __construct()
    {
        $this->promoThirtyDayPostInstragramPoints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getDateCollectAt(): ?\DateTimeInterface
    {
        return $this->date_collect_at;
    }

    public function setDateCollectAt(\DateTimeInterface $date_collect_at): self
    {
        $this->date_collect_at = $date_collect_at;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getDatePublicAt(): ?\DateTimeInterface
    {
        return $this->date_public_at;
    }

    public function setDatePublicAt(\DateTimeInterface $date_public_at): self
    {
        $this->date_public_at = $date_public_at;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|PromoThirtyDayPostInstragramPoint[]
     */
    public function getPromoThirtyDayPostInstragramPoints(): Collection
    {
        return $this->promoThirtyDayPostInstragramPoints;
    }

    public function addPromoThirtyDayPostInstragramPoint(PromoThirtyDayPostInstragramPoint $promoThirtyDayPostInstragramPoint): self
    {
        if (!$this->promoThirtyDayPostInstragramPoints->contains($promoThirtyDayPostInstragramPoint)) {
            $this->promoThirtyDayPostInstragramPoints[] = $promoThirtyDayPostInstragramPoint;
            $promoThirtyDayPostInstragramPoint->setPostInstagram($this);
        }

        return $this;
    }

    public function removePromoThirtyDayPostInstragramPoint(PromoThirtyDayPostInstragramPoint $promoThirtyDayPostInstragramPoint): self
    {
        if ($this->promoThirtyDayPostInstragramPoints->contains($promoThirtyDayPostInstragramPoint)) {
            $this->promoThirtyDayPostInstragramPoints->removeElement($promoThirtyDayPostInstragramPoint);
            // set the owning side to null (unless already changed)
            if ($promoThirtyDayPostInstragramPoint->getPostInstagram() === $this) {
                $promoThirtyDayPostInstragramPoint->setPostInstagram(null);
            }
        }

        return $this;
    }
}
