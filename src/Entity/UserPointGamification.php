<?php

namespace App\Entity;

use App\Repository\UserPointGamificationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPointGamificationRepository::class)
 */
class UserPointGamification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userPointGamifications")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=InteractionsGamification::class, inversedBy="userPointGamifications")
     */
    private $interation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $content_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $content_type_id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_store_manager;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_clerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_pharmaceutical;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_mission;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_show_popup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getInteration(): ?InteractionsGamification
    {
        return $this->interation;
    }

    public function setInteration(?InteractionsGamification $interation): self
    {
        $this->interation = $interation;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getContentType(): ?string
    {
        return $this->content_type;
    }

    public function setContentType(?string $content_type): self
    {
        $this->content_type = $content_type;

        return $this;
    }

    public function getContentTypeId(): ?string
    {
        return $this->content_type_id;
    }

    public function setContentTypeId(?string $content_type_id): self
    {
        $this->content_type_id = $content_type_id;

        return $this;
    }

    public function getProfileStoreManager(): ?bool
    {
        return $this->profile_store_manager;
    }

    public function setProfileStoreManager(?bool $profile_store_manager): self
    {
        $this->profile_store_manager = $profile_store_manager;

        return $this;
    }

    public function getProfileClerk(): ?bool
    {
        return $this->profile_clerk;
    }

    public function setProfileClerk(?bool $profile_clerk): self
    {
        $this->profile_clerk = $profile_clerk;

        return $this;
    }

    public function getProfilePharmaceutical(): ?bool
    {
        return $this->profile_pharmaceutical;
    }

    public function setProfilePharmaceutical(?bool $profile_pharmaceutical): self
    {
        $this->profile_pharmaceutical = $profile_pharmaceutical;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsMission(): ?bool
    {
        return $this->is_mission;
    }

    public function setIsMission(?bool $is_mission): self
    {
        $this->is_mission = $is_mission;

        return $this;
    }

    public function getIsShowPopup(): ?bool
    {
        return $this->is_show_popup;
    }

    public function setIsShowPopup(?bool $is_show_popup): self
    {
        $this->is_show_popup = $is_show_popup;

        return $this;
    }
}
