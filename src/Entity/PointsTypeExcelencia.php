<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PointsTypeExcelenciaRepository")
 */
class PointsTypeExcelencia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cod_interaction;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createadAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPointsExcelencia", mappedBy="point_type_excelencia")
     */
    private $userPointsExcelencias;

    public function __construct()
    {
        $this->userPointsExcelencias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCodInteraction(): ?string
    {
        return $this->cod_interaction;
    }

    public function setCodInteraction(?string $cod_interaction): self
    {
        $this->cod_interaction = $cod_interaction;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreateadAt(): ?\DateTimeInterface
    {
        return $this->createadAt;
    }

    public function setCreateadAt(\DateTimeInterface $createadAt): self
    {
        $this->createadAt = $createadAt;

        return $this;
    }

    /**
     * @return Collection|UserPointsExcelencia[]
     */
    public function getUserPointsExcelencias(): Collection
    {
        return $this->userPointsExcelencias;
    }

    public function addUserPointsExcelencia(UserPointsExcelencia $userPointsExcelencia): self
    {
        if (!$this->userPointsExcelencias->contains($userPointsExcelencia)) {
            $this->userPointsExcelencias[] = $userPointsExcelencia;
            $userPointsExcelencia->setPointTypeExcelencia($this);
        }

        return $this;
    }

    public function removeUserPointsExcelencia(UserPointsExcelencia $userPointsExcelencia): self
    {
        if ($this->userPointsExcelencias->contains($userPointsExcelencia)) {
            $this->userPointsExcelencias->removeElement($userPointsExcelencia);
            // set the owning side to null (unless already changed)
            if ($userPointsExcelencia->getPointTypeExcelencia() === $this) {
                $userPointsExcelencia->setPointTypeExcelencia(null);
            }
        }

        return $this;
    }
}
