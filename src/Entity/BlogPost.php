<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BlogPostRepository")
 */
class BlogPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $post;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tags;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $podcastFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $videoFile;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $score = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $views = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $readingTime = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="BlogPostCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="BlogPostType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="BlogPostScore", mappedBy="post")
     *
     */
    private $scores;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToPharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToClerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPharmacy;

    /**
     * @ORM\ManyToMany(targetEntity=SpecialCampaign::class, mappedBy="blog_post")
     */
    private $specialCampaigns;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->specialCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(?string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(?float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getPodcastFile(): ?string
    {
        return $this->podcastFile;
    }

    public function setPodcastFile(?string $podcastFile): self
    {
        $this->podcastFile = $podcastFile;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addFavoritePost($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeFavoritePost($this);
        }

        return $this;
    }

    public function getReadingTime(): ?int
    {
        return $this->readingTime;
    }

    public function getReadingTimeField(): ?string
    {
        return gmdate("H:i:s", $this->readingTime);
    }

    public function getReadingTimeString(): ?string
    {
        $returnTime = gmdate("H:i:s", $this->readingTime);
        $time = explode(':', $returnTime);

        $hour = isset($time[0]) ? (int)($time[0]) : 0;
        $minute = isset($time[1]) ? (int)($time[1]) : 0;
        $second = isset($time[2]) ? (int)($time[2]) : 0;

        if($hour == 0 ){
            if($minute == 0){
                return "{$second}s";
            } else {
                if($second == 0){
                    return "{$minute}m";
                } else {
                    return "{$minute}m {$second}s";
                }
            }
        } else {
            if($minute == 0){
                if($second == 0){
                    return "{$hour}h";
                } else {
                    return "{$hour}h {$second}s";
                }
            } else {
                if($second == 0){
                    return "{$hour}h {$minute}m";
                } else {
                    return "{$hour}h {$minute}m {$second}s";
                }
            }
        }
    }

    public function setReadingTime(?int $readingTime): self
    {
        $this->readingTime = $readingTime;

        return $this;
    }

    public function getCategory(): ?BlogPostCategory
    {
        return $this->category;
    }

    public function setCategory(?BlogPostCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getType(): ?BlogPostType
    {
        return $this->type;
    }

    public function setType(?BlogPostType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|BlogPostScore[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(BlogPostScore $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setPost($this);
        }

        return $this;
    }

    public function removeScore(BlogPostScore $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getPost() === $this) {
                $score->setPost(null);
            }
        }

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(?int $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getIsOnlyToPharmaceutical(): ?bool
    {
        return $this->isOnlyToPharmaceutical;
    }

    public function setIsOnlyToPharmaceutical(?bool $isOnlyToPharmaceutical): self
    {
        $this->isOnlyToPharmaceutical = $isOnlyToPharmaceutical;

        return $this;
    }
    
    public function getIsOnlyToClerk(): ?bool
    {
        return $this->isOnlyToClerk;
    }

    public function setIsOnlyToClerk(?bool $isOnlyToClerk): self
    {
        $this->isOnlyToClerk = $isOnlyToClerk;

        return $this;
    }
    
    public function getVideoFile(): ?string
    {
        return $this->videoFile;
    }

    public function setVideoFile(?string $videoFile): self
    {
        $this->videoFile = $videoFile;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(?bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getIsPharmacy(): ?bool
    {
        return $this->isPharmacy;
    }

    public function setIsPharmacy(?bool $isPharmacy): self
    {
        $this->isPharmacy = $isPharmacy;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }

    /**
     * @return Collection|SpecialCampaign[]
     */
    public function getSpecialCampaigns(): Collection
    {
        return $this->specialCampaigns;
    }

    public function addSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if (!$this->specialCampaigns->contains($specialCampaign)) {
            $this->specialCampaigns[] = $specialCampaign;
            $specialCampaign->addBlogPost($this);
        }

        return $this;
    }

    public function removeSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if ($this->specialCampaigns->removeElement($specialCampaign)) {
            $specialCampaign->removeBlogPost($this);
        }

        return $this;
    }
}
