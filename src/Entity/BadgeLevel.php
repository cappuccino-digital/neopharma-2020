<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BadgeLevelRepository")
 */
class BadgeLevel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minPointPharmaceutical;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minPointClerk;

    /**
     * @ORM\ManyToOne(targetEntity="Badge")
     * @ORM\JoinColumn(name="badge_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $badge;

    /**
     * @ORM\OneToMany(targetEntity="UserBadge", mappedBy="badgeLevel")
     *
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getMinPoint(): ?int
    {
        return $this->minPoint;
    }

    public function setMinPoint(?int $minPoint): self
    {
        $this->minPoint = $minPoint;

        return $this;
    }

    public function getGivenPoint(): ?int
    {
        return $this->givenPoint;
    }

    public function setGivenPoint(int $givenPoint): self
    {
        $this->givenPoint = $givenPoint;

        return $this;
    }

    public function getMinPointPharmaceutical(): ?int
    {
        return $this->minPointPharmaceutical;
    }

    public function setMinPointPharmaceutical(?int $minPointPharmaceutical): self
    {
        $this->minPointPharmaceutical = $minPointPharmaceutical;

        return $this;
    }

    public function getGivenPointPharmaceutical(): ?int
    {
        return $this->givenPointPharmaceutical;
    }

    public function setGivenPointPharmaceutical(int $givenPointPharmaceutical): self
    {
        $this->givenPointPharmaceutical = $givenPointPharmaceutical;

        return $this;
    }

    public function getMinPointClerk(): ?int
    {
        return $this->minPointClerk;
    }

    public function setMinPointClerk(?int $minPointClerk): self
    {
        $this->minPointClerk = $minPointClerk;

        return $this;
    }

    public function getGivenPointClerk(): ?int
    {
        return $this->givenPointClerk;
    }

    public function setGivenPointClerk(int $givenPointClerk): self
    {
        $this->givenPointClerk = $givenPointClerk;

        return $this;
    }

    public function getBadge(): ?Badge
    {
        return $this->badge;
    }

    public function setBadge(?Badge $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * @return Collection|UserBadge[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(UserBadge $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setBadgeLevel($this);
        }

        return $this;
    }

    public function removeUser(UserBadge $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getBadgeLevel() === $this) {
                $user->setBadgeLevel(null);
            }
        }

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }
}
