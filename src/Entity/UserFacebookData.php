<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserFacebookDataRepository")
 */
class UserFacebookData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=510 ,nullable=true)
     */
    private $accessToken;
    
    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $expiresIn;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $signedRequest;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $dataAccessExpirationTime;

    /**
     * @ORM\OneToOne(targetEntity="User", cascade={"remove"})
     *
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getUserID(): ?string
    {
        return $this->userID;
    }

    public function setUserID(?string $userID): self
    {
        $this->userID = $userID;

        return $this;
    }

    public function getExpiresIn(): ?string
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(?string $expiresIn): self
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    public function getSignedRequest(): ?string
    {
        return $this->signedRequest;
    }

    public function setSignedRequest(?string $signedRequest): self
    {
        $this->signedRequest = $signedRequest;

        return $this;
    }

    public function getDataAccessExpirationTime(): ?string
    {
        return $this->dataAccessExpirationTime;
    }

    public function setDataAccessExpirationTime(?string $dataAccessExpirationTime): self
    {
        $this->dataAccessExpirationTime = $dataAccessExpirationTime;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }
}
