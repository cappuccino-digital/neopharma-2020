<?php

namespace App\Entity;

use App\Repository\LeadsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeadsRepository::class)
 */
class Leads
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_facebook;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_google;

    /**
     * @ORM\ManyToOne(targetEntity=Role::class, inversedBy="leads")
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_newsletter;

    /**
     * @ORM\OneToMany(targetEntity=LeadEmblue::class, mappedBy="lead")
     */
    private $leadEmblues;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $cnpj;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estate_address;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $educationalLevel;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $friendIndication;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crf;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $step;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cellphone;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    public function __construct()
    {
        $this->leadEmblues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getIsFacebook(): ?bool
    {
        return $this->is_facebook;
    }

    public function setIsFacebook(bool $is_facebook): self
    {
        $this->is_facebook = $is_facebook;

        return $this;
    }

    public function getIsGoogle(): ?bool
    {
        return $this->is_google;
    }

    public function setIsGoogle(bool $is_google): self
    {
        $this->is_google = $is_google;

        return $this;
    }

    public function getIsNewsletter(): ?bool
    {
        return $this->is_newsletter;
    }

    public function setIsNewsletter(?bool $is_newsletter): self
    {
        $this->is_newsletter = $is_newsletter;

        return $this;
    }

    /**
     * @return Collection|LeadEmblue[]
     */
    public function getLeadEmblues(): Collection
    {
        return $this->leadEmblues;
    }

    public function addLeadEmblue(LeadEmblue $leadEmblue): self
    {
        if (!$this->leadEmblues->contains($leadEmblue)) {
            $this->leadEmblues[] = $leadEmblue;
            $leadEmblue->setLead($this);
        }

        return $this;
    }

    public function removeLeadEmblue(LeadEmblue $leadEmblue): self
    {
        if ($this->leadEmblues->removeElement($leadEmblue)) {
            // set the owning side to null (unless already changed)
            if ($leadEmblue->getLead() === $this) {
                $leadEmblue->setLead(null);
            }
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(?string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(?string $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getEducationalLevel(): ?string
    {
        return $this->educationalLevel;
    }

    public function setEducationalLevel(?string $educationalLevel): self
    {
        $this->educationalLevel = $educationalLevel;

        return $this;
    }

    public function getFriendIndication(): ?string
    {
        return $this->friendIndication;
    }

    public function setFriendIndication(?string $friendIndication): self
    {
        $this->friendIndication = $friendIndication;

        return $this;
    }

    public function getCrf(): ?string
    {
        return $this->crf;
    }

    public function setCrf(?string $crf): self
    {
        $this->crf = $crf;

        return $this;
    }

    public function getStep(): ?string
    {
        return $this->step;
    }

    public function setStep(?string $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getEstateAddress(): ?string
    {
        return $this->estate_address;
    }

    public function setEstateAddress(?string $estate_address): self
    {
        $this->estate_address = $estate_address;

        return $this;
    }
}
