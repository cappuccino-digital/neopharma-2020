<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoThirtyDayMovementUserMovieRepository")
 */
class PromoThirtyDayMovementUserMovie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserPointPromoThirtyDayMovement", cascade={"persist", "remove"})
     */
    private $UserPointPromoThirtyDayMovement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="promoThirtyDayMovementUserMovies")
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PromoThirtyDayMovementMovies", inversedBy="promoThirtyDayMovementUserMovies")
     */
    private $PromoThirtyDayMovementMovies;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserPointPromoThirtyDayMovement(): ?UserPointPromoThirtyDayMovement
    {
        return $this->UserPointPromoThirtyDayMovement;
    }

    public function setUserPointPromoThirtyDayMovement(?UserPointPromoThirtyDayMovement $UserPointPromoThirtyDayMovement): self
    {
        $this->UserPointPromoThirtyDayMovement = $UserPointPromoThirtyDayMovement;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getPromoThirtyDayMovementMovies(): ?PromoThirtyDayMovementMovies
    {
        return $this->PromoThirtyDayMovementMovies;
    }

    public function setPromoThirtyDayMovementMovies(?PromoThirtyDayMovementMovies $PromoThirtyDayMovementMovies): self
    {
        $this->PromoThirtyDayMovementMovies = $PromoThirtyDayMovementMovies;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
