<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPointRepository")
 */
class UserPoint
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isValid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pointType;

    /**
     * @ORM\Column(type="integer")
     */
    private $pointTypeId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pointTypeDescription;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Badge")
     * @ORM\JoinColumn(name="badge_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $badge;

    /**
     * @ORM\Column(name="is_pharmaceutical", type="boolean" ,nullable=true)
     */
    private $isPharmaceutical;

    /**
     * @ORM\Column(name="is_pharmaceutical_content", type="boolean" ,nullable=true)
     */
    private $isPharmaceuticalContent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoint(): ?int
    {
        return $this->point;
    }

    public function setPoint(?int $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getPointType(): ?string
    {
        return $this->pointType;
    }

    public function setPointType(?string $pointType): self
    {
        $this->pointType = $pointType;

        return $this;
    }

    public function getPointTypeId(): ?int
    {
        return $this->pointTypeId;
    }

    public function setPointTypeId(int $pointTypeId): self
    {
        $this->pointTypeId = $pointTypeId;

        return $this;
    }

    public function getPointTypeDescription(): ?string
    {
        return $this->pointTypeDescription;
    }

    public function setPointTypeDescription(?string $pointTypeDescription): self
    {
        $this->pointTypeDescription = $pointTypeDescription;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBadge(): ?Badge
    {
        return $this->badge;
    }

    public function setBadge(?Badge $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    public function getIsPharmaceuticalContent(): ?bool
    {
        return $this->isPharmaceuticalContent;
    }

    public function setIsPharmaceuticalContent(?bool $isPharmaceuticalContent): self
    {
        $this->isPharmaceuticalContent = $isPharmaceuticalContent;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->isPharmaceutical;
    }

    public function setIsPharmaceutical(?bool $isPharmaceutical): self
    {
        $this->isPharmaceutical = $isPharmaceutical;

        return $this;
    }
}
