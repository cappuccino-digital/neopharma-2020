<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRegisterIncompleteRepository")
 */
class UserRegisterIncomplete
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userRegisters")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_register_complete;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_gestor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_clerk;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_social_network;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $field;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsRegisterComplete(): ?bool
    {
        return $this->is_register_complete;
    }

    public function setIsRegisterComplete(bool $is_register_complete): self
    {
        $this->is_register_complete = $is_register_complete;

        return $this;
    }

    public function getIsGestor(): ?bool
    {
        return $this->is_gestor;
    }

    public function setIsGestor(bool $is_gestor): self
    {
        $this->is_gestor = $is_gestor;

        return $this;
    }

    public function getIsSocialNetwork(): ?bool
    {
        return $this->is_social_network;
    }

    public function setIsSocialNetwork(bool $is_social_network): self
    {
        $this->is_social_network = $is_social_network;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }

    public function getIsClerk(): ?bool
    {
        return $this->is_clerk;
    }

    public function setIsClerk(bool $is_clerk): self
    {
        $this->is_clerk = $is_clerk;

        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
