<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

   /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introduction;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pdfFile;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="QuizResult", mappedBy="quiz")
     *
     */
    private $results;

    /**
     * @ORM\OneToMany(targetEntity="QuizQuestion", mappedBy="quiz")
     *
     */
    private $questions;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $views = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToPharmaceutical;

    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic;

    /**
     * @ORM\OneToMany(targetEntity="QuizScore", mappedBy="quiz")
     *
     */
    private $scores;

    /**
     * @ORM\Column(type="boolean", nullable=true)
    */
    private $isPharmacy;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $score = 0;

    public function __construct()
    {
        $this->results = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->scores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|QuizResult[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(QuizResult $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setQuiz($this);
        }

        return $this;
    }

    public function removeResult(QuizResult $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getQuiz() === $this) {
                $result->setQuiz(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuizQuestion[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(QuizQuestion $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setQuiz($this);
        }

        return $this;
    }

    public function removeQuestion(QuizQuestion $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getQuiz() === $this) {
                $question->setQuiz(null);
            }
        }

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(?int $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getPdfFile(): ?string
    {
        return $this->pdfFile;
    }

    public function setPdfFile(?string $pdfFile): self
    {
        $this->pdfFile = $pdfFile;

        return $this;
    }

    public function getIsOnlyToPharmaceutical(): ?bool
    {
        return $this->isOnlyToPharmaceutical;
    }

    public function setIsOnlyToPharmaceutical(?bool $isOnlyToPharmaceutical): self
    {
        $this->isOnlyToPharmaceutical = $isOnlyToPharmaceutical;

        return $this;
    }

    /**
     * @return Collection|QuizScore[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(QuizScore $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setPost($this);
        }

        return $this;
    }

    public function removeScore(QuizScore $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getPost() === $this) {
                $score->setPost(null);
            }
        }

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(?float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(?string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(?bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getIsPharmacy(): ?bool
    {
        return $this->isPharmacy;
    }

    public function setIsPharmacy(?bool $isPharmacy): self
    {
        $this->isPharmacy = $isPharmacy;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }
}
