<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerContentRepository")
 */
class ReferrerContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mainContentId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mainContentType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $referrerContentId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referrerContentType;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMainContentId(): ?int
    {
        return $this->mainContentId;
    }

    public function setMainContentId(int $mainContentId): self
    {
        $this->mainContentId = $mainContentId;

        return $this;
    }

    public function getMainContentType(): ?string
    {
        return $this->mainContentType;
    }

    public function setMainContentType(?string $mainContentType): self
    {
        $this->mainContentType = $mainContentType;

        return $this;
    }

    public function getReferrerContentId(): ?int
    {
        return $this->referrerContentId;
    }

    public function setReferrerContentId(?int $referrerContentId): self
    {
        $this->referrerContentId = $referrerContentId;

        return $this;
    }

    public function getReferrerContentType(): ?string
    {
        return $this->referrerContentType;
    }

    public function setReferrerContentType(?string $referrerContentType): self
    {
        $this->referrerContentType = $referrerContentType;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
