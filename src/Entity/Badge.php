<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 */
class Badge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pointDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="UserPoint", mappedBy="badge")
     *
     */
    private $points;

    /**
     * @ORM\OneToMany(targetEntity="BadgeLevel", mappedBy="badge")
     *
     */
    private $levels;

    /**
     * @ORM\Column(type="integer")
     */
    private $givenPointPharmaceutical;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $givenPointClerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToPharmaceutical;

    public function __construct()
    {
        $this->points = new ArrayCollection();
        $this->levels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|UserPoint[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    public function addPoint(UserPoint $point): self
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setBadge($this);
        }

        return $this;
    }

    public function removePoint(UserPoint $point): self
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            // set the owning side to null (unless already changed)
            if ($point->getBadge() === $this) {
                $point->setBadge(null);
            }
        }

        return $this;
    }

    public function getIsOnlyToPharmaceutical(): ?bool
    {
        return $this->isOnlyToPharmaceutical;
    }

    public function setIsOnlyToPharmaceutical(?bool $isOnlyToPharmaceutical): self
    {
        $this->isOnlyToPharmaceutical = $isOnlyToPharmaceutical;

        return $this;
    }

    public function getMinPoint(): ?int
    {
        return $this->minPoint;
    }

    public function setMinPoint(?int $minPoint): self
    {
        $this->minPoint = $minPoint;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getGivenPoint(): ?int
    {
        return $this->givenPoint;
    }

    public function setGivenPoint(?int $givenPoint): self
    {
        $this->givenPoint = $givenPoint;

        return $this;
    }

    public function getPointDescription(): ?string
    {
        return $this->pointDescription;
    }

    public function setPointDescription(?string $pointDescription): self
    {
        $this->pointDescription = $pointDescription;

        return $this;
    }

    /**
     * @return Collection|BadgeLevel[]
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(BadgeLevel $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
            $level->setBadge($this);
        }

        return $this;
    }

    public function removeLevel(BadgeLevel $level): self
    {
        if ($this->levels->contains($level)) {
            $this->levels->removeElement($level);
            // set the owning side to null (unless already changed)
            if ($level->getBadge() === $this) {
                $level->setBadge(null);
            }
        }

        return $this;
    }

    public function getGivenPointPharmaceutical(): ?int
    {
        return $this->givenPointPharmaceutical;
    }

    public function setGivenPointPharmaceutical(int $givenPointPharmaceutical): self
    {
        $this->givenPointPharmaceutical = $givenPointPharmaceutical;

        return $this;
    }

    public function getGivenPointClerk(): ?int
    {
        return $this->givenPointClerk;
    }

    public function setGivenPointClerk(int $givenPointClerk): self
    {
        $this->givenPointClerk = $givenPointClerk;

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }
}
