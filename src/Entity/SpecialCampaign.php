<?php

namespace App\Entity;

use App\Repository\SpecialCampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SpecialCampaignRepository::class)
 */
class SpecialCampaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=MedalsGamification::class, inversedBy="specialCampaign", cascade={"persist", "remove"})
     */
    private $medals;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $route_path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity=Course::class, inversedBy="specialCampaigns")
     */
    private $course;

    /**
     * @ORM\ManyToMany(targetEntity=BlogPost::class, inversedBy="specialCampaigns")
     */
    private $blog_post;

    /**
     * @ORM\ManyToMany(targetEntity=QuizAll::class, inversedBy="specialCampaigns")
     */
    private $quiz_all;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="specialCampaigns")
     */
    private $product;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_menu;

    public function __construct()
    {
        $this->course = new ArrayCollection();
        $this->blog_post = new ArrayCollection();
        $this->quiz_all = new ArrayCollection();
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMedals(): ?MedalsGamification
    {
        return $this->medals;
    }

    public function setMedals(?MedalsGamification $medals): self
    {
        $this->medals = $medals;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getRoutePath(): ?string
    {
        return $this->route_path;
    }

    public function setRoutePath(?string $route_path): self
    {
        $this->route_path = $route_path;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourse(): Collection
    {
        return $this->course;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->course->contains($course)) {
            $this->course[] = $course;
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        $this->course->removeElement($course);

        return $this;
    }

    /**
     * @return Collection|BlogPost[]
     */
    public function getBlogPost(): Collection
    {
        return $this->blog_post;
    }

    public function addBlogPost(BlogPost $blogPost): self
    {
        if (!$this->blog_post->contains($blogPost)) {
            $this->blog_post[] = $blogPost;
        }

        return $this;
    }

    public function removeBlogPost(BlogPost $blogPost): self
    {
        $this->blog_post->removeElement($blogPost);

        return $this;
    }

    /**
     * @return Collection|QuizAll[]
     */
    public function getQuizAll(): Collection
    {
        return $this->quiz_all;
    }

    public function addQuizAll(QuizAll $quizAll): self
    {
        if (!$this->quiz_all->contains($quizAll)) {
            $this->quiz_all[] = $quizAll;
        }

        return $this;
    }

    public function removeQuizAll(QuizAll $quizAll): self
    {
        $this->quiz_all->removeElement($quizAll);

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->product->removeElement($product);

        return $this;
    }

    public function getIsMenu(): ?bool
    {
        return $this->is_menu;
    }

    public function setIsMenu(?bool $is_menu): self
    {
        $this->is_menu = $is_menu;

        return $this;
    }
}
