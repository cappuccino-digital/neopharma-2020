<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NeoTvVideoRepository")
 */
class NeoTvVideo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isHighlightHome;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $youtubeId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getYoutubeId(): ?string
    {
        return $this->youtubeId;
    }

    public function setYoutubeId(?string $youtubeId): self
    {
        $this->youtubeId = $youtubeId;

        return $this;
    }

    public function getIsHighlightHome(): ?bool
    {
        return $this->isHighlightHome;
    }

    public function setIsHighlightHome(?bool $isHighlightHome): self
    {
        $this->isHighlightHome = $isHighlightHome;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }
}
