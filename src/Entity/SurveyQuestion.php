<?php

namespace App\Entity;

use App\Repository\SurveyQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurveyQuestionRepository::class)
 */
class SurveyQuestion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=SurveyQuestionResponse::class, mappedBy="question")
     */
    private $surveyQuestionResponses;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $response_yes;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $response_no;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $response_empate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    public function __construct()
    {
        $this->surveyQuestionResponses = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|SurveyQuestionResponse[]
     */
    public function getSurveyQuestionResponses(): Collection
    {
        return $this->surveyQuestionResponses;
    }

    public function addSurveyQuestionResponse(SurveyQuestionResponse $surveyQuestionResponse): self
    {
        if (!$this->surveyQuestionResponses->contains($surveyQuestionResponse)) {
            $this->surveyQuestionResponses[] = $surveyQuestionResponse;
            $surveyQuestionResponse->setQuestion($this);
        }

        return $this;
    }

    public function removeSurveyQuestionResponse(SurveyQuestionResponse $surveyQuestionResponse): self
    {
        if ($this->surveyQuestionResponses->contains($surveyQuestionResponse)) {
            $this->surveyQuestionResponses->removeElement($surveyQuestionResponse);
            // set the owning side to null (unless already changed)
            if ($surveyQuestionResponse->getQuestion() === $this) {
                $surveyQuestionResponse->setQuestion(null);
            }
        }

        return $this;
    }

    public function getResponseYes(): ?string
    {
        return $this->response_yes;
    }

    public function setResponseYes(?string $response_yes): self
    {
        $this->response_yes = $response_yes;

        return $this;
    }

    public function getResponseNo(): ?string
    {
        return $this->response_no;
    }

    public function setResponseNo(?string $response_no): self
    {
        $this->response_no = $response_no;

        return $this;
    }

    public function getResponseEmpate(): ?string
    {
        return $this->response_empate;
    }

    public function setResponseEmpate(?string $response_empate): self
    {
        $this->response_empate = $response_empate;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }
}
