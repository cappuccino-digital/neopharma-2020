<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $manager;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $internalCode;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $presentation;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $fullPresentation;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $sapName;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $refMedicine;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $composition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $barcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activeSubstance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $anvisaRegistration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $indication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contraindication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cautionAndWarning;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dosage;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publicityPieceImpressionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bullaFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bannerMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bannerMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $bannerMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bannerMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $bannerMetaDescription;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $score = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appViews = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $views = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $otherPresentation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToPharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isHomeHighLight;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPsicotropic;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="ProductAsset", mappedBy="product")
     *
     */
    private $assets;

    /**
     * @ORM\OneToMany(targetEntity="ProductScore", mappedBy="product")
     *
     */
    private $scores;

    /**
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="ProductClassifiationByDistribAndPresc")
     * @ORM\JoinColumn(name="classification_id", referencedColumnName="id")
     */
    private $classification;

    /**
     * @ORM\ManyToOne(targetEntity="ProductCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="ProductSistemicClass")
     * @ORM\JoinColumn(name="sistemic_class_id", referencedColumnName="id")
     */
    private $sistemicClass;

    /**
     * @ORM\ManyToMany(targetEntity=SpecialCampaign::class, mappedBy="product")
     */
    private $specialCampaigns;

    public function __construct()
    {
        $this->assets = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->specialCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setManager(?string $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getInternalCode(): ?string
    {
        return $this->internalCode;
    }

    public function setInternalCode(?string $internalCode): self
    {
        $this->internalCode = $internalCode;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getFullPresentation(): ?string
    {
        return $this->fullPresentation;
    }

    public function setFullPresentation(?string $fullPresentation): self
    {
        $this->fullPresentation = $fullPresentation;

        return $this;
    }

    public function getSapName(): ?string
    {
        return $this->sapName;
    }

    public function setSapName(?string $sapName): self
    {
        $this->sapName = $sapName;

        return $this;
    }

    public function getRefMedicine(): ?string
    {
        return $this->refMedicine;
    }

    public function setRefMedicine(?string $refMedicine): self
    {
        $this->refMedicine = $refMedicine;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getActiveSubstance(): ?string
    {
        return $this->activeSubstance;
    }

    public function setActiveSubstance(?string $activeSubstance): self
    {
        $this->activeSubstance = $activeSubstance;

        return $this;
    }

    public function getAnvisaRegistration(): ?string
    {
        return $this->anvisaRegistration;
    }

    public function setAnvisaRegistration(?string $anvisaRegistration): self
    {
        $this->anvisaRegistration = $anvisaRegistration;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getIndication(): ?string
    {
        return $this->indication;
    }

    public function setIndication(?string $indication): self
    {
        $this->indication = $indication;

        return $this;
    }

    public function getContraindication(): ?string
    {
        return $this->contraindication;
    }

    public function setContraindication(?string $contraindication): self
    {
        $this->contraindication = $contraindication;

        return $this;
    }

    public function getCautionAndWarning(): ?string
    {
        return $this->cautionAndWarning;
    }

    public function setCautionAndWarning(?string $cautionAndWarning): self
    {
        $this->cautionAndWarning = $cautionAndWarning;

        return $this;
    }

    public function getDosage(): ?string
    {
        return $this->dosage;
    }

    public function setDosage(?string $dosage): self
    {
        $this->dosage = $dosage;

        return $this;
    }

    public function getPublicityPieceImpressionDate(): ?\DateTimeInterface
    {
        return $this->publicityPieceImpressionDate;
    }

    public function setPublicityPieceImpressionDate(?\DateTimeInterface $publicityPieceImpressionDate): self
    {
        $this->publicityPieceImpressionDate = $publicityPieceImpressionDate;

        return $this;
    }

    public function getBullaFile(): ?string
    {
        return $this->bullaFile;
    }

    public function setBullaFile(?string $bullaFile): self
    {
        $this->bullaFile = $bullaFile;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getBannerMobile(): ?string
    {
        return $this->bannerMobile;
    }

    public function setBannerMobile(?string $bannerMobile): self
    {
        $this->bannerMobile = $bannerMobile;

        return $this;
    }

    public function getBannerMobileMetaTitle(): ?string
    {
        return $this->bannerMobileMetaTitle;
    }

    public function setBannerMobileMetaTitle(?string $bannerMobileMetaTitle): self
    {
        $this->bannerMobileMetaTitle = $bannerMobileMetaTitle;

        return $this;
    }

    public function getBannerMobileMetaDescription(): ?string
    {
        return $this->bannerMobileMetaDescription;
    }

    public function setBannerMobileMetaDescription(?string $bannerMobileMetaDescription): self
    {
        $this->bannerMobileMetaDescription = $bannerMobileMetaDescription;

        return $this;
    }

    public function getBannerMetaTitle(): ?string
    {
        return $this->bannerMetaTitle;
    }

    public function setBannerMetaTitle(?string $bannerMetaTitle): self
    {
        $this->bannerMetaTitle = $bannerMetaTitle;

        return $this;
    }

    public function getBannerMetaDescription(): ?string
    {
        return $this->bannerMetaDescription;
    }

    public function setBannerMetaDescription(?string $bannerMetaDescription): self
    {
        $this->bannerMetaDescription = $bannerMetaDescription;

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(?float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getAppViews(): ?int
    {
        return $this->appViews;
    }

    public function setAppViews(?int $appViews): self
    {
        $this->appViews = $appViews;

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(?int $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getOtherPresentation(): ?string
    {
        return $this->otherPresentation;
    }

    public function setOtherPresentation(?string $otherPresentation): self
    {
        $this->otherPresentation = $otherPresentation;

        return $this;
    }

    public function getOtherPresentationArray(): ?array
    {
        return json_decode($this->otherPresentation, true);
    }

    public function addOtherPresentation($id): ?self
    {

        $otherPresentationArray = json_decode($this->otherPresentation, true);

        if (\is_array($otherPresentationArray)) {
            if (!\in_array($id, $otherPresentationArray)) {
                $otherPresentationArray[] = $id;
            }
        } else {
            $otherPresentationArray = [];
            $otherPresentationArray[] = $id;
        }


        $this->otherPresentation = json_encode($otherPresentationArray);

        return $this;
    }

    public function removeOtherPresentation($id): ?self
    {
        $otherPresentationArray = json_decode($this->otherPresentation, true);

        if (!\in_array($id, $otherPresentationArray)) {
            $key = array_search($id, $otherPresentationArray);
            unset($otherPresentationArray[$key]);
        }

        $this->otherPresentation = json_encode($otherPresentationArray);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsOnlyToPharmaceutical(): ?bool
    {
        return $this->isOnlyToPharmaceutical;
    }

    public function setIsOnlyToPharmaceutical(?bool $isOnlyToPharmaceutical): self
    {
        $this->isOnlyToPharmaceutical = $isOnlyToPharmaceutical;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(?bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getIsHomeHighLight(): ?bool
    {
        return $this->isHomeHighLight;
    }

    public function setIsHomeHighLight(?bool $isHomeHighLight): self
    {
        $this->isHomeHighLight = $isHomeHighLight;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsPsicotropic(): ?bool
    {
        return $this->isPsicotropic;
    }

    public function setIsPsicotropic(?bool $isPsicotropic): self
    {
        $this->isPsicotropic = $isPsicotropic;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|ProductAsset[]
     */
    public function getAssets(): Collection
    {
        return $this->assets;
    }

    public function addAsset(ProductAsset $asset): self
    {
        if (!$this->assets->contains($asset)) {
            $this->assets[] = $asset;
            $asset->setProduct($this);
        }

        return $this;
    }

    public function removeAsset(ProductAsset $asset): self
    {
        if ($this->assets->contains($asset)) {
            $this->assets->removeElement($asset);
            // set the owning side to null (unless already changed)
            if ($asset->getProduct() === $this) {
                $asset->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductScore[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(ProductScore $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setProduct($this);
        }

        return $this;
    }

    public function removeScore(ProductScore $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getProduct() === $this) {
                $score->setProduct(null);
            }
        }

        return $this;
    }

    public function getType(): ?ProductType
    {
        return $this->type;
    }

    public function setType(?ProductType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getClassification(): ?ProductClassifiationByDistribAndPresc
    {
        return $this->classification;
    }

    public function setClassification(?ProductClassifiationByDistribAndPresc $classification): self
    {
        $this->classification = $classification;

        return $this;
    }

    public function getCategory(): ?ProductCategory
    {
        return $this->category;
    }

    public function setCategory(?ProductCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSistemicClass(): ?ProductSistemicClass
    {
        return $this->sistemicClass;
    }

    public function setSistemicClass(?ProductSistemicClass $sistemicClass): self
    {
        $this->sistemicClass = $sistemicClass;

        return $this;
    }

    public function getComposition(): ?string
    {
        return $this->composition;
    }

    public function setComposition(?string $composition): self
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return Collection|SpecialCampaign[]
     */
    public function getSpecialCampaigns(): Collection
    {
        return $this->specialCampaigns;
    }

    public function addSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if (!$this->specialCampaigns->contains($specialCampaign)) {
            $this->specialCampaigns[] = $specialCampaign;
            $specialCampaign->addProduct($this);
        }

        return $this;
    }

    public function removeSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if ($this->specialCampaigns->removeElement($specialCampaign)) {
            $specialCampaign->removeProduct($this);
        }

        return $this;
    }
}
