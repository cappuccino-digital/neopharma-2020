<?php

namespace App\Entity;

use App\Repository\QuizAllAlternativesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuizAllAlternativesRepository::class)
 */
class QuizAllAlternatives
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=QuizAllQuestions::class, inversedBy="quizAllAlternatives")
     */
    private $quiz_all_questions;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_desk;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_alt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_correct;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=QuizAllQuestions::class, mappedBy="quiz_all_alternatives")
     */
    private $quizAllQuestions;

    public function __construct()
    {
        $this->quizAllQuestions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizAllQuestions(): ?QuizAllQuestions
    {
        return $this->quiz_all_questions;
    }

    public function setQuizAllQuestions(?QuizAllQuestions $quiz_all_questions): self
    {
        $this->quiz_all_questions = $quiz_all_questions;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getImageDesk(): ?string
    {
        return $this->image_desk;
    }

    public function setImageDesk(?string $image_desk): self
    {
        $this->image_desk = $image_desk;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->image_mobile;
    }

    public function setImageMobile(?string $image_mobile): self
    {
        $this->image_mobile = $image_mobile;

        return $this;
    }

    public function getImageAlt(): ?string
    {
        return $this->image_alt;
    }

    public function setImageAlt(?string $image_alt): self
    {
        $this->image_alt = $image_alt;

        return $this;
    }

    public function getImageDescription(): ?string
    {
        return $this->image_description;
    }

    public function setImageDescription(?string $image_description): self
    {
        $this->image_description = $image_description;

        return $this;
    }

    public function getIsCorrect(): ?bool
    {
        return $this->is_correct;
    }

    public function setIsCorrect(?bool $is_correct): self
    {
        $this->is_correct = $is_correct;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function addQuizAllQuestion(QuizAllQuestions $quizAllQuestion): self
    {
        if (!$this->quizAllQuestions->contains($quizAllQuestion)) {
            $this->quizAllQuestions[] = $quizAllQuestion;
            $quizAllQuestion->setQuizAllAlternatives($this);
        }

        return $this;
    }

    public function removeQuizAllQuestion(QuizAllQuestions $quizAllQuestion): self
    {
        if ($this->quizAllQuestions->removeElement($quizAllQuestion)) {
            // set the owning side to null (unless already changed)
            if ($quizAllQuestion->getQuizAllAlternatives() === $this) {
                $quizAllQuestion->setQuizAllAlternatives(null);
            }
        }

        return $this;
    }
}
