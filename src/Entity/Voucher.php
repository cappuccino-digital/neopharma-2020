<?php

namespace App\Entity;

use App\Repository\VoucherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoucherRepository::class)
 */
class Voucher
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ordered;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $card_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $length;

    /**
     * @ORM\OneToMany(targetEntity=UserVoucherUber::class, mappedBy="voucher")
     */
    private $userVoucherUbers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    public function __construct()
    {
        $this->userVoucherUbers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrdered(): ?string
    {
        return $this->ordered;
    }

    public function setOrdered(?string $ordered): self
    {
        $this->ordered = $ordered;

        return $this;
    }

    public function getCardType(): ?string
    {
        return $this->card_type;
    }

    public function setCardType(?string $card_type): self
    {
        $this->card_type = $card_type;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(?string $length): self
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return Collection|UserVoucherUber[]
     */
    public function getUserVoucherUbers(): Collection
    {
        return $this->userVoucherUbers;
    }

    public function addUserVoucherUber(UserVoucherUber $userVoucherUber): self
    {
        if (!$this->userVoucherUbers->contains($userVoucherUber)) {
            $this->userVoucherUbers[] = $userVoucherUber;
            $userVoucherUber->setVoucher($this);
        }

        return $this;
    }

    public function removeUserVoucherUber(UserVoucherUber $userVoucherUber): self
    {
        if ($this->userVoucherUbers->removeElement($userVoucherUber)) {
            // set the owning side to null (unless already changed)
            if ($userVoucherUber->getVoucher() === $this) {
                $userVoucherUber->setVoucher(null);
            }
        }

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }
}
