<?php

namespace App\Entity;

use App\Repository\MedalsGamificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MedalsGamificationRepository::class)
 */
class MedalsGamification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon_not;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_campaign;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=UserMedalsGamification::class, mappedBy="medal")
     */
    private $userMedalsGamifications;

    /**
     * @ORM\OneToOne(targetEntity=SpecialCampaign::class, mappedBy="medals", cascade={"persist", "remove"})
     */
    private $specialCampaign;

    public function __construct()
    {
        $this->medalsGamifications = new ArrayCollection();
        $this->userMedalsGamifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getIconNot(): ?string
    {
        return $this->icon_not;
    }

    public function setIconNot(?string $icon_not): self
    {
        $this->icon_not = $icon_not;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsCampaign(): ?bool
    {
        return $this->is_campaign;
    }

    public function setIsCampaign(?bool $is_campaign): self
    {
        $this->is_campaign = $is_campaign;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|UserMedalsGamification[]
     */
    public function getUserMedalsGamifications(): Collection
    {
        return $this->userMedalsGamifications;
    }

    public function addUserMedalsGamification(UserMedalsGamification $userMedalsGamification): self
    {
        if (!$this->userMedalsGamifications->contains($userMedalsGamification)) {
            $this->userMedalsGamifications[] = $userMedalsGamification;
            $userMedalsGamification->setMedal($this);
        }

        return $this;
    }

    public function removeUserMedalsGamification(UserMedalsGamification $userMedalsGamification): self
    {
        if ($this->userMedalsGamifications->removeElement($userMedalsGamification)) {
            // set the owning side to null (unless already changed)
            if ($userMedalsGamification->getMedal() === $this) {
                $userMedalsGamification->setMedal(null);
            }
        }

        return $this;
    }

    public function getSpecialCampaign(): ?SpecialCampaign
    {
        return $this->specialCampaign;
    }

    public function setSpecialCampaign(?SpecialCampaign $specialCampaign): self
    {
        // unset the owning side of the relation if necessary
        if ($specialCampaign === null && $this->specialCampaign !== null) {
            $this->specialCampaign->setMedals(null);
        }

        // set the owning side of the relation if necessary
        if ($specialCampaign !== null && $specialCampaign->getMedals() !== $this) {
            $specialCampaign->setMedals($this);
        }

        $this->specialCampaign = $specialCampaign;

        return $this;
    }
}
