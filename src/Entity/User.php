<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(name="is_active", type="boolean" ,nullable=true)
     */
    private $isActive = false;

    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $isFacebookConnected = false;

    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $isGoogleConnected = false;
    
    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $isRegisterComplete = false;

    /**
     * @ORM\Column(name="is_pharmaceutical", type="boolean" ,nullable=true)
     */
    private $isPharmaceutical = false;
    
    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $newsletter = false;

    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $promotion = false;

    /**
     * @ORM\Column(type="boolean" ,nullable=true)
     */
    private $smsNotification = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $token_api_excelencia;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="UserFavorite", mappedBy="user")
     */
    private $favorites;

    /**
     * @ORM\OneToOne(targetEntity="UserInvite")
     *
     */
    private $invite;


     /**
     * @ORM\OneToOne(targetEntity="UserNewPassword")
     *
     */
    private $newPasswordToken;

    /**
     * @ORM\OneToMany(targetEntity="UserPoint", mappedBy="user")
     *
     */
    private $points;

    /**
     * @ORM\OneToMany(targetEntity="UserCourse", mappedBy="user")
     *
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="UserBadge", mappedBy="user")
     *
     */
    private $badges;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $crf;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $cnpj;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $shareCode;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $cellphone;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $ddd_cellphone;

    /**
     * @ORM\Column(type="string", length=1024,  nullable=true)
     */
    private $interests;

    /**
     * @ORM\Column(type="string", length=1024,  nullable=true)
     */
    private $educationalLevel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $appAccessToken;


    /**
     * @ORM\OneToOne(targetEntity="UserRacine")
     *
     */
    private $racineData;


    /**
     * @ORM\OneToOne(targetEntity="UserFacebookData")
     *
     */
    private $facebookData;

    /**
     * @ORM\OneToOne(targetEntity="UserGoogleData")
     *
     */
    private $googleData;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPharmacy;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $codExcelencia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stateRegistration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fantasyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $neighborhood;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referencePoint;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRegisterCompleteExcelencia;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNotificationWhatsapp;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSendEmail;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPromoThirtyDayMovement", mappedBy="user")
     */
    private $userPromoThirtyDayMovements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPointPromoThirtyDayMovement", mappedBy="user")
     */
    private $userPointPromoThirtyDayMovements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromoThirtyDayMovementUserMovie", mappedBy="User")
     */
    private $promoThirtyDayMovementUserMovies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromoThirtyDayPostInstragramPoint", mappedBy="user")
     */
    private $promoThirtyDayPostInstragramPoints;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrientMe", mappedBy="user")
     */
    private $orientMes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRegisterIncomplete", mappedBy="UserRegisterIncomplete")
     */
    private $userRegistersIncomplete;


    /**
     * @ORM\OneToMany(targetEntity=SurveyQuestionResponse::class, mappedBy="user")
     */
    private $surveyQuestionResponses;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estate_address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_regulation = false;

    /**
     * @ORM\OneToMany(targetEntity=UserStoreManagerResponsibleChange::class, mappedBy="user")
     */
    private $userStoreManagerResponsibleChanges;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $team_football;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_test = false;

    /**
     * @ORM\OneToMany(targetEntity=UserLogin::class, mappedBy="user")
     */
    private $userLogins;

    /**
     * @ORM\OneToMany(targetEntity=UserPreferential::class, mappedBy="user_id")
     */
    private $userPreferentials;

    /**
     * @ORM\OneToMany(targetEntity=UserEmblue::class, mappedBy="user")
     */
    private $userEmblues;

    /**
     * @ORM\OneToMany(targetEntity=UserInactivity::class, mappedBy="user")
     */
    private $userInactivities;

    /**
     * @ORM\OneToMany(targetEntity=UserCongressPharmaceutical::class, mappedBy="user")
     */
    private $userCongressPharmaceuticals;

    /**
     * @ORM\OneToMany(targetEntity=UserTicketsCorinthians::class, mappedBy="user")
     */
    private $userTicketsCorinthians;

    /**
     * @ORM\OneToMany(targetEntity=UserCorinthiansAlwaysReady::class, mappedBy="user")
     */
    private $userCorinthiansAlwaysReady;

    /**
     * @ORM\OneToMany(targetEntity=UserCorinthiansFluminense::class, mappedBy="user")
     */
    private $userCorinthiansFluminense;

    /**
     * @ORM\OneToMany(targetEntity=UserVoucherUber::class, mappedBy="user")
     */
    private $userVoucherUber;

    /**
     * @ORM\OneToMany(targetEntity=UserCourseEmblue::class, mappedBy="user")
     */
    private $userCourseEmblues;

    /**

     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_admin_client;
    /**
     * @ORM\OneToMany(targetEntity=UserPointGamification::class, mappedBy="user")
     */
    private $userPointGamifications;

    /**
     * @ORM\OneToMany(targetEntity=UserPointTotalGamification::class, mappedBy="user")
     */
    private $userPointTotalGamifications;

    /**
     * @ORM\OneToMany(targetEntity=UserMedalsGamification::class, mappedBy="user")
     */
    private $userMedalsGamifications;

    /**
     * @ORM\OneToMany(targetEntity=UserLevelGamification::class, mappedBy="user")
     */
    private $userLevelGamifications;

    /**
     * @ORM\OneToMany(targetEntity=SatisfactionArena::class, mappedBy="user")
     */
    private $satisfactionArenas;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct() {
        $this->roles = new ArrayCollection();
        $this->favoritePosts = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->points = new ArrayCollection();
        $this->courses = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->badges = new ArrayCollection();
        $this->userPromoThirtyDayMovements = new ArrayCollection();
        $this->userPointPromoThirtyDayMovements = new ArrayCollection();
        $this->promoThirtyDayMovementUserMovies = new ArrayCollection();
        $this->promoThirtyDayPostInstragramPoints = new ArrayCollection();
        $this->orientMes = new ArrayCollection();
        $this->userRegistersIncomplete = new ArrayCollection();
        $this->surveyQuestionResponses = new ArrayCollection();
        $this->userStoreManagerResponsibleChanges = new ArrayCollection();
        $this->userLogins = new ArrayCollection();
        $this->userPreferentials = new ArrayCollection();
        $this->userEmblues = new ArrayCollection();
        $this->userInactivities = new ArrayCollection();
        $this->userCongressPharmaceuticals = new ArrayCollection();
        $this->userTicketsCorinthians = new ArrayCollection();
        $this->userCorinthiansAlwaysReady = new ArrayCollection();
        $this->userCorinthiansFluminense = new ArrayCollection();
        $this->userVoucherUber = new ArrayCollection();
        $this->userCourseEmblues = new ArrayCollection();
        $this->userPointGamifications = new ArrayCollection();
        $this->userPointTotalGamifications = new ArrayCollection();
        $this->userMedalsGamifications = new ArrayCollection();
        $this->userLevelGamifications = new ArrayCollection();
        $this->satisfactionArenas = new ArrayCollection();
    }

    public function eraseCredentials() {
        
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @return Collection|Role[]
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @return Array|Role[]
     */
    public function getRolesArray(): Array
    {
        $roles = [];
        
        foreach($this->roles as $role){
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    
    public function getIsPharmacy(): ?bool
    {
        return $this->isPharmacy;
    }

    public function setIsPharmacy(?bool $isPharmacy): self
    {
        $this->isPharmacy = $isPharmacy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->lastLoginAt;
    }

    public function setLastLoginAt(?\DateTimeInterface $lastLoginAt): self
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    /**
     * @return Collection|BlogPost[]
     */
    public function getFavoritePosts(): Collection
    {
        return $this->favoritePosts;
    }

    public function addFavoritePost(BlogPost $favoritePost): self
    {
        if (!$this->favoritePosts->contains($favoritePost)) {
            $this->favoritePosts[] = $favoritePost;
        }

        return $this;
    }

    public function removeFavoritePost(BlogPost $favoritePost): self
    {
        if ($this->favoritePosts->contains($favoritePost)) {
            $this->favoritePosts->removeElement($favoritePost);
        }

        return $this;
    }

    /**
     * @return Collection|BlogPostScore[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(BlogPostScore $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setUser($this);
        }

        return $this;
    }

    public function removeScore(BlogPostScore $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getUser() === $this) {
                $score->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPoint[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    public function addPoint(UserPoint $point): self
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setUser($this);
        }

        return $this;
    }

    public function removePoint(UserPoint $point): self
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            // set the owning side to null (unless already changed)
            if ($point->getUser() === $this) {
                $point->setUser(null);
            }
        }

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getCrf(): ?string
    {
        return $this->crf;
    }

    public function setCrf(?string $crf): self
    {
        $this->crf = $crf;

        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(?string $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAppAccessToken(): ?string
    {
        return $this->appAccessToken;
    }

    public function setAppAccessToken(?string $appAccessToken): self
    {
        $this->appAccessToken = $appAccessToken;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

 
    public function getCodExcelencia(): ?string
    {
        return $this->codExcelencia;
    }

    public function setCodExcelencia(?string $codExcelencia): self
    {
        $this->codExcelencia = $codExcelencia;

        return $this;
    }


    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

   
    public function getTokenApiExcelencia(): ?string
    {
        return $this->token_api_excelencia;
    }

    public function setTokenApiExcelencia(?string $token_api_excelencia): self
    {
        $this->token_api_excelencia = $token_api_excelencia;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->isPharmaceutical;
    }

    public function setIsPharmaceutical(?bool $isPharmaceutical): self
    {
        $this->isPharmaceutical = $isPharmaceutical;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection|UserCourse[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(UserCourse $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setUser($this);
        }

        return $this;
    }

    public function removeCourse(UserCourse $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getUser() === $this) {
                $course->setUser(null);
            }
        }

        return $this;
    }

    public function getRacineData(): ?UserRacine
    {
        return $this->racineData;
    }

    public function setRacineData(?UserRacine $racineData): self
    {
        $this->racineData = $racineData;

        return $this;
    }

    /**
     * @return Collection|UserFavorite[]
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    public function addFavorite(UserFavorite $favorite): self
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites[] = $favorite;
            $favorite->setUser($this);
        }

        return $this;
    }

    public function removeFavorite(UserFavorite $favorite): self
    {
        if ($this->favorites->contains($favorite)) {
            $this->favorites->removeElement($favorite);
            // set the owning side to null (unless already changed)
            if ($favorite->getUser() === $this) {
                $favorite->setUser(null);
            }
        }

        return $this;
    }

    public function getIsFacebookConnected(): ?bool
    {
        return $this->isFacebookConnected;
    }

    public function setIsFacebookConnected(?bool $isFacebookConnected): self
    {
        $this->isFacebookConnected = $isFacebookConnected;

        return $this;
    }

    public function getIsGoogleConnected(): ?bool
    {
        return $this->isGoogleConnected;
    }

    public function setIsGoogleConnected(?bool $isGoogleConnected): self
    {
        $this->isGoogleConnected = $isGoogleConnected;

        return $this;
    }

    public function getFacebookData(): ?UserFacebookData
    {
        return $this->facebookData;
    }

    public function setFacebookData(?UserFacebookData $facebookData): self
    {
        $this->facebookData = $facebookData;

        return $this;
    }

    public function getGoogleData(): ?UserGoogleData
    {
        return $this->googleData;
    }

    public function setGoogleData(?UserGoogleData $googleData): self
    {
        $this->googleData = $googleData;

        return $this;
    }

    public function getIsRegisterComplete(): ?bool
    {
        return $this->isRegisterComplete;
    }

    public function setIsRegisterComplete(?bool $isRegisterComplete): self
    {
        $this->isRegisterComplete = $isRegisterComplete;

        return $this;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(?bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getDddCellphone(): ?string
    {
        return $this->ddd_cellphone;
    }

    public function setDddCellphone(?string $ddd_cellphone): self
    {
        $this->ddd_cellphone = $ddd_cellphone;

        return $this;
    }

    public function getInterestsArray(): ?array
    {
        return json_decode($this->interests, true);
    }

    public function getInterests(): ?string
    {
        return $this->interests;
    }

    public function setInterests(?string $interests): self
    {
        $this->interests = $interests;

        return $this;
    }

    public function getEducationalLevel(): ?string
    {
        return $this->educationalLevel;
    }

    public function setEducationalLevel(?string $educationalLevel): self
    {
        $this->educationalLevel = $educationalLevel;

        return $this;
    }

    public function getPromotion(): ?bool
    {
        return $this->promotion;
    }

    public function setPromotion(?bool $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }

    public function getSmsNotification(): ?bool
    {
        return $this->smsNotification;
    }

    public function setSmsNotification(?bool $smsNotification): self
    {
        $this->smsNotification = $smsNotification;

        return $this;
    }

    public function getIsNotificationWhatsapp(): ?bool
    {
        return $this->isNotificationWhatsapp;
    }

    public function setIsNotificationWhatsapp(?bool $isNotificationWhatsapp): self
    {
        $this->isNotificationWhatsapp = $isNotificationWhatsapp;

        return $this;
    }

    public function getIsSendEmail(): ?bool
    {
        return $this->isSendEmail;
    }

    public function setIsSendEmail(?bool $isSendEmail): self
    {
        $this->isSendEmail = $isSendEmail;

        return $this;
    }

    public function getShareCode(): ?string
    {
        return $this->shareCode;
    }

    public function setShareCode(?string $shareCode): self
    {
        $this->shareCode = $shareCode;

        return $this;
    }

    public function getInvite(): ?UserInvite
    {
        return $this->invite;
    }

    public function setInvite(?UserInvite $invite): self
    {
        $this->invite = $invite;

        return $this;
    }

    /* News Fields */

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getStateRegistration(): ?string
    {
        return $this->stateRegistration;
    }

    public function setStateRegistration(?string $stateRegistration): self
    {
        $this->stateRegistration = $stateRegistration;

        return $this;
    }


    public function getFantasyName(): ?string
    {
        return $this->fantasyName;
    }

    public function setFantasyName(?string $fantasyName): self
    {
        $this->fantasyName = $fantasyName;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getReferencePoint(): ?string
    {
        return $this->referencePoint;
    }

    public function setReferencePoint(?string $referencePoint): self
    {
        $this->referencePoint = $referencePoint;

        return $this;
    }
    
    public function getIsRegisterCompleteExcelencia(): ?bool
    {
        return $this->isRegisterCompleteExcelencia;
    }

    public function setIsRegisterCompleteExcelencia(?bool $isRegisterCompleteExcelencia): self
    {
        $this->isRegisterCompleteExcelencia = $isRegisterCompleteExcelencia;

        return $this;
    }
    /** end */

    /**
     * @return Collection|UserBadge[]
     */
    public function getBadges(): Collection
    {
        return $this->badges;
    }

    public function addBadge(UserBadge $badge): self
    {
        if (!$this->badges->contains($badge)) {
            $this->badges[] = $badge;
            $badge->setUser($this);
        }

        return $this;
    }

    public function removeBadge(UserBadge $badge): self
    {
        if ($this->badges->contains($badge)) {
            $this->badges->removeElement($badge);
            // set the owning side to null (unless already changed)
            if ($badge->getUser() === $this) {
                $badge->setUser(null);
            }
        }

        return $this;
    }

    public function getNewPasswordToken(): ?UserNewPassword
    {
        return $this->newPasswordToken;
    }

    public function setNewPasswordToken(?UserNewPassword $newPasswordToken): self
    {
        $this->newPasswordToken = $newPasswordToken;

        return $this;
    }

    /**
     * @return Collection|UserPromoThirtyDayMovement[]
     */
    public function getUserPromoThirtyDayMovements(): Collection
    {
        return $this->userPromoThirtyDayMovements;
    }

    public function addUserPromoThirtyDayMovement(UserPromoThirtyDayMovement $userPromoThirtyDayMovement): self
    {
        if (!$this->userPromoThirtyDayMovements->contains($userPromoThirtyDayMovement)) {
            $this->userPromoThirtyDayMovements[] = $userPromoThirtyDayMovement;
            $userPromoThirtyDayMovement->setUser($this);
        }

        return $this;
    }

    public function removeUserPromoThirtyDayMovement(UserPromoThirtyDayMovement $userPromoThirtyDayMovement): self
    {
        if ($this->userPromoThirtyDayMovements->contains($userPromoThirtyDayMovement)) {
            $this->userPromoThirtyDayMovements->removeElement($userPromoThirtyDayMovement);
            // set the owning side to null (unless already changed)
            if ($userPromoThirtyDayMovement->getUser() === $this) {
                $userPromoThirtyDayMovement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPointPromoThirtyDayMovement[]
     */
    public function getUserPointPromoThirtyDayMovements(): Collection
    {
        return $this->userPointPromoThirtyDayMovements;
    }

    public function addUserPointPromoThirtyDayMovement(UserPointPromoThirtyDayMovement $userPointPromoThirtyDayMovement): self
    {
        if (!$this->userPointPromoThirtyDayMovements->contains($userPointPromoThirtyDayMovement)) {
            $this->userPointPromoThirtyDayMovements[] = $userPointPromoThirtyDayMovement;
            $userPointPromoThirtyDayMovement->setUser($this);
        }

        return $this;
    }

    public function removeUserPointPromoThirtyDayMovement(UserPointPromoThirtyDayMovement $userPointPromoThirtyDayMovement): self
    {
        if ($this->userPointPromoThirtyDayMovements->contains($userPointPromoThirtyDayMovement)) {
            $this->userPointPromoThirtyDayMovements->removeElement($userPointPromoThirtyDayMovement);
            // set the owning side to null (unless already changed)
            if ($userPointPromoThirtyDayMovement->getUser() === $this) {
                $userPointPromoThirtyDayMovement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PromoThirtyDayMovementUserMovie[]
     */
    public function getPromoThirtyDayMovementUserMovies(): Collection
    {
        return $this->promoThirtyDayMovementUserMovies;
    }

    public function addPromoThirtyDayMovementUserMovie(PromoThirtyDayMovementUserMovie $promoThirtyDayMovementUserMovie): self
    {
        if (!$this->promoThirtyDayMovementUserMovies->contains($promoThirtyDayMovementUserMovie)) {
            $this->promoThirtyDayMovementUserMovies[] = $promoThirtyDayMovementUserMovie;
            $promoThirtyDayMovementUserMovie->setUser($this);
        }

        return $this;
    }

    public function removePromoThirtyDayMovementUserMovie(PromoThirtyDayMovementUserMovie $promoThirtyDayMovementUserMovie): self
    {
        if ($this->promoThirtyDayMovementUserMovies->contains($promoThirtyDayMovementUserMovie)) {
            $this->promoThirtyDayMovementUserMovies->removeElement($promoThirtyDayMovementUserMovie);
            // set the owning side to null (unless already changed)
            if ($promoThirtyDayMovementUserMovie->getUser() === $this) {
                $promoThirtyDayMovementUserMovie->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PromoThirtyDayPostInstragramPoint[]
     */
    public function getPromoThirtyDayPostInstragramPoint(): Collection
    {
        return $this->promoThirtyDayPostInstragramPoints;
    }

    /**
     * @return Collection|OrientMe[]
     */
    public function getOrientMes(): Collection
    {
        return $this->orientMes;
    }

    public function addOrientMe(OrientMe $orientMe): self
    {
        if (!$this->orientMes->contains($orientMe)) {
            $this->orientMes[] = $orientMe;
            $orientMe->setUser($this);
        }

        return $this;
    }

    public function removeOrientMe(OrientMe $orientMe): self
    {
        if ($this->orientMes->contains($orientMe)) {
            $this->orientMes->removeElement($orientMe);
            // set the owning side to null (unless already changed)
            if ($orientMe->getUser() === $this) {
                $orientMe->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRegisterIncomplete[]
     */
    public function getUserRegistersIncomplete(): Collection
    {
        return $this->userRegistersIncomplete;
    }

    public function addUserRegisterIncomplete(UserRegisterIncomplete $userRegistersIncomplete): self
    {
        if (!$this->userRegistersIncomplete->contains($userRegistersIncomplete)) {
            $this->userRegistersIncomplete[] = $userRegistersIncomplete;
            $userRegistersIncomplete->setUserRegisterIncomplete($this);
        }

        return $this;
    }

    public function removeUserRegisterIncomplete(UserRegisterIncomplete $userRegistersIncomplete): self
    {
        if ($this->userRegistersIncomplete->contains($userRegistersIncomplete)) {
            $this->userRegistersIncomplete->removeElement($userRegistersIncomplete);
            // set the owning side to null (unless already changed)
            if ($userRegistersIncomplete->getUserRegisterIncomplete() === $this) {
                $userRegistersIncomplete->setUserRegisterIncomplete(null);
            }
        }

        return $this;
    }

 
    /**
     * @return Collection|SurveyQuestionResponse[]
     */
    public function getSurveyQuestionResponses(): Collection
    {
        return $this->surveyQuestionResponses;
    }

    public function addSurveyQuestionResponse(SurveyQuestionResponse $surveyQuestionResponse): self
    {
        if (!$this->surveyQuestionResponses->contains($surveyQuestionResponse)) {
            $this->surveyQuestionResponses[] = $surveyQuestionResponse;
            $surveyQuestionResponse->setUser($this);
        }

        return $this;
    }

    public function removeSurveyQuestionResponse(SurveyQuestionResponse $surveyQuestionResponse): self
    {
        if ($this->surveyQuestionResponses->contains($surveyQuestionResponse)) {
            $this->surveyQuestionResponses->removeElement($surveyQuestionResponse);
            // set the owning side to null (unless already changed)
            if ($surveyQuestionResponse->getUser() === $this) {
                $surveyQuestionResponse->setUser(null);
            }
        }

        return $this;
    }

    public function getEstateAddress(): ?string
    {
        return $this->estate_address;
    }

    public function setEstateAddress(?string $estate_address): self
    {
        $this->estate_address = $estate_address;

        return $this;
    }

    public function getIsRegulation(): ?bool
    {
        return $this->is_regulation;
    }

    public function setIsRegulation(?bool $is_regulation): self
    {
        $this->is_regulation = $is_regulation;

        return $this;
    }


    /**
     * @return Collection|UserStoreManagerResponsibleChange[]
     */
    public function getUserStoreManagerResponsibleChanges(): Collection
    {
        return $this->userStoreManagerResponsibleChanges;
    }

    public function addUserStoreManagerResponsibleChange(UserStoreManagerResponsibleChange $userStoreManagerResponsibleChange): self
    {
        if (!$this->userStoreManagerResponsibleChanges->contains($userStoreManagerResponsibleChange)) {
            $this->userStoreManagerResponsibleChanges[] = $userStoreManagerResponsibleChange;
            $userStoreManagerResponsibleChange->setUser($this);
        }

        return $this;
    }

    public function removeUserStoreManagerResponsibleChange(UserStoreManagerResponsibleChange $userStoreManagerResponsibleChange): self
    {
        if ($this->userStoreManagerResponsibleChanges->removeElement($userStoreManagerResponsibleChange)) {
            // set the owning side to null (unless already changed)
            if ($userStoreManagerResponsibleChange->getUser() === $this) {
                $userStoreManagerResponsibleChange->setUser(null);
            }
        }

        return $this;
    }

    public function getTeamFootball(): ?string
    {
        return $this->team_football;
    }

    public function setTeamFootball(string $team_football): self
    {
        $this->team_football = $team_football;

        return $this;
    }

    public function getIsTest(): ?bool
    {
        return $this->is_test;
    }

    public function setIsTest(?bool $is_test): self
    {
        $this->is_test = $is_test;

        return $this;
    }

    /**
     * @return Collection|UserLogin[]
     */
    public function getUserLogins(): Collection
    {
        return $this->userLogins;
    }

    public function addUserLogin(UserLogin $userLogin): self
    {
        if (!$this->userLogins->contains($userLogin)) {
            $this->userLogins[] = $userLogin;
            $userLogin->setUser($this);
        }

        return $this;
    }

    public function removeUserLogin(UserLogin $userLogin): self
    {
        if ($this->userLogins->removeElement($userLogin)) {
            // set the owning side to null (unless already changed)
            if ($userLogin->getUser() === $this) {
                $userLogin->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPreferential[]
     */
    public function getUserPreferentials(): Collection
    {
        return $this->userPreferentials;
    }

    public function addUserPreferential(UserPreferential $userPreferential): self
    {
        if (!$this->userPreferentials->contains($userPreferential)) {
            $this->userPreferentials[] = $userPreferential;
            $userPreferential->setUser($this);
        }

        return $this;
    }

    public function removeUserPreferential(UserPreferential $userPreferential): self
    {
        if ($this->userPreferentials->removeElement($userPreferential)) {
            // set the owning side to null (unless already changed)
            if ($userPreferential->getUser() === $this) {
                $userPreferential->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserEmblue[]
     */
    public function getUserEmblues(): Collection
    {
        return $this->userEmblues;
    }

    public function addUserEmblue(UserEmblue $userEmblue): self
    {
        if (!$this->userEmblues->contains($userEmblue)) {
            $this->userEmblues[] = $userEmblue;
            $userEmblue->setUser($this);
        }

        return $this;
    }

    public function removeUserEmblue(UserEmblue $userEmblue): self
    {
        if ($this->userEmblues->removeElement($userEmblue)) {
            // set the owning side to null (unless already changed)
            if ($userEmblue->getUser() === $this) {
                $userEmblue->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserInactivity[]
     */
    public function getUserInactivities(): Collection
    {
        return $this->userInactivities;
    }

    public function addUserInactivity(UserInactivity $userInactivity): self
    {
        if (!$this->userInactivities->contains($userInactivity)) {
            $this->userInactivities[] = $userInactivity;
            $userInactivity->setUser($this);
        }

        return $this;
    }

    public function removeUserInactivity(UserInactivity $userInactivity): self
    {
        if ($this->userInactivities->removeElement($userInactivity)) {
            // set the owning side to null (unless already changed)
            if ($userInactivity->getUser() === $this) {
                $userInactivity->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCongressPharmaceutical[]
     */
    public function getUserCongressPharmaceuticals(): Collection
    {
        return $this->userCongressPharmaceuticals;
    }

    public function addUserCongressPharmaceutical(UserCongressPharmaceutical $userCongressPharmaceutical): self
    {
        if (!$this->userCongressPharmaceuticals->contains($userCongressPharmaceutical)) {
            $this->userCongressPharmaceuticals[] = $userCongressPharmaceutical;
            $userCongressPharmaceutical->setUser($this);
        }

        return $this;
    }

    public function removeUserCongressPharmaceutical(UserCongressPharmaceutical $userCongressPharmaceutical): self
    {
        if ($this->userCongressPharmaceuticals->removeElement($userCongressPharmaceutical)) {
            // set the owning side to null (unless already changed)
            if ($userCongressPharmaceutical->getUser() === $this) {
                $userCongressPharmaceutical->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserTicketsCorinthians[]
     */
    public function getUserTicketsCorinthians(): Collection
    {
        return $this->userTicketsCorinthians;
    }

    public function addUserTicketsCorinthian(UserTicketsCorinthians $userTicketsCorinthian): self
    {
        if (!$this->userTicketsCorinthians->contains($userTicketsCorinthian)) {
            $this->userTicketsCorinthians[] = $userTicketsCorinthian;
            $userTicketsCorinthian->setUser($this);
        }

        return $this;
    }

    public function removeUserTicketsCorinthian(UserTicketsCorinthians $userTicketsCorinthian): self
    {
        if ($this->userTicketsCorinthians->removeElement($userTicketsCorinthian)) {
            // set the owning side to null (unless already changed)
            if ($userTicketsCorinthian->getUser() === $this) {
                $userTicketsCorinthian->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCourseEmblue[]
     */
    public function getUserCourseEmblues(): Collection
    {
        return $this->userCourseEmblues;
    }

    public function addUserCourseEmblue(UserCourseEmblue $userCourseEmblue): self
    {
        if (!$this->userCourseEmblues->contains($userCourseEmblue)) {
            $this->userCourseEmblues[] = $userCourseEmblue;
            $userCourseEmblue->setUser($this);
        }

        return $this;
    }

    public function removeUserCourseEmblue(UserCourseEmblue $userCourseEmblue): self
    {
        if ($this->userCourseEmblues->removeElement($userCourseEmblue)) {
            // set the owning side to null (unless already changed)
            if ($userCourseEmblue->getUser() === $this) {
                $userCourseEmblue->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCorinthiansAlwaysReady[]
     */
    public function getUserCorinthiansAlwaysReady(): Collection
    {
        return $this->userCorinthiansAlwaysReady;
    }

    public function addUserCorinthiansAlwaysReady(UserCorinthiansAlwaysReady $userCorinthiansAlwaysReady): self
    {
        if (!$this->userCorinthiansAlwaysReady->contains($userCorinthiansAlwaysReady)) {
            $this->userCorinthiansAlwaysReady[] = $userCorinthiansAlwaysReady;
            $userCorinthiansAlwaysReady->setUser($this);
        }

        return $this;
    }

    public function removeUserCorinthiansAlwaysReady(UserCorinthiansAlwaysReady $userCorinthiansAlwaysReady): self
    {
        if ($this->userCorinthiansAlwaysReady->removeElement($userCorinthiansAlwaysReady)) {
            // set the owning side to null (unless already changed)
            if ($userCorinthiansAlwaysReady->getUser() === $this) {
                $userCorinthiansAlwaysReady->setUser(null);
            }
        }

        return $this;
    }

    public function getIsAdminClient(): ?bool
    {
        return $this->is_admin_client;
    }

    public function setIsAdminClient(?bool $is_admin_client): self
    {
        $this->is_admin_client = $is_admin_client;

        return $this;
    }

    /**
     * @return Collection|UserCorinthiansFluminense[]
     */
    public function getUserCorinthiansFluminense(): Collection
    {
        return $this->userCorinthiansFluminense;
    }

    public function addUserCorinthiansFluminense(UserCorinthiansFluminense $userCorinthiansFluminense): self
    {
        if (!$this->userCorinthiansFluminense->contains($userCorinthiansFluminense)) {
            $this->userCorinthiansFluminense[] = $userCorinthiansFluminense;
            $userCorinthiansFluminense->setUser($this);
        }

        return $this;
    }

    public function removeUserCorinthiansFluminense(UserCorinthiansFluminense $userCorinthiansFluminense): self
    {
        if ($this->userCorinthiansFluminense->removeElement($userCorinthiansFluminense)) {
            // set the owning side to null (unless already changed)
            if ($userCorinthiansFluminense->getUser() === $this) {
                $userCorinthiansFluminense->setUser(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|UserPointGamification[]
     */
    public function getUserPointGamifications(): Collection
    {
        return $this->userPointGamifications;
    }

    public function addUserPointGamification(UserPointGamification $userPointGamification): self
    {
        if (!$this->userPointGamifications->contains($userPointGamification)) {
            $this->userPointGamifications[] = $userPointGamification;
            $userPointGamification->setUser($this);
        }

        return $this;
    }

    public function removeUserPointGamification(UserPointGamification $userPointGamification): self
    {
        if ($this->userPointGamifications->removeElement($userPointGamification)) {
            // set the owning side to null (unless already changed)
            if ($userPointGamification->getUser() === $this) {
                $userPointGamification->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPointTotalGamification[]
     */
    public function getUserPointTotalGamifications(): Collection
    {
        return $this->userPointTotalGamifications;
    }

    public function addUserPointTotalGamification(UserPointTotalGamification $userPointTotalGamification): self
    {
        if (!$this->userPointTotalGamifications->contains($userPointTotalGamification)) {
            $this->userPointTotalGamifications[] = $userPointTotalGamification;
            $userPointTotalGamification->setUser($this);
        }

        return $this;
    }

    public function removeUserPointTotalGamification(UserPointTotalGamification $userPointTotalGamification): self
    {
        if ($this->userPointTotalGamifications->removeElement($userPointTotalGamification)) {
            // set the owning side to null (unless already changed)
            if ($userPointTotalGamification->getUser() === $this) {
                $userPointTotalGamification->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserMedalsGamification[]
     */
    public function getUserMedalsGamifications(): Collection
    {
        return $this->userMedalsGamifications;
    }

    public function addUserMedalsGamification(UserMedalsGamification $userMedalsGamification): self
    {
        if (!$this->userMedalsGamifications->contains($userMedalsGamification)) {
            $this->userMedalsGamifications[] = $userMedalsGamification;
            $userMedalsGamification->setUser($this);
        }

        return $this;
    }

    public function removeUserMedalsGamification(UserMedalsGamification $userMedalsGamification): self
    {
        if ($this->userMedalsGamifications->removeElement($userMedalsGamification)) {
            // set the owning side to null (unless already changed)
            if ($userMedalsGamification->getUser() === $this) {
                $userMedalsGamification->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserLevelGamification[]
     */
    public function getUserLevelGamifications(): Collection
    {
        return $this->userLevelGamifications;
    }

    public function addUserLevelGamification(UserLevelGamification $userLevelGamification): self
    {
        if (!$this->userLevelGamifications->contains($userLevelGamification)) {
            $this->userLevelGamifications[] = $userLevelGamification;
            $userLevelGamification->setUser($this);
        }

        return $this;
    }

    public function removeUserLevelGamification(UserLevelGamification $userLevelGamification): self
    {
        if ($this->userLevelGamifications->removeElement($userLevelGamification)) {
            // set the owning side to null (unless already changed)
            if ($userLevelGamification->getUser() === $this) {
                $userLevelGamification->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SatisfactionArena[]
     */
    public function getSatisfactionArenas(): Collection
    {
        return $this->satisfactionArenas;
    }

    public function addSatisfactionArena(SatisfactionArena $satisfactionArena): self
    {
        if (!$this->satisfactionArenas->contains($satisfactionArena)) {
            $this->satisfactionArenas[] = $satisfactionArena;
            $satisfactionArena->setUser($this);
        }

        return $this;
    }

    public function removeSatisfactionArena(SatisfactionArena $satisfactionArena): self
    {
        if ($this->satisfactionArenas->removeElement($satisfactionArena)) {
            // set the owning side to null (unless already changed)
            if ($satisfactionArena->getUser() === $this) {
                $satisfactionArena->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserVoucherUber[]
     */
    public function getUserVoucherUber(): Collection
    {
        return $this->userVoucherUber;
    }

    public function addUserVoucherUber(UserVoucherUber $userVoucherUber): self
    {
        if (!$this->userCorinthiansFluminense->contains($userVoucherUber)) {
            $this->userVoucherUber[] = $userVoucherUber;
            $userVoucherUber->setUser($this);
        }

        return $this;
    }

    public function removeUserVoucherUber(UserVoucherUber $userVoucherUber): self
    {
        if ($this->userVoucherUber->removeElement($userVoucherUber)) {
            // set the owning side to null (unless already changed)
            if ($userVoucherUber->getUser() === $this) {
                $userVoucherUber->setUser(null);
            }
        }

        return $this;
    }
}
