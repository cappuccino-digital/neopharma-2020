<?php

namespace App\Entity;

use App\Repository\UserVoucherUberRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserVoucherUberRepository::class)
 */
class UserVoucherUber
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userVoucherUber")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_new_register;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_run_cron;

    /**
     * @ORM\ManyToOne(targetEntity=Voucher::class, inversedBy="userVoucherUbers")
     */
    private $voucher;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(?bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }

    public function getIsNewRegister(): ?bool
    {
        return $this->is_new_register;
    }

    public function setIsNewRegister(?bool $is_new_register): self
    {
        $this->is_new_register = $is_new_register;

        return $this;
    }

    public function getIsRunCron(): ?bool
    {
        return $this->is_run_cron;
    }

    public function setIsRunCron(?bool $is_run_cron): self
    {
        $this->is_run_cron = $is_run_cron;

        return $this;
    }

    public function getVoucher(): ?Voucher
    {
        return $this->voucher;
    }

    public function setVoucher(?Voucher $voucher): self
    {
        $this->voucher = $voucher;

        return $this;
    }
}
