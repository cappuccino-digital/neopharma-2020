<?php

namespace App\Entity;

use App\Repository\UserStoreManagerResponsibleChangeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserStoreManagerResponsibleChangeRepository::class)
 */
class UserStoreManagerResponsibleChange
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userStoreManagerResponsibleChanges")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cnpj;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cpf;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_clerk;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_store_manager;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_store_manager_responsible;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(string $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getIsClerk(): ?bool
    {
        return $this->is_clerk;
    }

    public function setIsClerk(bool $is_clerk): self
    {
        $this->is_clerk = $is_clerk;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }

    public function getIsStoreManager(): ?bool
    {
        return $this->is_store_manager;
    }

    public function setIsStoreManager(bool $is_store_manager): self
    {
        $this->is_store_manager = $is_store_manager;

        return $this;
    }

    public function getIsStoreManagerResponsible(): ?bool
    {
        return $this->is_store_manager_responsible;
    }

    public function setIsStoreManagerResponsible(bool $is_store_manager_responsible): self
    {
        $this->is_store_manager_responsible = $is_store_manager_responsible;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

}
