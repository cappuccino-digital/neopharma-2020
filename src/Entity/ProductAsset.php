<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductAssetRepository")
 */
class ProductAsset
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $fileMetaDescription;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="prod_id", referencedColumnName="id")
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getFileMetaTitle(): ?string
    {
        return $this->fileMetaTitle;
    }

    public function setFileMetaTitle(?string $fileMetaTitle): self
    {
        $this->fileMetaTitle = $fileMetaTitle;

        return $this;
    }

    public function getFileMetaDescription(): ?string
    {
        return $this->fileMetaDescription;
    }

    public function setFileMetaDescription(?string $fileMetaDescription): self
    {
        $this->fileMetaDescription = $fileMetaDescription;

        return $this;
    }

}
