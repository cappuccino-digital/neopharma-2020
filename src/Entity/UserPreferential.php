<?php

namespace App\Entity;

use App\Repository\UserPreferentialRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPreferentialRepository::class)
 */
class UserPreferential
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userPreferentials")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hobby;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $team_football;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $how_many_children;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=PreferentialChildren::class, mappedBy="user_preferential")
     */
    private $preferentialChildrens;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_children;

    public function __construct()
    {
        $this->preferentialChildrens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getHobby(): ?string
    {
        return $this->hobby;
    }

    public function setHobby(?string $hobby): self
    {
        $this->hobby = $hobby;

        return $this;
    }

    public function getSport(): ?string
    {
        return $this->sport;
    }

    public function setSport(?string $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getTeamFootball(): ?string
    {
        return $this->team_football;
    }

    public function setTeamFootball(?string $team_football): self
    {
        $this->team_football = $team_football;

        return $this;
    }

    public function getHowManyChildren(): ?string
    {
        return $this->how_many_children;
    }

    public function setHowManyChildren(?string $how_many_children): self
    {
        $this->how_many_children = $how_many_children;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|PreferentialChildren[]
     */
    public function getChildren(): Collection
    {
        return $this->preferentialChildrens;
    }

    public function addChild(PreferentialChildren $preferentialChildrens): self
    {
        if (!$this->preferentialChildrens->contains($preferentialChildrens)) {
            $this->preferentialChildrens[] = $preferentialChildrens;
            $preferentialChildrens->setUserPreferential($this);
        }

        return $this;
    }

    public function removeChild(PreferentialChildren $preferentialChildrens): self
    {
        if ($this->preferentialChildrens->removeElement($preferentialChildrens)) {
            // set the owning side to null (unless already changed)
            if ($preferentialChildrens->getUserPreferential() === $this) {
                $preferentialChildrens->setUserPreferential(null);
            }
        }

        return $this;
    }

    public function getIsChildren(): ?bool
    {
        return $this->is_children;
    }

    public function setIsChildren(?bool $is_children): self
    {
        $this->is_children = $is_children;

        return $this;
    }
}
