<?php

namespace App\Entity;

use App\Repository\UserSalesIncentiveRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserSalesIncentiveRepository::class)
 */
class UserSalesIncentive
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="type_id")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_amount_clerk;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_amount_pharmaceutical;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_store_manager;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsAmountClerk(): ?bool
    {
        return $this->is_amount_clerk;
    }

    public function setIsAmountClerk(?bool $is_amount_clerk): self
    {
        $this->is_amount_clerk = $is_amount_clerk;

        return $this;
    }

    public function getIsAmountPharmaceutical(): ?bool
    {
        return $this->is_amount_pharmaceutical;
    }

    public function setIsAmountPharmaceutical(bool $is_amount_pharmaceutical): self
    {
        $this->is_amount_pharmaceutical = $is_amount_pharmaceutical;

        return $this;
    }

    public function getIsStoreManager(): ?bool
    {
        return $this->is_store_manager;
    }

    public function setIsStoreManager(bool $is_store_manager): self
    {
        $this->is_store_manager = $is_store_manager;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->type_id;
    }

    public function setTypeId(int $type_id): self
    {
        $this->type_id = $type_id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
