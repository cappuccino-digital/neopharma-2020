<?php

namespace App\Entity;

use App\Repository\UserPointTotalGamificationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPointTotalGamificationRepository::class)
 */
class UserPointTotalGamification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userPointTotalGamifications")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_store_manager;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_clerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $profile_pharmaceutical;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_regulation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_regulation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPoint(): ?string
    {
        return $this->point;
    }

    public function setPoint(?string $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getProfileStoreManager(): ?bool
    {
        return $this->profile_store_manager;
    }

    public function setProfileStoreManager(?bool $profile_store_manager): self
    {
        $this->profile_store_manager = $profile_store_manager;

        return $this;
    }

    public function getProfileClerk(): ?bool
    {
        return $this->profile_clerk;
    }

    public function setProfileClerk(?bool $profile_clerk): self
    {
        $this->profile_clerk = $profile_clerk;

        return $this;
    }

    public function getProfilePharmaceutical(): ?bool
    {
        return $this->profile_pharmaceutical;
    }

    public function setProfilePharmaceutical(?bool $profile_pharmaceutical): self
    {
        $this->profile_pharmaceutical = $profile_pharmaceutical;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsRegulation(): ?bool
    {
        return $this->is_regulation;
    }

    public function setIsRegulation(?bool $is_regulation): self
    {
        $this->is_regulation = $is_regulation;

        return $this;
    }

    public function getDateRegulation(): ?\DateTimeInterface
    {
        return $this->date_regulation;
    }

    public function setDateRegulation(?\DateTimeInterface $date_regulation): self
    {
        $this->date_regulation = $date_regulation;

        return $this;
    }
}
