<?php

namespace App\Entity;

use App\Repository\UserInactivityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserInactivityRepository::class)
 */
class UserInactivity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userInactivities")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_send_emblue;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_login_date_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_run_cron;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contactID;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $updateFieldsCustom;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLastLoginDateAt(): ?\DateTimeInterface
    {
        return $this->last_login_date_at;
    }

    public function setLastLoginDateAt(?\DateTimeInterface $last_login_date_at): self
    {
        $this->last_login_date_at = $last_login_date_at;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateRunCron(): ?\DateTimeInterface
    {
        return $this->date_run_cron;
    }

    public function setDateRunCron(?\DateTimeInterface $date_run_cron): self
    {
        $this->date_run_cron = $date_run_cron;

        return $this;
    }

    public function getIsSendEmblue(): ?bool
    {
        return $this->is_send_emblue;
    }

    public function setIsSendEmblue(?bool $is_send_emblue): self
    {
        $this->is_send_emblue = $is_send_emblue;

        return $this;
    }

    public function getContactID(): ?string
    {
        return $this->contactID;
    }

    public function setContactID(?string $contactID): self
    {
        $this->contactID = $contactID;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getUpdateFieldsCustom(): ?bool
    {
        return $this->updateFieldsCustom;
    }

    public function setUpdateFieldsCustom(?bool $updateFieldsCustom): self
    {
        $this->updateFieldsCustom = $updateFieldsCustom;

        return $this;
    }
}
