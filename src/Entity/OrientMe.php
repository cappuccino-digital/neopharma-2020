<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrientMeRepository")
 */
class OrientMe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orientMes")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accept_regulation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $wasSent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRegisterApi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAcceptRegulation(): ?bool
    {
        return $this->accept_regulation;
    }

    public function setAcceptRegulation(bool $accept_regulation): self
    {
        $this->accept_regulation = $accept_regulation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getWasSent(): ?bool
    {
        return $this->wasSent;
    }

    public function setWasSent(?bool $isRegisterApi): self
    {
        $this->isRegisterApi = $isRegisterApi;

        return $this;
    }

    public function getIsRegisterApi(): ?bool
    {
        return $this->isRegisterApi;
    }

    public function setIsRegisterApi(?bool $isRegisterApi): self
    {
        $this->isRegisterApi = $isRegisterApi;

        return $this;
    }


}
