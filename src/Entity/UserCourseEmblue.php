<?php

namespace App\Entity;

use App\Repository\UserCourseEmblueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserCourseEmblueRepository::class)
 */
class UserCourseEmblue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userCourseEmblues")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Course::class, inversedBy="userCourseEmblues")
     */
    private $course;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_send_emblue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $return_api;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_run_cron;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsSendEmblue(): ?bool
    {
        return $this->is_send_emblue;
    }

    public function setIsSendEmblue(?bool $is_send_emblue): self
    {
        $this->is_send_emblue = $is_send_emblue;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getReturnApi(): ?string
    {
        return $this->return_api;
    }

    public function setReturnApi(?string $return_api): self
    {
        $this->return_api = $return_api;

        return $this;
    }

    public function getDateRunCron(): ?\DateTimeInterface
    {
        return $this->date_run_cron;
    }

    public function setDateRunCron(?\DateTimeInterface $date_run_cron): self
    {
        $this->date_run_cron = $date_run_cron;

        return $this;
    }
}
