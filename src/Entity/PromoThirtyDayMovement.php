<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoThirtyDayMovementRepository")
 */
class PromoThirtyDayMovement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $team;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount_clerk;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount_pharmaceutical;
    
    // /**
    //  * @ORM\Column(type="datetime")
    //  */
    // private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ultimate_cron_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_incentivo_vendas = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $store_manager;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeam(): ?string
    {
        return $this->team;
    }

    public function setTeam(string $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getAmountClerk(): ?int
    {
        return $this->amount_clerk;
    }

    public function setAmountClerk(int $amount_clerk): self
    {
        $this->amount_clerk = $amount_clerk;

        return $this;
    }

    public function getAmountPharmaceutical(): ?int
    {
        return $this->amount_pharmaceutical;
    }

    public function setAmountPharmaceutical(int $amount_pharmaceutical): self
    {
        $this->amount_pharmaceutical = $amount_pharmaceutical;

        return $this;
    }


    // public function getCreatedAt(): ?\DateTimeInterface
    // {
    //     return $this->created_at;
    // }

    // public function setCreatedAt(\DateTimeInterface $created_at): self
    // {
    //     $this->created_at = $created_at;

    //     return $this;
    // }

    public function getUltimateCronAt(): ?\DateTimeInterface
    {
        return $this->ultimate_cron_at;
    }

    public function setUltimateCronAt(\DateTimeInterface $ultimate_cron_at): self
    {
        $this->ultimate_cron_at = $ultimate_cron_at;

        return $this;
    }

    public function getIsIncentivoVendas(): ?bool
    {
        return $this->is_incentivo_vendas;
    }

    public function setIsIncentivoVendas(bool $is_incentivo_vendas): self
    {
        $this->is_incentivo_vendas = $is_incentivo_vendas;

        return $this;
    }

    public function getStoreManager(): ?int
    {
        return $this->store_manager;
    }

    public function setStoreManager(int $store_manager): self
    {
        $this->store_manager = $store_manager;

        return $this;
    }
}
