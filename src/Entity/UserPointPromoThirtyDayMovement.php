<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPointPromoThirtyDayMovementRepository")
 */
class UserPointPromoThirtyDayMovement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserPromoThirtyDayMovement", inversedBy="userPointPromoThirtyDayMovements")
     */
    private $user_promo_thirty_day_movement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPointPromoThirtyDayMovements")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $point;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $point_type;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $point_type_description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_pharmaceutical_content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_month_match;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserPromoThirtyDayMovement(): ?UserPromoThirtyDayMovement
    {
        return $this->user_promo_thirty_day_movement;
    }

    public function setUserPromoThirtyDayMovement(?UserPromoThirtyDayMovement $user_promo_thirty_day_movement): self
    {
        $this->user_promo_thirty_day_movement = $user_promo_thirty_day_movement;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPoint(): ?string
    {
        return $this->point;
    }

    public function setPoint(string $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getPointType(): ?string
    {
        return $this->point_type;
    }

    public function setPointType(string $point_type): self
    {
        $this->point_type = $point_type;

        return $this;
    }

    public function getPointTypeDescription(): ?string
    {
        return $this->point_type_description;
    }

    public function setPointTypeDescription(string $point_type_description): self
    {
        $this->point_type_description = $point_type_description;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }
    
    public function getIsMonthMatch(): ?bool
    {
        return $this->is_month_match;
    }

    public function setIsMonthMatch(bool $is_month_match): self
    {
        $this->is_month_match = $is_month_match;

        return $this;
    }

    public function getIsPharmaceuticalContent(): ?bool
    {
        return $this->is_pharmaceutical_content;
    }

    public function setIsPharmaceuticalContent(bool $is_pharmaceutical_content): self
    {
        $this->is_pharmaceutical_content = $is_pharmaceutical_content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
