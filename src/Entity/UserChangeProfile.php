<?php

namespace App\Entity;

use App\Repository\UserChangeProfileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserChangeProfileRepository::class)
 */
class UserChangeProfile implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fantasy_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cnpj;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $neighborhood;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference_point;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ddd_cellphone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cellphone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_notification_whatsapp;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_term_use;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_notification_news;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_send_email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_change_profile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isValid;
 
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $newsletter = false;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $team_football;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_test = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->company_name;
    }

    public function setCompanyName(?string $company_name): self
    {
        $this->company_name = $company_name;

        return $this;
    }

    public function getFantasyName(): ?string
    {
        return $this->fantasy_name;
    }

    public function setFantasyName(?string $fantasy_name): self
    {
        $this->fantasy_name = $fantasy_name;

        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(?string $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getReferencePoint(): ?string
    {
        return $this->reference_point;
    }

    public function setReferencePoint(?string $reference_point): self
    {
        $this->reference_point = $reference_point;

        return $this;
    }

    public function getDddCellphone(): ?string
    {
        return $this->ddd_cellphone;
    }

    public function setDddCellphone(?string $ddd_cellphone): self
    {
        $this->ddd_cellphone = $ddd_cellphone;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsNotificationWhatsapp(): ?bool
    {
        return $this->is_notification_whatsapp;
    }

    public function setIsNotificationWhatsapp(?bool $is_notification_whatsapp): self
    {
        $this->is_notification_whatsapp = $is_notification_whatsapp;

        return $this;
    }

    public function getIsTermUse(): ?bool
    {
        return $this->is_term_use;
    }

    public function setIsTermUse(?bool $is_term_use): self
    {
        $this->is_term_use = $is_term_use;

        return $this;
    }

    public function getIsNotificationNews(): ?bool
    {
        return $this->is_notification_news;
    }

    public function setIsNotificationNews(?bool $is_notification_news): self
    {
        $this->is_notification_news = $is_notification_news;

        return $this;
    }

    public function getIsSendEmail(): ?bool
    {
        return $this->is_send_email;
    }

    public function setIsSendEmail(bool $is_send_email): self
    {
        $this->is_send_email = $is_send_email;

        return $this;
    }

    public function getIsChangeProfile(): ?bool
    {
        return $this->is_change_profile;
    }

    public function setIsChangeProfile(bool $is_change_profile): self
    {
        $this->is_change_profile = $is_change_profile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getRoles() {

    }
    public function getUsername(){

    }
    public function eraseCredentials(){
        
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTokenEmail(): ?string
    {
        return $this->token_email;
    }

    public function setTokenEmail(?string $token_email): self
    {
        $this->token_email = $token_email;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(?\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(?bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(?bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getTeamFootball(): ?string
    {
        return $this->team_football;
    }

    public function setTeamFootball(string $team_football): self
    {
        $this->team_football = $team_football;

        return $this;
    }

    public function getIsTest(): ?bool
    {
        return $this->is_test;
    }

    public function setIsTest(?bool $is_test): self
    {
        $this->is_test = $is_test;

        return $this;
    }
    
}
