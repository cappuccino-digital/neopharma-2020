<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPointsExcelenciaRepository")
 */
class UserPointsExcelencia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPointsExcelencias")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PointsTypeExcelencia", inversedBy="userPointsExcelencias")
     */
    private $point_type_excelencia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="userPointsExcelencias")
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BlogPost", inversedBy="userPointsExcelencias")
     */
    private $blog_post;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quiz", inversedBy="userPointsExcelencias")
     */
    private $quiz;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_gestor;

    /**
     * @ORM\Column(type="boolean", nullable=true,)
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_clerk;

    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     */
    private $cod_return_api;

    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     */
    private $description_return_api;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPointTypeExcelencia(): ?PointsTypeExcelencia
    {
        return $this->point_type_excelencia;
    }

    public function setPointTypeExcelencia(?PointsTypeExcelencia $point_type_excelencia): self
    {
        $this->point_type_excelencia = $point_type_excelencia;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getBlogPost(): ?BlogPost
    {
        return $this->blog_post;
    }

    public function setBlogPost(?BlogPost $blog_post): self
    {
        $this->blog_post = $blog_post;

        return $this;
    }

    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getIsGestor(): ?bool
    {
        return $this->is_gestor;
    }

    public function setIsGestor(bool $is_gestor): self
    {
        $this->is_gestor = $is_gestor;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }

    public function getIsClerk(): ?bool
    {
        return $this->is_clerk;
    }

    public function setIsClerk(bool $is_clerk): self
    {
        $this->is_clerk = $is_clerk;

        return $this;
    }


    public function getCodReturnApi(): ?string
    {
        return $this->cod_return_api;
    }

    public function setCodReturnApi(string $cod_return_api): self
    {
        $this->cod_return_api = $cod_return_api;

        return $this;
    }

    public function getDescriptionReturnApi(): ?string
    {
        return $this->description_return_api;
    }

    public function setDescriptionReturnApi(string $description_return_api): self
    {
        $this->description_return_api = $description_return_api;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
