<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoThirtyDayPostInstragramPointRepository")
 */
class PromoThirtyDayPostInstragramPoint
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PromoThirtyDayPostInstagram", inversedBy="promoThirtyDayPostInstragramPoints")
     */
    private $post_instagram;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="promoThirtyDayPostInstragramPoints")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserPromoThirtyDayMovement", inversedBy="userPromoThirtyDayMovements")
     */
    private $user_promo;

    /**
     * @ORM\Column(type="integer")
     */
    private $point;

    /**
     * @ORM\Column(type="integer")
     */
    private $point_extra;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_public_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostInstagram(): ?PromoThirtyDayPostInstagram
    {
        return $this->post_instagram;
    }

    public function setPostInstagram(?PromoThirtyDayPostInstagram $post_instagram): self
    {
        $this->post_instagram = $post_instagram;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserPromo(): ?UserPromoThirtyDayMovement
    {
        return $this->user_promo;
    }

    public function setUserPromo(?UserPromoThirtyDayMovement $user_promo): self
    {
        $this->user_promo = $user_promo;

        return $this;
    }

    public function getPoint(): ?int
    {
        return $this->point;
    }

    public function setPoint(int $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getPointExtra(): ?int
    {
        return $this->point_extra;
    }

    public function setPointExtra(int $point_extra): self
    {
        $this->point_extra = $point_extra;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDatePublicAt(): ?\DateTimeInterface
    {
        return $this->date_public_at;
    }

    public function setDatePublicAt(\DateTimeInterface $date_public_at): self
    {
        $this->date_public_at = $date_public_at;

        return $this;
    }
}
