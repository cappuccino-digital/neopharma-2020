<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $courseId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $classes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $courseTime = 0;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $courseLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobile;

        /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMobileMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMobileMetaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageMetaTitle;

    /**
     * @ORM\Column(type="string", length=510, nullable=true)
     */
    private $imageMetaDescription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToPharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOnlyToClerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPharmacy;


    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $views = 0;

    /**
     * @ORM\OneToMany(targetEntity="UserCourse", mappedBy="course")
     *
     */
    private $subscribes;

    /**
     * @ORM\OneToMany(targetEntity="CourseScore", mappedBy="course")
     *
     */
    private $scores;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $score = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cod_interaction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @ORM\OneToMany(targetEntity=UserCourseEmblue::class, mappedBy="course")
     */
    private $userCourseEmblues;

    /**
     * @ORM\ManyToMany(targetEntity=SpecialCampaign::class, mappedBy="course")
     */
    private $specialCampaigns;

    public function __construct()
    {
        $this->subscribes = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->userCourseEmblues = new ArrayCollection();
        $this->specialCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageMobile(): ?string
    {
        return $this->imageMobile;
    }

    public function setImageMobile(?string $imageMobile): self
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsOnlyToPharmaceutical(): ?bool
    {
        return $this->isOnlyToPharmaceutical;
    }

    public function setIsOnlyToPharmaceutical(?bool $isOnlyToPharmaceutical): self
    {
        $this->isOnlyToPharmaceutical = $isOnlyToPharmaceutical;

        return $this;
    }

    public function getCourseId(): ?string
    {
        return $this->courseId;
    }

    public function setCourseId(?string $courseId): self
    {
        $this->courseId = $courseId;

        return $this;
    }

    public function getCodInteraction(): ?string
    {
        return $this->cod_interaction;
    }

    public function setCodInteraction(?string $cod_interaction): self
    {
        $this->cod_interaction = $cod_interaction;

        return $this;
    }

    public function getCourseLink(): ?string
    {
        return $this->courseLink;
    }

    public function setCourseLink(?string $courseLink): self
    {
        $this->courseLink = $courseLink;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getClasses(): ?int
    {
        return $this->classes;
    }

    public function setClasses(?int $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(?int $views): self
    {
        $this->views = $views;

        return $this;
    }

    /**
     * @return Collection|UserCourse[]
     */
    public function getSubscribes(): Collection
    {
        return $this->subscribes;
    }

    public function addSubscribe(UserCourse $subscribe): self
    {
        if (!$this->subscribes->contains($subscribe)) {
            $this->subscribes[] = $subscribe;
            $subscribe->setCourse($this);
        }

        return $this;
    }

    public function removeSubscribe(UserCourse $subscribe): self
    {
        if ($this->subscribes->contains($subscribe)) {
            $this->subscribes->removeElement($subscribe);
            // set the owning side to null (unless already changed)
            if ($subscribe->getCourse() === $this) {
                $subscribe->setCourse(null);
            }
        }

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * @return Collection|CourseScore[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(CourseScore $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setPost($this);
        }

        return $this;
    }

    public function removeScore(CourseScore $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getPost() === $this) {
                $score->setPost(null);
            }
        }

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(?float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getIsPharmacy(): ?bool
    {
        return $this->isPharmacy;
    }

    public function setIsPharmacy(?bool $isPharmacy): self
    {
        $this->isPharmacy = $isPharmacy;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(?bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getIsOnlyToClerk(): ?bool
    {
        return $this->isOnlyToClerk;
    }

    public function setIsOnlyToClerk(?bool $isOnlyToClerk): self
    {
        $this->isOnlyToClerk = $isOnlyToClerk;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getImageMobileMetaTitle(): ?string
    {
        return $this->imageMobileMetaTitle;
    }

    public function setImageMobileMetaTitle(?string $imageMobileMetaTitle): self
    {
        $this->imageMobileMetaTitle = $imageMobileMetaTitle;

        return $this;
    }

    public function getImageMobileMetaDescription(): ?string
    {
        return $this->imageMobileMetaDescription;
    }

    public function setImageMobileMetaDescription(?string $imageMobileMetaDescription): self
    {
        $this->imageMobileMetaDescription = $imageMobileMetaDescription;

        return $this;
    }

    public function getImageMetaTitle(): ?string
    {
        return $this->imageMetaTitle;
    }

    public function setImageMetaTitle(?string $imageMetaTitle): self
    {
        $this->imageMetaTitle = $imageMetaTitle;

        return $this;
    }

    public function getImageMetaDescription(): ?string
    {
        return $this->imageMetaDescription;
    }

    public function setImageMetaDescription(?string $imageMetaDescription): self
    {
        $this->imageMetaDescription = $imageMetaDescription;

        return $this;
    }

    public function getCourseTime(): ?int
    {
        return $this->courseTime;
    }

    public function setCourseTime(?int $courseTime): self
    {
        $this->courseTime = $courseTime;

        return $this;
    }

    public function getCourseTimeField(): ?string
    {
        return gmdate("H:i:s", $this->courseTime);
    }

    public function getCourseTimeString(): ?string
    {
        $returnTime = gmdate("H:i:s", $this->courseTime);
        $time = explode(':', $returnTime);

        $hour = isset($time[0]) ? (int)($time[0]) : 0;
        $minute = isset($time[1]) ? (int)($time[1]) : 0;
        $second = isset($time[2]) ? (int)($time[2]) : 0;

        if($hour == 0 ){
            if($minute == 0){
                return "{$second}s";
            } else {
                if($second == 0){
                    return "{$minute}m";
                } else {
                    return "{$minute}m {$second}s";
                }
            }
        } else {
            if($minute == 0){
                if($second == 0){
                    return "{$hour}h";
                } else {
                    return "{$hour}h {$second}s";
                }
            } else {
                if($second == 0){
                    return "{$hour}h {$minute}m";
                } else {
                    return "{$hour}h {$minute}m {$second}s";
                }
            }
        }
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    /**
     * @return Collection|UserCourseEmblue[]
     */
    public function getUserCourseEmblues(): Collection
    {
        return $this->userCourseEmblues;
    }

    public function addUserCourseEmblue(UserCourseEmblue $userCourseEmblue): self
    {
        if (!$this->userCourseEmblues->contains($userCourseEmblue)) {
            $this->userCourseEmblues[] = $userCourseEmblue;
            $userCourseEmblue->setCourse($this);
        }

        return $this;
    }

    public function removeUserCourseEmblue(UserCourseEmblue $userCourseEmblue): self
    {
        if ($this->userCourseEmblues->removeElement($userCourseEmblue)) {
            // set the owning side to null (unless already changed)
            if ($userCourseEmblue->getCourse() === $this) {
                $userCourseEmblue->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SpecialCampaign[]
     */
    public function getSpecialCampaigns(): Collection
    {
        return $this->specialCampaigns;
    }

    public function addSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if (!$this->specialCampaigns->contains($specialCampaign)) {
            $this->specialCampaigns[] = $specialCampaign;
            $specialCampaign->addCourse($this);
        }

        return $this;
    }

    public function removeSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if ($this->specialCampaigns->removeElement($specialCampaign)) {
            $specialCampaign->removeCourse($this);
        }

        return $this;
    }
}
