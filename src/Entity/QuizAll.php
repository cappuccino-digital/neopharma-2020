<?php

namespace App\Entity;

use App\Repository\QuizAllRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuizAllRepository::class)
 */
class QuizAll
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_public;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_pharmaceutical;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_clerk;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_store_manager;

    /**
     * @ORM\OneToMany(targetEntity=QuizAllQuestions::class, mappedBy="quiz_all")
     */
    private $quizAllQuestions;

    /**
     * @ORM\OneToMany(targetEntity=QuizAllRuleResult::class, mappedBy="quiz_all")
     */
    private $quizAllRuleResults;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_alt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity=SpecialCampaign::class, mappedBy="quiz_all")
     */
    private $specialCampaigns;

    public function __construct()
    {
        $this->quizAllQuestions = new ArrayCollection();
        $this->quizAllRuleResults = new ArrayCollection();
        $this->specialCampaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->is_public;
    }

    public function setIsPublic(?bool $is_public): self
    {
        $this->is_public = $is_public;

        return $this;
    }

    public function getIsPharmaceutical(): ?bool
    {
        return $this->is_pharmaceutical;
    }

    public function setIsPharmaceutical(?bool $is_pharmaceutical): self
    {
        $this->is_pharmaceutical = $is_pharmaceutical;

        return $this;
    }

    public function getIsClerk(): ?bool
    {
        return $this->is_clerk;
    }

    public function setIsClerk(?bool $is_clerk): self
    {
        $this->is_clerk = $is_clerk;

        return $this;
    }

    public function getIsStoreManager(): ?bool
    {
        return $this->is_store_manager;
    }

    public function setIsStoreManager(?bool $is_store_manager): self
    {
        $this->is_store_manager = $is_store_manager;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|QuizAllQuestions[]
     */
    public function getQuizAllQuestions(): Collection
    {
        return $this->quizAllQuestions;
    }

    public function addQuizAllQuestion(QuizAllQuestions $quizAllQuestion): self
    {
        if (!$this->quizAllQuestions->contains($quizAllQuestion)) {
            $this->quizAllQuestions[] = $quizAllQuestion;
            $quizAllQuestion->setQuizAll($this);
        }

        return $this;
    }

    public function removeQuizAllQuestion(QuizAllQuestions $quizAllQuestion): self
    {
        if ($this->quizAllQuestions->removeElement($quizAllQuestion)) {
            // set the owning side to null (unless already changed)
            if ($quizAllQuestion->getQuizAll() === $this) {
                $quizAllQuestion->setQuizAll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuizAllRuleResult[]
     */
    public function getQuizAllRuleResults(): Collection
    {
        return $this->quizAllRuleResults;
    }

    public function addQuizAllRuleResult(QuizAllRuleResult $quizAllRuleResult): self
    {
        if (!$this->quizAllRuleResults->contains($quizAllRuleResult)) {
            $this->quizAllRuleResults[] = $quizAllRuleResult;
            $quizAllRuleResult->setQuizAll($this);
        }

        return $this;
    }

    public function removeQuizAllRuleResult(QuizAllRuleResult $quizAllRuleResult): self
    {
        if ($this->quizAllRuleResults->removeElement($quizAllRuleResult)) {
            // set the owning side to null (unless already changed)
            if ($quizAllRuleResult->getQuizAll() === $this) {
                $quizAllRuleResult->setQuizAll(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageAlt(): ?string
    {
        return $this->image_alt;
    }

    public function setImageAlt(?string $image_alt): self
    {
        $this->image_alt = $image_alt;

        return $this;
    }

    /**
     * @return Collection|SpecialCampaign[]
     */
    public function getSpecialCampaigns(): Collection
    {
        return $this->specialCampaigns;
    }

    public function addSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if (!$this->specialCampaigns->contains($specialCampaign)) {
            $this->specialCampaigns[] = $specialCampaign;
            $specialCampaign->addQuizAll($this);
        }

        return $this;
    }

    public function removeSpecialCampaign(SpecialCampaign $specialCampaign): self
    {
        if ($this->specialCampaigns->removeElement($specialCampaign)) {
            $specialCampaign->removeQuizAll($this);
        }

        return $this;
    }
}
