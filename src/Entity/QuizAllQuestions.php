<?php

namespace App\Entity;

use App\Repository\QuizAllQuestionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuizAllQuestionsRepository::class)
 */
class QuizAllQuestions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=QuizAll::class, inversedBy="quizAllQuestions")
     */
    private $quiz_all;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=QuizAllAlternatives::class, mappedBy="quiz_all_questions")
     */
    private $quizAllAlternatives;


    public function __construct()
    {
        $this->quizAllAlternatives = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizAll(): ?QuizAll
    {
        return $this->quiz_all;
    }

    public function setQuizAll(?QuizAll $quiz_all): self
    {
        $this->quiz_all = $quiz_all;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|QuizAllAlternatives[]
     */
    public function getQuizAllAlternatives(): Collection
    {
        return $this->quizAllAlternatives;
    }

    public function addQuizAllAlternative(QuizAllAlternatives $quizAllAlternative): self
    {
        if (!$this->quizAllAlternatives->contains($quizAllAlternative)) {
            $this->quizAllAlternatives[] = $quizAllAlternative;
            $quizAllAlternative->setQuizAllQuestions($this);
        }

        return $this;
    }

    public function removeQuizAllAlternative(QuizAllAlternatives $quizAllAlternative): self
    {
        if ($this->quizAllAlternatives->removeElement($quizAllAlternative)) {
            // set the owning side to null (unless already changed)
            if ($quizAllAlternative->getQuizAllQuestions() === $this) {
                $quizAllAlternative->setQuizAllQuestions(null);
            }
        }

        return $this;
    }
}
