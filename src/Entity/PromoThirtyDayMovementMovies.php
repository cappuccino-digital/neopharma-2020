<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoThirtyDayMovementMoviesRepository")
 */
class PromoThirtyDayMovementMovies
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromoThirtyDayMovementUserMovie", mappedBy="PromoThirtyDayMovementMovies")
     */
    private $promoThirtyDayMovementUserMovies;

    public function __construct()
    {
        $this->promoThirtyDayMovementUserMovies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|PromoThirtyDayMovementUserMovie[]
     */
    public function getPromoThirtyDayMovementUserMovies(): Collection
    {
        return $this->promoThirtyDayMovementUserMovies;
    }

    public function addPromoThirtyDayMovementUserMovie(PromoThirtyDayMovementUserMovie $promoThirtyDayMovementUserMovie): self
    {
        if (!$this->promoThirtyDayMovementUserMovies->contains($promoThirtyDayMovementUserMovie)) {
            $this->promoThirtyDayMovementUserMovies[] = $promoThirtyDayMovementUserMovie;
            $promoThirtyDayMovementUserMovie->setPromoThirtyDayMovementMovies($this);
        }

        return $this;
    }

    public function removePromoThirtyDayMovementUserMovie(PromoThirtyDayMovementUserMovie $promoThirtyDayMovementUserMovie): self
    {
        if ($this->promoThirtyDayMovementUserMovies->contains($promoThirtyDayMovementUserMovie)) {
            $this->promoThirtyDayMovementUserMovies->removeElement($promoThirtyDayMovementUserMovie);
            // set the owning side to null (unless already changed)
            if ($promoThirtyDayMovementUserMovie->getPromoThirtyDayMovementMovies() === $this) {
                $promoThirtyDayMovementUserMovie->setPromoThirtyDayMovementMovies(null);
            }
        }

        return $this;
    }
}
