<?php

namespace App\Form;

use App\Entity\BlogPost;
use App\Entity\Course;
use App\Entity\MedalsGamification;
use App\Entity\Product;
use App\Entity\QuizAll;
use App\Entity\SpecialCampaign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SpecialCampaignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Título',
            ])
            ->add('slug', null, [
                'label' => 'Slug',
            ])
            ->add('route_path', null, [
                'label' => 'Route name',
            ])
            ->add('image', FileType::class, [
                'label' => 'Thumb',
                // 'help' => 'Todas as imagem precisam seguir as dimensões 1280x900 no formato PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => true,
                'attr' => ['class' => 'file-input'],
            ])
            ->add('course', EntityType::class, [
                // looks for choices from this entity
                'class' => Course::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select-multiple'],
                'label' => 'Curso',
            ])
            ->add('blog_post', EntityType::class, [
                // looks for choices from this entity
                'class' => BlogPost::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.title', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'title',
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select-multiple'],
                'label' => 'Post/Podcast',
            ])
            ->add('quiz_all', EntityType::class, [
                // looks for choices from this entity
                'class' => QuizAll::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select-multiple'],
                'label' => 'Quiz',
            ])
            ->add('product', EntityType::class, [
                // looks for choices from this entity
                'class' => Product::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select-multiple'],
                'label' => 'Produtos',
            ])
            ->add('medals', EntityType::class, [
                // looks for choices from this entity
                'class' => MedalsGamification::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => false,
                // 'multiple' => true,
                'attr' => ['class' => 'select-multiple'],
                'label' => 'Medalhas',
            ])
            ->add('is_menu', null, [
                'label' => 'Exibir no menu?',
            ])
            ->add('is_active', null, [
                'label' => 'Ativo',
            ])
            
            // ->add('position')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SpecialCampaign::class,
        ]);
    }
}
