<?php

namespace App\Form;

use App\Entity\MedalsGamification;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MedalsGamificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Medalha',
                'required' => true
            ])
            ->add('slug', null, [
                'label' => 'Slug',
                'required' => true
            ])
            ->add('icon', FileType::class, [
                'label' => 'Ícone',
                // 'help' => 'Todas as imagem precisam seguir as dimensões 1280x900 no formato PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => true,
                'attr' => ['class' => 'file-input'],
            ])
            // ->add('icon_not')
            ->add('description', null, [
                'label' => 'Descrição',
                'required' => true
            ])
            ->add('position', null, [
                'label' => 'Pontuação',
                'required' => true
            ])
            ->add('is_active', null, [
                'label' => 'Ativo',
            ])
            ->add('is_campaign', null, [
                'label' => 'Medalha de campanha?',
            ])
            //->add('created_at')
            //->add('updated_at')
            // ->add('specialCampaign')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MedalsGamification::class,
        ]);
    }
}
