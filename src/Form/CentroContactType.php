<?php

namespace App\Form;

use App\Entity\CentroContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class CentroContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nome completo*',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('perfil', ChoiceType::class, [
                'choices' => [
                    'Selecione...' => '',
                    'Balconista' => 'Balconista',
                    'Farmacêutico' => 'Farmacêutico'
                ],
                'label' => 'Função*',
                'required' => true,
                'attr' => ['class' => 'form-control select-content'],
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail*',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('address', null, [
                'label' => 'Rua*',
                'required' => true,
                'attr' => ['class' => 'form-control address'],
            ])
            ->add('number', null, [
                'label' => 'Numero*',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('zip_code', null, [
                'label' => 'CEP*',
                'required' => true,
                'attr' => ['class' => 'form-control zip_code'],
            ])
            ->add('neighborhood', null, [
                'label' => 'Bairro*',
                'required' => true,
                'attr' => ['class' => 'form-control neighborhood'],
            ])
            ->add('city', null, [
                'label' => 'Bairro*',
                'required' => true,
                'attr' => ['class' => 'form-control city'],
            ])
            ->add('state', ChoiceType::class, [
                'choices' => [
                    "Acre" => "AC",
                    "Alagoas" => "AL",
                    "Amapá" => "AP",
                    "Amazonas" => "AM",
                    "Bahia" => "BA",
                    "Ceará" => "CE",
                    "Distrito Federal" => "DF",
                    "Espírito Santo" => "ES",
                    "Goiás" => "GO",
                    "Maranhão" => "MA",
                    "Mato Grosso" => "MT",
                    "Mato Grosso do Sul" => "MS",
                    "Minas Gerais" => "MG",
                    "Pará" => "PA",
                    "Paraíba" => "PB",
                    "Paraná" => "PR",
                    "Pernambuco" => "PE",
                    "Piauí" => "PI",
                    "Rio de Janeiro" => "RJ",
                    "Rio Grande do Norte" => "RN",
                    "Rio Grande do Sul" => "RS",
                    "Rondônia" => "RO",
                    "Roraima" => "RR",
                    "Santa Catarina" => "SC",
                    "São Paulo" => "SP",
                    "Sergipe" => "SE",
                    "Tocantins" => "TO"
                ],
                'label' => 'Estado',
                'required' => true,
                'attr' => ['class' => 'form-control select-content state'],
            ])
            ->add('phone', null, [
                'label' => 'Telefone*',
                'required' => true,
                'attr' => ['class' => 'form-control phone'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CentroContact::class,
        ]);
    }
}
