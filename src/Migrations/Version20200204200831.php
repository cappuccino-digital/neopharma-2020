<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204200831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promo_thirty_day_movement (id INT AUTO_INCREMENT NOT NULL, team VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, code VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, update_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE sales_force');
        $this->addSql('ALTER TABLE banner DROP position');
        $this->addSql('ALTER TABLE user ADD company_name VARCHAR(255) DEFAULT NULL, ADD fantasy_name VARCHAR(255) DEFAULT NULL, ADD zipcode VARCHAR(255) DEFAULT NULL, ADD address VARCHAR(255) DEFAULT NULL, ADD number VARCHAR(255) DEFAULT NULL, ADD complement VARCHAR(255) DEFAULT NULL, ADD neighborhood VARCHAR(255) DEFAULT NULL, ADD reference_point VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sales_force (id INT AUTO_INCREMENT NOT NULL, nm_regional VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, nm_divisao VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, sede_setor VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, colaborador VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, hash VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE promo_thirty_day_movement');
        $this->addSql('ALTER TABLE banner ADD position INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP company_name, DROP fantasy_name, DROP zipcode, DROP address, DROP number, DROP complement, DROP neighborhood, DROP reference_point');
    }
}
