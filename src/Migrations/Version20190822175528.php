<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190822175528 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_score DROP FOREIGN KEY FK_DB3230B94B89032C');
        $this->addSql('DROP INDEX IDX_DB3230B94B89032C ON course_score');
        $this->addSql('ALTER TABLE course_score CHANGE post_id course_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course_score ADD CONSTRAINT FK_DB3230B9591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('CREATE INDEX IDX_DB3230B9591CC992 ON course_score (course_id)');
        $this->addSql('ALTER TABLE product_score DROP FOREIGN KEY FK_93C54C0D4B89032C');
        $this->addSql('DROP INDEX IDX_93C54C0D4B89032C ON product_score');
        $this->addSql('ALTER TABLE product_score CHANGE post_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_score ADD CONSTRAINT FK_93C54C0D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_93C54C0D4584665A ON product_score (product_id)');
        $this->addSql('ALTER TABLE quiz_score DROP FOREIGN KEY FK_CEEA4ED14B89032C');
        $this->addSql('DROP INDEX IDX_CEEA4ED14B89032C ON quiz_score');
        $this->addSql('ALTER TABLE quiz_score CHANGE post_id quiz_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz_score ADD CONSTRAINT FK_CEEA4ED1853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id)');
        $this->addSql('CREATE INDEX IDX_CEEA4ED1853CD175 ON quiz_score (quiz_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_score DROP FOREIGN KEY FK_DB3230B9591CC992');
        $this->addSql('DROP INDEX IDX_DB3230B9591CC992 ON course_score');
        $this->addSql('ALTER TABLE course_score CHANGE course_id post_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course_score ADD CONSTRAINT FK_DB3230B94B89032C FOREIGN KEY (post_id) REFERENCES course (id)');
        $this->addSql('CREATE INDEX IDX_DB3230B94B89032C ON course_score (post_id)');
        $this->addSql('ALTER TABLE product_score DROP FOREIGN KEY FK_93C54C0D4584665A');
        $this->addSql('DROP INDEX IDX_93C54C0D4584665A ON product_score');
        $this->addSql('ALTER TABLE product_score CHANGE product_id post_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_score ADD CONSTRAINT FK_93C54C0D4B89032C FOREIGN KEY (post_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_93C54C0D4B89032C ON product_score (post_id)');
        $this->addSql('ALTER TABLE quiz_score DROP FOREIGN KEY FK_CEEA4ED1853CD175');
        $this->addSql('DROP INDEX IDX_CEEA4ED1853CD175 ON quiz_score');
        $this->addSql('ALTER TABLE quiz_score CHANGE quiz_id post_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz_score ADD CONSTRAINT FK_CEEA4ED14B89032C FOREIGN KEY (post_id) REFERENCES quiz (id)');
        $this->addSql('CREATE INDEX IDX_CEEA4ED14B89032C ON quiz_score (post_id)');
    }
}
