<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200315013037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promo_thirty_day_post_instragram_point (id INT AUTO_INCREMENT NOT NULL, post_instagram_id INT DEFAULT NULL, user_id INT DEFAULT NULL, point INT NOT NULL, point_extra INT NOT NULL, created_at DATETIME NOT NULL, date_public_at DATETIME NOT NULL, INDEX IDX_C482430A3724EFDC (post_instagram_id), INDEX IDX_C482430AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promo_thirty_day_post_instragram_point ADD CONSTRAINT FK_C482430A3724EFDC FOREIGN KEY (post_instagram_id) REFERENCES promo_thirty_day_post_instagram (id)');
        $this->addSql('ALTER TABLE promo_thirty_day_post_instragram_point ADD CONSTRAINT FK_C482430AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE questions');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(1) NOT NULL COLLATE utf8_general_ci, two VARCHAR(1) NOT NULL COLLATE utf8_general_ci, three VARCHAR(1) NOT NULL COLLATE utf8_general_ci, four VARCHAR(1) NOT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, ip_address VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE promo_thirty_day_post_instragram_point');
    }
}
