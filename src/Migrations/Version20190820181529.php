<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190820181529 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE badge (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, point_description VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, is_only_to_pharmaceutical TINYINT(1) DEFAULT NULL, min_point INT DEFAULT NULL, given_point INT DEFAULT NULL, UNIQUE INDEX UNIQ_FEF0481D989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE banner (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_6F9DB8E7C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE banner_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_84670643989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_post (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, type_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, post LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, tags VARCHAR(255) DEFAULT NULL, podcast_file VARCHAR(255) DEFAULT NULL, video_file VARCHAR(255) DEFAULT NULL, score DOUBLE PRECISION DEFAULT NULL, views INT DEFAULT NULL, reading_time INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, is_only_to_pharmaceutical TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_BA5AE01D989D9B62 (slug), INDEX IDX_BA5AE01D12469DE2 (category_id), INDEX IDX_BA5AE01DC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_post_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_CA275A0C989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_post_score (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, post_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_43449957A76ED395 (user_id), INDEX IDX_434499574B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_post_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E0F5D86D989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, course_id VARCHAR(255) DEFAULT NULL, classes INT DEFAULT NULL, course_link VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, is_only_to_pharmaceutical TINYINT(1) DEFAULT NULL, slug VARCHAR(255) NOT NULL, views INT DEFAULT NULL, score DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_169E6FB9989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course_score (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, post_id INT DEFAULT NULL, score INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_DB3230B9A76ED395 (user_id), INDEX IDX_DB3230B94B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE neo_tv_video (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, youtube_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, active_substance VARCHAR(255) DEFAULT NULL, barcode VARCHAR(255) DEFAULT NULL, anvisa_registration VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, indication LONGTEXT DEFAULT NULL, contraindication LONGTEXT DEFAULT NULL, caution_and_warning LONGTEXT DEFAULT NULL, dosage LONGTEXT DEFAULT NULL, publicity_piece_impression_date DATETIME DEFAULT NULL, bulla_file VARCHAR(255) DEFAULT NULL, banner VARCHAR(255) DEFAULT NULL, banner_mobile VARCHAR(255) DEFAULT NULL, score DOUBLE PRECISION DEFAULT NULL, app_views INT DEFAULT NULL, views INT DEFAULT NULL, other_presentation LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) NOT NULL, slug VARCHAR(255) NOT NULL, classification_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_D34A04AD989D9B62 (slug), INDEX IDX_D34A04ADC54C8C93 (type_id), INDEX IDX_D34A04AD2A86559F (classification_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_asset (id INT AUTO_INCREMENT NOT NULL, prod_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, INDEX IDX_A3F321001C83F75 (prod_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_CDFC7356989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_classifiation_by_distrib_and_presc (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_FB4B2FA2989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_score (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, post_id INT DEFAULT NULL, score INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_93C54C0DA76ED395 (user_id), INDEX IDX_93C54C0D4B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1367588989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, introduction LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, pdf_file VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, views INT DEFAULT NULL, is_only_to_pharmaceutical TINYINT(1) DEFAULT NULL, score DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_A412FA92989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_answer (id INT AUTO_INCREMENT NOT NULL, question_id INT DEFAULT NULL, answer LONGTEXT DEFAULT NULL, feedback LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, value INT DEFAULT NULL, is_correct TINYINT(1) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, INDEX IDX_3799BA7C1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_question (id INT AUTO_INCREMENT NOT NULL, quiz_id INT DEFAULT NULL, question LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, has_multiple_answers TINYINT(1) DEFAULT NULL, number_of_answers INT DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_6033B00B853CD175 (quiz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_result (id INT AUTO_INCREMENT NOT NULL, quiz_id INT DEFAULT NULL, final_message LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, min_score DOUBLE PRECISION DEFAULT NULL, max_score DOUBLE PRECISION DEFAULT NULL, INDEX IDX_FE2E314A853CD175 (quiz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quiz_score (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, post_id INT DEFAULT NULL, score INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_CEEA4ED1A76ED395 (user_id), INDEX IDX_CEEA4ED14B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referrer_content (id INT AUTO_INCREMENT NOT NULL, main_content_id INT NOT NULL, main_content_type VARCHAR(255) DEFAULT NULL, referrer_content_id INT DEFAULT NULL, referrer_content_type VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, role VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_57698A6A57698A6A (role), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, racine_data_id INT DEFAULT NULL, facebook_data_id INT DEFAULT NULL, google_data_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, salt VARCHAR(32) DEFAULT NULL, password VARCHAR(64) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, is_facebook_connected TINYINT(1) DEFAULT NULL, is_google_connected TINYINT(1) DEFAULT NULL, is_pharmaceutical TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, last_login_at DATETIME DEFAULT NULL, birthday DATETIME DEFAULT NULL, cpf VARCHAR(255) DEFAULT NULL, crf VARCHAR(255) DEFAULT NULL, cnpj VARCHAR(255) DEFAULT NULL, gender VARCHAR(255) DEFAULT NULL, app_access_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D64982CF74B1 (racine_data_id), UNIQUE INDEX UNIQ_8D93D64941CEBD47 (facebook_data_id), UNIQUE INDEX UNIQ_8D93D649C9B19CBA (google_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), INDEX IDX_2DE8C6A3D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_course (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, course_id INT DEFAULT NULL, access_token VARCHAR(255) NOT NULL, subscribed_at DATETIME DEFAULT NULL, conclusion_at DATETIME DEFAULT NULL, INDEX IDX_73CC7484A76ED395 (user_id), INDEX IDX_73CC7484591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_facebook_data (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_50845DFEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_favorite (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, type_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, update_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_88486AD9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_google_data (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_E0B08FF5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_point (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, badge_id INT DEFAULT NULL, point INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, is_valid TINYINT(1) NOT NULL, point_type VARCHAR(255) DEFAULT NULL, point_type_id INT NOT NULL, point_type_description VARCHAR(255) DEFAULT NULL, is_pharmaceutical TINYINT(1) DEFAULT NULL, INDEX IDX_5567087CA76ED395 (user_id), INDEX IDX_5567087CF7A2C2FC (badge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_racine (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, racine_id INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_9E1B3315A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX translations_lookup_idx (locale, object_class, foreign_key), UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('ALTER TABLE banner ADD CONSTRAINT FK_6F9DB8E7C54C8C93 FOREIGN KEY (type_id) REFERENCES banner_type (id)');
        $this->addSql('ALTER TABLE blog_post ADD CONSTRAINT FK_BA5AE01D12469DE2 FOREIGN KEY (category_id) REFERENCES blog_post_category (id)');
        $this->addSql('ALTER TABLE blog_post ADD CONSTRAINT FK_BA5AE01DC54C8C93 FOREIGN KEY (type_id) REFERENCES blog_post_type (id)');
        $this->addSql('ALTER TABLE blog_post_score ADD CONSTRAINT FK_43449957A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE blog_post_score ADD CONSTRAINT FK_434499574B89032C FOREIGN KEY (post_id) REFERENCES blog_post (id)');
        $this->addSql('ALTER TABLE course_score ADD CONSTRAINT FK_DB3230B9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE course_score ADD CONSTRAINT FK_DB3230B94B89032C FOREIGN KEY (post_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC54C8C93 FOREIGN KEY (type_id) REFERENCES product_type (id)');
        $this->addSql('ALTER TABLE product_asset ADD CONSTRAINT FK_A3F321001C83F75 FOREIGN KEY (prod_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_score ADD CONSTRAINT FK_93C54C0DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_score ADD CONSTRAINT FK_93C54C0D4B89032C FOREIGN KEY (post_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE quiz_answer ADD CONSTRAINT FK_3799BA7C1E27F6BF FOREIGN KEY (question_id) REFERENCES quiz_question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE quiz_question ADD CONSTRAINT FK_6033B00B853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE quiz_result ADD CONSTRAINT FK_FE2E314A853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE quiz_score ADD CONSTRAINT FK_CEEA4ED1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE quiz_score ADD CONSTRAINT FK_CEEA4ED14B89032C FOREIGN KEY (post_id) REFERENCES quiz (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64982CF74B1 FOREIGN KEY (racine_data_id) REFERENCES user_racine (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64941CEBD47 FOREIGN KEY (facebook_data_id) REFERENCES user_facebook_data (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649C9B19CBA FOREIGN KEY (google_data_id) REFERENCES user_google_data (id)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_course ADD CONSTRAINT FK_73CC7484A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_course ADD CONSTRAINT FK_73CC7484591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE user_facebook_data ADD CONSTRAINT FK_50845DFEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_favorite ADD CONSTRAINT FK_88486AD9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_google_data ADD CONSTRAINT FK_E0B08FF5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_point ADD CONSTRAINT FK_5567087CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_point ADD CONSTRAINT FK_5567087CF7A2C2FC FOREIGN KEY (badge_id) REFERENCES badge (id)');
        $this->addSql('ALTER TABLE user_racine ADD CONSTRAINT FK_9E1B3315A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_point DROP FOREIGN KEY FK_5567087CF7A2C2FC');
        $this->addSql('ALTER TABLE banner DROP FOREIGN KEY FK_6F9DB8E7C54C8C93');
        $this->addSql('ALTER TABLE blog_post_score DROP FOREIGN KEY FK_434499574B89032C');
        $this->addSql('ALTER TABLE blog_post DROP FOREIGN KEY FK_BA5AE01D12469DE2');
        $this->addSql('ALTER TABLE blog_post DROP FOREIGN KEY FK_BA5AE01DC54C8C93');
        $this->addSql('ALTER TABLE course_score DROP FOREIGN KEY FK_DB3230B94B89032C');
        $this->addSql('ALTER TABLE user_course DROP FOREIGN KEY FK_73CC7484591CC992');
        $this->addSql('ALTER TABLE product_asset DROP FOREIGN KEY FK_A3F321001C83F75');
        $this->addSql('ALTER TABLE product_score DROP FOREIGN KEY FK_93C54C0D4B89032C');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC54C8C93');
        $this->addSql('ALTER TABLE quiz_question DROP FOREIGN KEY FK_6033B00B853CD175');
        $this->addSql('ALTER TABLE quiz_result DROP FOREIGN KEY FK_FE2E314A853CD175');
        $this->addSql('ALTER TABLE quiz_score DROP FOREIGN KEY FK_CEEA4ED14B89032C');
        $this->addSql('ALTER TABLE quiz_answer DROP FOREIGN KEY FK_3799BA7C1E27F6BF');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE blog_post_score DROP FOREIGN KEY FK_43449957A76ED395');
        $this->addSql('ALTER TABLE course_score DROP FOREIGN KEY FK_DB3230B9A76ED395');
        $this->addSql('ALTER TABLE product_score DROP FOREIGN KEY FK_93C54C0DA76ED395');
        $this->addSql('ALTER TABLE quiz_score DROP FOREIGN KEY FK_CEEA4ED1A76ED395');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE user_course DROP FOREIGN KEY FK_73CC7484A76ED395');
        $this->addSql('ALTER TABLE user_facebook_data DROP FOREIGN KEY FK_50845DFEA76ED395');
        $this->addSql('ALTER TABLE user_favorite DROP FOREIGN KEY FK_88486AD9A76ED395');
        $this->addSql('ALTER TABLE user_google_data DROP FOREIGN KEY FK_E0B08FF5A76ED395');
        $this->addSql('ALTER TABLE user_point DROP FOREIGN KEY FK_5567087CA76ED395');
        $this->addSql('ALTER TABLE user_racine DROP FOREIGN KEY FK_9E1B3315A76ED395');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64941CEBD47');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649C9B19CBA');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64982CF74B1');
        $this->addSql('DROP TABLE badge');
        $this->addSql('DROP TABLE banner');
        $this->addSql('DROP TABLE banner_type');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('DROP TABLE blog_post_category');
        $this->addSql('DROP TABLE blog_post_score');
        $this->addSql('DROP TABLE blog_post_type');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE course_score');
        $this->addSql('DROP TABLE neo_tv_video');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_asset');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE product_classifiation_by_distrib_and_presc');
        $this->addSql('DROP TABLE product_score');
        $this->addSql('DROP TABLE product_type');
        $this->addSql('DROP TABLE quiz');
        $this->addSql('DROP TABLE quiz_answer');
        $this->addSql('DROP TABLE quiz_question');
        $this->addSql('DROP TABLE quiz_result');
        $this->addSql('DROP TABLE quiz_score');
        $this->addSql('DROP TABLE referrer_content');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE user_course');
        $this->addSql('DROP TABLE user_facebook_data');
        $this->addSql('DROP TABLE user_favorite');
        $this->addSql('DROP TABLE user_google_data');
        $this->addSql('DROP TABLE user_point');
        $this->addSql('DROP TABLE user_racine');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE ext_log_entries');
    }
}
