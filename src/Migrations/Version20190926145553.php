<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190926145553 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_asset ADD file_meta_title VARCHAR(255) DEFAULT NULL, ADD file_meta_description VARCHAR(510) DEFAULT NULL, DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_asset ADD image_meta_title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD image_meta_description VARCHAR(510) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE file_meta_title image_mobile_meta_title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE file_meta_description image_mobile_meta_description VARCHAR(510) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
