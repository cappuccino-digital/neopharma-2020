<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190827204913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_invite (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, share_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_A2B00BEAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_invite ADD CONSTRAINT FK_A2B00BEAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD invite_id INT DEFAULT NULL, ADD share_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649EA417747 FOREIGN KEY (invite_id) REFERENCES user_invite (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649EA417747 ON user (invite_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EA417747');
        $this->addSql('DROP TABLE user_invite');
        $this->addSql('DROP INDEX UNIQ_8D93D649EA417747 ON user');
        $this->addSql('ALTER TABLE user DROP invite_id, DROP share_code');
    }
}
