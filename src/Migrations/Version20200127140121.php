<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200127140121 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sales_force (id INT AUTO_INCREMENT NOT NULL, nm_regional VARCHAR(255) NOT NULL, nm_divisao VARCHAR(255) NOT NULL, sede_setor VARCHAR(255) NOT NULL, colaborador VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, hash VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE banner ADD `order` INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP company_name, DROP fantasy_name, DROP address, DROP number, DROP complement, DROP neighborhood, DROP reference_point, DROP zipcode');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sales_force');
        $this->addSql('ALTER TABLE banner DROP `order`');
        $this->addSql('ALTER TABLE user ADD company_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD fantasy_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD address VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD complement VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD neighborhood VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD reference_point VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD zipcode VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
