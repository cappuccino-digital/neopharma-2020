<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190920174443 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog_post ADD is_public TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE course ADD is_public TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD is_only_to_pharmaceutical TINYINT(1) DEFAULT NULL, ADD is_public TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz ADD is_public TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog_post DROP is_public');
        $this->addSql('ALTER TABLE course DROP is_public');
        $this->addSql('ALTER TABLE product DROP is_only_to_pharmaceutical, DROP is_public');
        $this->addSql('ALTER TABLE quiz DROP is_public');
    }
}
