<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191004120841 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_sistemic_class (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B93C2339989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD sistemic_class_id INT DEFAULT NULL, ADD internal_code VARCHAR(255) DEFAULT NULL, ADD presentation LONGTEXT DEFAULT NULL, ADD full_presentation LONGTEXT DEFAULT NULL, ADD sap_name LONGTEXT DEFAULT NULL, ADD ref_medicine LONGTEXT DEFAULT NULL, ADD composition LONGTEXT DEFAULT NULL, ADD is_psicotropic TINYINT(1) DEFAULT NULL, CHANGE is_active is_active TINYINT(1) DEFAULT NULL, CHANGE sistemic_class manager VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD68B82223 FOREIGN KEY (sistemic_class_id) REFERENCES product_sistemic_class (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD68B82223 ON product (sistemic_class_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD68B82223');
        $this->addSql('DROP TABLE product_sistemic_class');
        $this->addSql('DROP INDEX IDX_D34A04AD68B82223 ON product');
        $this->addSql('ALTER TABLE product ADD sistemic_class VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP sistemic_class_id, DROP manager, DROP internal_code, DROP presentation, DROP full_presentation, DROP sap_name, DROP ref_medicine, DROP composition, DROP is_psicotropic, CHANGE is_active is_active TINYINT(1) NOT NULL');
    }
}
