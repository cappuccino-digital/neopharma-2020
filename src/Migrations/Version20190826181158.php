<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826181158 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_facebook_data ADD name VARCHAR(255) NOT NULL, ADD email VARCHAR(255) NOT NULL, ADD access_token VARCHAR(510) DEFAULT NULL, ADD facebook_id VARCHAR(255) DEFAULT NULL, ADD expires_in VARCHAR(255) DEFAULT NULL, ADD signed_request VARCHAR(510) DEFAULT NULL, ADD data_access_expiration_time VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_google_data ADD name VARCHAR(255) NOT NULL, ADD email VARCHAR(255) NOT NULL, ADD google_id VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_facebook_data DROP name, DROP email, DROP access_token, DROP facebook_id, DROP expires_in, DROP signed_request, DROP data_access_expiration_time');
        $this->addSql('ALTER TABLE user_google_data DROP name, DROP email, DROP google_id');
    }
}
