<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190926132439 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE badge ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE banner ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE blog_post ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL, ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE course ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description VARCHAR(510) DEFAULT NULL, ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE neo_tv_video ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL, ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD banner_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD banner_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD banner_meta_title VARCHAR(255) DEFAULT NULL, ADD banner_meta_description VARCHAR(510) DEFAULT NULL, ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE product_asset ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL, ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description VARCHAR(510) DEFAULT NULL');
        $this->addSql('ALTER TABLE quiz_answer ADD image_mobile_meta_title VARCHAR(255) DEFAULT NULL, ADD image_mobile_meta_description VARCHAR(510) DEFAULT NULL, ADD image_meta_title VARCHAR(255) DEFAULT NULL, ADD image_meta_description VARCHAR(510) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE badge DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
        $this->addSql('ALTER TABLE banner DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
        $this->addSql('ALTER TABLE blog_post DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description, DROP meta_title, DROP meta_description');
        $this->addSql('ALTER TABLE course DROP meta_title, DROP meta_description, DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
        $this->addSql('ALTER TABLE neo_tv_video DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description, DROP meta_title, DROP meta_description');
        $this->addSql('ALTER TABLE product DROP banner_mobile_meta_title, DROP banner_mobile_meta_description, DROP banner_meta_title, DROP banner_meta_description, DROP meta_title, DROP meta_description');
        $this->addSql('ALTER TABLE product_asset DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
        $this->addSql('ALTER TABLE quiz DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description, DROP meta_title, DROP meta_description');
        $this->addSql('ALTER TABLE quiz_answer DROP image_mobile_meta_title, DROP image_mobile_meta_description, DROP image_meta_title, DROP image_meta_description');
    }
}
