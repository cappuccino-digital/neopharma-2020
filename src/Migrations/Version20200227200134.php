<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200227200134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questions');
        $this->addSql('ALTER TABLE promo_thirty_day_movement ADD points INT NOT NULL, DROP point');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(1) NOT NULL COLLATE utf8_general_ci, two VARCHAR(1) NOT NULL COLLATE utf8_general_ci, three VARCHAR(1) NOT NULL COLLATE utf8_general_ci, four VARCHAR(1) NOT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, ip_address VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE promo_thirty_day_movement ADD point VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, DROP points');
    }
}
