<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191004175928 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE presentation presentation VARCHAR(510) DEFAULT NULL, CHANGE full_presentation full_presentation VARCHAR(510) DEFAULT NULL, CHANGE sap_name sap_name VARCHAR(510) DEFAULT NULL, CHANGE ref_medicine ref_medicine VARCHAR(510) DEFAULT NULL, CHANGE composition composition LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE presentation presentation LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE full_presentation full_presentation LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sap_name sap_name LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE ref_medicine ref_medicine LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE composition composition VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
