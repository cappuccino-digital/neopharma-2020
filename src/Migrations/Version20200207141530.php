<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200207141530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_point_promo_thirty_day_movement (id INT AUTO_INCREMENT NOT NULL, user_promo_thirty_day_movement_id INT DEFAULT NULL, user_id INT DEFAULT NULL, point VARCHAR(100) NOT NULL, point_type VARCHAR(100) NOT NULL, point_type_description VARCHAR(100) NOT NULL, is_pharmaceutical TINYINT(1) NOT NULL, is_pharmaceutical_content TINYINT(1) NOT NULL, is_month_match TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A30B0A436F01A721 (user_promo_thirty_day_movement_id), INDEX IDX_A30B0A43A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_promo_thirty_day_movement (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, whatsapp VARCHAR(20) NOT NULL, instagram VARCHAR(50) NOT NULL, is_regulament TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CE8E5D0BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_point_promo_thirty_day_movement ADD CONSTRAINT FK_A30B0A436F01A721 FOREIGN KEY (user_promo_thirty_day_movement_id) REFERENCES user_promo_thirty_day_movement (id)');
        $this->addSql('ALTER TABLE user_point_promo_thirty_day_movement ADD CONSTRAINT FK_A30B0A43A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_promo_thirty_day_movement ADD CONSTRAINT FK_CE8E5D0BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE questions');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_point_promo_thirty_day_movement DROP FOREIGN KEY FK_A30B0A436F01A721');
        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(1) NOT NULL COLLATE utf8_general_ci, two VARCHAR(1) NOT NULL COLLATE utf8_general_ci, three VARCHAR(1) NOT NULL COLLATE utf8_general_ci, four VARCHAR(1) NOT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, ip_address VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE user_point_promo_thirty_day_movement');
        $this->addSql('DROP TABLE user_promo_thirty_day_movement');
    }
}
