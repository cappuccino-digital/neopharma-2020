<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200303201456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promo_thirty_day_movement_movies (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, thumb VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, link VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promo_thirty_day_movement_user_movie (id INT AUTO_INCREMENT NOT NULL, user_point_promo_thirty_day_movement_id INT DEFAULT NULL, user_id INT DEFAULT NULL, promo_thirty_day_movement_movies_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_BFD60ADE79A0C708 (user_point_promo_thirty_day_movement_id), INDEX IDX_BFD60ADEA76ED395 (user_id), INDEX IDX_BFD60ADEC6FD1F4E (promo_thirty_day_movement_movies_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promo_thirty_day_movement_user_movie ADD CONSTRAINT FK_BFD60ADE79A0C708 FOREIGN KEY (user_point_promo_thirty_day_movement_id) REFERENCES user_point_promo_thirty_day_movement (id)');
        $this->addSql('ALTER TABLE promo_thirty_day_movement_user_movie ADD CONSTRAINT FK_BFD60ADEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE promo_thirty_day_movement_user_movie ADD CONSTRAINT FK_BFD60ADEC6FD1F4E FOREIGN KEY (promo_thirty_day_movement_movies_id) REFERENCES promo_thirty_day_movement_movies (id)');
        $this->addSql('DROP TABLE questions');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE promo_thirty_day_movement_user_movie DROP FOREIGN KEY FK_BFD60ADEC6FD1F4E');
        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(1) NOT NULL COLLATE utf8_general_ci, two VARCHAR(1) NOT NULL COLLATE utf8_general_ci, three VARCHAR(1) NOT NULL COLLATE utf8_general_ci, four VARCHAR(1) NOT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, ip_address VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE promo_thirty_day_movement_movies');
        $this->addSql('DROP TABLE promo_thirty_day_movement_user_movie');
    }
}
