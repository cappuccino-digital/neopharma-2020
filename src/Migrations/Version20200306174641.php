<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200306174641 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promo_thirty_day_ranking_clerk_pharmaceutical (id INT AUTO_INCREMENT NOT NULL, user_promo_id_id INT DEFAULT NULL, user_id INT DEFAULT NULL, points INT NOT NULL, created_at DATETIME NOT NULL, updatet_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_9FBEC533F766B54 (user_promo_id_id), UNIQUE INDEX UNIQ_9FBEC53A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promo_thirty_day_ranking_clerk_pharmaceutical ADD CONSTRAINT FK_9FBEC533F766B54 FOREIGN KEY (user_promo_id_id) REFERENCES user_promo_thirty_day_movement (id)');
        $this->addSql('ALTER TABLE promo_thirty_day_ranking_clerk_pharmaceutical ADD CONSTRAINT FK_9FBEC53A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE questions');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(1) NOT NULL COLLATE utf8_general_ci, two VARCHAR(1) NOT NULL COLLATE utf8_general_ci, three VARCHAR(1) NOT NULL COLLATE utf8_general_ci, four VARCHAR(1) NOT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, ip_address VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE promo_thirty_day_ranking_clerk_pharmaceutical');
    }
}
