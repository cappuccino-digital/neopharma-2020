<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190911192412 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE badge_level (id INT AUTO_INCREMENT NOT NULL, badge_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, image_mobile VARCHAR(255) DEFAULT NULL, min_point_pharmaceutical INT DEFAULT NULL, min_point_clerk INT DEFAULT NULL, INDEX IDX_FE256A3FF7A2C2FC (badge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE badge_level ADD CONSTRAINT FK_FE256A3FF7A2C2FC FOREIGN KEY (badge_id) REFERENCES badge (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE badge ADD given_point_pharmaceutical INT NOT NULL, ADD given_point_clerk INT NOT NULL, DROP min_point, DROP given_point');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE badge_level');
        $this->addSql('ALTER TABLE badge ADD min_point INT DEFAULT NULL, ADD given_point INT DEFAULT NULL, DROP given_point_pharmaceutical, DROP given_point_clerk');
    }
}
