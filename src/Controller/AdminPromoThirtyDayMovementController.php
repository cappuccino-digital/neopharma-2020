<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\PromoThirtyDayPostInstagram;
use App\Entity\PromoThirtyDayPostInstragramPoint;

class AdminPromoThirtyDayMovementController extends Controller
{
    /**
     * @Route("/admin/campanha/30dias-de-movimento/post-instagram", name="admin_promothirtyday_import_post")
     */
    public function index(Request $request)
    {
        set_time_limit(0);

        $em = $this->getDoctrine()->getManager();
        
        if ($request->files->has('post') && $request->isMethod('POST')) {
            $files = $request->files->get('post');
            
            $csv_array = $this->import_csv_to_array($files);
            
            foreach ($csv_array as $key => $csv) {
               
                $date_public = str_replace("/", "-", $csv['data_publicacao']);
                $date_collect = str_replace("/", "-", $csv['data_coleta']);
                
                $date_public = new \DateTime(date('Y-m-d H:i:s', strtotime($date_public)));
                $date_collect = new \DateTime(date('Y-m-d H:i:s', strtotime($date_collect)));
               
                $post_instagram = $em->getRepository('App:PromoThirtyDayPostInstagram')->findBy(['username'=> $csv['nome_instagram'], 'date_public_at' => $date_public]);
                
                if (!$post_instagram) {
                    $post_instagram = new PromoThirtyDayPostInstagram(); //salvando os ´post do instagram
                    $post_instagram->setUsername($csv['nome_instagram']);
                    $post_instagram->setDateCollectAt($date_collect);
                    $post_instagram->setPost($csv['permalink']);
                    $post_instagram->setDatePublicAt($date_public);
                    $post_instagram->setCreatedAt(new \DateTime());

                    $em->persist($post_instagram);
                    $em->flush();

                    $user_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOrInstagram($csv['nome_instagram']);

                    if ($user_promo) { // verifico se está participando da promoção e se o instragram está correto
                        if ($post_instagram->getId()) {

                            $check_post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findPostDatePublic($date_public, $user_promo->getId());

                            if (!$check_post_instagram) { //conferindo se existe mais de um post do mesmo dia 

                                $is_exist_point = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $user_promo->getId()]);

                                $point_extra = 0;

                                if (!$is_exist_point) {
                                    $point_extra = 2;
                                } else if (count($is_exist_point) == 7)  {
                                    $point_extra = 4;
                                } else if (count($is_exist_point) == 14) {
                                    $point_extra = 6;
                                } else if (count($is_exist_point) == 21) {
                                    $point_extra = 8;
                                } else if (count($is_exist_point) == 30) {
                                    $point_extra = 16;
                                }
                                
                                $post_instagram_point = new PromoThirtyDayPostInstragramPoint(); //a cada uma foto 1 ponto e mais 2 pontos extras
                                $post_instagram_point->setPostInstagram($post_instagram);
                                $post_instagram_point->setUserPromo($user_promo);
                                $post_instagram_point->setUser($user_promo->getUser());
                                $post_instagram_point->setPoint(1);
                                $post_instagram_point->setPointExtra($point_extra);
                                $post_instagram_point->setDatePublicAt($date_public);
                                $post_instagram_point->setCreatedAt(new \DateTime());

                                $em->persist($post_instagram_point);
                                $em->flush();
                                
                            //     $count_post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $user_promo->getId()]);

                            //     $point_extra = 0; 
                            //    /// dump(count($count_post_instagram)); die();
                            //     if (count($count_post_instagram) == 7)  {
                            //         $point_extra = 4;
                            //     } else if (count($count_post_instagram) == 14) {
                            //         $point_extra = 6;
                            //     } else if (count($count_post_instagram) == 21) {
                            //         $point_extra = 8;
                            //     } else if (count($count_post_instagram) == 30) {
                            //         $point_extra = 16;
                            //     }

                            //     if ($point_extra > 0 ) {
                            //         $post_instagram_point = new PromoThirtyDayPostInstragramPoint(); //adicionando pontuação extra
                            //         $post_instagram_point->setPostInstagram($post_instagram);
                            //         $post_instagram_point->setUserPromo($user_promo);
                            //         $post_instagram_point->setUser($user_promo->getUser());
                            //         $post_instagram_point->setPoint(1);
                            //         $post_instagram_point->setPointExtra($point_extra);
                            //         $post_instagram_point->setDatePublicAt($date_public);
                            //         $post_instagram_point->setCreatedAt(new \DateTime());

                            //         $em->persist($post_instagram_point);
                            //         $em->flush();
                            //     }
                            }
                        }
                    }
                        
                }
            }

            $this->addFlash("success", "Posts instagram importado com sucesso.");
            
        }

        $user_post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findUserPostInstagram();
        //dump($post_instagram); die();

        $posts = array();
        $point = 0;

        foreach ($user_post_instagram as $key => $post_row) {

            $point_post_instagram = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $post_row['userPromo']]);
            $point = 0;

            foreach ($point_post_instagram as $key => $post) {
                $point += $post->getPoint() + $post->getPointExtra();
            }

            $posts[] = array('user_promo_id' => $post->getUserPromo()->getId(), 'name' => $post->getUser()->getName(), 'whatsapp' => $post->getUserPromo()->getWhatsapp(), 'instagram' => $post->getUserPromo()->getInstagram(), 'point'=> $point);
        }   

        $posts = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->getInt('page', 1),
            10
        );
       // dump($posts); die();
        return $this->render('admin_promo_thirty_day_movement/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/admin/campanha/30dias-de-movimento/post-instagram/exibir/{user_promo}", name="admin_promothirtyday_detail_post")
     */
    public function postDetail($user_promo, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('App:PromoThirtyDayPostInstragramPoint')->findBy(['user_promo' => $user_promo]);
        //dump($posts); die();
        return $this->render('admin_promo_thirty_day_movement/detail.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     *
     * This function allows you to import a CSV file and export it into a PHP array
     *
     * @param string $file      The file you want to import the data from
     * @param string $enclosure The type of enclosure used in the CSV file
     *
     * @return array            The array containing the CSV infos
     */
    function import_csv_to_array($file, $enclosure = '"')
    {

        // Let's get the content of the file and store it in the string
        $csv_string = file_get_contents($file);

        // Let's detect what is the delimiter of the CSV file
        $delimiter = $this->detect_delimiter($csv_string);

        // Get all the lines of the CSV string
        $lines = explode("\n", $csv_string);

        // The first line of the CSV file is the headers that we will use as the keys
        $head_temp = str_getcsv(array_shift($lines),$delimiter,$enclosure);

        foreach ($head_temp as $key => $h) { //removendo os espaços do nome das colunas 
            $head[$key] = str_replace(' ', '_', strtolower($h));
        }
       
        $array = array();

        // For all the lines within the CSV
        foreach ($lines as $line) {

            // Sometimes CSV files have an empty line at the end, we try not to add it in the array
            if(empty($line)) {
                continue;
            }

            // Get the CSV data of the line
            $csv = str_getcsv($line,$delimiter,$enclosure);

            // Combine the header and the lines data
            $array[] = array_combine( $head, $csv );

        }

        // Returning the array
        return $array;
    }

    /**
     *
     * This function detects the delimiter inside the CSV file.
     *
     * It allows the function to work with different types of delimiters, ";", "," "\t", or "|"
     *
     *
     *
     * @param string $csv_string    The content of the CSV file
     * @return string               The delimiter used in the CSV file
     */
    function detect_delimiter($csv_string)
    {

        // List of delimiters that we will check for
        $delimiters = array(';' => 0,',' => 0,"\t" => 0,"|" => 0);

        // For every delimiter, we count the number of time it can be found within the csv string
        foreach ($delimiters as $delimiter => &$count) {
            $count = substr_count($csv_string,$delimiter);
        }

        // The delimiter used is probably the one that has the more occurrence in the file
        return array_search(max($delimiters), $delimiters);

    }

}
