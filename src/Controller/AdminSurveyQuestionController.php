<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\SurveyQuestion;
use App\Form\SurveyQuestionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/enquete")
 */
class AdminSurveyQuestionController extends Controller
{
    /**
     * @Route("/", name="admin_survey_question")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $surveys = $em->getRepository('App:SurveyQuestion')->findBy([], ['created_at' => 'DESC']);
       
        
        return $this->render('admin_survey_question/index.html.twig', [
            'surveys' => $surveys
        ]);
    }
 
     /**
      * @Route("/novo", name="admin_survey_question_new")
      */
     public function new(Request $request)
     {
        $faq_question = new SurveyQuestion();
        $form = $this->createForm(SurveyQuestionType::class, $faq_question);
        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $faq_question = $form->getData();
            
            $faq_question->setCreatedAt(new \DateTime());
            // $faq_question->setUpdatedAt(new \DateTime());
            $em->persist($faq_question);
            $em->flush();

            $this->addFlash('success', 'Enquete cadastrada com sucesso.');

            return $this->redirectToRoute('admin_survey_question');
        }

        return $this->render('admin_survey_question/new.html.twig', [
            'form' => $form->createView(),
        ]);
     }
 
     /**
      * @Route("/editar/{id}", name="admin_survey_question_edit")
      * @ParamConverter("id", class="App\Entity\SurveyQuestion", options={"id": "id"})
      */
     public function edit(SurveyQuestion $survey_question, Request $request)
     {        
        $form = $this->createForm(SurveyQuestionType::class, $survey_question);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        
        if ($form->isSubmitted() && $form->isValid()) {
            $survey_question = $form->getData();

            // $faq_question->setUpdatedAt(new \DateTime());
            $em->persist($survey_question);
            $em->flush();

            $this->addFlash('success', 'Enquete atualizado com sucesso.');

            return $this->redirectToRoute('admin_survey_question');
        }
 
        return $this->render('admin_survey_question/edit.html.twig', [
            'form' => $form->createView(),
            'survey_question' => $survey_question
        ]);
     }
 
     /**
      * @Route("/{id}/deletar", name="admin_survey_question_delete")
      * @ParamConverter("id", class="App\Entity\SurveyQuestion", options={"id": "id"})
      */
     public function delete(Request $request, SurveyQuestion $survey_question)
     { 
        $form = $this->createDeleteForm($survey_question);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository('App:SurveyQuestionResponse')->findOneBy(['question' => $survey_question]);

        if ($question) {
            $this->addFlash('danger', 'Não foi possível excluir! Enquete está vinculada a resposta do usuário.');

            return $this->redirectToRoute('admin_survey_question');
        }
        
        $em->remove($survey_question);
        $em->flush();

        $this->addFlash('success', 'Enquete deletado com sucesso.');

        return $this->redirectToRoute('admin_survey_question');
     }
 
     /**
      * Creates a form to delete a user entity.
      *
      * @param SurveyQuestion $survey_question The user entity
      *
      * @return \Symfony\Component\Form\Form The form
      */
     private function createDeleteForm(SurveyQuestion $survey_question)
     {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_survey_question_delete', array('id' => $survey_question->getId())))
            ->setMethod('DELETE')
            ->getForm();
     }
}
