<?php

namespace App\Controller;

use App\Entity\OrientMe;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;
use App\Service\OrienteMeService;
use Symfony\Component\HttpFoundation\Cookie;
class OrientemeController extends Controller
{
    /**
     * @Route("/oriente-me", name="orienteme")
     */
    public function index(Request $request)
    {
        return $this->redirectToRoute('home');
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        // if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
        //     $em = $this->getDoctrine()->getManager();
        //     $user = $this->getUser();

        //     $isOrient = $em->getRepository('App:OrientMe')->findOneBy(['user' => $user->getId()]);
            
        //     if ($isOrient) {
        //         return $this->redirectToRoute('orienteme_info');
        //     }
        // }

        return $this->render('orienteme/index.html.twig', [
            'controller_name' => 'OrientemeController',
        ]);
    }

    /**
     * @Route("/oriente-me/cadastro", name="orienteme_save")
     */
    public function register(Request $request, \Swift_Mailer $mailer)
    {
        return $this->redirectToRoute('home');
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }
           
        // if ($request->request->has('r')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $isOrient = $em->getRepository('App:OrientMe')->findOneBy(['user' => $user->getId()]);
          
            if ($isOrient) {
                return $this->redirectToRoute('orienteme_info');
            }

            $logger = new Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                       
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo(['Leonardo.Bonavina@cappuccinodigital.com.br']);
        
            $message
            ->setReplyTo($user->getEmail())
            ->setSubject('Portal Neo Pharma - Oriente Me')
            ->setBody(
                $this->renderView(
                    'orienteme/mail.html.twig',
                    ['user' => $user]
                ),
                'text/html'
            );

            $response = $mailer->send($message); //dispara o e-mail

            $orient = new OrientMe(); //Grava no banco
            $orient->setUser($user);
            $orient->setAcceptRegulation(true);
            $orient->setCreatedAt(new DateTime());

            if (!empty($user->getCpf())) { //validação CPF
                //Integração API Oriente ME
                $orienteme_service = new OrienteMeService( 
                    $this->getParameter('orienteme_api'),
                    $this->getParameter('orienteme_token')
                );

                $inserir_paciente = $orienteme_service->inserirPacientePagoComCPF($user->getCpf());
                
                if ($inserir_paciente['status'] && $this->getUser()->getCpf()) {
                    $orient->setIsRegisterApi(true);
                }
            }
        
            if($response == 1){
                $orient->setWasSent(true);
            } else {
                $orient->setWasSent(false);
            }
    
            $em = $this->getDoctrine()->getManager();
            $em->persist($orient);
            $em->flush();

            return $this->redirect('orienteme');
            //return $this->redirect('https://app.orienteme.com.br/Pacientes/');
        // }
    }

    /**
     * @Route("/oriente-me/informacoes", name="orienteme_info")
     */
    public function info(Request $request)
    {
        return $this->redirectToRoute('home');
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        } 
        
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $isOrient = $em->getRepository('App:OrientMe')->findOneBy(['user' => $user->getId()]);
            
            if ($isOrient) {
                return $this->redirect('https://app.orienteme.com.br/Pacientes/');
            }
        }

        return $this->redirect('orienteme');
    }

    /**
     * @Route("/oriente-me/regulamento", name="orienteme_regulamento")
     */
    public function orientemeRegulamento(Request $request)
    {
        return $this->redirectToRoute('home');
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $isOrient = $em->getRepository('App:OrientMe')->findOneBy(['user' => $user->getId()]);
            
            if ($isOrient) {
                return $this->redirectToRoute('orienteme_info');
            }
        }

        return $this->render('orienteme/regulamento.html.twig', [
            
        ]);
    }

}
