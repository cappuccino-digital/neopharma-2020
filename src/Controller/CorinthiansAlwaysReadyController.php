<?php

namespace App\Controller;

use App\Entity\CorinthiansAlwaysReadyCompanion;
use App\Entity\UserCorinthiansAlwaysReady;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;

class CorinthiansAlwaysReadyController extends Controller
{
    /**
     * @Route("/corinthians-x-always-ready", name="corinthians-always-ready")
     */
    public function index(Request $request, \Swift_Mailer $mailer) 
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }

        //remover essa redenrização somente na quarta feira para liberar o formulário
        return $this->render('corinthians_always_ready/teaser.html.twig', []); die();

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //apenas 10 pessoas pode pedir os ingressos
        $user_tickets_corinthians_all = $em->getRepository('App:UserCorinthiansAlwaysReady')->findAll();

        if (count($user_tickets_corinthians_all) >= 3) {
            return $this->redirectToRoute('corinthians-always-ready-result', ['result'=> 'ingressos-esgotados']);
        }

        //dump($user); die();
        if ($request->request->has('tc')) {
            $error = array();

            $name = trim($request->request->get('name'));
            $zip_code = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address = trim($request->request->get('address'));
            $number = trim($request->request->get('number'));
            $complement = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city = trim($request->request->get('city'));
            $state = trim($request->request->get('state'));
            $cellphone = trim($request->request->get('cellphone'));
            $email = trim($request->request->get('email'));
            //acompanhante
            $name_companion = trim($request->request->get('name_companion'));
            $cellphone_companion = trim($request->request->get('cellphone_companion'));
            $email_companion = trim($request->request->get('email_companion'));
    
            if ((!$this->checkCity($city, $state)) || ($state != 'SP')) {
                $url = $request->getSchemeAndHttpHost() . '/corinthians-x-always-ready/ingressos-indisponivel';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            if ($state != 'SP') {
                $error['state'] = 'Estado não permitido.';
            } 
            if ($name) {
                $user->setName($request->request->get('name'));
            } else {
                $error['name'] = 'O nome é obrigatório.';
            }

            if ($zip_code) {
                $user->setZipCode($zip_code);
            } else {
                $error['zip_code'] = 'O CEP é obrigatório.';
            }
            
            if ($address) {
                $user->setAddress($address);
            } else {
                $error['address'] = 'O endereço é obrigatório.';
            }
            
            if ($number) {
                $user->setNumber($number);
            } else {
                $error['number'] = 'O número é obrigatório.';
            }
            
            if ($complement) {
                $user->setComplement($complement);
            } else {
                //$error['complement'] = 'O complemento é obrigatório.';
            }
            
            if ($neighborhood) {
                $user->setNeighborhood($neighborhood);
            } else {
                $error['neighborhood'] = 'O bairro é obrigatório.';
            }
            
            if ($city) {
                $user->setCity($city);
            } else {
                $error['city'] = 'A cidade é obrigatória.';
            }
            
            if ($state) {
                $user->setEstateAddress($state);
            } else {
                $error['state'] = 'O estado é obrigatório.';
            }
           
            if ($cellphone) {
                if (strlen($cellphone) < 15) { //contando com as maskaras
                    $error['cellphone'] = "Celular inválido";
                } else {
                    $user->setCellphone($cellphone);
                }
            } else {
                $error['cellphone'] = 'O celular é obrigatório.';
            }

            if ($email) {
                $user->setEmail($email);
                if ($user->getIsPharmacy()) {
                     
                } else {
                    $user->setUsername($email);
                }
            } else {
                $error['email'] = 'O celular é obrigatório.';
            }

            //validação e-mail principal
            if (!$email) {
                $error['email'] = 'O e-mail é obrigatório';
            } else {

                if ($email != $user->getEmail()) {
                   
                    $user_check_email = $em->getRepository('App:User')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email or u.username = :username')
                            ->setParameter('email', $email)
                            ->setParameter('username', $email)
                            ->getQuery()
                            ->getResult();
                    
                    if ($user_check_email) {
                        $error['email'] = 'Email está sendo utilizado.';
                    }

                    $user_participant_check = $em->getRepository('App:UserCorinthiansAlwaysReady')->createQueryBuilder('uc')
                                        ->innerJoin('App:User', 'u' , 'WITH' , 'uc.user = u.id')
                                        ->where('u.email = :email or u.username = :username')
                                        ->setParameter('email', $email)
                                        ->setParameter('username', $email)
                                        ->getQuery()
                                        ->getResult();

                    if ($user_participant_check) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                } else {
                    
                    $user_companion_check_email = $em->getRepository('App:CorinthiansAlwaysReadyCompanion')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email')
                            ->setParameter('email', $email)
                            ->getQuery()
                            ->getResult();

                    if ($user_companion_check_email) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                }
                
                //validação e-mail
                if (!$this->validaEmail($email)) {
                    $error['email'] = 'E-mail inválido';
                }
            }

            //acompanhante
            if (!$name_companion) {           
                $error['name_companion'] = 'O nome é obrigatório.';
            }
            
            if (!$cellphone_companion) {
                $error['cellphone_companion'] = 'O celular é obrigatório.';
            } else {
                
                if (strlen($cellphone_companion) < 15) { //contando com as maskaras
                    $error['cellphone_companion'] = "Celular inválido";
                }
            }

            if (!$email_companion) {
                $error['email_companion'] = 'O e-mail é obrigatório.';
            } else {

                $is_check_email_companion = false;

                $user_check_corinthians_always_ready = $em->getRepository('App:UserCorinthiansAlwaysReady')->findOneBy(['user' => $user]);

                if ($user_check_corinthians_always_ready) {
                    $user_check_companion_corinthians_always_ready = $em->getRepository('App:CorinthiansAlwaysReadyCompanion')->findOneBy(['user_corinthians_always_ready' => $user_check_corinthians_always_ready]);
                    
                    if (($email_companion != $user_check_companion_corinthians_always_ready->getEmail())) {
                        $is_check_email_companion = true;                       
                    }
                } else {
                    $is_check_email_companion = true;
                }

                if ($is_check_email_companion) {
                    $user_companion_check_email = $em->getRepository('App:CorinthiansAlwaysReadyCompanion')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email')
                            ->setParameter('email', $email_companion)
                            ->getQuery()
                            ->getResult();

                    if ($user_companion_check_email) {
                        $error['email_companion'] = 'Email está sendo utilizado';
                    }

                    $user_participant_check = $em->getRepository('App:UserCorinthiansAlwaysReady')->createQueryBuilder('uc')
                                            ->innerJoin('App:User', 'u' , 'WITH' , 'uc.user = u.id')
                                            ->where('u.email = :email or u.username = :username')
                                            ->setParameter('email', $email_companion)
                                            ->setParameter('username', $email_companion)
                                            ->getQuery()
                                            ->getResult();

                    if ($user_participant_check) {
                        $error['email_companion'] = 'Email está sendo utilizado';
                    }
                }


                //validação e-mail
                if (!$this->validaEmail($email_companion)) {
                    $error['email_companion'] = 'E-mail inválido';
                }
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }
            
            $em = $this->getDoctrine()->getManager();
            $user_tickets_corinthians = $em->getRepository('App:UserCorinthiansAlwaysReady')->findOneBy(['user' => $user]);

            if (!$user_tickets_corinthians) {
                //cadastro do usuário para receber os ingressos
                $user_tickets_corinthians = new UserCorinthiansAlwaysReady();  
                $user_tickets_corinthians->setUser($user); 
                $user_tickets_corinthians->setCreatedAt(new \DateTime());
                $user_tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($user_tickets_corinthians);
                $em->flush();

                //cadastro acompanhante
                $tickets_corinthians = new CorinthiansAlwaysReadyCompanion(); 
                $tickets_corinthians->setUserCorinthiansAlwaysReady($user_tickets_corinthians); 
                $tickets_corinthians->setName($name_companion); 
                $tickets_corinthians->setCellphone($cellphone_companion); 
                $tickets_corinthians->setEmail($email_companion); 
                $tickets_corinthians->setCreatedAt(new \DateTime());
                $tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($tickets_corinthians);
                $em->flush();

                //update info user
                $em->persist($user);
                $em->flush();

                $emails = ['wellington.santos@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br'];
                //$emails = ['wellington.santos@cappuccinodigital.com.br'];
                    
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo($emails);
              
                $message
                    ->setReplyTo($email)
                    ->setSubject('Portal Neo Pharma - Ingressos do jogo do Corinthians')
                    ->setBody(
                      'Nome: ' . $user->getName() . ', ' . 
                      'CEP: ' . $user->getZipCode() . ', ' . 
                      'Endereço: ' . $user->getAddress() . ', ' . 
                      'Número: ' . $user->getNumber() . ', ' . 
                      'Bairro: ' . $user->getNeighborhood() . ', ' . 
                      'Cidade: ' . $user->getCity() . ', ' . 
                      'Estado: ' . $user->getState() . ', ' . 
                      'Celular: ' . $user->getCellphone() . ', ' . 
                      'E-mail: ' . $user->getEmail() . ' '. 
                      ' Dados do Acompanhante '. ', ' . 
                      'Nome: ' . $name_companion . ', ' . 
                      'Celular: ' . $cellphone_companion . ', ' . 
                      'E-mail: ' . $email_companion 
                    );

                $response = $mailer->send($message);

                $url = $request->getSchemeAndHttpHost() . '/corinthians-x-always-ready/inscricao-realizada-com-sucesso';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            return new JsonResponse([
                'status' => false,
                'is_register' => true, 
                'message' => 'Você já fez o cadastro, aguarde as instruções que serão enviadas por e-mail'
            ]);
        }

        $corinthians_always_ready_companion = [];
        $user_corinthians_always_ready = $em->getRepository('App:UserCorinthiansAlwaysReady')->findOneBy(['user' => $user]);
        if ($user_corinthians_always_ready) {
            $corinthians_always_ready_companion = $em->getRepository('App:CorinthiansAlwaysReadyCompanion')->findOneBy(['user_corinthians_always_ready' => $user_corinthians_always_ready]);  
        }
      
        return $this->render('corinthians_always_ready/index.html.twig', [
            'corinthians_always_ready_companion' => $corinthians_always_ready_companion
        ]);
    }

    /**
     * @Route("/corinthians-x-always-ready/{result}", name="corinthians-always-ready-result", defaults={"result"=null})
     */
    public function result(Request $request, $result)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        //remover essa redenrização somente na quarta feira para liberar o formulário
        return $this->redirectToRoute('corinthians-always-ready'); die();

        $array = ['inscricao-realizada-com-sucesso', 'ingressos-esgotados', 'ingressos-indisponivel'];

        if (!in_array($result, $array)) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('corinthians_always_ready/result.html.twig', [
            'result' => $result
        ]);
    }

    // /**
    //  * @Route("/corinthians-x-always-ready/pagina/teaser", name="corinthians-always-ready-teaser")
    //  */
    // public function teaser(Request $request) 
    // {
    //     return $this->render('corinthians_always_ready/teaser.html.twig', []);
    // }

    private function checkCity($city, $state)
    {
        $array = [
            'Arujá',
            'Barueri',
            'Biritiba Mirim',
            'Caieiras',
            'Cajamar',
            'Carapicuíba',
            'Cotia',
            'Diadema',
            'Embu das Artes',
            'Embu-Guaçu',
            'Ferraz de Vasconcelos',
            'Francisco Morato',
            'Franco da Rocha',
            'Guararema',
            'Guarulhos',
            'Itapecerica da Serra',
            'Itapevi',
            'Itaquaquecetuba',
            'Jandira',
            'Juquitiba',
            'Mairiporã',
            'Mauá',
            'Mogi das Cruzes',
            'Osasco',
            'Pirapora do Bom Jesus',
            'Poá',
            'Ribeirão Pires',
            'Rio Grande da Serra',
            'Salesópolis',
            'Santa Isabel',
            'Santana de Parnaíba',
            'Santo André',
            'São Bernardo do Campo',
            'São Caetano do Sul',
            'São Lourenço da Serra',
            'São Paulo',
            'Suzano',
            'Taboão da Serra',
            'Vargem Grande Paulista'
        ];

        if ($state != 'SP') {
            return false;
        }

        if (!in_array($city, $array)) {
            return false;
        }

        return true;
    }

    private function validaEmail($mail){
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }
}
