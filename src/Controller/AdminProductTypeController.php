<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\ProductType;

/**
 * @Route("/admin/product/type")
 */
class AdminProductTypeController extends Controller
{
    /**
     * @Route("", name="admin_product_type_list")
     */
    public function index(Request $request)
    {
        $builder = $this
        ->getDoctrine()
        ->getRepository('App:ProductType')
        ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $types = $builder->getQuery()->getResult();

        $types = $this->get('knp_paginator')->paginate(
            $types,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('admin_product_type/list.html.twig', [
            'types' => $types
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_type_new")
     */
    public function productTypeNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_product_type/new.html.twig', []);
        }

        $form = $request->request->get('form');

        $type = new ProductType();
        $type->setName($form['name']);
        $type->setIsActive(isset($form['isActive']) ? ($form['isActive'] == 'on' ? true : false ): false );
        $type->setCreatedAt(new \DateTime());
        $type->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($type);
        $em->flush();

        return $this->redirectToRoute('admin_product_type_list');
    }
    
    /**
     * @Route("/edit/{id}", name="admin_product_type_edit")
     */
    public function productTypeEdit($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('App:ProductType')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_product_type/edit.html.twig', [
                'type' => $type
            ]);
        }
        $form = $request->request->get('form');

        $type->setName($form['name']);
        $type->setIsActive(isset($form['isActive']) ? ($form['isActive'] == 'on' ? true : false ): false );
        $type->setUpdatedAt(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($type);
        $em->flush();

        return $this->redirectToRoute('admin_product_type_list');
    }
}
