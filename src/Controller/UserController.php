<?php

namespace App\Controller;

use App\Entity\UserRegisterIncomplete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\NeoPharmaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;
/**
 * @Route("/usuario")
 */
class UserController extends Controller
{
    /**
     * @Route("/favoritos", name="user-favorites")
     */
    public function userFavorites(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home-login');
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        $em = $this->getDoctrine()->getManager();	
        $user = $this->getUser();
        
        $favorites = $em->getRepository('App:UserFavorite')->getUserFavoriteContent($user->getId());

        return $this->render('user/favorites.html.twig', [
            'favorites' => $favorites
        ]);
    }

    /**
     * @Route("/convidar-amigo", name="user-invite")
     */
    public function userInvite(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home-login');
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        $user = $this->getUser();

        if(!$user->getShareCode() ||$user->getShareCode() == ''){
            $user->setShareCode($this->generateShareCode($user));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('user/invite.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/{paramenter}", name="user-home", defaults={"paramenter"=null})
     */
    public function userData(Request $request, $paramenter, UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();	
        
        //request 
        $occupation = trim($request->request->get('occupation'));

        if ($request->isMethod('GET')) {
            //validação para evitar roubo de dados confidenciais 
            if ($request->query->has('username') || $request->query->has('password')) {
                throw $this->createNotFoundException('A página não foi encontrada');
            }

            if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                /** Gravando a referencia do conteúdo */
                $url_reference = $request->getUri();
                $response = $this->redirectToRoute('home-login');
                $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

                return $response;
                /*** END  */
            }

            //regra para gestor de loja
            if ($user->getIsPharmacy()) {
                $user_company_name = $em->getRepository('App:User')->findOneBy(['id' => $user->getId(), 'isPharmacy' => true]);
                
                if (!$user_company_name->getCompanyName()) {
                    $pharmacies_elegible = $em->getRepository('App:PharmaciesElegible')->findOneBy(['cnpj' => $user->getCnpj(), 'is_active' => true]);
                   
                    if ($pharmacies_elegible) {
                        $user->setCompanyName($pharmacies_elegible->getCompanyName());
                    }
                }
                
                return $this->render('user/user_gestor_de_loja.html.twig', [
                    'user' => $user
                ]);  
            }

            /** corrigindo cadastros que estão incompletos */
            $isIncomplete = false;
            if ($paramenter == 'cadastro-incompleto') { 
                $isIncomplete = true;
            } else if ($occupation == 'balconista' && !trim($user->getCnpj())) {
                $isIncomplete = true;
            } else if ($occupation == 'farmaceutico' && !trim($user->getCrf())) {
                $isIncomplete = true;
            }

            if ($isIncomplete) { 
                $field = null;
                if ($occupation == 'balconista') {
                    $field = 'cnpj';
                } else if ($occupation == 'farmaceutico') {
                    $field = 'crf';
                }

                if ($field) {
                    $user_register = $em->getRepository('App:UserRegisterIncomplete')->findOneBy(['user' => $user, 'field' => $field]);

                    if (!$user_register) {
                        $user_register =  new UserRegisterIncomplete();
                    
                        $user_register->setUser($user);
                        $user_register->setIsRegisterComplete(true);
                        $user_register->setIsGestor($occupation == 'gestor' ? true : false);
                        $user_register->setIsPharmaceutical($occupation == 'farmaceutico' ? true : false);
                        $user_register->setIsClerk($occupation == 'balconista' ? true : false);
                        $user_register->setIsSocialNetwork(false);
                        $user_register->setField($field);
                        $user_register->setCreatedAt(new \DateTime());
                        $em->persist($user_register);
                        $em->flush();

                        $this->addFlash("success", "Cadastro completado com sucesso.");
                    }
                }
                
             }
             /** end */

            return $this->render('user/index.html.twig', [
                'user' => $user
            ]);
        }

        //request 
        $name = trim($request->request->get('name'));
        $email = trim($request->request->get('email'));
        $phone = trim($request->request->get('phone'));
        $cpf = str_replace(['.', ' ', '-'], '', trim($request->request->get('cpf')));
        $cnpj = str_replace(['.', ' ', '-', '/'], '', trim($request->request->get('cnpj')));
        $crf = trim($request->request->get('crf'));
        $state = trim($request->request->get('state'));
        $estate_address = trim($request->request->get('estate_address'));
        $city = trim($request->request->get('city'));
        $birthday = trim($request->request->get('birthday'));
        $gender = trim($request->request->get('gender'));
        $educationalLevel = trim($request->request->get('educationalLevel'));
        $cellphone = trim($request->request->get('phone'));
        $newsletter = trim($request->request->get('newsletter'));
        $promotion = trim($request->request->get('promotion'));
        $smsNotification = trim($request->request->get('smsNotification'));
        //$interests = trim($request->request->get('interests'));
        $password = trim($request->request->get('password'));
        $passwordOld = trim($request->request->get('passwordOld'));
        $passwordRepeat = trim($request->request->get('passwordRepeat'));
        $interests = ($request->request->get('interests') ? json_encode($request->request->get('interests')) : null);
        $team_football = trim($request->request->get('team_football_other')) != '' ? $request->request->get('team_football_other') : trim($request->request->get('team_football'));

        $zip_code = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
        $address = trim($request->request->get('address'));
        $number = trim($request->request->get('number'));
        $complement = trim($request->request->get('complement'));
        $neighborhood = trim($request->request->get('neighborhood'));

        //validação 
        $errors = array();
        
        if ($email != $user->getEmail()) {
            $userCheckEmail = $em->getRepository('App:User')
                ->createQueryBuilder('u')
                ->andWhere('u.email = :email or u.username = :username')
                ->setParameter('email', $email)
                ->setParameter('username', $email)
                ->getQuery()
                ->getResult();

            if ($userCheckEmail) {
                $errors['email'] = "Email está sendo utilizado";
            }
        }

        if (!$name) {
            $errors['name'] = 'O nome é obrigatório';
        } 
        
        if (!$email) {
            $errors['email'] = 'O e-mail é obrigatório';
        }

        if (!$phone) {
            $errors['phone'] = 'O Celular é obrigatório';
        } 

        if (!$cpf) {
            $errors['cpf'] = 'O CPF é obrigatório';
        } 

        if ($occupation == 'farmaceutico') {
            if (!$crf) {
                $errors['crf'] = 'O CRF é obrigatório';
            } 

            if (!$state) {
                $errors['state'] = 'Para cadastrar um CRF você precisa selecionar um Estado';
            } 

        } else if ($occupation == 'balconista') { 
            if (!$cnpj) {
                $errors['cnpj'] = 'O CNPJ é obrigatório';
            } 
        }
        
        if ($password) {
            if (!$passwordRepeat) {
                $errors['passwordRepeat'] = 'O campo repetir senha é obrigatório.';
            }
        } 

        if ($passwordRepeat) {
            if (!$password) {
                $errors['senha'] = 'A senha é obrigatória';
            }
        }
         
        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'error' => $errors,
            ]);
        }

        //troca de perfil 
        $roleBalc = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
        $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');

        $user->setName($name);
        $user->setEmail($email);
        $user->setIsRegisterComplete(true);
        $user->setCpf($cpf);
        $user->setCrf($crf);
        $user->setNewsletter($newsletter ? $newsletter : false);
        $user->setCnpj($cnpj);
        $user->setCellphone($cellphone);
        $user->setNewsletter($newsletter);
        $user->setPromotion($promotion);
        $user->setSmsNotification($smsNotification);
        $user->setGender($gender);
        $user->setEducationalLevel($educationalLevel);
        $user->setCity($city);
        $user->setState($state);
        $user->setEstateAddress($estate_address);
        $user->setInterests($interests);
        $user->setTeamFootball($team_football);
        $user->setZipCode($zip_code);
        $user->setAddress($address);
        $user->setNumber($number);
        $user->setComplement($complement);
        $user->setNeighborhood($neighborhood);
        
        if($birthday != ''){
            //dump($birthday); die();
            // $d = explode('/', $birthday);
            // if(isset($d[0]) && isset($d[1]) && isset($d[2])){
                //$birthday = "{$d[2]}-{$d[1]}-{$d[0]}";
                $user->setBirthday(new \DateTime($birthday));
            // }
        }
               
        if ($password) {
            $user->setSalt(md5(uniqid()));
            $user->setPassword($encoder->encodePassword($user, $password));
        }
         
        if ($occupation == 'balconista') {
            $user->addRole($roleBalc);
            $user->removeRole($roleFarmac);
            $user->setIsPharmaceutical(false);
        } else if ($occupation == 'farmaceutico') {
            $user->addRole($roleFarmac);
            $user->removeRole($roleBalc);
            $user->setIsPharmaceutical(true);
            $user->setCrf($crf);
            $user->setState($state);
        } else if ($occupation == 'farmacia') {
           // 
        }

        //atualizando cadastro do usuário no Racine
        $apiNeoPharma   = new NeoPharmaService(
            $this->getParameter('ws_racine_url'),
            $this->getParameter('ws_racine_token')
        );

        $checkUserName  = $apiNeoPharma->getUser($user);
        
        if (isset($checkUserName->users[0]->username)) { //checar se existe o username do usuário 
            $userId = $checkUserName->users[0]->id;
            $apiNeoPharma->updatedUser($user, $userId);
        } 
        
        $em->persist($user);
        $em->flush();

        $em->getRepository('App:UserPointGamification')->checkRegisterComplete($user); //Gamificação interação
        
        return new JsonResponse([
            'status' => true,
            'message' => 'Cadastro atualizado com sucesso.'
        ]);
    }

    /**
     * @Route("/pontos", name="user-points")
     */
    public function userPoints(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home-login');
        }

        $em = $this->getDoctrine()->getManager();	
        $user = $this->getUser();
        
        $points = $em->getRepository('App:User')->getAllPointsBadges($user);
        $superNeo = $em->getRepository('App:Badge')->findOneBySlug('super-neo');

        
        $results = [];

        $results[0]['badge'] = $superNeo;
        $results[0]['rank'] = $em->getRepository('App:User')->getUserRankByBadge($user, $superNeo);
        
        foreach($points as $key=>$point){
            $badge = $point->getBadge();
            $results[$key+1]['badge'] = $badge;
            $results[$key+1]['rank'] = $em->getRepository('App:User')->getUserRankByBadge($user, $badge);
        }

        return $this->render('user/points.html.twig', [
            'results' => $results
        ]);
        
    }

    private function generateShareCode($user)
    {
        $name = explode(' ', $user->getName());
        $better_token = substr(md5(uniqid(rand(), true)),rand(0,26),5);
        return strtolower($name[0] . $better_token);
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
