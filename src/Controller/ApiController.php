<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\User;
use App\Entity\Role;
use App\Entity\UserFacebookData;
use App\Entity\UserGoogleData;
use App\Entity\QuizScore;
use App\Entity\BlogPostScore;
use App\Entity\ProductScore;
use App\Entity\CourseScore;
use App\Entity\UserFavorite;
use App\Entity\Leads;
use App\Service\CorreiosService;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * @Route("/get-referred", name="api-get-referred")
     */
    public function getReferredContent(Request $request)
    {

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            return new JsonResponse([
                'status' => false,
                'message' => 'Faltando parâmetros'
            ]);
        }

        $type = $request->query->has('type') ? $request->query->get('type') : ($request->request->has('type') ? $request->request->get('type') : null);
        

        if(!$type){
            return new JsonResponse([
                'status' => false
            ]);
        }

        $response = [];

        $em = $this->getDoctrine()->getManager();
        
        switch($type){
            case 'post' : {
                $data =  $em->getRepository('App:BlogPost')->findBy(['isActive' => true]);
                foreach($data as $d){
                    $response[] = ['value' => $d->getId(), 'label' => $d->getTitle()];
                }
                break;
            }
            case 'produto' : {
                $data =  $em->getRepository('App:Product')->findBy(['isActive' => true]);
                foreach($data as $d){
                    $response[] = ['value' => $d->getId(), 'label' => $d->getName()];
                }
                break;
            }
            case 'curso' : {
                $data =  $em->getRepository('App:Course')->findBy(['isActive' => true]);
                foreach($data as $d){
                    $response[] = ['value' => $d->getId(), 'label' => $d->getName()];
                }
                break;
            }
            case 'quiz' : {
                $data =  $em->getRepository('App:Quiz')->findBy(['isActive' => true]);
                foreach($data as $d){
                    $response[] = ['value' => $d->getId(), 'label' => $d->getTitle()];
                }
                break;
            }
            default : {
                return new JsonResponse([
                    'status' => false
                ]);
            }
        }

        return new JsonResponse([
            'status' => true,
            'values' => $response
        ]);

    }
   
    /**
     * @Route("/remove-referred", name="api-remove-referred")
     */
    public function removeReferredContent(Request $request)
    {

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            return new JsonResponse([
                'status' => false,
                'message' => 'Faltando parâmetros'
            ]);
        }

        $id = $request->query->has('id') ? $request->query->get('id') : ($request->request->has('id') ? $request->request->get('id') : null);
        

        if(!$id){
            return new JsonResponse([
                'status' => false,
                'message' => 'Faltando parâmetros'
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $contentReferrer = $em->getRepository('App:ReferrerContent')->find($id);

        if(!$contentReferrer){
            return new JsonResponse([
                'status' => false,
                'message' => 'Faltando conteudo'
            ]);
        }

        $contentReferrerBack = $em->getRepository('App:ReferrerContent')->findOneBy([
            'mainContentId' => $contentReferrer->getReferrerContentId(),
            'mainContentType' => $contentReferrer->getReferrerContentType(),
            'referrerContentId' => $contentReferrer->getMainContentId(),
            'referrerContentType' => $contentReferrer->getMainContentType(),
        ]);

        if(!$contentReferrerBack){
            return new JsonResponse([
                'status' => false,
                'message' => 'Faltando conteudo referenciado'
            ]);
        }

        $em->remove($contentReferrer);
        $em->remove($contentReferrerBack);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);

    }

    /**
     * @Route("/set-score/{id}/{type}/{rate}", name="api-set-score")
     */
    public function setStars($id,$type,$rate)
    {

        $user = $this->getUser();

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Você precisa estar logado'
            ]);    
        }
        
        $em = $this->getDoctrine()->getManager();

        switch($type){
            case 'blog' : {
                $object = $em->getRepository('App:BlogPost')->find($id);
                
                $score = $em->getRepository('App:BlogPostScore')->findOneBy(['user' => $user, 'post' => $object]);
                
                if($score){
                    $score->setScore($rate);
                    $score->setUpdatedAt(new \DateTime());
                } else {
                    $score = new BlogPostScore();
                    $score->setUser($user);
                    $score->setScore($rate);
                    $score->setPost($object);
                    $score->setUpdatedAt(new \DateTime());
                    $score->setCreatedAt(new \DateTime());
                }
                
                $em->persist($score);
                $em->flush();

                $avgScore = $em->getRepository('App:BlogPostScore')->getAvgScore($object->getId());

                $object->setScore($avgScore);
                $em->persist($object);
                $em->flush();

                break;
            }
            case 'quiz' : {
                $object = $em->getRepository('App:Quiz')->find($id);

                $score = $em->getRepository('App:QuizScore')->findOneBy(['user' => $user, 'quiz' => $object]);
                
                if($score){
                    $score->setScore($rate);
                    $score->setUpdatedAt(new \DateTime());
                } else {
                    $score = new QuizScore();
                    $score->setUser($user);
                    $score->setScore($rate);
                    $score->setQuiz($object);
                    $score->setUpdatedAt(new \DateTime());
                    $score->setCreatedAt(new \DateTime());
                }

                $em->persist($score);
                $em->flush();

                $avgScore = $em->getRepository('App:QuizScore')->getAvgScore($object->getId());

                $object->setScore($avgScore);
                $em->persist($object);
                $em->flush();
                
                break;
            }
            case 'product' : {
                $object = $em->getRepository('App:Product')->find($id);

                $score = $em->getRepository('App:ProductScore')->findOneBy(['user' => $user, 'product' => $object]);
                
                if($score){
                    $score->setScore($rate);
                    $score->setUpdatedAt(new \DateTime());
                } else {
                    $score = new ProductScore();
                    $score->setUser($user);
                    $score->setScore($rate);
                    $score->setProduct($object);
                    $score->setUpdatedAt(new \DateTime());
                    $score->setCreatedAt(new \DateTime());
                }
                
                $em->persist($score);
                $em->flush();

                $avgScore = $em->getRepository('App:ProductScore')->getAvgScore($object->getId());

                $object->setScore($avgScore);
                $em->persist($object);
                $em->flush();
                
                break;
            }
            case 'course' : {
                $object = $em->getRepository('App:Course')->find($id);

                $score = $em->getRepository('App:CourseScore')->findOneBy(['user' => $user, 'course' => $object]);
                
                if($score){
                    $score->setScore($rate);
                    $score->setUpdatedAt(new \DateTime());
                } else {
                    $score = new CourseScore();
                    $score->setUser($user);
                    $score->setScore($rate);
                    $score->setCourse($object);
                    $score->setUpdatedAt(new \DateTime());
                    $score->setCreatedAt(new \DateTime());
                }
                
                $em->persist($score);
                $em->flush();

                $avgScore = $em->getRepository('App:CourseScore')->getAvgScore($object->getId());

                $object->setScore($avgScore);
                $em->persist($object);
                $em->flush();
                
                break;
            }
            default : {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Wrong type...'
                ]);
            }
        }

        return new JsonResponse([
            'status' => true,
            'avgScore' => isset($avgScore) ? $avgScore : false
        ]);
    }

    /**
     * @Route("/send-notification", name="api-send-notify")
     */
    public function sendMessageNotify(){
        
        $tokenArray = ['ewIugHyO2Frbx3iRiFwASU:APA91bGU1z2pcrygBrBoUA25A42xTzSqNWDVrqHoxQGq2i40mGrMV_4nKiVIKQ5IwFpd8VjBMK0NeHVPFLjjRUrPeXdBQ5eG0v9awqauiF7JRzABpqE-URhyOpGTkpSm3DF3fcQNFryQ'];

        if(!count($tokenArray)){
            return false;
        }

        $url = "https://fcm.googleapis.com/fcm/send";

        $fields = [
            'notification' => [
                'title' => 'Portal Neo Pharma',
                'body' => 'Apenas um teste de envio'
            ],
            'data' => [
                'title' => 'Portal Neo Pharma',
                'message' => 'Apenas um teste de envio'
            ],
            'registration_ids'=> $tokenArray,
        ];

        $jsonPost = json_encode($fields);

        $header = [
            "Authorization:key=AIzaSyDHGHUJJlx9y3EZV-svjUDB1J-igKHJV-U",
            'Content-Type: application/json'
        ];
        
        $ch = curl_init ();
        curl_setopt( $ch, CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonPost);

        //execute post
        $result = curl_exec($ch);
        r($result);
        if($result==FALSE){
            return new JsonResponse([
                'status' => false,
                'message' => 'ERROR'
            ]);   
        }

        curl_close($ch);
        
        return new JsonResponse([
            'status' => true,
            'message' => $result 
        ]);   
    }

    /**
     * @Route("/subscribe-notification/{token}", name="api-subscribe-notify")
     */
    public function subscribeNotify($token){
        
        $url = "https://iid.googleapis.com/iid/v1/{$token}/rel/topics/web";

        $header = [
            "Authorization:key=AIzaSyDHGHUJJlx9y3EZV-svjUDB1J-igKHJV-U",
            'Content-Type: application/json'
        ];
        
        $fields = [
            'token' => $token
        ];

        $jsonPost = json_encode($fields);

        $ch = curl_init ();
        curl_setopt( $ch, CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonPost);

        //execute post
        $result = curl_exec($ch);

        if($result==FALSE){
            return new JsonResponse([
                'status' => false,
                'message' => 'ERROR'
            ]);   
        }

        curl_close($ch);
        
        return new JsonResponse([
            'status' => true,
            'message' => $result 
        ]);   
    }

    /**
     * @Route("/add-favorite/{id}/{type}", name="api-add-favorite")
     */
    public function addFavorite($id, $type)
    {
        $user = $this->getUser();

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Você precisa estar logado'
            ]);    
        }
        
        $em = $this->getDoctrine()->getManager();

        $favorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $id, 'type' => $type, 'user' => $user]);

        if($favorite){
            if($favorite->getIsActive()){
                $favorite->setIsActive(false);
            } else {
                $favorite->setIsActive(true);
            }
            $favorite->setUpdatedAt(new \DateTime());
        } else {
            $favorite = new UserFavorite();
            $favorite->setIsActive(true);
            $favorite->setTypeId($id);
            $favorite->setType($type);
            $favorite->setUpdatedAt(new \DateTime());
            $favorite->setCreatedAt(new \DateTime());
            $favorite->setUser($user);
        }

        $em->persist($favorite);
        $em->flush();

        $favorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $id, 'type' => $type, 'user' => $user]);

        return new JsonResponse([
            'status' => true,
            'isActive' => $favorite->getIsActive()
        ]); 

    }

    /**
     * @Route("/set-quiz-done/{id}", name="api-set-quiz-done")
     */
    public function setQuizDone($id){
        $user = $this->getUser();
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Você precisa estar logado'
            ]);    
        }
        
        $em = $this->getDoctrine()->getManager();
        $object = $em->getRepository('App:Quiz')->find($id);

        if(!$object){
            return new JsonResponse([
                'status' => false
            ]);  
        }

        $em->getRepository('App:UserPoint')->setPointsToBadge($user, $object, 'fazedor-de-quiz', 'fazedor-de-quiz');

        return new JsonResponse([
            'status' => true
        ]); 
    }
    

    /**
     * @Route("/share-content-points/{type}/{id}", name="api-share-content-points")
     */
    public function shareContentPoints($type, $id)
    {
        $user = $this->getUser();

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Você precisa estar logado'
            ]);    
        }
        
        $em = $this->getDoctrine()->getManager();

        switch($type){
            case 'post': {
                $object = $em->getRepository('App:BlogPost')->find($id);
                break;
            }
            case 'quiz': {
                $object = $em->getRepository('App:Quiz')->find($id);
                break;
            }
            case 'course': {
                $object = $em->getRepository('App:Course')->find($id);
                break;
            }
        }

        if(!$object){
            return new JsonResponse([
                'status' => false
            ]);  
        }

        $em->getRepository('App:UserPoint')->setPointsToBadge($user, $object, 'o-compartilhador', $type);

        return new JsonResponse([
            'status' => true
        ]);        
    }

    /**
     * @Route("/login", name="api-login")
     */
    public function doLogin(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $username = $request->request->has('u') ? $request->request->get('u') : null;
        $pass = $request->request->has('p') ? $request->request->get('p') : null;

        if(!$username || !$pass){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário ou senha faltando'
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneByUsername($username);
        
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado'
            ]);
        }

        $bool = ($encoder->isPasswordValid($user, $pass)) ? true : false;

        if(!$bool){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário ou senha incorreta'
            ]);
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return new JsonResponse([
            'status' => true,
        ]);

    }

        /**
     * @Route("/add-social-media-login", name="api-add-social-media-login")
     */
    public function addRemoveSocialMediaLogin(Request $request)
    {
        $data = $request->request->all();

        if(!$data){
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-AR-001. Tente novamente mais tarde'
            ]);    
        }

        $type = isset($data['type']) ? $data['type'] : null;

        if(!$type){
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-AR-002. Tente novamente mais tarde'
            ]);    
        }

        $user = $this->getUser();

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-AR-003. Tente novamente mais tarde'
            ]);    
        }

        $em = $this->getDoctrine()->getManager();	

        if($type == 'facebook'){
            if($user){
                if($user->getIsFacebookConnected()){
                    $facebookData = $em->getRepository('App:UserFacebookData')->findOneByUser($user);
                    
                    $user->setIsFacebookConnected(false);
                    $user->setFacebookData(null);
                    $user->setUpdatedAt(new \DateTime());
                    
                    $em->remove($facebookData);
                    $em->persist($user);
                    $em->flush();
                } else {
                    $facebookData = new UserFacebookData();
                    $facebookData->setUser($user);
                    $facebookData->setName($data['userInfo']['name']);
                    $facebookData->setEmail($data['userInfo']['email']);
                    $facebookData->setAccessToken($data['authResponse']['accessToken']);
                    $facebookData->setExpiresIn($data['authResponse']['expiresIn']);
                    $facebookData->setSignedRequest($data['authResponse']['signedRequest']);
                    $facebookData->setDataAccessExpirationTime($data['authResponse']['data_access_expiration_time']);
                    $facebookData->setFacebookId($data['authResponse']['userID']);

                    $em->persist($facebookData);
                    $em->flush();

                    $user->setIsFacebookConnected(true);
                    $user->setFacebookData($facebookData);
                    $user->setUpdatedAt(new \DateTime());

                    $em->persist($user);
                    $em->flush();
                }
            } 
        } else {
            if($user){
                if($user->getIsGoogleConnected()){
                    $googleData = $em->getRepository('App:UserGoogleData')->findOneByUser($user);
                    
                    $user->setIsGoogleConnected(false);
                    $user->setGoogleData(null);
                    $user->setUpdatedAt(new \DateTime());
                    
                    $em->remove($googleData);
                    $em->persist($user);
                    $em->flush();
                } else {
                    $googleData = new UserGoogleData();
                    $googleData->setUser($user);
                    $googleData->setName($data['userInfo']['name']);
                    $googleData->setEmail($data['userInfo']['email']);
                    $googleData->setGoogleId($data['userInfo']['id']);

                    $em->persist($googleData);
                    $em->flush();

                    $user->setIsGoogleConnected(true);
                    $user->setGoogleData($googleData);
                    $user->setUpdatedAt(new \DateTime());

                    $em->persist($user);
                    $em->flush();
                }
            } 
        }
    }

    /**
     * @Route("/social-media-login", name="api-login-social-media")
     */
    public function socialMediaLogin(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = $request->request->all();
        
        if (!$data) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-LG-001. Tente novamente mais tarde'
            ]);    
        }

        $type = isset($data['type']) ? $data['type'] : null;

        if (!$type) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-LG-002. Tente novamente mais tarde'
            ]);    
        }
        
        // $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();	
        $role = $em->getRepository('App:Role')->findOneByRole('ROLE_SOCIAL_MEDIA');

        if ($type == 'facebook') {    
            $user = $em->getRepository('App:User')->findOneByEmail($data['userInfo']['email']);
            $facebookData = $em->getRepository('App:UserFacebookData')->findOneByEmail($data['userInfo']['email']);
            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);

            if ($user) {
                if ($leads && $facebookData) { 
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
            
                    $this->get('security.token_storage')->setToken($token);
                    $this->get('session')->set('_security_main', serialize($token));
                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            
                    $user->setAppAccessToken(md5($token));
                    $em->persist($user);
                    $em->flush();

                    return new JsonResponse([
                        'status' => true,
                        'isLogged' => true
                    ]);
                }
            }

            if ($leads && $facebookData) {
                return new JsonResponse([
                    'status' => true,
                    'lead' => $leads->getId()
                ]);
            }

            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);
           
            if ($leads) {
                return new JsonResponse([
                    'status' => true,
                    'lead' => $leads->getId()
                ]);
            }

            $facebookData = $em->getRepository('App:UserFacebookData')->findOneByEmail($data['userInfo']['email']);
        
            if (!$facebookData) {
                $facebookData = new UserFacebookData();
            } 

            // $facebookData->setUser($user);
            $facebookData->setName($data['userInfo']['name']);
            $facebookData->setEmail($data['userInfo']['email']);
            $facebookData->setAccessToken($data['authResponse']['accessToken']);
            $facebookData->setExpiresIn($data['authResponse']['expiresIn']);
            $facebookData->setSignedRequest($data['authResponse']['signedRequest']);
            $facebookData->setDataAccessExpirationTime($data['authResponse']['data_access_expiration_time']);
            $facebookData->setFacebookId($data['authResponse']['userID']);

            $em->persist($facebookData);
            $em->flush();
            
            //LEADS        
            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);
            //dump($request); die();
            if (!$leads) {
                $leads = New Leads();
            }

            $leads->setIsFacebook(true);
            $leads->setIsGoogle(false);
            $leads->setName($data['userInfo']['name']);
            $leads->setEmail($data['userInfo']['email']);
            $leads->setRole($role);
            $leads->setCreatedAt(new \DateTime());
    
            $em->persist($leads);
            $em->flush();
           
            return new JsonResponse([
                'status' => true,
                'lead' => $leads->getId(),
            ]);
        } else {
            $user = $em->getRepository('App:User')->findOneByEmail($data['userInfo']['email']);
            $googleData = $em->getRepository('App:UserGoogleData')->findOneByEmail($data['userInfo']['email']);
            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);

            if ($user) {
                if ($leads && $googleData) { 
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
            
                    $this->get('security.token_storage')->setToken($token);
                    $this->get('session')->set('_security_main', serialize($token));
                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            
                    $user->setAppAccessToken(md5($token));
                    $em->persist($user);
                    $em->flush();

                    return new JsonResponse([
                        'status' => true,
                        'isLogged' => true
                    ]);
                }
            }
           
            if ($leads && $googleData) {
                return new JsonResponse([
                    'status' => true,
                    'lead' => $leads->getId()
                ]);
            }

            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);
           
            if ($leads) {
                return new JsonResponse([
                    'status' => true,
                    'lead' => $leads->getId()
                ]);
            }

            $googleData = $em->getRepository('App:UserGoogleData')->findOneByEmail($data['userInfo']['email']);

            if (!$googleData) {
                $googleData = new UserGoogleData();
            } 

            $googleData->setName($data['userInfo']['name']);
            $googleData->setEmail($data['userInfo']['email']);
            $googleData->setGoogleId($data['userInfo']['id']);

            $em->persist($googleData);
            $em->flush();

            //LEADS        
            $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $data['userInfo']['email']]);
            //dump($request); die();
            if (!$leads) {
                $leads = New Leads();
            }

            $leads->setIsFacebook(false);
            $leads->setIsGoogle(true);
            $leads->setName($data['userInfo']['name']);
            $leads->setEmail($data['userInfo']['email']);
            $leads->setRole($role);
            $leads->setCreatedAt(new \DateTime());
    
            $em->persist($leads);
            $em->flush();
        
            return new JsonResponse([
                'status' => true,
                'lead' => $leads->getId()
            ]);
        }
          
        return new JsonResponse([
            'status' => false
        ]);
        
    }

        /**
     * @Route("/check-email", name="api-check-email")
     */
    public function checkEmail(Request $request){

        $email = $request->request->has('e') ? $request->request->get('e'): null;

        if(!$email){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }

        $user = $this->getUser();

        if($user){
            if($user->getEmail() == $email){
                return new JsonResponse([
                    'status' => true,
                    'message' => ''
                ]);
            }
        }

        $em = $this->getDoctrine()->getManager();	
        $user = $em->getRepository('App:User')
                    ->createQueryBuilder('u')
                    ->andWhere('u.email = :email or u.username = :username')
                    ->setParameter('email', $email)
                    ->setParameter('username', $email)
                    ->getQuery()
                    ->getResult();
     
        if($user){
            if ($request->request->get('multi_cpf') == 'GL-002') {
                $is_gestor = false;

                foreach ($user as $u) {
                    if ($u->getIsPharmacy()) {
                        $is_gestor = true;
                    }
                }
                
                if ($is_gestor) {
                    return new JsonResponse([
                        'status' => true,
                        'message' => ''
                    ]);
                }
            }

            $user = $em->getRepository('App:User')->findOneByEmail($email);
           
            if ($user) {
                
                if ((!$user->getIsRegisterComplete() && $user->getFacebookData()) || (!$user->getIsRegisterComplete() && $user->getGoogleData())) {
                    return new JsonResponse([
                        'status' => true,
                        'message' => ''
                    ]);
                }
            }
       
            
            return new JsonResponse([
                'status' => false,
                'message' => 'Email está sendo utilizado'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
                'message' => ''
            ]);
        }
    }

    /**
     * @Route("/check-cpf", name="api-check-cpf")
     */
    public function checkCpf(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('c') || $request->query->has('e') || $request->query->has('multi_cpf')){
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        $cpf = $request->request->has('c') ? $request->request->get('c'): null;
        $is_check = $request->request->has('e') ? $request->request->get('e'): null; //permissão para ter duplicidade de CPF na base

        if (!$cpf) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CPF é obrigatório',
               //'message' => 'CPF é obrigatório'
            ]);
        }

        
        $user = $this->getUser();

        if ($user) {
            if($user->getCpf() == str_replace(['.', ' ', '-'], '', $cpf)){
                return new JsonResponse([
                    'status' => true,
                ]);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneByCpf(str_replace(['.', ' ', '-'], '', $cpf));
              
        if ($user) {       
            if ($is_check) { // usado para o cadastro de gestor de loja
                $code_multi_cpf = 'GL-002';

                if ($request->request->has('multi_cpf')) { 
                    if ($request->request->get('multi_cpf') != $code_multi_cpf) {
                        if ($user->getIsPharmacy() == true) {
                        
                            return new JsonResponse([
                                'status'=> false, 
                                'message' => 'CPF já utilizado no cadastro de outra loja. Confirma a utilização do mesmo CPF para outra loja?', 
                                'multi_cpf' => true, 
                                'code' => $code_multi_cpf,
                                'email' =>  $user->getEmail()
                            ]);
                        }
                    }
                }

                return new JsonResponse([
                    'status' => true
                ]);
            } else {
               
                return new JsonResponse([
                    'status' => false,
                    'message' => 'CPF está sendo utilizado'
                ]);
            }
           
        } else if(!$this->validaCPF($cpf)) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CPF inválido'
            ]);
        } else {
        
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    public function validaCPF($cpf = null) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
    
            return true;
        }
    }

    /**
     * @Route("/check-cnpj", name="api-check-cnpj")
     */
    public function checkCnpj(Request $request)
    {
        $cnpj = $request->request->has('c') ? $request->request->get('c'): $request->query->get('c');
        //dump($cnpj); die();
        if(!$cnpj){
            return new JsonResponse([
                'status' => false,
                'message' => 'CNPJ é obrigatório'
                //'message' => 'CNPJ é obrigatório'
            ]);
        }

        $cnpj = str_replace(['.', ' ', '-', '/'], '', $cnpj);

        $em = $this->getDoctrine()->getManager();
        $pharmacy = $em->getRepository('App:Pharmacy')->findOneBy(['cnpj' =>  $cnpj, 'isActive' => true]);
        
        if(!$pharmacy){
            return new JsonResponse([
                'status' => false,
                'message' => 'Farmácia não encontrada'
            ]);
        } else if(!$this->validaCNPJ($cnpj)) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CNPJ inválido'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    public function validaCNPJ($cnpj = null) {
        // Verifica se um número foi informado
        if(empty($cnpj)) {
            return false;
        }
       // dump($cnpj); die();
        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cnpj) != 14) {
            return false;
        }
        
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' || 
            $cnpj == '11111111111111' || 
            $cnpj == '22222222222222' || 
            $cnpj == '33333333333333' || 
            $cnpj == '44444444444444' || 
            $cnpj == '55555555555555' || 
            $cnpj == '66666666666666' || 
            $cnpj == '77777777777777' || 
            $cnpj == '88888888888888' || 
            $cnpj == '99999999999999') {
            return false;
            
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
         
            $j = 5;
            $k = 6;
            $soma1 = 0;
            $soma2 = 0;
    
            for ($i = 0; $i < 13; $i++) {
    
                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;
                
                $soma2 += ($cnpj{$i} * $k);
    
                if ($i < 12) {
                    $soma1 += ($cnpj{$i} * $j);
                }
    
                $k--;
                $j--;
    
            }
    
            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;
    
            return (($cnpj{12} == $digito1) and ($cnpj{13} == $digito2));
         
        }
    }

    /**
     * @Route("/check-crf", name="api-check-crf")
     */
    public function checkCrf(Request $request)
    {
        $crf = $request->request->has('c') ? $request->request->get('c'): $request->query->get('c');
        $state = $request->request->has('s') ? $request->request->get('s'): $request->query->get('s');

        if (!$crf) {
            return new JsonResponse([
                'status' => false,
                'message' => '',
                //'message' => 'CRF é obrigatório'
            ]);
        }

        if (!$state){
            return new JsonResponse([
                'status' => false,
                'message' => '',
                //'message' => 'Estado é obrigatório'
            ]);
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneBy(['crf' => str_replace(['.', ' ', '-', '/'], '', $crf), 'state' => $state]);

        if ($this->getUser()) {
            if ($this->getUser()->getCrf() == $crf) {
                return new JsonResponse([
                    'status' => true,
                ]);
            }
        }
       
        if($user){
            return new JsonResponse([
                'status' => false,
                'message' => 'CRF está sendo utilizado'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    /**
     * @Route("/check-username", name="api-check-username")
     */
    public function checkUsername(Request $request){
        
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
       
        $username = isset($dataPost['u']) ? $dataPost['u'] : null;

        if(!$username){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneByUsername($username);

        if($user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Username está sendo utilizado'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    /**
     * @Route("/consulta-cep", name="api-correios")
     */
    public function consultaCEP(Request $request)
    {
        if (!$request->request->has('cep')) {
           return new JsonResponse(['status' => false, 'message' => 'CEP não identificado.']);
        }
        
        $cep = str_replace([' ', '-'], '', $request->request->get('cep'));
      
        $correios = new CorreiosService();
        $response = $correios->consultaCEP($cep);

        return new JsonResponse($response);    
    }

    private function saveAvatar($user, $url)
    {
        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/user')) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/user', 777);
        }

        $fileName =  $user->getId() . '.jpg';
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user/' . $fileName;

        file_put_contents($filePath, file_get_contents($url));

        return 'uploads/user/' . $fileName;
        
    }
}
