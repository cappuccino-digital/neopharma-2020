<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GameficationController extends Controller
{
    /**
     * @Route("/neo-quimica-conquista", name="gamefication")
     */
    public function index(Request $request)
    {   
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        } 

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $medals = $em->getRepository('App:MedalsGamification')->findBy(['is_active' => true], ['position' => 'ASC']);
        $user_medals = $em->getRepository('App:UserMedalsGamification')->findBy(['user' => $user]);
        $user_ranking = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
        $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user]);

        return $this->render('gameficacao/index.html.twig', [
            'medals' => $medals,
            'user_medals' => $user_medals,
            'user_ranking' => $user_ranking,
            'user_level' => $user_level,
        ]);
    }

    /**
     * @Route("/meus-pontos", name="gamefication_points")
     */
    public function gameficationPoints(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }   

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $medals = $em->getRepository('App:MedalsGamification')->findBy(['is_active' => true], ['position' => 'ASC']);
        $user_medals = $em->getRepository('App:UserMedalsGamification')->findBy(['user' => $user]);
        $user_ranking = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
        $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user]);
       
        return $this->render('gameficacao/pontos.html.twig', [
            'medals' => $medals,
            'user_medals' => $user_medals,
            'user_ranking' => $user_ranking,
            'user_level' => $user_level,
        ]);
    }

     /**
     * @Route("/gameficacao/notificacao", name="gamefication_notification")
     */
    public function closeNotification(Request $request)
    {   
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {         
        
            $user = $this->getUser();

            if ($request->request->has('t')) {
                $em = $this->getDoctrine()->getManager();

                if ($request->request->get('t') == 'mission') {
                    $user_point = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'id' => $request->request->get('i')]);
                    $user_point->setIsShowPopup(true);
                    $user_point->setUpdatedAt(new \DateTime());
    
                    $em->persist($user_point);
                    $em->flush();
                }

                if ($request->request->get('t') == 'level') {
                    $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user, 'id' => $request->request->get('il')]);
                    
                    $user_level->setIsShowPopup(true);
                    $user_level->setUpdatedAt(new \DateTime());
    
                    $em->persist($user_level);
                    $em->flush();
                }

                return new JsonResponse([
                    'status' => true
                ]);
            }

        }

        return new JsonResponse([
            'status' => false
        ]);

    }

}