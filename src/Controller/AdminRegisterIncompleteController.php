<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class AdminRegisterIncompleteController extends Controller
{
    /**
     * @Route("/admin/register/incomplete", name="admin_register_incomplete")
     */
    public function index()
    {
        return $this->render('admin_register_incomplete/index.html.twig', [
            'controller_name' => 'AdminRegisterIncompleteController',
        ]);
    }
}
