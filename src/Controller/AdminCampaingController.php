<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\NeoTvVideo;

/**
 * @Route("/admin/campaing")
 */
class AdminCampaingController extends Controller
{
    /**
     * @Route("", name="admin_neotv_list")
     */
    public function list(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:NeoTvVideo')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $neotvs = $builder->getQuery()->getResult();

        $neotvs = $this->get('knp_paginator')->paginate(
            $neotvs,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_campaing/list.html.twig', [
            'neotvs' => $neotvs
        ]);
    }

    /**
     * @Route("/novo", name="admin_neotv_new")
     */
    public function new(Request $request)
    {
        if ($request->isMethod('GET')) {
            return $this->render('admin_campaing/new.html.twig', []);
        }

        $neotvFiles = $request->files->get('neotv');
        $neotvInfo = $request->request->get('neotv');

        $tv = new NeoTvVideo();

        $tv->setTitle($neotvInfo['title']);
        $tv->setShortDescription($neotvInfo['shortDescription']);
        $tv->setDescription(html_entity_decode($neotvInfo['description'], ENT_COMPAT, 'UTF-8'));
        $tv->setCreatedAt(new \DateTime());
        $tv->setUpdatedAt(new \DateTime());
        $tv->setIsActive(isset($neotvInfo['isActive']) ? $neotvInfo['isActive'] : false);
        $tv->setIsHighlightHome(isset($neotvInfo['isHighlightHome']) ? $neotvInfo['isHighlightHome'] : false);
        $tv->setYoutubeId($neotvInfo['youtubeId']);

        $tv->setMetaTitle($neotvInfo['metaTitle']);
        $tv->setMetaDescription($neotvInfo['metaDescription']);
        $tv->setImageMetaTitle($neotvInfo['imageMetaTitle']);
        $tv->setImageMetaDescription($neotvInfo['imageMetaDescription']);
        $tv->setImageMobileMetaTitle($neotvInfo['imageMobileMetaTitle']);
        $tv->setImageMobileMetaDescription($neotvInfo['imageMobileMetaDescription']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tv);
        $em->flush();

        if (isset($neotvFiles['image'])) {
            if (filesize($neotvFiles["image"]) > 1048576) {

                $this->addFlash("alert", "Imagem maior que o permitido.");

                return $this->redirectToRoute('admin_neotv_new');
            }
            $tv->setImage($this->saveFile($neotvFiles['image'], $tv, 'neotv'));
        } else {
            $tv->setImage('_dist/image/default_desktop.png');
        }

        if (isset($neotvFiles['imageMobile'])) {
            if (filesize($neotvFiles["imageMobile"]) > 1048576) {

                $this->addFlash("alert", "Imagem maior que o permitido.");

                return $this->redirectToRoute('admin_neotv_new');
            }
            $tv->setImageMobile($this->saveFile($neotvFiles['imageMobile'], $tv, 'neotv'));
        } else {
            $tv->setImageMobile('_dist/image/default_mobile.png');
        }

        $em->persist($tv);
        $em->flush();

        return $this->redirectToRoute('admin_neotv_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_neotv_edit")
     */
    public function edit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $neotv = $em->getRepository('App:NeoTvVideo')->find($id);

        if ($request->isMethod('GET')) {
            return $this->render('admin_campaing/edit.html.twig', [
                'neotv' => $neotv
            ]);
        }

        $neotvFiles = $request->files->get('neotv');
        $neotvInfo = $request->request->get('neotv');


        $neotv->setTitle($neotvInfo['title']);
        $neotv->setShortDescription($neotvInfo['shortDescription']);
        $neotv->setDescription(html_entity_decode($neotvInfo['description'], ENT_COMPAT, 'UTF-8'));
        $neotv->setUpdatedAt(new \DateTime());
        $neotv->setIsActive(isset($neotvInfo['isActive']) ? $neotvInfo['isActive'] : false);
        $neotv->setIsHighlightHome(isset($neotvInfo['isHighlightHome']) ? $neotvInfo['isHighlightHome'] : false);
        $neotv->setYoutubeId($neotvInfo['youtubeId']);

        $neotv->setMetaTitle($neotvInfo['metaTitle']);
        $neotv->setMetaDescription($neotvInfo['metaDescription']);
        $neotv->setImageMetaTitle($neotvInfo['imageMetaTitle']);
        $neotv->setImageMetaDescription($neotvInfo['imageMetaDescription']);
        $neotv->setImageMobileMetaTitle($neotvInfo['imageMobileMetaTitle']);
        $neotv->setImageMobileMetaDescription($neotvInfo['imageMobileMetaDescription']);

        if (isset($neotvFiles['image'])) {
            if ($neotv->getImage() != null && $neotv->getImage() != '') {
                $this->removeFile($neotv->getImage());
            }
            $neotv->setImage($this->saveFile($neotvFiles['image'], $neotv, 'neotv'));
        }

        if (isset($neotvFiles['imageMobile'])) {
            if ($neotv->getImageMobile() != null && $neotv->getImageMobile() != '') {
                $this->removeFile($neotv->getImageMobile());
            }
            $neotv->setImageMobile($this->saveFile($neotvFiles['imageMobile'], $neotv, 'neotv'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($neotv);
        $em->flush();

        return $this->redirectToRoute('admin_neotv_list');
    }

    private function saveFile($file, $post, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $post->getId() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
