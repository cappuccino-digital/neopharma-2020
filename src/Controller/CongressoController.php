<?php

namespace App\Controller;

use App\Entity\UserCongressPharmaceutical;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;
class CongressoController extends Controller
{
    /**
     * @Route("/congresso-crf-sp", name="congresso_crm")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {   
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }
        return $this->redirectToRoute('congresso_crm_result', ['result'=> 'ingressos-esgotados']);
        $em = $this->getDoctrine()->getManager();	
        
        //somente o perfil de farmacêutico pode participar  
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_FARMACO')) {
            return $this->redirectToRoute('congresso_crm_result', ['result'=> 'solicitacao-nao-permitida']);
        }

        //apenas 15 pessoas pode se inscrever no congresso
        $user_congress_pharmaceutical_all = $em->getRepository('App:UserCongressPharmaceutical')->findAll();

        if (count($user_congress_pharmaceutical_all) == 15) {
            return $this->redirectToRoute('congresso_crm_result', ['result'=> 'ingressos-esgotados']);
        }
       
        $user = $this->getUser();
      
        if ($request->request->has('c')) {
            $name  = trim($request->request->get('name'));
            $cpf   = str_replace(['.', ' ', '-'], '', trim($request->request->get('cpf')));
            $crf   = trim(str_replace(['.', ' ', '-', '/'], '', $request->request->get('crf')));
            $email = trim($request->request->get('email'));

            $error = array();

            //validação nome
            if (!$name) {
                $error['name'] = 'Nome é obrigatório';
            } 

            //validação CPF
            if (!$cpf) {
                $error['cpf'] = 'CPF é obrigatório';
            } else {
                if ($cpf != $user->getCpf()) {
                    $user_check_cpf = $em->getRepository('App:User')->findOneByCpf(str_replace(['.', ' ', '-'], '', $cpf));

                    if ($user_check_cpf) {
                        $error['cpf'] = 'CPF está sendo utilizado';
                    } else if(!$this->validaCPF($cpf)) {
                        $error['cpf'] = 'CPF inválido';
                    } 
                }
            }

            //validação CRF
            if (!$crf) {
                $error['crf'] = 'CRF é obrigatório';
            } else {
                if ($crf != $user->getCrf()) {
                    $user_check_crf = $em->getRepository('App:User')->findOneBy(['crf' => $crf]);

                    if ($user_check_crf) {
                        $error['crf'] = 'CRF está sendo utilizado';
                    }
                }
            }

            //validação e-mail
            if (!$email) {
                $error['email'] = 'E-mail é obrigatório';
            } else {

                if (($email != $user->getEmail()) || $email != $user->getUsername()) {
                   
                    $user_check_email = $em->getRepository('App:User')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email or u.username = :username')
                            ->setParameter('email', $email)
                            ->setParameter('username', $email)
                            ->getQuery()
                            ->getResult();
                    
                    if ($user_check_email) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                } 
                //validação e-mail
                if (!$this->validaEmail($email)) {
                    $error['email'] = 'E-mail inválido';
                }
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }            
            
            //verificando se o usuário já possui cadastro
            $user_congress_pharmaceutical = $em->getRepository('App:UserCongressPharmaceutical')->findOneBy(['user' => $user]);

            if (!$user_congress_pharmaceutical) {
                //cadastro no congresso
                $user_congress_pharmaceutical = new UserCongressPharmaceutical(); 
                $user_congress_pharmaceutical->setUser($user); 
                $user_congress_pharmaceutical->setCreatedAt(new \DateTime());
                $user_congress_pharmaceutical->setUpdatedAt(new \DateTime());  

                $em->persist($user_congress_pharmaceutical);
                $em->flush();

                //update user
                $user->setName($name);
                $user->setEmail($email);
                $user->setUsername($email);
                $user->setCpf($cpf);
                $user->setCrf($crf);

                $em->persist($user);
                $em->flush();

                $emails = ['wellington.santos@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br', 'caroline.veiga@cappuccinodigital.com.br'];
                    
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo($emails);
              
                $message
                    ->setReplyTo($email)
                    ->setSubject('Portal Neo Pharma - Inscrição Congresso Farmacêutico de São Paulo')
                    ->setBody(
                      'Nome: ' . $user->getName() . ', ' . 
                      'CPF: ' . $user->getCpf() . ', ' . 
                      'CRF: ' . $user->getCrf() . ', ' . 
                      'E-mail: ' . $user->getEmail() . ' ' 
                    );

                $response = $mailer->send($message);

                //$url = $this->redirectToRoute('congresso_crm_result', ['result'=> 'inscricao-realizada-com-sucesso']);
                $url = $request->getSchemeAndHttpHost() . '/congresso-crf-sp/inscricao-realizada-com-sucesso';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            } 
             
            return new JsonResponse([
                'status' => false,
                'is_register' => true, 
                'message' => 'Você já está inscrito no congresso'
            ]);
        }

        return $this->render('congresso/index.html.twig', []);
    }


    /**
     * @Route("/congresso-crf-sp/{result}", name="congresso_crm_result", defaults={"result"=null})
     */
    public function result(Request $request, $result)
    {
        $array = ['inscricao-realizada-com-sucesso', 'ingressos-esgotados', 'solicitacao-nao-permitida'];

        if (!in_array($result, $array)) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('congresso/result.html.twig', [
            'result' => $result
        ]);
    }

    private function validaCPF($cpf = null) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
    
            return true;
        }
    }

    private function validaEmail($mail){
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }
}