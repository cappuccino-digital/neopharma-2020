<?php

namespace App\Controller;

use App\Entity\UserInactivity;
use App\Service\EmBlueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserLoginController extends Controller
{
    /**
     * @Route("/cron-inactivity/90day", name="cron-inactivity-90")
     */
    public function cronInactivity(Request $request)
    {
        if ($request->query->has('q')) {          
            $event = 'inatividade_90_prod';
            $start = date('Y-m-d', strtotime("-90 days")); //
           // dump($start); die();
            $amount = $request->query->get('amount');
            
            if ($start) {
                $em = $this->getDoctrine()->getManager();
                $user_inactivity  = $em->getRepository('App:UserLogin')->findByInactivity($start, $amount); //pegando todos os usuários que estão no prazo de 60 ou 90 dias sem acessar o portal 
                $total_inactivity = $em->getRepository('App:UserLogin')->findByInactivity($start);
                //dump($user_inactivity); die();
                if (count($user_inactivity) > 0) {
                    foreach ($user_inactivity as $u_inactivity) {
                        $user = $em->getRepository('App:User')->findOneBy(['id' => $u_inactivity['id']]);
                        
                        if ($user) {
                            //pegando o último o login do usuário
                            $user_login = $em->getRepository('App:UserLogin')->findOneBy(['user' => $u_inactivity['id']], ['last_login_at' => 'DESC']);

                            //gravando todos usuários que estão no prazo de 60 ou 90 dias sem acessar o portal 
                            $user_inactivity_register = new UserInactivity();
                            $user_inactivity_register->setUser($user);
                            $user_inactivity_register->setEmail($user->getEmail());
                            $user_inactivity_register->setType($event);

                            //enviando o contato para emBlue
                            $emBlue = new EmBlueService();
                            $contact = $emBlue->eventInactivity($user, $event);

                            if (isset($contact['status'])) {   
                                $user_inactivity_register->setIsSendEmblue($contact['status']);          
                                if ($contact['status']) {                
                                    //$user_inactivity_register->setContactID($contact['contactID']);
                                    //$user_inactivity_register->setUpdateFieldsCustom($contact['updateFieldsCustom']);
                                    $user_inactivity_register->setMessage($contact['message']);
                                } else {
                                    $user_inactivity_register->setMessage($contact['message']); 
                                    //$user_inactivity_register->setUpdateFieldsCustom(false); 
                                }  
                            }
                            
                            $user_inactivity_register->setLastLoginDateAt($user_login->getLastLoginAt());
                            $user_inactivity_register->setDateRunCron(new \DateTime());

                            $em->persist($user_inactivity_register);
                            $em->flush();
                        }
                        
                                                                              
                    } //end foreach

                    return new JsonResponse([
                        'status' => true,
                        'pending' => count($total_inactivity) - $amount,
                        'total_inactivity' => count($total_inactivity),
                        'message' => 'Enviado com sucessso'
                    ]);  

                } else {
                    if ((count($total_inactivity) > 0) && (count($total_inactivity) == $amount)) {
                        return new JsonResponse([
                            'status' => true,
                            'message' => 'Todos os contatos foram enviados com sucesso'
                        ]);  
                    }

                } //end if 
            } //end if 

            return new JsonResponse([
                'status' => false,
                'message' => 'Nenhum registro foi encontrado'
            ]);  
        
        }//end if 
       
        return $this->render('user_login/inactivity-90day.html.twig', [
               
        ]);
        
    }

    /**
     * @Route("/cron-inactivity/60day", name="cron-inactivity-60")
     */
    public function cronInactivity60(Request $request)
    {
        if ($request->query->has('q')) {          
            $event = 'inatividade_60_prod';
            $start = date('Y-m-d', strtotime("-60 days")); //
           // dump($start); //die();
            $amount = $request->query->get('amount');
            
            if ($start) {
                $em = $this->getDoctrine()->getManager();
                $user_inactivity  = $em->getRepository('App:UserLogin')->findByInactivity($start, $amount); //pegando todos os usuários que estão no prazo de 60 ou 90 dias sem acessar o portal 
                $total_inactivity = $em->getRepository('App:UserLogin')->findByInactivity($start);
                //dump($user_inactivity); die();
                if (count($user_inactivity) > 0) {
                    foreach ($user_inactivity as $u_inactivity) {
                        $user = $em->getRepository('App:User')->findOneBy(['id' => $u_inactivity['id']]);
                        
                        if ($user) {
                            //pegando o último o login do usuário
                            $user_login = $em->getRepository('App:UserLogin')->findOneBy(['user' => $u_inactivity['id']], ['last_login_at' => 'DESC']);

                            //gravando todos usuários que estão no prazo de 60 ou 90 dias sem acessar o portal 
                            $user_inactivity_register = new UserInactivity();
                            $user_inactivity_register->setUser($user);
                            $user_inactivity_register->setEmail($user->getEmail());
                            $user_inactivity_register->setType($event);

                            //enviando o contato para emBlue
                            $emBlue = new EmBlueService();
                            $contact = $emBlue->eventInactivity($user, $event);

                            if (isset($contact['status'])) {   
                                $user_inactivity_register->setIsSendEmblue($contact['status']);          
                                if ($contact['status']) {                
                                    //$user_inactivity_register->setContactID($contact['contactID']);
                                    //$user_inactivity_register->setUpdateFieldsCustom($contact['updateFieldsCustom']);
                                    $user_inactivity_register->setMessage($contact['message']);
                                } else {
                                    $user_inactivity_register->setMessage($contact['message']); 
                                    //$user_inactivity_register->setUpdateFieldsCustom(false); 
                                }  
                            }
                            
                            $user_inactivity_register->setLastLoginDateAt($user_login->getLastLoginAt());
                            $user_inactivity_register->setDateRunCron(new \DateTime());

                            $em->persist($user_inactivity_register);
                            $em->flush();
                        }
                        
                                                                              
                    } //end foreach

                    return new JsonResponse([
                        'status' => true,
                        'pending' => count($total_inactivity) - $amount,
                        'total_inactivity' => count($total_inactivity),
                        'message' => 'Enviado com sucessso'
                    ]);  

                } else {
                    if ((count($total_inactivity) > 0) && (count($total_inactivity) == $amount)) {
                        return new JsonResponse([
                            'status' => true,
                            'message' => 'Todos os contatos foram enviados com sucesso'
                        ]);  
                    }

                } //end if 
            } //end if 

            return new JsonResponse([
                'status' => false,
                'message' => 'Nenhum registro foi encontrado'
            ]);  
        
        }//end if 
       
        return $this->render('user_login/inactivity-60day.html.twig', [
               
        ]);
        
    }
}
