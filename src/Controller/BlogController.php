<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/blog")
 */
class BlogController extends Controller
{
    /**
     * @Route("/{tipo}", name="blog_home", defaults={"tipo"=null})
     */
    public function blogHome(Request $request, $tipo)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('tipo') || $request->query->has('filter')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $em = $this->getDoctrine()->getManager();
        $banner_type = $em->getRepository('App:BannerType')->findOneBy(['slug' => $tipo]);
        
        $banner = [];
        if ($banner_type) {
            $banner = $em->getRepository('App:Banner')->findOneBy(['type' => $banner_type]);
        } 

        $post_last = $em->getRepository('App:BlogPost')->findOneBy(['isActive' => true], ['createdAt' => 'DESC']);
    
        $builder = $em->getRepository('App:BlogPost')->createQueryBuilder('b')->innerJoin('App:BlogPostType', 'bt' , 'WITH' , 'b.type = bt.id')->innerJoin('App:BlogPostCategory', 'bc', 'WITH', 'b.category = bc.id');

        if ($request->query->has('q')) {
            $builder
                ->andWhere('b.title LIKE :query OR b.shortDescription LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        if ($tipo) {
            //dump($tipo); die();
            $builder
                ->andWhere("bt.slug = :tipo")
                ->setParameter('tipo', $tipo);
        }

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("b.isOnlyToPharmaceutical = false");
        } else {

            /**** regra */
            if ($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }

                $builder->andWhere("b.isOnlyToPharmaceutical = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }

                $builder->andWhere("b.isOnlyToClerk = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("b.isOnlyToPharmaceutical = true");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar o contéudo do blog."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);

                $builder->andWhere("b.isPublic = true");
            }
            /** end */

        }
     
        if ($request->request->has('filter')) {
            $filter = $request->request->get('filter');
            
            if(isset($filter['readingTime']) && $filter['readingTime'] != ""){

                switch($filter['readingTime']){
                    case '1' : {
                        $builder->andWhere('b.readingTime <= 300');
                        break;
                    }
                    case '2' : {
                        $builder->andWhere('b.readingTime BETWEEN 301 AND 600');
                        break;
                    }
                    case '3' : {
                        $builder->andWhere('b.readingTime BETWEEN 601 AND 900');
                        break;
                    }
                    case '4' : {
                        $builder->andWhere('b.readingTime > 900');
                        break;
                    }
                }
            }
            
            if(isset($filter['type']) && $filter['type'] != ""){
                $sqlType = '';
                foreach ($filter['type'] as $key => $type){
                    $key = str_replace('type-', '', $key);
                    reset($filter['type']);
                    if ($key === key($filter['type'])){
                        $sqlType .= "bt.slug = '{$type}'";
                    } else {
                        $sqlType .= " OR bt.slug = '{$type}'";
                    }
                }
                $builder->andWhere($sqlType);
            }

            if(isset($filter['category']) && $filter['category'] != ""){
                $sqlCategory = '';
                foreach ($filter['category'] as $key => $category){
                    $key = str_replace('categoy-', '', $key);
                    reset($filter['category']);
                    if ($key === key($filter['category'])){
                    //if($key == count($filter['category']) - 1){
                        $sqlCategory .= "bc.slug = '{$category}' ";
                    } else {
                        $sqlCategory .= "OR bc.slug = '{$category}' ";
                    }
                }
                $builder->andWhere($sqlCategory);
            }
            

            if(isset($filter['orderBy']) && $filter['orderBy'] != ""){
                switch($filter['orderBy']){
                    case '1' : {
                        $builder->orderBy('b.createdAt','DESC');
                        break;
                    }
                    case '2' : {
                        $builder->orderBy('b.createdAt','ASC');
                        break;
                    }
                    case '3' : {
                        $builder->orderBy('b.title','ASC');
                        break;
                    }
                    case '4' : {
                        $builder->orderBy('b.views','DESC');
                        break;
                    }
                    case '5' : {
                        $builder->orderBy('b.score','DESC');
                        break;
                    }
                }
            } else {
                $builder->orderBy('b.createdAt','DESC');
            }
        } else {
            $builder->orderBy('b.createdAt','DESC');
        }

        } else {
            //publico
            $builder->andWhere("b.isOnlyToPharmaceutical = true and b.isOnlyToClerk = true");
            $builder->andWhere("b.isOnlyToClerk = true")
            ->orderBy('b.createdAt','DESC');
            #$builder->andWhere("b.isOnlyToPharmaceutical = true and b.isOnlyToClerk = true OR b.isPublic = true");
            
        }
      
        $builder->andWhere("b.isActive = true");
        $posts = $builder->getQuery()->getResult();
       
        $posts = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->getInt('page', 1),
           7
        );

        return $this->render('blog/index.html.twig', [
            'posts' => $posts,
            'banner' => $banner,
            'filter' => isset($filter) ? $filter : null,
            'post_last' => $post_last,
        ]);
    }

    /**
     * @Route("/post/{slug}", name="blog_detail")
     */
    public function blogDetail($slug, Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('App:BlogPost')->findOneBySlug($slug);
        
        if(!$post){
            return $this->redirectToRoute('blog_home');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') && !$post->getIsPublic()) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }   

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$post->getIsPublic()){
            return $this->redirectToRoute('blog_home');
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$post->getIsOnlyToClerk()){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
               
                return $this->redirectToRoute('blog_home');
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$post->getIsOnlyToClerk()){
                return $this->redirectToRoute('blog_home');
            }else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->getUser()->getIsRegisterComplete() && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar o contéudo do blog."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            }
        }

        //validação
        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        //LP inverno obrigatório estar logado
        $posts_lp_inverno = array(
            'como-agem-os-antialergicos',
            'a-tosse-e-seu-tratamento',
            'como-agem-os-antialergicos',
            'a-tosse-e-seu-tratamento',
        );
        
        if (in_array($slug, $posts_lp_inverno)) { 
            if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                /** Gravando a referencia do conteúdo */
                $url_reference = $request->getUri();
                $response = $this->redirectToRoute('home-login');
                $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
                return $response;
                /*** END  */
            }
        } 
        /* end lógica LP */

        $postNextPrevious = $em->getRepository('App:BlogPost')->getNextAndPrevious($post->getId());
        
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContentView($post->getId(),'post');
        $isSaved = false;
        
        $user = $this->getUser();
  
        if($user){
            if ($post->getType()->getSlug() == 'video') {
                $em->getRepository('App:UserPoint')->setPointsToBadge($user, $post, 'fanatico-por-videos', 'post');
            } else if ($post->getType()->getSlug() == 'podcast') {
                $em->getRepository('App:UserPoint')->setPointsToBadge($user, $post, 'ouvinte-de-podcast', 'post');
        
            } else if ($post->getType()->getSlug() == 'materias') {
                $em->getRepository('App:UserPoint')->setPointsToBadge($user, $post, 'leitor-assiduo', 'post');
                                
                if ($request->query->has('lp')) {
                    $em->getRepository('App:UserPointGamification')->setInterationLandingPage($user, 'post', $post->getId()); //Gamificação interação
                } else {
                    $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'ler-uma-materia', 'post', $post->getId()); //Gamificação interação
                }
            }
            
            $isFavorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $post->getId(), 'type' => 'blog', 'user' => $user]);
            
            if($isFavorite){
                $isSaved = true;
            }
        }    
        
        $postViewedToo = $em->getRepository('App:User')->getViewByOthers($user, $post, 'post', 2);    
        
        $post->setViews($post->getViews() == null ? 1 : ($post->getViews() + 1));
        $em->persist($post);
        $em->flush();
        //dump($post->getType()); die();
        return $this->render('blog/detail.html.twig', [
            'post' => $post,
            'contentReferrer' => $contentReferrer,
            'postNextPrevious' => $postNextPrevious,
            'postViewedToo' => $postViewedToo,
            'isSaved' => $isSaved
        ]);
    }

    /**
     * @Route("/post/interacao/podcast", name="blog_interaction")
     */
    public function interationPodcast(Request $request) 
    {
       
        if ($request->request->get('g')) {
            $em = $this->getDoctrine()->getManager();
            
            if ($request->request->has('slug')) {
                
                $user = $this->getUser();
                if ($user) {
                    $slug = $request->request->get('slug');
                    $post = $em->getRepository('App:BlogPost')->findOneBySlug($slug);
                    
                    $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'ouvir-podcast', 'podcast', $post->getId()); //Gamificação interação
                
                    return new JsonResponse([
                        'status' => true,
                        'message' => 'Interação registrada com sucesso'
                    ]);    
                }
            }
         
           
        }       
    }

    /**
     * Componente Filtro do Blog
     */
     public function filterBlog($filter)
     {
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:BlogPostType')->findBy(['isActive' => true]);
        $categories = $em->getRepository('App:BlogPostCategory')->findBy(['isActive' => true]);

        return $this->render('component/blog-filter.html.twig', [
            'types' => $types,
            'categories' => $categories,
            'filter' => $filter
        ]);
     }
}
