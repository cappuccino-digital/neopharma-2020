<?php

namespace App\Controller;

use App\Entity\SpecialCampaign;
use App\Form\SpecialCampaignType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
/**
 * @Route("/admin/special/campaign")
 */
class AdminSpecialCampaignController extends Controller
{

    /**
     * @Route("/", name="admin_special_campaign")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $special_campaign = $em->getRepository('App:SpecialCampaign')->findBy([], ['created_at' => 'DESC']);
        //dump($special_campaign); die();
        return $this->render('admin_special_campaign/index.html.twig', [
            'special_campaign' => $special_campaign
        ]);
    }
 
    /**
      * @Route("/novo", name="admin_special_campaign_new")
      */
    public function new(Request $request, FileUploader $fileUploader)
    {
    $special_campaign = new SpecialCampaign();
    $form = $this->createForm(SpecialCampaignType::class, $special_campaign);
    $em = $this->getDoctrine()->getManager();
   
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        
        $special_campaign = $form->getData();

        $imageFile = $form->get('image')->getData();
        if ($imageFile) {
            $fileName = $fileUploader->upload($imageFile, 'specials');
            $special_campaign->setImage($fileName);
        }
        
        $special_campaign->setCreatedAt(new \DateTime());
        
        $em->persist($special_campaign);
        $em->flush();

        $this->addFlash('success', 'Campanha cadastrada com sucesso.');

        return $this->redirectToRoute('admin_special_campaign');
    }

    return $this->render('admin_special_campaign/new.html.twig', [
        'form' => $form->createView(),
    ]);
    }
 
    /**
     * @Route("/editar/{id}", name="admin_special_campaign_edit")
    * @ParamConverter("id", class="App\Entity\SpecialCampaign", options={"id": "id"})
    */
    public function edit(SpecialCampaign $special_campaign, Request $request, FileUploader $fileUploader)
    {        
        $form = $this->createForm(SpecialCampaignType::class, $special_campaign);
        $form->handleRequest($request);

        $image_old = $special_campaign->getImage();
        
        $em = $this->getDoctrine()->getManager();
        
        if ($form->isSubmitted() && $form->isValid()) {
            $special_campaign = $form->getData();

            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'specials');
                $special_campaign->setImage($fileName);
            } else {
                $special_campaign->setImage($image_old);
            }

            // $faq_question->setUpdatedAt(new \DateTime());
            $em->persist($special_campaign);
            $em->flush();

            $this->addFlash('success', 'Enquete atualizado com sucesso.');

            return $this->redirectToRoute('admin_special_campaign');
    }

    return $this->render('admin_special_campaign/edit.html.twig', [
        'form' => $form->createView(),
        'special_campaign' => $special_campaign
    ]);
    }
 
    /**
     * @Route("/{id}/deletar", name="admin_special_campaign_delete")
    * @ParamConverter("id", class="App\Entity\SpecialCampaign", options={"id": "id"})
    */
    public function delete(Request $request, SpecialCampaign $special_campaign)
    { 
        $form = $this->createDeleteForm($special_campaign);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository('App:SpecialCampaign')->findOneBy(['question' => $special_campaign]);

        // if ($question) {
        //     $this->addFlash('danger', 'Não foi possível excluir! Enquete está vinculada a resposta do usuário.');

        //     return $this->redirectToRoute('admin_special_campaign');
        // }
        
        $em->remove($special_campaign);
        $em->flush();

        $this->addFlash('success', 'Enquete deletado com sucesso.');

        return $this->redirectToRoute('admin_special_campaign');
    }
 
    /**
     * Creates a form to delete a user entity.
     *
     * @param SpecialCampaign $special_campaign The user entity
     *
     * @return \Symfony\Component\Form\Form The form
    */
    private function createDeleteForm(SpecialCampaign $special_campaign)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_special_campaign_delete', array('id' => $special_campaign->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
