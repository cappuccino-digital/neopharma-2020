<?php

namespace App\Controller;

use App\Entity\SatisfactionArena;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;

class SatisfacaoArenaController extends Controller
{
    /**
     * @Route("/satisfacao-arena", name="satisfacao_arena")
     */
    public function index(Request $request)
    {   
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }  

        $questions = ['a' => 'Quão fácil foi participar da distribuição de ingressos?', 'b' => 'Como foi sua experiência com a Neo Química Arena?', 'c' => 'Quão disposto você estaria a participar de mais ações como essa?'];

        if ($request->request->has('s')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $comment = $request->request->get('comment');
            
            foreach ($questions as $key => $q) {
                $question = $request->request->has($key) ? $request->request->get($key) : null; 
                $answer = $request->request->has($key . '-answer') ? $request->request->get($key . '-answer') : null;
               
                if ($question && $answer) {
                    $satisfaction = $em->getRepository('App:SatisfactionArena')->findOneBy(['user' => $user, 'question' => $question]);
                    if (!$satisfaction) {
                        $satisfaction = new SatisfactionArena();
                        $satisfaction->setUser($user);
                        $satisfaction->setCreatedAt(new \DateTime());
                    } 
                   
                    $satisfaction->setQuestion($question);
                    $satisfaction->setAnswer($answer);
                    $satisfaction->setComment($comment);
                    $satisfaction->setUpdatedAt(new \DateTime());
                   
                    $em->persist($satisfaction);
                    $em->flush();
                }  
            }

            return $this->redirectToRoute('satisfacao_agredecimento');
        }

        return $this->render('satisfacao-arena/index.html.twig', [
            'questions' => $questions,
        ]);
    }

     /**
     * @Route("/satisfacao-agredecimento", name="satisfacao_agredecimento")
     */
    public function agredecimentoarena(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }  

        return $this->render('satisfacao-arena/agradecimento.html.twig', []);
    }

}