<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PharmaciesElegibleController extends Controller
{
    /**
     * @Route("/pharmacies/elegible", name="pharmacies_elegible")
     */
    public function checkPharmaciesElegible(Request $request)
    {
        $cnpj = str_replace(['.', ' ', '-', '/'], '', trim($request->request->get('cnpj')));

        if (!$cnpj) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CNPJ é obrigatório.'
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $pharmacies_elegible = $em->getRepository('App:PharmaciesElegible')->findOneBy(['cnpj' => $cnpj, 'is_active' => true]);

        if (!$pharmacies_elegible) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Farmácia não encontrada.'
            ]);
        }   

        return new JsonResponse([
            'status' => true,
            'c' =>  $pharmacies_elegible->getCnpj(),
            'cn' => $pharmacies_elegible->getCompanyName(),
            'uf' => $pharmacies_elegible->getUf(),
            'message' => 'success'
        ]);
    }
}
