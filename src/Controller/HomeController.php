<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\NeoPharmaExcelenciaService;
use App\Entity\Contact;
use App\Entity\Leads;
use App\Entity\UserNewPassword;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\Cookie;
use App\Service\ExcelenciaSonoraService;
use DateTime;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;

class HomeController extends Controller
{
    /** 
     * @Route("", name="home")
     */
    public function index(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }      

        $em = $this->getDoctrine()->getManager();
        $banners = $em->getRepository('App:Banner')->getBannerByTypeName('home');
        $popup = $em->getRepository('App:Banner')->getBannerByTypeName('pop-up');
        $survey = $em->getRepository('App:SurveyQuestion')->findOneBy([], ['created_at' => 'DESC']);
        // dump($banners); die();
        $products = array();
        $posts = array();
        $courses= array();

        $user = $this->getUser();

        if(!$user){
            $products = $em->getRepository('App:Product')->findBy(['isActive' => true, 'isHomeHighLight' => true, 'isOnlyToPharmaceutical' => false],['createdAt' => 'DESC'], 3);
            $posts = $em->getRepository('App:BlogPost')->findBy(['isActive' => true, 'isPublic' => true],['createdAt' => 'DESC'], 3);
            $courses = $em->getRepository('App:Course')->findBy(['isActive' => true, 'isOnlyToClerk' => true], ['createdAt' => 'DESC'], 3);
            $quizs = $em->getRepository('App:QuizAll')->findBy(['is_active' => true, 'is_clerk' => false]);
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }

                $products = $em->getRepository('App:Product')->findBy(['isActive' => true, 'isHomeHighLight' => true],['createdAt' => 'DESC'], 3);
                //$posts = $em->getRepository('App:BlogPost')->findBy(['isActive' => true, 'isOnlyToPharmaceutical' => true],['createdAt' => 'DESC'], 3);
                $courses = $em->getRepository('App:Course')->findBy(['isActive' => true, 'isOnlyToPharmaceutical' => true], ['createdAt' => 'DESC'], 3);
                $quizs = $em->getRepository('App:QuizAll')->findBy(['is_active' => true, 'is_pharmaceutical' => true]);

                $posts = $em->getRepository('App:BlogPost')->createQueryBuilder('q')
                    ->where('q.isOnlyToPharmaceutical = true OR q.isPublic = true')
                    ->orderBy('q.createdAt', 'DESC')
                    ->setMaxResults(3)
                    ->getQuery()
                    ->getResult();

            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                $products = $em->getRepository('App:Product')->findBy(['isActive' => true, 'isHomeHighLight' => true, 'isOnlyToPharmaceutical' => false],['createdAt' => 'DESC'], 3);
                $posts = $em->getRepository('App:BlogPost')->findBy(['isActive' => true, 'isOnlyToClerk' => true, 'isPublic' => true],['createdAt' => 'DESC'], 3);
                $courses = $em->getRepository('App:Course')->findBy(['isActive' => true, 'isOnlyToClerk' => true], ['createdAt' => 'DESC'], 3);
                $quizs = $em->getRepository('App:QuizAll')->findBy(['is_active' => true, 'is_clerk' => true]);
            }else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $products = $em->getRepository('App:Product')->findBy(['isActive' => true, 'isHomeHighLight' => true, 'isOnlyToPharmaceutical' => false],['createdAt' => 'DESC'], 3);
                $posts = $em->getRepository('App:BlogPost')->findBy(['isActive' => true, 'isOnlyToPharmaceutical' => false, 'isPublic' => true],['createdAt' => 'DESC'], 3);
                $courses = $em->getRepository('App:Course')->findBy(['isActive' => true, 'isOnlyToClerk' => true, 'isPharmacy' => true], ['createdAt' => 'DESC'], 3);
                $quizs = $em->getRepository('App:QuizAll')->findBy(['is_active' => true, 'is_store_manager' => true]);
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $products = $em->getRepository('App:Product')->findBy(['isHomeHighLight' => true],['createdAt' => 'DESC'], 3);
                $posts = $em->getRepository('App:BlogPost')->findBy(['isActive' => true],['createdAt' => 'DESC'], 3);
                $courses = $em->getRepository('App:Course')->findBy(['isActive' => true], ['createdAt' => 'DESC'], 3);
                $quizs = $em->getRepository('App:QuizAll')->findBy(['is_active' => true]);
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar os contéudos do site."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            }
        }
        
        if (count($popup) > 0) {
            $popup = $popup[0];
        }
        
        $post_views = $em->getRepository('App:BlogPost')->findBy(['isActive' => true], ['views'=> 'DESC'], 2);
        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);
   
        //Gameficação
        $user_medals = $em->getRepository('App:UserMedalsGamification')->findBy(['user' => $user]);
        $user_ranking = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
        $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user]);
        
        $response = $this->render('home/index.html.twig', [
            'banners' => $banners,
            'popup' => $popup,
            'products' => $products,
            'posts' => $posts,
            'courses' => $courses,
            'quizs' => $quizs,
            'post_views' => $post_views,
            'survey' => $survey,
            'user_preferential' => $user_preferential,
            'user_medals' => $user_medals,
            'user_ranking' => $user_ranking,
            'user_level' => $user_level
        ]);

        //parâmetro que será enviado da página do congresso 
        if ($request->query->get('p') == 'congresso-crf-sp') {
            $response->headers->setCookie(new Cookie('_congressocrf', true));
        }
       
        return $response;
    }

    /**
     * @Route("/login/{r}", name="home-login", defaults={"r" = null})
     */
    public function login(Request $request, $r, UserPasswordEncoderInterface $encoder)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        if($request->isMethod('GET')){
            if ($request->query->has('form')) {
                throw $this->createNotFoundException('A página não foi encontrada');
            }
            
            return $this->render('home/login.html.twig', [
                'r' => $r
            ]);
        }
        
        $form = $request->request->get('form');

        if(!$form['username'] || !$form['password']){
            return $this->render('home/login.html.twig', [
                'error' => 'Todos os campos devem ser preenchidos.'    
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['username' => $form['username'], 'isActive' => true]);
        
        if(!$user){
            $user = $em->getRepository('App:User')->findOneBy(['username' => $form['username'], 'isActive' => false, 'is_test' => true]); //Usuário de teste
            if (!$user) {
                return $this->render('home/login.html.twig', [
                    'error' => 'Usuário ou senha incorretos'    
                ]);
            }
        }

        $bool = ($encoder->isPasswordValid($user, $form['password'])) ? true : false;

        if(!$bool){
            return $this->render('home/login.html.twig', [
                'error' => 'Usuário ou senha incorretos'    
            ]);
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setLastLoginAt(new \DateTime());
        //$user->setTokenApiExcelencia($this->generateToken()); //token gerado para API de excelencia

        $em->persist($user);
        $em->flush();

        //register login user
        $user_login = $em->getRepository('App:UserLogin')->saveLastLogin($user);
       
        /** validação para os usuários que não preencheram o cadasto corretamente */
        $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
        $roleBalc = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
        
        if ($user->getRoles()->contains($roleBalc)) {
            if (!trim($user->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if ($user->getRoles()->contains($roleFarmac)) {
            if (!trim($user->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        }
        /* end validação */

        //Redirecionamento para a referência após o login 
        $url_reference = $request->cookies->get('_referenceURL') ? $request->cookies->get('_referenceURL') : null;
        if ($url_reference) {
            $response = $this->redirect($url_reference);
            $response->headers->setCookie(new Cookie('_referenceURL', false));
            return $response;
        }   
        /** END */

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('admin_home');
        } else {
            return $this->redirectToRoute('home');
        }
    }

    public function routeExists($name)
    {
        // I assume that you have a link to the container in your twig extension class
        $router = $this->container->get('router');
        return (null === $router->getRouteCollection()->get($name)) ? false : true;
    }

    /**
     * @Route("/neopharmaexcelencia/", name="home-neopharmaexcelencia-slash")
     */
    public function redirectNeopharmaexcelenciaSlash(){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: https://excelencia.portalneopharma.com.br");
        exit();
    }

    /**
     * @Route("/neopharmaexcelencia", name="home-neopharmaexcelencia")
     */
    public function redirectNeopharmaexcelencia(){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: https://excelencia.portalneopharma.com.br");
        exit();
    }
    
    /**
     * @Route("/template-forms", name="home-tamplate-forms")
     */
    public function form(Request $request)
    {
        return $this->render('form/index.html.twig', [

        ]);
    }


    /**
     * @Route("/consulta/acesso", name="consulta-acesso-excelencia")
     */
    public function consultaAcessoExcelencia(Request $request)
    {
        $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
        $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf')); // o password é o cpf

        $excelencia_service = new NeoPharmaExcelenciaService($this->getParameter('api_excelencia'));
        $consultaAcesso = $excelencia_service->consultaAcesso(
                                                    $this->getParameter('admin_excelencia_user'),
                                                    $this->getParameter('admin_excelencia_pass'), 
                                                    $cnpj, 
                                                    $cpf
                                                );
        
        if (!$consultaAcesso['status']) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CNPJ ou Senha incorreta.'
            ]);  
        }
    
        if (!$consultaAcesso['elegivel']) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não identificado.'
            ]);
        }
                
        return new JsonResponse($consultaAcesso);
    }

    /**
     * @Route("/recover-pass-form", name="home-recover-pass-form")
     */
    public function contactShowForm(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->redirectToRoute('home');
        return $this->render('home/renew-password.html.twig', ['user' => $this->getUser(), 'newPassword' => $this->getUser()->getNewPasswordToken()]);
    }

    /**
     * @Route("/fale-conosco", name="home-contact-us")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if($request->isMethod('GET')){
            if ($request->query->has('form')) {
                throw $this->createNotFoundException('A página não foi encontrada');
            }

            return $this->render('home/contact-us.html.twig', []);
        }
        
        if ($request->request->has('form')) {
            $recaptcha = $request->query->has('g-recaptcha-response') ? $request->query->get('g-recaptcha-response') : ($request->request->has('g-recaptcha-response') ? $request->request->get('g-recaptcha-response') : null);

            if(!$recaptcha || $recaptcha == ''){
                return $this->render('home/contact-us.html.twig', [
                    'error' => 'preencha o captcha'
                ]);
            }

            if($this->validateCaptcha($request->request->get('g-recaptcha-response')) == false){
                return $this->render('home/contact-us.html.twig', [
                    'error' => 'problemas com o captcha'
                ]);
            }
        }
        
        $form = $request->request->get('form');

        $logger = new Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
        

        if($this->getParameter('APP_ENV') == 'dev'){
            $message = (new \Swift_Message())->setFrom(['cappuccinodeveloper@gmail.com'])
                ->setTo(['caroline.veiga@cappuccinodigital.com.br','wellington.santos@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br']);
        } else {
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo(['falecom@hypera.com.br']);
        }

        if ($form['subject'] == 'neoexcelencia') {
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo(['excelencia@portalneopharma.com.br']);

            $form['subject'] = 'Fale conosco Neopharma Excelência';
        } 
        
        $message
            ->setReplyTo($form['email'])
            ->setSubject('Portal Neo Pharma - ' . $form['subject'])
            ->setBody(
                $this->renderView(
                    'home/contact-email.html.twig',
                    ['form' => $form]
                ),
                'text/html'
            );

        $response = $mailer->send($message);

        $contact = new Contact();
        $contact->setName($form['name']);
        $contact->setEmail($form['email']);
        $contact->setSubject($form['subject']);
        $contact->setMessage($form['message']);
        $contact->setCreatedAt(new \DateTime());
        
        if($response == 1){
            $contact->setWasSent(true);
        } else {
            $contact->setWasSent(false);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();
        

        return $this->render('home/contact-us.html.twig', [
            'status' => 'Enviado com sucesso. Em breve entraremos em contato.'
        ]);

    }

    /**
     * @Route("/sobre-o-portal-neo-pharma", name="home-about")
     */
    public function about(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('home/about.html.twig', []);
    }


    /**
     * @Route("/bem-vindo-a-neo-pharma", name="home-welcome")
     */
    public function welcome(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('home/welcome.html.twig', []);
    }

    /**
     * @Route("/politica-de-privacidade", name="home-privacy")
     */
    public function privacy(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('home/privacy.html.twig', [

        ]);
    }

    /**
     * @Route("/404", name="home-not-found")
     */
    public function notFound(Request $request)
    {
        return $this->render('404/index.html.twig', [

        ]);
    }

    /**
     * @Route("/termos-de-uso", name="home-term")
     */
    public function term(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('home/term.html.twig', [

        ]);
    }

    /**
     * @Route("/busca", name="home-search")
     */
    public function search(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        $q = $request->query->has('q') ? $request->query->get('q') : ($request->request->has('q') ? $request->request->get('q') : null);
        $page = $request->query->has('page') ? $request->query->get('page') : ($request->request->has('page') ? $request->request->get('page') : 1);
        
        if (!$q) {
            return $this->render('home/search.html.twig', [
                'q' => $q, 
                'result' => [],
                'hasMore' => [],
                'thisPage' => [],
                'totalResults' => 0,
                'totalPages' => 0
            ]);
        }

        $contentProduct = [];
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $contentProduct = $this->getSearchProduct($q);
        }
        
        $contentQuiz = $this->getSearchQuiz($q);
        $contentBlog = $this->getSearchBlog($q);
        $contentCourse = $this->getSearchCourse($q);
        $contentCampaing = $this->getSearchCampaing($q);
       // dump($contentCourse); die();
        $result = [];
        $index = 0;
        $itensPerPage = 5;
        $page = $page - 1;
      
        if (count($contentProduct) > 0) {
            foreach($contentProduct as $p){
                $result[$index]['title'] = $p->getName();
                $result[$index]['class'] = 'product';
                $result[$index]['cat'] = '5';
                $result[$index]['object'] = $p;
                $index++;
            }
        }

        foreach($contentQuiz as $p){
            $result[$index]['title'] = $p->getTitle();
            $result[$index]['class'] = 'quiz';
            $result[$index]['cat'] = '2';
            $result[$index]['object'] = $p;
            $index++;
        }
        
        foreach($contentBlog as $p){
            $result[$index]['title'] = $p->getTitle();
            $result[$index]['class'] = 'blog';
            $result[$index]['cat'] = '1';
            $result[$index]['object'] = $p;
            $index++;
        }
        
        foreach($contentCourse as $p){
            $result[$index]['title'] = $p->getName();
            $result[$index]['class'] = 'course';
            $result[$index]['cat'] = '3';
            $result[$index]['object'] = $p;
            $index++;
        }
      

        foreach($contentCampaing as $p){
            $result[$index]['title'] = $p->getTitle();
            $result[$index]['class'] = 'campaing';
            $result[$index]['cat'] = '4';
            $result[$index]['object'] = $p;
            $index++;
        }

        usort($result, function($a, $b){
            return strcmp($a["title"], $b["title"]);
        });
        
        $startIndex = $page * $itensPerPage;
        $endIndex =  ($page + 1) * $itensPerPage;
        
        $response = [];
       
        $count = count($contentProduct) + count($contentQuiz) + count($contentBlog) + count($contentCourse);
       
        for($aux = $startIndex; $aux < $count; $aux++){

            if(isset($result[$aux])) $response[] = $result[$aux];
        }
      
        if(count($response) < $itensPerPage){
            $hasMore = false;
        } else {
            $hasMore = true;
        }

        return $this->render('home/search.html.twig', [
            'q' => $q, 
            'result' => $response,
            'hasMore' => $hasMore,
            'thisPage' => $page,
            //'totalResults' => count($result),
            //'totalPages' => ceil(count($result)/$itensPerPage)
            'totalResults' => count($response),
            'totalPages' => ceil(count($response)/$itensPerPage)
        ]);
    }

    private function getSearchProduct($query){
        $em = $this->getDoctrine()->getManager();
        $preposicoes = ['a', 'ao', 'ante', 'ate', 'apos', 'com', 'de', 'desde', 'para', 'perante', 'por', 'sem', 'sob', 'sobre', 'tras', 'de', 'das', 'dos'];
        
        $builder = $em->getRepository('App:Product')->createQueryBuilder('q');

        // $terms = explode(' ', $query);

        // foreach($terms as $k=>$t){
        //     if(in_array($t, $preposicoes)){
        //         unset($terms[$k]);
        //     }
        // }

        // $terms = array_values(array_filter($terms));

        // $stringSearch = '';

        // foreach($terms as $k => $t){
        //     $stringSearch .= "q.name LIKE '%{$t}%' OR q.shortDescription LIKE '%{$t}%' OR q.activeSubstance LIKE '%{$t}%'";
        //     if($k+1 < count($terms)){
        //         $stringSearch .= ' OR ';
        //     }
        // }

        $stringSearch = "q.name LIKE '%{$query}%' OR q.shortDescription LIKE '%{$query}%' OR q.activeSubstance LIKE '%{$query}%'";

        $builder->andWhere($stringSearch);

        // if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
        //     $builder->andWhere("q.isOnlyToPharmaceutical = false");
        // } else {
        //     if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){

        //     } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){                
        //         $builder->andWhere("q.isOnlyToPharmaceutical = false");
        //     } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        //         $builder->andWhere("q.isOnlyToPharmaceutical = false");
        //     } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
        //         $builder->andWhere("q.isPublic = true");
        //     }
        // }

        $builder->andWhere('q.isActive = true');

        return $builder->getQuery()->getResult();
    }

    private function getSearchQuiz($query)
    {
        $em = $this->getDoctrine()->getManager();
        $preposicoes = ['a', 'ao', 'ante', 'ate', 'apos', 'com', 'de', 'desde', 'para', 'perante', 'por', 'sem', 'sob', 'sobre', 'tras', 'de', 'das', 'dos'];
        $builder = $em->getRepository('App:Quiz')->createQueryBuilder('q');
        
        // $terms = explode(' ', $query);

        // foreach($terms as $k=>$t){
        //     if(in_array($t, $preposicoes)){
        //         unset($terms[$k]);
        //     }
        // }

        // $terms = array_values(array_filter($terms));

        // $stringSearch = '';

        // foreach($terms as $k => $t){
        //     $stringSearch .= "q.title LIKE '%{$t}%' OR q.shortDescription LIKE '%{$t}%' OR q.description LIKE '%{$t}%'";
        //     if($k+1 < count($terms)){
        //         $stringSearch .= ' OR ';
        //     }
        // }

        $stringSearch = "q.title LIKE '%{$query}%' OR q.shortDescription LIKE '%{$query}%' OR q.description LIKE '%{$query}%'";
        $builder->andWhere($stringSearch);

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("q.isOnlyToPharmaceutical = false");
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isOnlyToPharmaceutical = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isOnlyToPharmaceutical = false");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isOnlyToPharmaceutical = false");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $builder->andWhere("q.isPublic = true");
            }  
        }

        $builder->andWhere('q.isActive = true');

        return $builder->getQuery()->getResult();
    }

    private function getSearchBlog($query)
    {
        $em = $this->getDoctrine()->getManager();
        $preposicoes = ['a', 'ao', 'ante', 'ate', 'apos', 'com', 'de', 'desde', 'para', 'perante', 'por', 'sem', 'sob', 'sobre', 'tras', 'de', 'das', 'dos'];
        $builder = $em->getRepository('App:BlogPost')->createQueryBuilder('q');
        // $terms = explode(' ', $query);

        // foreach($terms as $k=>$t){
        //     if(in_array($t, $preposicoes)){
        //         unset($terms[$k]);
        //     }
        // }

        // $terms = array_values(array_filter($terms));

        // $stringSearch = '';
        // foreach($terms as $k => $t){
        //     $stringSearch .= "q.title LIKE '%{$t}%' OR q.shortDescription LIKE '%{$t}%' OR q.description LIKE '%{$t}%' OR q.tags LIKE '%{$t}%'";
        //     if($k+1 < count($terms)){
        //         $stringSearch .= ' OR ';
        //     }
        // }
        //dump($query); die();
        $stringSearch = "q.title LIKE '%{$query}%' OR q.shortDescription LIKE '%{$query}%' OR q.description LIKE '%{$query}%' OR q.tags LIKE '%{$query}%'";
        $builder->andWhere($stringSearch);

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("q.isOnlyToPharmaceutical = false");
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isOnlyToPharmaceutical = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isOnlyToPharmaceutical = false");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isPharmacy = true OR q.isOnlyToPharmaceutical = false");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $builder->andWhere("q.isPublic = true");
            }
        }

        $builder->andWhere('q.isActive = true');

        return $builder->getQuery()->getResult();
    }

    private function getSearchCourse($query)
    {
    
        $em = $this->getDoctrine()->getManager();
        $preposicoes = ['a', 'ao', 'ante', 'ate', 'apos', 'com', 'de', 'desde', 'para', 'perante', 'por', 'sem', 'sob', 'sobre', 'tras', 'de', 'das', 'dos'];
        $builder = $em->getRepository('App:Course')->createQueryBuilder('q');
       
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("q.isOnlyToClerk = true OR q.isPublic = true");     
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                $builder->andWhere("q.isOnlyToPharmaceutical = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                $builder->andWhere("q.isOnlyToClerk = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        
                $builder->andWhere("q.isOnlyToClerk = true");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar o curso."); 
                
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isActive = true");
            }
        }
     
        $stringSearch = "q.name LIKE '%{$query}%' OR q.shortDescription LIKE '%{$query}%' OR q.description LIKE '%{$query}%'";
        $builder->andWhere($stringSearch);
       
        return $builder->andWhere("q.isActive = true")->getQuery()->getResult();
    }

    private function getSearchCampaing($query){
        $em = $this->getDoctrine()->getManager();
        $preposicoes = ['a', 'ao', 'ante', 'ate', 'apos', 'com', 'de', 'desde', 'para', 'perante', 'por', 'sem', 'sob', 'sobre', 'tras', 'de', 'das', 'dos'];
        
        $builder = $em->getRepository('App:NeoTvVideo')->createQueryBuilder('q');

        $terms = explode(' ', $query);

        foreach($terms as $k=>$t){
            if(in_array($t, $preposicoes)){
                unset($terms[$k]);
            }
        }

        $terms = array_values(array_filter($terms));

        $stringSearch = '';

        foreach($terms as $k => $t){
            $stringSearch .= "q.title LIKE '%{$t}%' OR q.shortDescription LIKE '%{$t}%' OR q.description LIKE '%{$t}%'";
            if($k+1 < count($terms)){
                $stringSearch .= ' OR ';
            }
        }

        $builder->andWhere($stringSearch);

        $builder->andWhere('q.isActive = true');

        return $builder->getQuery()->getResult();
    }

    /**
     * @Route("/lembrar-senha", name="home-remember-password")
     */
    public function rememberPassword(Request $request, UserPasswordEncoderInterface $encoder,  \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        if($request->isMethod('GET')){
            if ($request->query->has('form')) {
                throw $this->createNotFoundException('A página não foi encontrada');
            }
            return $this->render('home/remember-password.html.twig', []);
        }
        
        $form = $request->request->get('form');

        if (empty($form['email'])) {
            return $this->render('home/remember-password.html.twig', [
                'status' => 'Preencha o campo e-mail é obrigatório.',
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneByEmail($form['email']);

        if(!$user){
            return $this->render('home/remember-password.html.twig', [
                'status' => 'Usuário não encontrado.'
            ]);
        }
        
        $token = sha1($user->getId().$user->getCreatedAt()->getTimestamp().$user->getSalt().$user->getPassword()).dechex(time()).dechex($user->getId());
        
        $expiresAt = new \DateTime();
        $expiresAt->add(new \DateInterval('PT48H'));
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneByUser($user);

        if(!$newPassword){
            $newPassword = new UserNewPassword();
        }
        
        $newPassword->setExpiresAt($expiresAt);
        $newPassword->setToken($token);
        $newPassword->setIsValid(true);
        $newPassword->setUser($user);

        $em->persist($newPassword);
        $em->flush();

        $logger = new Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

        if($this->getParameter('APP_ENV') == 'dev'){
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
        } else {
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
        }

        $message->setTo($user->getEmail())
        ->setSubject('Recuperar Senha')
        ->setBody(
            $this->renderView(
                'home/renew-password.html.twig',
                ['newPassword' => $newPassword, 'user' => $user]
            ),
            'text/html'
        );

        $response = $mailer->send($message);
        // dump($response); die();
        if($response == 1){
            return $this->render('home/remember-password.html.twig', [
                'status' => 'Um email foi enviado com as instruções para a troca de senha. Verifique também a sua caixa de SPAM.'
            ]);
        }

        return $this->render('home/remember-password.html.twig', [
            'status' => 'Algo deu errado, tente novamente mais tarde'
        ]);
    }

    /**
     * @Route("/trocar-senha", name="home-change-password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder,  \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $token = $request->query->get('token');

        if(!$token){
            return $this->redirectToRoute('home');
        }
        
        $em = $this->getDoctrine()->getManager();
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneBy(['token' => $token, 'isValid' => true]);
       
        if (!$newPassword) {
            return $this->render('home/change-password.html.twig', [
                'status' => false,
                'message' => 'Este link não é mais válido, tente recuperar sua senha novamente'
            ]);
        }
       
        $now = new \DateTime();

        if ($newPassword->getExpiresAt()->getTimestamp() <= $now->getTimestamp()){
            return $this->render('home/change-password.html.twig', [
                'status' => false,
                'message' => 'Este link expirou, tente recuperar sua senha novamente'
            ]);
        }

        $user = $newPassword->getUser();
        $form = $request->request->get('form');
        $user_gestors = $em->getRepository('App:User')->findBy(['email' => $user->getEmail(), 'isPharmacy' => true]);

        if($request->isMethod('GET')){
            return $this->render('home/change-password.html.twig', [
                'status' => true,
                'token' => $newPassword->getToken(),
                'user_gestors' => $user_gestors
            ]); 
        }
      
        // regra para trocar a senha de todos os gestores que está utilizando o mesmo e-mail
        if ($user_gestors) {
            foreach ($user_gestors as $gestor) {
                $gestor->setSalt(md5(uniqid()));
                $gestor->setPassword($encoder->encodePassword($gestor, $form['password']));
                $em->persist($gestor);
                $em->flush();
                
                $newPassword->setIsValid(false);
                $newPassword->setExpiresAt(new \DateTime());

                $em->persist($newPassword);
                $em->flush();
            }
        } else {
            $user->setSalt(md5(uniqid()));
            $user->setPassword($encoder->encodePassword($user, $form['password']));
            $em->persist($user);
            $em->flush();

            $newPassword->setIsValid(false);
            $newPassword->setExpiresAt(new \DateTime());
            $em->persist($newPassword);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
            
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }

        return $this->redirectToRoute('user-home');
    }

    private function generateShareCode($user)
    {
        $name = explode(' ', $user->getName());
        $better_token = substr(md5(uniqid(rand(), true)),rand(0,26),5);
        return strtolower($name[0] . $better_token);
    }

    private function validateCaptcha($recaptcha)
    {
        $validation = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcsdrUUAAAAAGIZc0gfunjTt-zLnRuyEmDKnefB&response=".$recaptcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        $response = json_decode($validation,true);
        return isset($response['success']) ? $response['success'] : false;
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    function validEmail($email)
    {
        $Syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if (preg_match($Syntaxe, $email))
            return true;
        else
            return false;
    }

    /**
     * @Route("/tapume", name="tapume")
     */
    public function tapume(Request $request)
    {

        return $this->render('home/tapume.html.twig', [

        ]);
    }

    /**
     * @Route("/cadastro-completo", name="registration-completed") 
     */
    public function registerComplete(Request $request)
    {
        //$em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('home');
        }

        if (!$request->query->has('rc')) {
            return $this->redirectToRoute('home');
        }

        $url = $request->getSchemeAndHttpHost();

        if ($user->getIsPharmacy()) {
            //SONORA enviar informações 
            $neopharma_excelencia = new ExcelenciaSonoraService(
                $this->getParameter('sonora_excelencia_secret'),
                $this->getParameter('sonora_excelencia_env')
            );
            $response = $neopharma_excelencia->impersonate($user);

            if ($response['status']) {
                $url = $response['url']; //redirecionar para o Excelência
            }
            //end
        }

        return $this->render('home/registration-completed.html.twig', [
            'url' => $url
        ]);
    }
   
}
