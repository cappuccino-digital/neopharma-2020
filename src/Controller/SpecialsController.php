<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

class SpecialsController extends Controller
{
    /**
     * @Route("/especiais/{slug}", name="specials", defaults={"slug" = null})
     */
    public function index(Request $request, $slug)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }  
        
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($slug) { 
            $special_campaign = $em->getRepository('App:SpecialCampaign')->findOneBy(['slug' => $slug, 'is_active' => true]);

            if ($special_campaign) {
                $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'acessar-uma-landing-page-especial', 'landing-page', $special_campaign->getId()); //Gamificação interação
                return $this->redirectToRoute($special_campaign->getRoutePath());
            } 
        }
     
        $special_campaign = $em->getRepository('App:SpecialCampaign')->findBy(['is_active' => true]);
        
        return $this->render('specials/index.html.twig', [
            'special_campaign' => $special_campaign,
        ]);
    }

    /**
     * @Route("/leitura/regulamento", name="user-regulation-gamefication")
     */
    public function regulationGamefication(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }  
        
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('term') == 'on') {

            $user_point_interation = $em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
        
            $user_point_interation->setIsRegulation(true);
            $user_point_interation->setDateRegulation(new \DateTime());
            
            $em->persist($user_point_interation);
            $em->flush();
        }
        

        return $this->redirectToRoute('home');
    }

    // /**
    //  * @Route("/especiais", name="specials")
    //  */
    // public function index(Request $request)
    // {
    //     if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
    //         /** Gravando a referencia do conteúdo */
    //         $url_reference = $request->getUri();
    //         $response = $this->redirectToRoute('home-login');
    //         $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

    //         return $response;
    //         /*** END  */
    //     }   

    //     return $this->render('specials/index.html.twig', [
    //         'controller_name' => 'DoresController',
    //     ]);
    // }

}