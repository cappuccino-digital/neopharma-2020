<?php

namespace App\Controller;

use App\Entity\UserPointsExcelencia;
use App\Service\NeoPharmaExcelenciaService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserPointsExcelenciaController extends Controller
{
    /**
     * @Route("/user/interacoes/excelencia", name="user_points_excelencia")
     */
    public function index()
    {
        // $sql = "SELECT c.id as id_curso, c.name as nome_curso, c.cod_interaction, u.cpf, u.cnpj, u.is_pharmaceutical, u.is_pharmacy, u.created_at, u.updated_at, uc.* FROM user_course  as uc
        //             inner join course as c
        //             on c.id = uc.course_id
        //             inner join user as u
        //             on u.id = uc.user_id
        //         where uc.conclusion_at is not null 
        //         and uc.conclusion_at >= '2020-05-01 00:00:59' 
        //         and uc.conclusion_at <= '2020-05-31 23:59:59' 
        //         order by uc.course_id desc;";

        // $em = $this->getDoctrine()->getManager();
        // $stmt = $em->getConnection()->prepare($sql);
        // $stmt->execute();
    
        // $user_curses = $stmt->fetchAll();
       
        // foreach ($user_curses as $user_curse) {
        //     if (trim($user_curse['cod_interaction']) && trim($user_curse['cpf']) && trim($user_curse['cnpj'])) {

        //         $excelencia_service = new NeoPharmaExcelenciaService( //API
        //             $this->getParameter('api_excelencia')
        //         );
                
        //         $em     = $this->getDoctrine()->getManager();	
        //         $user   =  $em->getRepository('App:User')->findOneBy(['id' => $user_curse['user_id']]);	//user
        //         $points = $em->getRepository('App:PointsTypeExcelencia')->findOneBy(['slug' => 'curso-mes-maio']); //point
        //         $course = $em->getRepository('App:Course')->findOneBy(['id' => $user_curse['course_id']]); //curso 

        //         $user_point = new UserPointsExcelencia();
        //         $user_point->setUser($user);
        //         $user_point->setPointTypeExcelencia($points);
        //         $user_point->setCourse($course);

        //         if ($user_curse['is_pharmaceutical']) {
        //             $user_point->setIsPharmaceutical(true);
        //         } else {
        //             if ($user_curse['is_pharmacy']) {
        //                 $user_point->setIsGestor(true);
        //             } else {
        //                 $user_point->setIsClerk(true);
        //             }
        //         }

        //         $user_point->setCreatedAt(new \DateTime());

        //         $em->persist($user_point);
        //         $em->flush();
                        
        //         $cadastra_interacao = $excelencia_service->cadastraInteracao(
        //             $this->getParameter('admin_excelencia_user'),
        //             $this->getParameter('admin_excelencia_pass'), 
        //             $user_curse['cnpj'], $user_curse['cpf'], $user_point->getId(), 24
        //         );
                
        //         if ($cadastra_interacao['status']) {
        //             $user_point->setCodReturnApi($cadastra_interacao['codigo']);
        //             $user_point->setDescriptionReturnApi($cadastra_interacao['descricao']);
        //         }

        //         $em->persist($user_point);
        //         $em->flush();
        //     }
        // }

        // return new JsonResponse([
        //     'status' => true,
        //     'message' => 'Interações enviadas com sucesso.',
        // ]);
    }
}
