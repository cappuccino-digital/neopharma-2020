<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Product;
use App\Entity\ProductAsset;
use App\Entity\ReferrerContent;

/**
 * @Route("admin/produto")
 */
class AdminProductController extends Controller
{
    /**
     * @Route("", name="admin_product_list")
     */
    public function adminProductList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Product')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $products = $builder->getQuery()->getResult();

        $products = $this->get('knp_paginator')->paginate(
            $products,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_product/list.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_new")
     */
    public function adminProductNew(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $types =  $em->getRepository('App:ProductType')->findBy(['isActive' => true]);
        $allProducts = $em->getRepository('App:Product')->findBy(['isActive' => true]);
        $classifications = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->findBy(['isActive' => true]);
        $categories = $em->getRepository('App:ProductCategory')->findBy(['isActive' => true]);
        $sistemicClass = $em->getRepository('App:ProductSistemicClass')->findBy(['isActive' => true]);
        if ($request->isMethod('GET')) {
            return $this->render('admin_product/new.html.twig', [
                'types' => $types,
                'allProducts' => $allProducts,
                'classifications' => $classifications,
                'categories' => $categories,
                'sistemicClass' => $sistemicClass
            ]);
        }

        $files = $request->files->all();
        $productInfo['info'] = $request->request->get('product');
        $productInfo['file'] = $files['product'];
        $productAssets['info'] = $request->request->get('assets');
        $productAssets['file'] = $files['assets'];
        $productReferred = $request->request->get('referrer');

        $product = $this->saveProduct($productInfo);

        if (isset($productInfo['file']['banner'])) {

            if (filesize($productInfo["file"]["banner"]) != false) {

                if (filesize($productInfo["file"]["banner"]) > 1048576) {


                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_product_new');
                }
                $product->setBanner($this->saveFile($productInfo['file']['banner'], $product, 'product'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");

                return $this->redirectToRoute('admin_product_new');
            }

            $product->setBanner($this->saveFile($productInfo['file']['banner'], $product, 'product'));

        } else {
            $product->setBanner('_dist/image/default_desktop.png');
        }

        if (isset($productInfo['file']['bannerMobile'])) {
            if (filesize($productInfo["file"]["bannerMobile"]) != false) {


                if (filesize($productInfo["file"]["bannerMobile"]) > 1048576) {
                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_product_new');
                }

                $product->setBannerMobile($this->saveFile($productInfo['file']['bannerMobile'], $product, 'product'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                return $this->redirectToRoute('admin_product_new');
            }
        } else {
            $product->setBannerMobile('_dist/image/default_mobile.png');
        }

        $this->saveAssets($product, $productAssets);

        if (is_array($productReferred)) $this->saveReferredContent($product, $productReferred);

        return $this->redirectToRoute('admin_product_list');
    }

    /**
     * @Route("/{id}", name="admin_product_view")
     */
    public function adminProductView($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App:Product')->find($id);

        return $this->render('admin_product/view.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_product_edit")
     */
    public function adminProductEdit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App:Product')->find($id);
        $types =  $em->getRepository('App:ProductType')->findBy(['isActive' => true]);

        $allProducts = $em->getRepository('App:Product')->findBy(['isActive' => true]);
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContent($product->getId(), 'produto');
        $classifications = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->findBy(['isActive' => true]);
        $categories = $em->getRepository('App:ProductCategory')->findBy(['isActive' => true]);
        $sistemicClass = $em->getRepository('App:ProductSistemicClass')->findBy(['isActive' => true]);

        if ($request->isMethod('GET')) {
            return $this->render('admin_product/edit.html.twig', [
                'product' => $product,
                'types' => $types,
                'allProducts' => $allProducts,
                'classifications' => $classifications,
                'contentReferrer' => $contentReferrer,
                'categories' => $categories,
                'sistemicClass' => $sistemicClass
            ]);
        }

        $files = $request->files->all();
        $productInfo['info'] = $request->request->get('product');
        $productInfo['file'] = $files['product'];
        $productAssets['info'] = $request->request->get('assets');
        $productAssets['file'] = isset($files['assets']) ? $files['assets'] : null;
        $productReferred = $request->request->get('referrer');

        $this->updateProduct($productInfo, $product);


        if (isset($productInfo['file']['banner'])) {
            if ($product->getBanner() != null && $product->getBanner() != '') {
                $this->removeFile($product->getBanner());
            }
            $product->setBanner($this->saveFile($productInfo['file']['banner'], $product, 'product'));
        }

        if (isset($productInfo['file']['bannerMobile'])) {
            if ($product->getBannerMobile() != null && $product->getBannerMobile() != '') {
                $this->removeFile($product->getBannerMobile());
            }
            $product->setBannerMobile($this->saveFile($productInfo['file']['bannerMobile'], $product, 'product'));
        }

        $this->saveAssetsUpdate($product, $productAssets);
        if (is_array($productReferred)) $this->saveReferredContent($product, $productReferred);

        return $this->redirectToRoute('admin_product_list');
    }

    /**
     * @Route("/remove-asset/{id}", name="admin_product_remove_asset")
     */
    public function adminProductRemoveAsset($id)
    {
        $em = $this->getDoctrine()->getManager();
        $asset = $em->getRepository('App:ProductAsset')->find($id);

        if ($asset) {
            $em->remove($asset);
            $em->flush();
        }

        return new JsonResponse([
            'status' => true
        ]);
    }

    private function saveProduct($productInfo)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('App:ProductType')->find($productInfo['info']['type']);
        $classification = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->find($productInfo['info']['classification']);
        $category = $em->getRepository('App:ProductCategory')->find($productInfo['info']['category']);
        $sistemicClass = $em->getRepository('App:ProductSistemicClass')->find($productInfo['info']['sistemicClass']);

        $product = new Product();


        $product->setName($productInfo['info']['name']);
        $product->setActiveSubstance($productInfo['info']['activeSubstance']);
        $product->setShortDescription($productInfo['info']['shortDescription']);
        $product->setBarcode($productInfo['info']['barcode']);
        $product->setAnvisaRegistration($productInfo['info']['anvisaRegistration']);

        $product->setManager($productInfo['info']['manager']);
        $product->setInternalCode($productInfo['info']['internalCode']);
        $product->setSapName($productInfo['info']['sapName']);
        $product->setRefMedicine($productInfo['info']['refMedicine']);

        $product->setPresentation($productInfo['info']['presentation']);
        $product->setFullPresentation($productInfo['info']['fullPresentation']);

        $product->setComposition(html_entity_decode($productInfo['info']['composition'], ENT_COMPAT, 'UTF-8'));
        $product->setIndication(html_entity_decode($productInfo['info']['indication'], ENT_COMPAT, 'UTF-8'));
        $product->setContraindication(html_entity_decode($productInfo['info']['contraindication'], ENT_COMPAT, 'UTF-8'));
        $product->setDosage(html_entity_decode($productInfo['info']['dosage'], ENT_COMPAT, 'UTF-8'));

        $product->setIsActive(isset($productInfo['info']['isActive']) ? $productInfo['info']['isActive'] : false);
        $product->setIsPublic(isset($productInfo['info']['isPublic']) ? $productInfo['info']['isPublic'] : false);
        $product->setIsHomeHighLight(isset($productInfo['info']['isHomeHighLight']) ? $productInfo['info']['isHomeHighLight'] : false);
        $product->setIsPsicotropic(isset($productInfo['info']['isPsicotropic']) ? $productInfo['info']['isPsicotropic'] : false);
 
        $product->setCreatedAt(new \DateTime());
        $product->setUpdatedAt(new \DateTime());
        $product->setPublicityPieceImpressionDate(new \DateTime($productInfo['info']['publicityPieceImpressionDate']));

        $product->setMetaTitle($productInfo['info']['metaTitle']);
        $product->setMetaDescription($productInfo['info']['metaDescription']);
        $product->setBannerMetaTitle($productInfo['info']['bannerMetaTitle']);
        $product->setBannerMetaDescription($productInfo['info']['bannerMetaDescription']);
        $product->setBannerMobileMetaTitle($productInfo['info']['bannerMobileMetaTitle']);
        $product->setBannerMobileMetaDescription($productInfo['info']['bannerMobileMetaDescription']);

        $product->setType($type);
        $product->setClassification($classification);
        $product->setCategory($category);
        $product->setSistemicClass($sistemicClass);

        if (isset($productInfo['info']['otherPresentation'])) {
            $product->setOtherPresentation(json_encode($productInfo['info']['otherPresentation']));
        }

        $em->persist($product);
        $em->flush();

        if (isset($productInfo['file']) && isset($productInfo['file']['bullaFile'])) {
            $file = $this->saveFile($productInfo['file']['bullaFile'], $product, 'product');
            $product->setBullaFile($file);
            $em->persist($product);
            $em->flush();
        }

        if (isset($productInfo['info']['otherPresentation'])) {
            foreach ($productInfo['info']['otherPresentation'] as $id) {
                $otherProduct = $em->getRepository('App:Product')->find($id);

                $otherProduct->addOtherPresentation($product->getId());

                $em->persist($otherProduct);
                $em->flush();
            }
        }

        return $product;
    }

    private function saveAssets($product, $productAssets)
    {
        $em = $this->getDoctrine()->getManager();

        if (is_array($productAssets['file'])) {
            foreach ($productAssets['file'] as $key => $file) {
                $file = $file['file'];

                $type = $productAssets['info'][$key]['type'];
                $fileMetaTitle = $productAssets['info'][$key]['fileMetaTitle'];
                $fileMetaDescription = $productAssets['info'][$key]['fileMetaDescription'];

                if ($file != null) {
                    $asset = new ProductAsset();
                    $asset->setType($type);
                    $asset->setFile($this->saveFile($file, $product, 'product'));
                    $asset->setFileMetaTitle($fileMetaTitle);
                    $asset->setFileMetaDescription($fileMetaDescription);
                    $asset->setProduct($product);

                    $em->persist($asset);
                    $em->flush();
                } else {
                    $asset = new ProductAsset();
                    $asset->setType($type);
                    $asset->setFileMetaTitle($fileMetaTitle);
                    $asset->setFileMetaDescription($fileMetaDescription);
                    $asset->setProduct($product);

                    $em->persist($asset);
                    $em->flush();
                }
            }
        }


        return true;
    }

    private function updateProduct($productInfo, $product)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('App:ProductType')->find($productInfo['info']['type']);
        $classification = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->find($productInfo['info']['classification']);
        $category = $em->getRepository('App:ProductCategory')->find($productInfo['info']['category']);
        $sistemicClass = $em->getRepository('App:ProductSistemicClass')->find($productInfo['info']['sistemicClass']);

        $product->setName($productInfo['info']['name']);
        $product->setActiveSubstance($productInfo['info']['activeSubstance']);
        $product->setShortDescription($productInfo['info']['shortDescription']);
        $product->setBarcode($productInfo['info']['barcode']);
        $product->setAnvisaRegistration($productInfo['info']['anvisaRegistration']);

        $product->setManager($productInfo['info']['manager']);
        $product->setInternalCode($productInfo['info']['internalCode']);
        $product->setSapName($productInfo['info']['sapName']);
        $product->setRefMedicine($productInfo['info']['refMedicine']);

        $product->setPresentation($productInfo['info']['presentation']);
        $product->setFullPresentation($productInfo['info']['fullPresentation']);

        $product->setComposition(html_entity_decode($productInfo['info']['composition'], ENT_COMPAT, 'UTF-8'));
        $product->setIndication(html_entity_decode($productInfo['info']['indication'], ENT_COMPAT, 'UTF-8'));
        $product->setContraindication(html_entity_decode($productInfo['info']['contraindication'], ENT_COMPAT, 'UTF-8'));
        $product->setDosage(html_entity_decode($productInfo['info']['dosage'], ENT_COMPAT, 'UTF-8'));

        $product->setIsPublic(isset($productInfo['info']['isPublic']) ? $productInfo['info']['isPublic'] : false);
        $product->setIsActive(isset($productInfo['info']['isActive']) ? $productInfo['info']['isActive'] : false);
        $product->setIsHomeHighLight(isset($productInfo['info']['isHomeHighLight']) ? $productInfo['info']['isHomeHighLight'] : false);
        $product->setIsPsicotropic(isset($productInfo['info']['isPsicotropic']) ? $productInfo['info']['isPsicotropic'] : false);

        $product->setUpdatedAt(new \DateTime());
        $product->setPublicityPieceImpressionDate(new \DateTime($productInfo['info']['publicityPieceImpressionDate']));

        $product->setMetaTitle($productInfo['info']['metaTitle']);
        $product->setMetaDescription($productInfo['info']['metaDescription']);
        $product->setBannerMetaTitle($productInfo['info']['bannerMetaTitle']);
        $product->setBannerMetaDescription($productInfo['info']['bannerMetaDescription']);
        $product->setBannerMobileMetaTitle($productInfo['info']['bannerMobileMetaTitle']);
        $product->setBannerMobileMetaDescription($productInfo['info']['bannerMobileMetaDescription']);

        $product->setType($type);
        $product->setClassification($classification);
        $product->setCategory($category);
        $product->setSistemicClass($sistemicClass);

        if (isset($productInfo['info']['otherPresentation'])) {
            $productOldOtherPresentation = json_decode($product->getOtherPresentation(), true);
            $product->setOtherPresentation(json_encode($productInfo['info']['otherPresentation']));

            if (is_array($productOldOtherPresentation)) {
                foreach ($productOldOtherPresentation as $oldId) {
                    if (!in_array($oldId, $productInfo['info']['otherPresentation'])) {
                        $otherProduct = $em->getRepository('App:Product')->find($oldId);
                        $otherProduct->removeOtherPresentation($product->getId());
                        $em->persist($otherProduct);
                        $em->flush();
                    }
                }
            }

            foreach ($productInfo['info']['otherPresentation'] as $id) {
                $otherProduct = $em->getRepository('App:Product')->find($id);
                $otherProduct->addOtherPresentation($product->getId());
                $em->persist($otherProduct);
                $em->flush();
            }
        }

        if (isset($productInfo['file']) && isset($productInfo['file']['bullaFile'])) {
            $file = $this->saveFile($productInfo['file']['bullaFile'], $product, 'product');
            $product->setBullaFile($file);
        }

        $em->persist($product);
        $em->flush();

        return $product;
    }

    private function saveAssetsUpdate($product, $productAssets)
    {
        $em = $this->getDoctrine()->getManager();
        if (is_array($productAssets['file'])) {
            foreach ($productAssets['file'] as $key => $file) {
                $file = $file['file'];
                $type = $productAssets['info'][$key]['type'];
                $id =  isset($productAssets['info'][$key]['id']) ? $productAssets['info'][$key]['id'] : null;
                $fileMetaTitle = $productAssets['info'][$key]['fileMetaTitle'];
                $fileMetaDescription = $productAssets['info'][$key]['fileMetaDescription'];

                if ($id) {
                    $asset = $em->getRepository('App:ProductAsset')->find($id);
                } else {
                    $asset = new ProductAsset();
                }

                if ($file != null) {

                    $asset->setType($type);
                    $asset->setFile($this->saveFile($file, $product, 'product'));
                    $asset->setFileMetaTitle($fileMetaTitle);
                    $asset->setFileMetaDescription($fileMetaDescription);
                    $asset->setProduct($product);

                    $em->persist($asset);
                    $em->flush();
                } else {

                    $asset->setType($type);
                    $asset->setFileMetaTitle($fileMetaTitle);
                    $asset->setFileMetaDescription($fileMetaDescription);
                    $asset->setProduct($product);

                    $em->persist($asset);
                    $em->flush();
                }
            }
        }

        return true;
    }

    private function saveReferredContent($product, $productReferred)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($productReferred as $referred) {
            $r = new ReferrerContent();
            $r->setMainContentId($product->getId());
            $r->setMainContentType('produto');
            $r->setReferrerContentId((int)$referred['contentId']);
            $r->setReferrerContentType($referred['type']);
            $r->setCreatedAt(new \DateTime());
            $r->setUpdatedAt(new \DateTime());

            $em->persist($r);

            $rBack = new ReferrerContent();
            $rBack->setReferrerContentId($product->getId());
            $rBack->setReferrerContentType('produto');
            $rBack->setMainContentId((int)$referred['contentId']);
            $rBack->setMainContentType($referred['type']);
            $rBack->setCreatedAt(new \DateTime());
            $rBack->setUpdatedAt(new \DateTime());

            $em->persist($rBack);
            $em->flush();
        }
    }

    private function saveFile($file, $product, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $product->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
