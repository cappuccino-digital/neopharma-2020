<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;
use App\Entity\BannerType;
use App\Entity\BlogPostType;
use App\Entity\BlogPostCategory;
use App\Entity\Role;
use App\Entity\ProductClassifiationByDistribAndPresc;
use App\Entity\ProductCategory;
use App\Entity\ProductType;
use App\Entity\Badge;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("", name="admin_home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if ($user->getIsAdminClient()) {
            return $this->redirectToRoute('admin_relatorios');
        }

        $userByMonth = $em->getRepository('App:User')->getByMonthGraph();
        $userByState = $em->getRepository('App:User')->getByStateMonthGraph();
        $userByGender = $em->getRepository('App:User')->getByGenderMonthGraph();
        $userByGenderLifetime = $em->getRepository('App:User')->getByGenderLifetime();
        $userByStateLifetime  = $em->getRepository('App:User')->getByStateLifetime();

        $graphMonth = $this->getGraphMonth($userByMonth);
        $graphState = $this->getGraphState($userByState);
        $graphGender = $this->getGraphGender($userByGender);
        $graphStateLifetime = $this->getGraphStateLifetime($userByStateLifetime);
        $graphGenderLifetime = $this->getGraphGenderLifetime($userByGenderLifetime);

        $postByCategory = $em->getRepository('App:BlogPost')->getPostByCategoryGraph();
        $postByType = $em->getRepository('App:BlogPost')->getPostByTypeGraph();
        $postByMonth = $em->getRepository('App:BlogPost')->getPostByMonthGraph();
        $postByViews = $em->getRepository('App:BlogPost')->getPostViewsGraph();
        $postByScore = $em->getRepository('App:BlogPost')->getPostScoreGraph();
        
        $graphPostByType = $this->getPostGraph($postByType);
        $graphPostByCategory = $this->getPostGraph($postByCategory);
        $graphPostByMonthGraph =  $this->getPostMonthGraph($postByMonth);

        $courseCompletes = $em->getRepository('App:UserCourse')->getCourseCompletedTotal();
        $courseSubscribes = $em->getRepository('App:UserCourse')->getCourseSubscribesTotal();
        $courseAvgTimeToComplete = $em->getRepository('App:UserCourse')->getAvgTimeToComplete();
        $courseStatistic = $this->getCourseMonthGraph($courseSubscribes, $courseCompletes);
        
        
        return $this->render('admin/index.html.twig', [
            'graphMonth' => $graphMonth,
            'graphGender' => $graphGender,
            'graphState' => $graphState,
            'graphStateLifetime' => $graphStateLifetime,
            'graphGenderLifetime' => $graphGenderLifetime,
            'graphPostByType' => $graphPostByType,
            'graphPostByCategory' => $graphPostByCategory,
            'graphPostByMonthGraph' => $graphPostByMonthGraph,
            'postByViews' => $postByViews,
            'postByScore' => $postByScore,
            'courseAvgTimeToComplete' => $courseAvgTimeToComplete,
            'courseStatistic' => $courseStatistic
        ]);
    }

     /**
     * @Route("/create-admin", name="admin-create-admin")
     */
    public function saveUser(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user->setName('Cappuccino Admin');
        $user->setUsername('admin');
        $user->setEmail('rodrigo.taira@cappuccinodigital.com.br');
        $salt = md5(uniqid());
        $user->setSalt($salt);
        $user->setPassword($encoder->encodePassword($user, 'cappuccino'));
        $user->setAvatar('default.png');
        $user->setIsActive(true);
        
        
		$em->persist($user);
    	$em->flush();

        $roles = [
            ['name'=>'Balconista', 'role'=>'ROLE_BALCAO'],
            ['name'=>'Farmacêutico', 'role'=>'ROLE_FARMACO'],
            ['name'=>'Admin', 'role'=>'ROLE_ADMIN'],
            ['name'=>'Super Admin', 'role'=>'ROLE_SUPER_ADMIN']
        ];

        foreach($roles as $role){
            $r = new Role();
            $r->setName($role['name']);
            $r->setRole($role['role']);
            $em->persist($r);
        }

        $em->flush();

        $superAdmin = $em->getRepository('App:Role')->findOneByName('Super Admin');
        $user->addRole($superAdmin);
        $em->persist($user);
        $em->flush();
        
        r( 'Super Admin Created');

		return $this->redirectToRoute('admin-create-badges');
    }

     /**
     * @Route("/create-badges", name="admin-create-badges")
     */
    public function createBadges()
    {
        $badges = [
            ['name' => 'Super Neo', 'shortDescription' => 'Somátoria de tudo'],
            ['name' => 'Compartilhador', 'shortDescription' => 'Compartilha mais conteúdo'],
            ['name' => 'Ouvinte de podcast', 'shortDescription' => 'Sscuta mais podcasts'],
            ['name' => 'Fazedor de quiz', 'shortDescription' => 'Faz mais quizes'],
            ['name' => 'Aluno dedicado', 'shortDescription' => 'Faz mais cursos'],
            ['name' => 'Leitor assíduo', 'shortDescription' => 'Leitura de posts do blog'],
            ['name' => 'Fanático por vídeos', 'shortDescription' => 'Visializador de vídeos do blog']
        ];

        $em = $this->getDoctrine()->getManager();

        foreach($badges as $b){
            $nb = new Badge();
            $nb->setName($b['name']);
            $nb->setPointDescription($b['name']);
            $nb->setShortDescription($b['name']);
            $nb->setDescription('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam condimentum lacus ut sapien posuere lacinia. Cras commodo purus ac elit egestas, ac eleifend leo laoreet. Donec eu leo non turpis finibus rutrum consectetur in lectus. Morbi auctor placerat diam eu laoreet. Donec quis ornare mauris. In tincidunt elit sit amet arcu condimentum viverra. Maecenas semper venenatis odio, at tristique magna ornare vitae. Vivamus in felis ac dolor vehicula ornare id ac erat. Aliquam ultricies condimentum est, id laoreet libero finibus non. Donec accumsan diam ac enim aliquet iaculis. Donec tincidunt libero sed dui blandit malesuada nec in sem. Suspendisse blandit ultricies nulla, semper vestibulum tortor iaculis ut. Duis tincidunt ligula a dapibus consequat. In dui eros, finibus ut ex at, iaculis sodales augue. Donec finibus felis risus, ullamcorper lobortis sapien tempus sed. In finibus vel quam ornare imperdiet.</p><p>Nam ultricies ex vitae lorem aliquet, ornare dignissim ligula dapibus. Aliquam ligula ante, dapibus ac est at, venenatis interdum risus. Maecenas maximus, ligula quis euismod volutpat, ligula mi vestibulum diam, id mollis odio diam in nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum malesuada sapien nisl, ut mattis nulla venenatis viverra. Cras lacinia purus orci, ac blandit libero elementum nec. Sed ex turpis, cursus nec interdum ac, lacinia iaculis tortor.</p>');
            $nb->setImage('uploads/badge/'.$b['name'].'.png');
            $nb->setImageMobile('uploads/badge/'.$b['name'].'.png');
            $nb->setIsActive(true);
            $nb->setCreatedAt(new \DateTime());
            $nb->setUpdatedAt(new \DateTime());
            $nb->setMinPoint(10000);
            $nb->setShortDescription($b['shortDescription']);
            $nb->setGivenPoint(5);
            
            $em->persist($nb);
            $em->flush();
        }

        r('Badge Created');

		return $this->redirectToRoute('admin-data-blog');
    }

    /**
     * @Route("/create-data-blog", name="admin-data-blog")
     */
    public function createDataBlog()
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * Create Blog Post
        */

        $blogType = ['post', 'video', 'podcast'];
        $blogCat = ['Produtos', 'Novidades', 'Saúde', 'Doenças'];

        foreach($blogType as $bt){
            $nBT = new BlogPostType();
            $nBT->setName($bt);
            $nBT->setCreatedAt(new \DateTime());
            $nBT->setUpdatedAt(new \DateTime());
            $nBT->setIsActive(true);
            $em->persist($nBT);
            $em->flush();
        }

        foreach($blogCat as $bc){
            $nBC = new BlogPostCategory();
            $nBC->setName($bc);
            $nBC->setCreatedAt(new \DateTime());
            $nBC->setUpdatedAt(new \DateTime());
            $nBC->setIsActive(true);
            $em->persist($nBC);
            $em->flush();
        }

        r( 'Blog Table Populated');
        return $this->redirectToRoute('admin-data-banner');
    }

    /**
     * @Route("/create-data-banner", name="admin-data-banner")
     */
    public function createDataBanner()
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * Create Banner
        */

        $bannerType = ['Home', 'Blog', 'NeoTv','Produtos', 'Cursos', 'Quiz', 'Rank', 'User'];
        

        foreach($bannerType as $bt){
            $nBT = new BannerType();
            $nBT->setName($bt);
            $nBT->setCreatedAt(new \DateTime());
            $nBT->setUpdatedAt(new \DateTime());
            $nBT->setIsActive(true);
            $em->persist($nBT);
            $em->flush();
        }

        r( 'Banner Table Populated');
        return $this->redirectToRoute('admin-data-product');
    }

    /**
     * @Route("/create-data-product", name="admin-data-product")
     */
    public function createDataProduct()
    {
        $em = $this->getDoctrine()->getManager();

        $medType = [
            'Comprimidos',
            'Cápsulas',
            'Drágeas',
            'Pílulas',
            'Soluções',
            'Suspensão',
            'Emulsão',
            'Óvulos',
            'Pomadas',
            'Supositórios',
            'Linimentos',
        ];
        
        foreach($medType as $type){
            $pt = new ProductType();
            $pt->setName($type);
            $pt->setCreatedAt(new \DateTime());
            $pt->setUpdatedAt(new \DateTime());
            $pt->setIsActive(true);

            $em->persist($pt);
            $em->flush();
        }
        
        $distribuicao = [
            'Sem Tarja',
            'Tarja Vermelha',
            'Tarja Preta'
        ];

        foreach($distribuicao as $dist){
            $pd = new ProductClassifiationByDistribAndPresc();
            $pd->setName($dist);
            $pd->setCreatedAt(new \DateTime());
            $pd->setUpdatedAt(new \DateTime());
            $pd->setIsActive(true);

            $em->persist($pd);
            $em->flush();
        }

        $categoria = [
            'Grandes Marcas',
            'Similar',
            'Genérico'
        ];

        foreach($categoria as $dist){
            $pd = new ProductCategory();
            $pd->setName($dist);
            $pd->setCreatedAt(new \DateTime());
            $pd->setUpdatedAt(new \DateTime());
            $pd->setIsActive(true);

            $em->persist($pd);
            $em->flush();
        }

        return new JsonResponse(['status' => 'Done']);
    }

    private function getCourseMonthGraph($courseSubscribes, $courseCompletes)
    {
        $response = [];
        $month = ['x'];

        if(!is_array($courseSubscribes) || !is_array($courseSubscribes)){
            return null;
        }

        foreach($courseSubscribes as $post){
            $dateValue = $post['ano'] .'-'. $post['mes'] . '-01';
            
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }

            $key = array_search ($dateValue, $month);

            if($post['is_pharmaceutical']){
                $response['farmaceutico subscribe'][$key] = $post['total'];
            } else {
                $response['balconista subscribe'][$key] = $post['total'];
            }
        }
        
        foreach($courseCompletes as $post){
            $dateValue = $post['ano'] .'-'. $post['mes'] . '-01';
            
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }

            $key = array_search ($dateValue, $month);

            if($post['is_pharmaceutical']){
                $response['farmaceutico complete'][$key] = $post['total'];
            } else {
                $response['balconista complete'][$key] = $post['total'];
            }
        }

        

        $totalKeys = count($month);

        $return = [$month];
        foreach($response as  $key => $values){
            for($i = 0; $i < $totalKeys; $i++){
                if($i == 0){
                    $temp[$i] = $key;
                } else {
                    if(!isset($values[$i])){
                        $temp[$i] = '';
                    } else {
                        $temp[$i] = $values[$i];
                    }
                }

            }
            $return[] = $temp;
        }


        return $return;
    }

    private function getPostGraph($postData)
    {
        $response = [];

        if(!is_array($postData)){
            return null;
        }

        foreach($postData as $post){
            $response[] = [$post['name'], $post['total']];
        }
        
        return $response;
    }

    private function getPostMonthGraph($postData)
    {
        $response = ['Posts'];
        $month = ['x'];

        if(!is_array($postData)){
            return null;
        }

        foreach($postData as $post){
            $dateValue = $post['ano'] .'-'. $post['mes'] . '-01';
            
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }

            $key = array_search ($dateValue, $month);
            $response[$key] = $post['total'];
        }
        
        return [$month, $response];
    }

    private function getGraphStateLifetime($userData)
    {
        $response = [];

        if(!is_array($userData)){
            return null;
        }

        foreach($userData as $user){
            if($user['is_pharmaceutical']){
                $response['farmaceutico'][] = [$user['state'], $user['total']];
            } else {
                $response['balconista'][] = [$user['state'], $user['total']];
            }
        }
        return $response;
    }

    private function getGraphGenderLifetime($userData)
    {
        $dataMonthBalcMasc = ['Masculino'];
        $dataMonthBalcFem = ['Feminino'];
        $dataMonthPharmacMasc = ['Masculino'];
        $dataMonthPharmacFem = ['Feminino'];

        if(!is_array($userData)){
            return null;
        }

        foreach($userData as $user){

            if($user['is_pharmaceutical']){
                if($user['gender'] == 'female'){
                    $dataMonthPharmacFem[] = $user['total'];
                } else {
                    $dataMonthPharmacMasc[] = $user['total'];
                }
            } else {
                if($user['gender'] == 'female'){
                    $dataMonthBalcFem[] = $user['total'];
                } else {
                    $dataMonthBalcMasc[] = $user['total'];
                }
            }
        }

        $response['farmaceutico'] = [$dataMonthPharmacMasc,$dataMonthPharmacFem];
        $response['balconista'] = [$dataMonthBalcMasc,$dataMonthBalcFem];

        return $response;
    }

    private function getGraphState($userData)
    {
        
        $month = ['x'];
        $data = [];
        $response = [];

        if(!is_array($userData)){
            return null;
        }

        foreach($userData as $user){
            $dateValue = $user['ano'] .'-'. $user['mes'] . '-01';
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }
            
            $key = array_search ($dateValue, $month);
            if($user['is_pharmaceutical']){
                    if(!isset($data['farmaceutico'][$user['state']])){
                        $data['farmaceutico'][$user['state']] = [];
                    }

                    $nextKey = count($data['farmaceutico'][$user['state']]);

                    if($key > $nextKey){
                        while($key != $nextKey){
                            $data['farmaceutico'][$user['state']][] = '';
                            $nextKey = count($data['farmaceutico'][$user['state']]);
                        }
                    }
                    $data['farmaceutico'][$user['state']][$key] = $user['total'];
            } else {
                if(!isset($data['balconista'][$user['state']])){
                    $data['balconista'][$user['state']] = [];
                }

                $nextKey = count($data['balconista'][$user['state']]);

                if($key > $nextKey){
                    while($key != $nextKey){
                        $data['balconista'][$user['state']][] = '';
                        $nextKey = count($data['balconista'][$user['state']]);
                    }
                }
                $data['balconista'][$user['state']][$key] = $user['total'];
            }
        }
        
        
        $totalKeys = count($month);

        $returnBalconista = [$month];
        
        if(isset($data['balconista'])){
            foreach($data['balconista'] as $k=>$d){
                for($i = 0; $i < $totalKeys; $i++){
                    if($i == 0){
                        $d[$i] = $k;
                    }
                    if(!isset($d[$i])){
                        $d[$i] = '';
                    }
                }
                $returnBalconista[] = $d;
            }
        }

        $returnFarmaceutico = [$month];
        
        if(isset($data['farmaceutico'])){
            foreach($data['farmaceutico'] as $k=>$d){
                for($i = 0; $i < $totalKeys; $i++){
                    if($i == 0){
                        $d[$i] = $k;
                    }
                    if(!isset($d[$i])){
                        $d[$i] = '';
                    }
                }
                $returnFarmaceutico[] = $d;
            }
        }        
        
        $response['balconista'] = $returnBalconista;
        $response['farmaceutico'] = $returnFarmaceutico;
        
        return $response;
    }

    private function getGraphGender($userData)
    {
        
        $month = ['x'];
        $dataMonthBalcMasc = ['balconista masculino'];
        $dataMonthBalcFem = ['balconista feminino'];
        $dataMonthPharmacMasc = ['farmacêutico masculino'];
        $dataMonthPharmacFem = ['farmacêutico feminino'];

        if(!is_array($userData)){
            return null;
        }

        foreach($userData as $user){

            $dateValue = $user['ano'] .'-'. $user['mes'] . '-01';
            
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }

            $key = array_search ($dateValue, $month);

            if($user['is_pharmaceutical']){
                if($user['gender'] == 'female'){
                    $nextKey = count($dataMonthPharmacFem);

                    if($key > $nextKey){
                        while($key != $nextKey){
                            $dataMonthPharmacFem[] = '';
                            $nextKey = count($dataMonthPharmacFem);
                        }
                    }
                    $dataMonthPharmacFem[$key] = $user['total'];
                } else {
                    $nextKey = count($dataMonthPharmacMasc);

                    if($key > $nextKey){
                        while($key != $nextKey){
                            $dataMonthPharmacMasc[] = '';
                            $nextKey = count($dataMonthPharmacMasc);
                        }
                    }
                    $dataMonthPharmacMasc[$key] = $user['total'];
                }
            } else {
                if($user['gender'] == 'female'){
                    $nextKey = count($dataMonthBalcFem);

                    if($key > $nextKey){
                        while($key > $nextKey){
                            $dataMonthBalcFem[] = '';
                            $nextKey = count($dataMonthBalcFem);
                        }
                    }

                    $dataMonthBalcFem[$key] = $user['total'];
                } else {
                    $nextKey = count($dataMonthBalcMasc);

                    if($key > $nextKey){
                        while($key > $nextKey){
                            $dataMonthBalcMasc[] = '';
                            $nextKey = count($dataMonthBalcMasc);
                        }
                    }

                    $dataMonthBalcMasc[$key] = $user['total'];
                }
            }
        }

        $response = [$month,$dataMonthBalcMasc,$dataMonthBalcFem,$dataMonthPharmacMasc,$dataMonthPharmacFem];
        $totalKeys = count($response[0]);
        $returnArray = [$month];
        foreach($response as $key => $values){
            if($key != 0){
                for($i = 0; $i < $totalKeys; $i++){
                    if(!isset($values[$i])){
                        
                        $values[$i] = '';
                    }
                }
                $returnArray[] = $values;
            } 
        }
        return $returnArray;
    }

    private function getGraphMonth($userData)
    {
        $month = ['x'];
        $dataMonthBalc = ['balconista'];
        $dataMonthPharmac = ['farmacêutico'];

        if(!is_array($userData)){
            return null;
        }

        foreach($userData as $user){

            $dateValue = $user['ano'] .'-'. $user['mes'] . '-01';
            if(!\in_array($dateValue , $month)){
                $month[] =  $dateValue;
            }

            $key = array_search ($dateValue, $month);

            if($user['is_pharmaceutical']){
                $nextKey = count($dataMonthPharmac);

                if($key > $nextKey){
                    while($key != $nextKey){
                        $dataMonthPharmac[] = '';
                        $nextKey = count($dataMonthPharmac);
                    }
                }
                $dataMonthPharmac[$key] = $user['total'];
            } else {
                $nextKey = count($dataMonthBalc);

                if($key > $nextKey){
                    while($key > $nextKey){
                        $dataMonthBalc[] = '';
                        $nextKey = count($dataMonthBalc);
                    }
                }

                $dataMonthBalc[$key] = $user['total'];
            }
        }
        
        
        $response = [$month, $dataMonthBalc, $dataMonthPharmac];
        $totalKeys = count($response[0]);
        $returnArray = [$month];
        
        foreach($response as $key => $values){
            if($key != 0){
                for($i = 0; $i < $totalKeys; $i++){
                    if(!isset($values[$i])){
                        
                        $values[$i] = '';
                    }
                }
                $returnArray[] = $values;
            } 
        }
        return $returnArray;
    }

    public function getMed($index){
        $meds = ['Equipo enteral Amika Universal - sem bolsa','neuropad®','Stratone Frontal','Porta Agulha','Conjunto de Instrumentais TESSYS - JOIMAX','Ótica Ultra','INSTRUMENTAL CIRÚRGICO EM TITÂNIO REDA','APANHADOR DE SUTURA KARL STORZ','tissue trimmer nti','Aparelhos de Audição Intracanal Digital A&M','Ferramentas para proGAV 2.0','ALUETTA ®','INSTRUMENTAIS ARTICULADOS NÃO CORTANTES PIUS LANG','instrumentos cirurgicos','ROEKO ENDO-FROST','ScalpelCric','CAPNOPEN™','FRACTIOMATIC X2','Brocas Diamantadas de perfuração óssea HiCut Adeor.','INSTRUMENTAIS PARA IMPLANTE ORTOPÉDICO','CONTAINERS DENTAL AESCULAP','Família de Instrumentos NOIR Aesculap','Software de análise de ECG Holter','Cânula Nasal Medin','CÂNULA DE TRAQUEOSTOMIA TRACOE COMFORT REF 102-D e REF 202-D','ANKYLOS Kit Cirúrgico BoneExpander','CÂNULA DE TRAQUEOSTOMIA TRACOE COMFORT FENESTRADA COM CONECTOR DE 15MM E ADAPTADOR DE 22MM','Fio Guia Hidrofílico EPFlex','VarseoWax Surgical GUIDE','Introdutores Canulados Tubalnsert','Limas de Aço Inoxidável Estéreis','CHAVE CIRÚRGICA INTERCUS','UNIDADE DE SUPRIMENTO DE TERAPIA INTENSIVA','CÂNULA INSULADA PARA ESTIMULAÇÃO E INJEÇÃO DE FARMACOS - PK BLOCK','F-OCTANE','F-OCTANE','Limas Níquel Titânio Estéreis (Reciprocantes)','CÂNULA DE TRAQUEOSTOMIA TRACOE VARIO COM FLANGE AJUSTÁVEL','Endoscópio','SISTEMA PARA PUNÇÃO, SUCÇÃO E IRRIGAÇÃO RZ','SMARTMATIC ENDO','Valvula para drenagem externa Uromed - 1501','FINALTOUCH','Accu-Chek Insight Adapter & Tube','Accu-Chek Flexlink','ICON VESTIBULAR','ANKYLOS Kit Protético Mini','Órteses para membros inferiores','Instrumentais para Microcirurgia oftalmológica - Canetas para aspiração #3','Familia de liga odontologica a base de niquel e cromo','ANKYLOS KIT SELEÇÃO DE PILARES','FIO GUIA TEFLONADO','SONDAS DILATADORAS AMPLATZ','Cânulas de ressecção óssea para coluna diamantadas','ankylos Kit Protetico Balance','KIT DE INSTRUMENTAIS PARA FIO DE CERCLAGEM - MARQUARDT','Mesa de Operações Móvel MEERA','Dynamesh','PINOS GUIA ARTHREX','Limas Reciproc Blue','Equipamento de Ressonância Magnética','Cesta Extratora de Cálculos Sem Ponta Endomaster','Lente Objetiva RIWO Não Autoclavável','OBTURADORES ARTHREX','LUMBAMED DISC','Obturadores I Synergy','GALILEO NEO','KIT DE DILATADORES TELESCÓPICOS KARL STORZ','Actimove® Clavícula','CATETER DESCARTÁVEL ERCP - CAN1','FAMÍLIA DE LÂMINAS','CESTA DE EXTRAÇÃO DE CÁLCULO','SPLINT NASAL EXTERNO','Broca White Post DC','Brocas Rotacionais Diamantadas','CONTAINERS MICROSTOP®','ELEMENTO DE TRABALHO','UMP - CONJUNTO ULTRA-MINI PCNL','INSUFLADOR PARA LAPAROSCOPIA H-TECH','Endolight Flex LED','Actimove® TaloWrap','Agulhas de Localização Mamária','ARO DE FIXAÇÃO PARA RETRATOR CIRURGICO','Instrumentais para microcirurgia oftalmológica - Cânulas lacrimais','KIT FIO GUIA MACS II ESTÉRIL','Bainha de Trocarte Plástico','ADMIRA FUSION FLOW','SISTEMA DE ARQUIVAMENTO E COMUNICAÇÃO DE IMAGEM (PACS) SYNGO.PLAZA','EQUIPAMENTO DE RESSONÂNCIA MAGNÉTICA','PLACA ORTODÔNTICA ERKODENT ERKODUR PETG','Trocartes KARL STORZ','Tubos para uso com Equipamentos de Irrigação/Aspiração KARL STORZ','EMBALAGEM TRANSPARENTE TYVEK/FILME','Instrumental para Placas de Mão Hermann','MEDIVEN ULCER KIT','Equipamento de Ressonância Magnética MAGNETOM Spectra','Cateter de Termodiluição PiCCO com fio guia de nitinol, luer Terlux e vasodilatador','Litotriptor Reutilizável - LIT2','Instrumental para Reconstrução de Rádio Hermann','Mesas cirúrgicas Operon','Stainless Steel Scissors Geister','Kit baixo priming HD/HF Diapact CRRT','Aparelho Auditivo Retro Digital Signia','Dispenser QuickMix','MEDIVEN BRAÇADEIRA DE COMPRESSÃO','EMBALAGEM TRANSPARENTE PAPEL/FILME','Filtros Estéreis KARL STORZ','Instrumentais para microcirurgia oftalmológica – Peça de mão #2','Actimove® Sling on Roll','Instrumentais para Microcirurgia oftalmológica – Canetas para aspiração #2','CÂNULA PARA MARCAÇÃO DE NÓDULO PULMONAR','INSTRUMENTAL NÃO ARTICULADO CORTANTE PARA OSTEOSSÍNTESE','SISTEMA  RETRATOR VAGINAL','BICO PARA LÁBIO LEPORINO NUK','ScanPost','Cânula de Traqueostomia Comfort - Ref.: 108','INSTRUMENTO NÃO ARTICULADO CORTANTE TC GOLD','Kit CCR Retrator Cervical Anterior','Ponteiras com Agulhas Cheyenne','Polidores Meisinger','Cânula de Traqueostomia Twist Plus com Insertor Atraumático – Ref.: 311','DT 100','BROCA DIAMANTADA NTI','DILATADORES RENAIS COM BAINHA AMPLATZ','AGULHA MULTIFLY DESCARTÁVEL PARA S-MONOVETTE® SARSTEDT','FAMÍLIA DE PINÇAS DE ARTROSCOPIA','CANNULA REPLACEMENT VALVES PACKS # 3','BROCAS DIAMANTADAS NTI-KAHLA','Polidor Twist','INSTRUMENTAL ARTICULADO CORTANTE MAXILOFACIAL','Cateter para Retirada de Calculos TRITON','ESFIGMOMANÔMETRO BIG BEN RIESTER','EXTRATOR DE CÁLCULOS','SISTEMA ZMM DE MOTOR PNEUMÁTICO ADEOR','FIXADOR EXTERNO TSF','Polidor com Diamante','Splint Nasal Externo em Aluminio Spiggle & Theis','KIT DE DILATADORES RENAIS DE PEQUENA DIMENSÃO AMPLATZ','FIXADORES EXTERNOS - PLATAFORMAS MEMBROS INFERIORES - MAHE','EURO BLOT ONE','Instrumental Articulado Cortante com Inserto Hermann','EXTAVIPRO 30G Kit de Aplicação','MEDIVEN STRUVA 23.','INSTRUMENTAL CIRÚRGICO ARTICULADO E NÃO CORTANTE EM INOX DIAMANTE REBSTOCK','SERINGA KARL STORZ','POLIDORES  DE CARBETO DE SILÍCIO DHSILIC','BD Bruker IVD MALDI Biotyper system','SHAVER - SISTEMA PARA CONTROLE DE POTÊNCIA','MARCADOR DE NÓDULO MAMÁRIO EM AÇO MOLLER.','FONTE DE LUZ LED','LEUKOMED','INSTRUMENTAL CIRÚRGICO PARA VIDEOCIRURGIA NÃO ARTICULADO NÃO CORTANTE DOCTOOLS','CONTAINERS E ACESSÓRIOS REDA','INSTRUMENTAIS CIRURGICOS - RISA','INSTRUMENTOS PARA MICROCIRURGIA OFTALMOLOGICA NÃO ARTICULADOS / NÃO CORTANTES / NÃO ESTEREIS EM TITANIO E NITINOL - RETRATORES','Brocas de Perfuração óssea RISA','APARELHO AUDITIVO INTRA-AURICULAR HORMANN','BROCAS CARBIDE MEISINGER','Kit Instrumental Metha Mono Aesculap','LANCETAS COAGUCHEK','MARS PTA-BALOON CATHETER','CABO DE LUZ','APARELHO AUDITIVO INTRAAURICULAR HANSATON','TUBO LARÍNGEO','REBILDA DC','TOUCA REUTILIZÁVEL PARA TERAPIA NCPAP','CENTRÍFUGA REFRIGERADA 5427 R','Circuito Ventilatório pediátrico para Umidificador Fisher & Paykel (8411041)','FIXADORES EXTERNOS MODULARES  SMITH & NEPHEW','SPINOMED III','EQUIPAMENTO DE PROTEÇÃO PESSOAL PARA PROTEÇÃO DE RADIAÇÃO IONIZADA','BROCAS DIAMANTADAS','EQUIPOS BBM LIVRES DE PVC, NÃO FOTOSSENSÍVEIS PARA INFUSÃO PARENTERAL EM BOMBAS DE INFUSÃO','Equipo Descartável Richard Wolf','CLASSICSTAR MÁSCARAS NASAIS','Fio-guia de Nitinol','FIXADORES DE TUBO','CÂNULA KARL STORZ','KIT DE INSTRUMENTOS PARA COLUMBUS REVISÃO','CONTAINER PARA INSTRUMENTAL KARL STORZ','INSTRUMENTAL EM TITÂNIO CORTANTE NÃO ARTICULADO AESCULAP','TURBINA','FIXADOR EXTERNO HEXAPODE POLI HEX','FIBRA LASER REUTILIZÁVEL KARL STORZ','SAPATILHAS DESCARTÁVEIS HYGOMAT','INSTRUMENTAL ARTICULADO NÃO CORTANTE','MANGUITO DE EXSANGUINAÇÃO','QUICKTRACH','ESCOVA DE CITOLOGIA','INSTRUMENTAIS CORTANTES DESMONTÁVEIS KARL STORZ','FIXADOR EXTERNO CUBE FIX LITOS','Pinça Não-Articulada Karl Storz','APARELHO AUDITIVO RETRO-AURICULAR HANSATON','MÁQUINAS CHEYENNE PARA DERMOPIGMENTAÇÃO','BROCAS ODONTOLÓGICAS NTI','DRENO PARA SUCÇÃO CAPILAR PENROSE DE SILICONE DAHLHAUSEN','INFUSOR A PRESSÃO - USO UNICO','SONDA EXTRATORA TIPO BALÃO','BOCAL PARA ENDOSCOPIA AUTOCALVÁVEL','INTERFACE KIT - 520F','CITO CENTRÍFUGA','espelho frontal ziegler','ESTILETE','INSTRUMENTAIS CIRÚRGICOS CORTANTES ARTICULADOS MEDICON','RIESTER ESTETOSCÓPIOS','FIO ORTODÔNTICO EM NITI','INTRODUTOR INTRADYN ARTERIAL','SLING PÉLVICO','SISTEMA DE AGULHAS DE FINA ASPIRAÇÃO POR ECO-ENDOSCOPIA','CÂNULAS PARA BIOPSIA PAJUNK','INSTRUMENTAL TARIC','KIT INSTRUMENTAL COMPLEMENTAR COLUMBUS AESCULAP','MULTISCOPE ASAP','KIT INSTRUMENTAL PARA SISTEMA RADIO DISTAL MARQUARDT','VISOMAT HANDY IV','MASTERCYCLER PRO','INSTRUMENTOS ARTICULADOS NÃO CORTANTES B','INSTRUMENTAL PARA ENDOSCOPIA NÃO CORTANTE NÃO ARTICULADO','ENAMEL PLUS HRI','KIT INSTRUMENTAL PARA SISTEMA PEDUS - MARQUARDT','ESCOVA PARA CITOLOGIA MTP','INSTRUMENTAL NÃO CORTANTE NÃO ARTICULADO MEDICON','PORTA AGULHAS ENDOPATH','3M ESPE RELYX VENEER CIMENTO RESINOSO FOTOPOLIMERIZÁVEL PARA FACETA','CAIXA DE INSTRUMENTAL PARA CIRURGIAS DE IMPLANTAÇÃO DE GAIOLAS MOBIS','FAMÍLIA THROMBOLYZER','SILAGUM COMFORT','Posicionador de Anel Estereotáxico.','LC PARTIGEN ALFA1-ANTITRIPSINA','TROCARTE DE SEGURANCA COM TRAVA AUTOMATICA REDA','3M ESPE PROTEMP 4 - MATERIAL PROVISÓRIO À BASE DE BISACRIL','GLUMA DESENSITIZER','Tunelizadores Descartáveis Aesculap','OBTURADORES','FAMÍLIA DE INSTRUMENTAIS ARTICULADOS NÃO CORTANTES P/ CIRURGIA DE COLUNA GUIADA POR IMAGENS BRAINLAB','INSTRUMENTOS CORTANTES ARTICULADOS','COMPOSEAL MOBILEA','SUPRASORB F - CABERTURA DE FILME EM ROLO PARA FERIDAS','IMOBILIZADOR PARA MÃOS BAUERFEIND','CENTRÍFUGAS VENTILADAS','ÓRTESE LOMBAR BAUERFEIND','MEIO DE CONTROLE DA ARTICULAÇÃO E OCLUSÃO','ALEGRIA','STATUS BLUE','ESTETOSCOPIO RIESTER','INSTRUMENTAL ARTICULADO CORTANTE REDA','EQUIPO VOLUMAT AGILIA ST01','EQUIPO VOLUMAT AGILIA VL PA02','INSTRUMENTAIS ARTICULADOS NÃO CORTANTES PARA ARTROSCOPIA','IONOSEAL','BROCAS CIRURGICAS EM ACO INOXIDAVEL KOMET MEDICAL','INSTRUMENTAL ARTICULADO NAO CORTANTE HERMANN','INSTRUMENTAL NAO ARTICULADO CORTANTE HERMAN','INSTRUMENTAIS CORTANTES ARTICULADOS H-TECH.','KIT INSTRUMENTAL GAMMA3 STRYKER','Video-endoscópio Karl Storz','CENTRIFUGA REFRIGERADA HETTICH','INSTRUMENTAL PARA SISTEMA HBS PARA FIXACAO DE FRATURAS INTRA-ARTICULARES MARTIN','KIT INSTRUMENTAL PARA CIRURGIA DE COLUNA ULRICH','APARELHO INSUFLADOR DE CO2 AESCULAP','KIT INSTRUMENTAL PARA IMPLANTES DE ANGULO FIXO PARA RADIO TREU','INSTRUMENTAIS CORTANTES NAO ARTICULADOS H-TECH','FAMILIA CADEIRA DE RODAS','CANULAS PARA CIRURGIA LAPAROSCOPICA','BOMBA DE IRRIGACAO PARA ARTROSCOPIA','CENTRIFUGA NAO REFRIGERADA','ENDOSCOPIO UROLOGICO RIGIDO','VIDRION CONDICIONADOR DENTINA','HEMOLIGHT PLUS','ETI–SS-B (La)','ETI–RNP/Sm','AGULHA PARA SUTURA ACUFIRM','CFAS PROTEINA','OPTOSIL XANTOPREN','CURATIVO TRANSPARENTE TEGADERM 3M','PERFUSOR SET PE - DISPOSITIVO PARA A EXTENSAO DEEQUIPOS','TELESCÓPIO','PINÇA HEMOSTÁTICA REDA','Prova de Espaçador de Quadril Subiton','Centro de Diagnóstico Móvel EXO (CDM)','KIT DE INSTRUMENTAIS CIRÚRGICOS REUTILIZAVEIS NOVAX','CAMPOS DE INCISÃO IODADOS','BAINHA DE ACESSO URETERAL INDRIVE','SEGURDENT PÓ ADESIVO PARA DENTADURAS','FRASCO UMIDIFICADOR','COMPONENTES E ARTIGOS PARA CONFECÇÃO DE ÓRTESES ORTOPÉDICAS EXTERNAS DE TRONCO','IONOMASTER F','AGULHA PARA BIOPSIA DE TECIDO MOLE HISTO','EQUIPO PLASTICO PARA RESSECCAO TRANSURETRAL RIVERO','ULTRACAPS S','RIVA LUTING - CIMENTO DE IONOMERO DE VIDRO','WAVE-COMPOSITO FOTOPOLIMERIZAVEL FLUIDO','KIT LINHA DE SANGUE PARA HEMODIÁLISE A099-V602(B)-B','EASI-V DISPOSITIVO PARA FIXAÇÃO DE LINHAS PERIFÉRICAS INTRAVENOSAS NA PELE','MONITOR OR1','pinça flexível reutilizável tipo cortante para procedimentos endoscópicos','IMPRESSORA DRYSTAR','FITA ADESIVA CIRÚRGICA Avery Dennison Medical Solutions','FAMÍLIA DE PRODUTOS ARTICULADOS CORTANTES DESCARTÁVEIS','FILME RADIOGRAFICO DENTAL AGFA INTRA-ORAL','COMPONENTES E ARTIGOS PARA CONFECÇÃO DE ÓRTESES ORTOPÉDICAS EXTERNAS DE TRONCO','PACOTE CIRÚRGICO PARA PARTO I','CÂNULA ESTIMULADORA DE RF - ANGULADA','ADAPTADOR DESCARTÁVEL PARA SONDA DIROS – ESTÉRIL','CABOS DIROS','CÂNULA ANGULADA HÍBRIDA COM INJECT','SISTEMA AMPLIFICADOR EEG','FL36 Hospital Bed','SUPORTE E POSICIONADOR PARA MEMBRO SPIDER','SISTEMA CLARITY','FAST 1','FAST X','SPIDER TECH','MAX','K-Y SENSUAL MASSAGE 2-EM-1 HOT','Lavadora/desinfetadora Reliance','CANULAS DESCARTAVEIS OWL','SERINGA DESCARTÁVEL COM AGULHA EMBRAMAC','PUPILOMETRO','AVENTAIS CIRÚRGICOS AERO CHROME* RESPIRÁVEL','AVENTAIS CIRÚRGICOS AERO BLUE*','PASTILHA TERMOPLÁSTICA COM FURO – PROMISEE DENTAL BRASIL','Lâmpada de Fenda Luxvision','APLICADORES ODONTOLÓGICOS – PROMISEE DENTAL BRASIL.','MEDIDOR DE PRESSÃO KN550BT','NEBULIZADOR NB-02','KIT CIRÚRGICO ORTOPÉDICO III – EXTREMIDADE INFERIOR','PISTOLA APLICADORA ODONTOLÓGICA – PROMISEE DENTAL BRASIL.','Eletrodo De Superfície Descartável Friendship Medical','Instrumentos Auxiliares na Pratica Odontologica Impla','FEVER FRIENDS BABYDEAS','Equipo enteral Amika ENFit - com bolsa','Instrumental Cirúrgico Metálico Não Articulado Não Cortante','LUVA PARA PROCEDIMENTO NÃO CIRÚRGICO DE VINIL COM PÓ','ESTERILIZA','Conector Descartável Hospitak','ENDOBAG DESCARTÁVEL HW','ESCALPE DESCARTÁVEL DESCARPACK II','AGULHA INJEX PARA CANETA','AVENTAL CIRÚRGICO  DESCARTÁVEL ESTERILIZADO','Conector Y Tipo Push','Micromotor Endodôntico para preparação de canal com localizador apical','Máscara Facial Anestésica','PULSEIRA DE IDENTIFICAÇÃO','CGH - Acticontrol Basic - Kit Universal II','Eletrodo para ECG','Cânula de Sucção e Irrigação HangZhou','Coletor Rígido Descarpack para Resíduos Perfurocortantes III','Lancetador Orbispharma','TUBO DE TRAQUESTOMIA COM BALÃO DESCARTÁVEL SOLIDOR®','Máscara Nasal Wisp','kit campo cirúrgico esterilizado descartavel','CONSULTÓRIO ODONTOLÓGICO','PROJETOR OFTALMOLÓGICO DE ACUIDADE VISUAL','Embalagem Para Esterilização','CURATIVO FILME TRANSPARENTE ESTÉRIL IV','Bocais Argus','campo cirurgico descartável esterilizado','MÁSCARA LARÍNGEA REFORÇADA DE SILICONE DE USO ÚNICO','insuflador de balão'];
        return $meds[$index];
    }

    public function randomDateInRange(\DateTime $start, \DateTime $end) {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new \DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate;
    }

    public function getStateCity(){
        
        $places = json_decode(\file_get_contents($this->get('kernel')->getProjectDir() . '/public_html/vendor/state_cities.json'), true);
        $states = $places['estados'];
        $key = rand(0, 26);

        $place = $states[$key];
        $cities = $place['cidades'];
        $response['state'] = $place['sigla'];
        $response['city'] = $cities[rand(0, count($cities) - 1)];

        return $response;
    }

    public static function cnpjRandom($mascara = "1") {
        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
        $n9 = 0;
        $n10 = 0;
        $n11 = 0;
        $n12 = 1;
        $d1 = $n12 * 2 + $n11 * 3 + $n10 * 4 + $n9 * 5 + $n8 * 6 + $n7 * 7 + $n6 * 8 + $n5 * 9 + $n4 * 2 + $n3 * 3 + $n2 * 4 + $n1 * 5;
        $d1 = 11 - (self::mod($d1, 11) );
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n12 * 3 + $n11 * 4 + $n10 * 5 + $n9 * 6 + $n8 * 7 + $n7 * 8 + $n6 * 9 + $n5 * 2 + $n4 * 3 + $n3 * 4 + $n2 * 5 + $n1 * 6;
        $d2 = 11 - (self::mod($d2, 11) );
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . "." . $n3 . $n4 . $n5 . "." . $n6 . $n7 . $n8 . "/" . $n9 . $n10 . $n11 . $n12 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $n10 . $n11 . $n12 . $d1 . $d2;
        }
        return $retorno;
    }
    
    /**
     * Método para gerar CPF válido, com máscara ou não
     * @example cpfRandom(0)
     *          para retornar CPF sem máscar
     * @param int $mascara
     * @return string
     */
    public static function cpfRandom($mascara = "1") {
        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
        $n9 = rand(0, 9);
        $d1 = $n9 * 2 + $n8 * 3 + $n7 * 4 + $n6 * 5 + $n5 * 6 + $n4 * 7 + $n3 * 8 + $n2 * 9 + $n1 * 10;
        $d1 = 11 - (self::mod($d1, 11) );
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n9 * 3 + $n8 * 4 + $n7 * 5 + $n6 * 6 + $n5 * 7 + $n4 * 8 + $n3 * 9 + $n2 * 10 + $n1 * 11;
        $d2 = 11 - (self::mod($d2, 11) );
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . $n3 . "." . $n4 . $n5 . $n6 . "." . $n7 . $n8 . $n9 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $d1 . $d2;
        }
        return $retorno;
    }
    
    /**
     * @param type $dividendo
     * @param type $divisor
     * @return type
     */
    private static function mod($dividendo, $divisor) {
        return round($dividendo - (floor($dividendo / $divisor) * $divisor));
    }
}
