<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
/**
 * @Route("/produtos")
 */
class ProductController extends Controller
{
    /**
     * @Route("", name="product_home")
     */
    public function index(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }
        
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('home');
        }

        $em = $this->getDoctrine()->getManager();
        $banners = $em->getRepository('App:Banner')->getBannerByTypeName('produtos');
        $categories = $em->getRepository('App:ProductCategory')->findBy(['isActive' => true]); 
        
        $builder = $em->getRepository('App:Product')->createQueryBuilder('p')
                        ->innerJoin('App:ProductType', 'pt' , 'WITH' , 'p.type = pt.id')
                        ->innerJoin('App:ProductCategory', 'pc' , 'WITH' , 'p.category = pc.id')
                        ->innerJoin('App:ProductSistemicClass', 'ps' , 'WITH' , 'p.sistemicClass = ps.id');

        if ($request->query->has('q')) {
            $builder
                ->where('p.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        if ($request->query->has('category') && $request->query->get('category') != "") {
            $builder
                ->andWhere("pc.slug = :category")
                ->setParameter('category', $request->query->get('category'));
        }
        
        if ($request->request->has('filter')) {
            $filter = $request->request->get('filter');

            if(isset($filter['activeSubstance']) && $filter['activeSubstance'] != ""){
                $builder->andWhere('p.activeSubstance = :activeSubstance')
                    ->setParameter('activeSubstance', $filter['activeSubstance']);
            }
            
            if(isset($filter['sistemicClass']) && $filter['sistemicClass'] != ""){
                $builder
                ->andWhere("ps.id = :sistemicClass")
                    ->setParameter('sistemicClass', $filter['sistemicClass']);
            }

            if(isset($filter['type']) && $filter['type'] != ""){
                $sqlType = '';
                foreach ($filter['type'] as $key => $type){
                    reset($filter['type']);
                    if ($key === key($filter['type'])){
                        $sqlType .= "pt.slug = '{$type}'";
                    } else {
                        $sqlType .= " OR pt.slug = '{$type}'";
                    }
                }
                $builder->andWhere($sqlType);
            }
         
        }

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("p.isOnlyToPharmaceutical = false");
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                $builder->andWhere("p.isOnlyToPharmaceutical = false");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("p.isOnlyToPharmaceutical = false");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar os produtos."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            }
        }

        $builder->andWhere("p.isActive = true");

        $builder->orderBy('p.name','ASC');

        $products = $builder->getQuery()->getResult();

        $products = $this->get('knp_paginator')->paginate(
            $products,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('product/index.html.twig', [
            'banners' => $banners,
            'products' => $products,
            'categories' => $categories,
            'selectCat' => $request->query->has('category') ? $request->query->get('category') : '',
            'filter' => isset($filter) ? $filter : null
        ]);
    }

    /**
     * @Route("/{slug}", name="product_detail")
     */
    public function produtoDetail($slug)
    {

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            return $this->redirectToRoute('home');
        }

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App:Product')->findOneBySlug($slug);

        if(!$product){
            return $this->redirectToRoute('home');
        }

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$product->getIsPublic()){
            return $this->redirectToRoute('product_home');
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && $product->getIsOnlyToPharmaceutical()){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                return $this->redirectToRoute('product_home');
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && $product->getIsOnlyToPharmaceutical()){
                return $this->redirectToRoute('product_home');
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->getUser()->getIsRegisterComplete() && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar os produtos."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            }
        }

        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContentView($product->getId(),'produto');

        $otherPresentations = $em->getRepository('App:Product')->getProductOtherPresentation($product->getOtherPresentation());
        
        $isSaved = false;
        $user = $this->getUser();
        if($user){
            $em->getRepository('App:UserPoint')->setPointsToBadge($user, $product, 'informacoes-sobre-medicamentos', 'produto');
            $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'consultar-um-produto-da-marca', 'produto', $product->getId()); //Gamificação interação

            $isFavorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $product->getId(), 'type' => 'product', 'user' => $user]);
            if($isFavorite){
                $isSaved = true;
            }
        }

        return $this->render('product/detail.html.twig', [
            'product' => $product,
            'contentReferrer' => $contentReferrer,
            'otherPresentations' => $otherPresentations,
            'isSaved' => $isSaved
        ]);
    }

    public function filterProduct($filter, $selectCat){
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:ProductType')->findBy(['isActive' => true]);
        $activeSubstance = $em->getRepository('App:Product')->getProductsByActiveSubstance();
        $sistemicClasses = $em->getRepository('App:ProductSistemicClass')->findByIsActive(true);        


        return $this->render('component/product-filter.html.twig', [
            'filter' => $filter,
            'types' => $types,
            'selectCat' => $selectCat,
            'activeSubstance' => $activeSubstance,
            'sistemicClasses' => $sistemicClasses,
        ]);
    }
}
