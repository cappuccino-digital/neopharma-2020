<?php

namespace App\Controller;

use App\Entity\QuizAllAlternatives;
use App\Entity\QuizAllQuestions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/admin/quiz-novo/perguntas")
 */
class AdminQuizAllQuestionController extends Controller
{
    /**
     * @Route("/", name="admin_quiz_all_question")
     */
    public function index(Request $request ): Response
    {
        $builder = $this
        ->getDoctrine()
        ->getRepository('App:QuizAll')
        ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        //$builder->orderBy('q.createdAt','DESC');
        $quizzes = $builder->getQuery()->getResult();

        $paginator = $this->get('knp_paginator')->paginate(
            $quizzes,
            $request->query->getInt('page', 1),
            10
        );
    
        return $this->render('admin_quiz_all_question/index.html.twig', [
            'quizAll' => $paginator,
        ]);
    }

    /**
     * @Route("/novo/{id}", name="admin_quiz_all_question_new")
     */
    public function new(Request $request, $id, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();
        
        if($request->isMethod('GET')){
            $quiz = $em->getRepository('App:QuizAll')->findBy([], ['name' => 'asc']);

            return $this->render('admin_quiz_all_question/new.html.twig', [
                'quiz' => $quiz,
                'quiz_id' => $id
            ]);
        }
        //dump($request); die();
        if ($request->request->has('form')) {
            $form = $request->request->get('form');
            $form_files = $request->files->get('form');
            $qtd_alternative = 4;

            foreach ($form as $key => $q) { 
                $quiz_id = isset($q['quiz_id']) ? $q['quiz_id'] : null;

                $quiz = $em->getRepository('App:QuizAll')->findOneBy(['id' => $quiz_id], ['name' => 'asc']);
             
                if ($quiz) {
                    $slug_question = $this->slugify($q['title']);

                    $question = new QuizAllQuestions();
                    $question->setQuizAll($quiz);
                    $question->setTitle($q['title']);
                    $question->setSlug($slug_question);
                    $question->setReference($q['reference']);
                    $question->setIsActive(true);
                    $question->setCreatedAt(new \DateTime());
                    $question->setUpdatedAt(new \DateTime());
                    
                    $em->persist($question);
                    $em->flush();
                    
                    //alternativas das perguntas
                    for ($i = 1; $i <= $qtd_alternative; $i++) {
                        $slug_alternative = $this->slugify($q['alternative'.$i]['comentario']);

                        $alternative = new QuizAllAlternatives();

                        $imagem_desk = isset($form_files[$key]['alternative'.$i]['imagem_desk']) ? $form_files[$key]['alternative'.$i]['imagem_desk'] : null;
                        if ($imagem_desk) {
                            $fileNameDesk = $fileUploader->upload($imagem_desk, 'quiz_all');
                            $alternative->setImageDesk($fileNameDesk);
                        }

                        $imagem_mobile = isset($form_files[$key]['alternative'.$i]['imagem_mobile']) ? $form_files[$key]['alternative'.$i]['imagem_mobile'] : null;
                        if ($imagem_mobile) {
                            $fileNameMobile = $fileUploader->upload($imagem_mobile, 'quiz_all');
                            $alternative->setImageMobile($fileNameMobile);
                        }
                                              
                        $alternative->setQuizAllQuestions($question);
                        $alternative->setTitle($q['alternative'.$i]['comentario']);
                        $alternative->setSlug($slug_alternative);
                        $alternative->setImageAlt($q['alternative'.$i]['image_alt']);
                        $alternative->setImageDescription($q['alternative'.$i]['image_description']);
                        $alternative->setIsCorrect($q['alternative'.$i]['is_active'] == 'Sim' ? true : false);

                        $alternative->setCreatedAt(new \DateTime());
                        $alternative->setUpdatedAt(new \DateTime());
                        
                        $em->persist($alternative);
                        $em->flush();
                    }
                }
            }
        }

        return $this->redirectToRoute('admin_quiz_all');
    }

   /**
     * @Route("/edit/{id}", name="admin_quiz_all_question_edit")
     */
    public function edit(Request $request, $id, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();
        
        if($request->isMethod('GET')){
            $quiz = $em->getRepository('App:QuizAll')->findBy([], ['name' => 'asc']);
            $quiz_question = $em->getRepository('App:QuizAllQuestions')->findBy(['quiz_all' => $id]);
           
            return $this->render('admin_quiz_all_question/edit.html.twig', [
                'quiz' => $quiz,
                'quiz_question' => $quiz_question,
                'id' => $id
            ]);
        }

        if ($request->request->has('form')) {
            $form = $request->request->get('form');
            $form_files = $request->files->get('form');
            $quiz_id    = $request->request->get('quiz_id');
          
            $qtd_alternative = 4;
            foreach ($form as $key => $q) { 
                $quiz = $em->getRepository('App:QuizAll')->findOneBy(['id' => $quiz_id], ['name' => 'asc']);
               
                if ($quiz) {
                    $slug_question = $this->slugify($q['title']);
                    if (isset($q['question_id'])) {
                        $question = $em->getRepository('App:QuizAllQuestions')->findOneBy(['id' => $q['question_id']]);
                    } else {
                    //if (!$question) {
                        $question = new QuizAllQuestions();
                        $question->setCreatedAt(new \DateTime());
                    }
                    
                    $question->setQuizAll($quiz);
                    $question->setTitle($q['title']);
                    $question->setSlug($slug_question);
                    $question->setReference($q['reference']);
                    $question->setIsActive(true);
                    $question->setUpdatedAt(new \DateTime());
                    
                    $em->persist($question);
                    $em->flush();

                    //alternativas das perguntas
                    for ($i = 1; $i <= $qtd_alternative; $i++) {
                        if (isset($q['alternative'.$i])) {
                            $slug_alternative = $this->slugify($q['alternative'.$i]['comentario']);

                            if (isset($q['alternative'.$i]['alternative_id'])) {
                                $alternative = $em->getRepository('App:QuizAllAlternatives')->findOneBy(['id' => $q['alternative'.$i]['alternative_id']]);
                            } else {
                                $alternative = new QuizAllAlternatives();
                                $alternative->setCreatedAt(new \DateTime());
                            }
                            $imagem_desk = isset($form_files[$key]['alternative'.$i]['imagem_desk']) ? $form_files[$key]['alternative'.$i]['imagem_desk'] : null;
                            if ($imagem_desk) {
                                $fileNameDesk = $fileUploader->upload($imagem_desk, 'quiz_all');
                                $alternative->setImageDesk($fileNameDesk);
                            }  
                    
                            $imagem_mobile = isset($form_files[$key]['alternative'.$i]['imagem_mobile']) ? $form_files[$key]['alternative'.$i]['imagem_mobile'] : null;
                            if ($imagem_mobile) {
                                $fileNameMobile = $fileUploader->upload($imagem_mobile, 'quiz_all');
                                $alternative->setImageMobile($fileNameMobile);
                            }
                                                
                            $alternative->setQuizAllQuestions($question);
                            $alternative->setTitle($q['alternative'.$i]['comentario']);
                            $alternative->setSlug($slug_alternative);
                            $alternative->setImageAlt($q['alternative'.$i]['image_alt']);
                            $alternative->setImageDescription($q['alternative'.$i]['image_description']);
                            $alternative->setIsCorrect($q['alternative'.$i]['is_active'] == 'Sim' ? true : false);
                            $alternative->setUpdatedAt(new \DateTime());

                            $em->persist($alternative);
                            $em->flush();
                        }
                    }//end for

                }//end if 
            }//end foreach
            
        }//end if
       
        return $this->redirectToRoute('admin_quiz_all');
    }


    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
