<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminRelatoriosController extends Controller
{
    /**
     * @Route("/admin/relatorios", name="admin_relatorios")
     */
    public function index(Request $request): Response
    {
        if ($request->query->has('q')) {
            $date_start = $request->query->get('data_inicio');
            $date_end   = $request->query->get('data_fim');
            $limit = 500;

            $amount = $request->query->has('amount') ? $request->query->get('amount') : null;

            if ($amount == 0) {
                $offset = ($amount * $limit);
            } else {
                $offset = ($amount * $limit) + 1;
            }
                        
            //dump($offeset); die();

            $em = $this->getDoctrine()->getManager();
            $users = $em->getRepository('App:User')->relatorioBaseQuizenal($date_start, $date_end, $offset, $limit);
            $users_total = $em->getRepository('App:User')->relatorioBaseQuizenaTotal($date_start, $date_end);
            //dump($users); die();
            
            $qtd_page = $users_total / $limit;
            $qtd_page = round($qtd_page, 0, PHP_ROUND_HALF_UP);
            
            if ($qtd_page == $amount) {
                // $amount = $amount - 1;
                // $count  = $amount * $limit;
                // $limit  = $users_total - $count;

                // $users = $em->getRepository('App:User')->relatorioBaseQuizenal($date_start, $date_end, $amount, $limit);
                
                $response = array(
                    'status' => true,
                    'data' => $users,
                    'message' => 'Planilha exportada com sucesso.'
                );
            } else {

                if ($users_total < $limit) {
                    $limit  = $users_total;
                    $users = $em->getRepository('App:User')->relatorioBaseQuizenal($date_start, $date_end, null, $limit);
                }
                
                $response = array(
                    'status' => true,
                    'data' => $users,
                    'number' => $users_total,
                    'pending' => $users_total > $limit ? $qtd_page : $users_total,
                    'message' => 'OK',
                );

                //dump($response); die();
            }
            
            return new JsonResponse($response);
         
        }
        
        return $this->render('admin_relatorios/index.html.twig', [

        ]);
    }
}
