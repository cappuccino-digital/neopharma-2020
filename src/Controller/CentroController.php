<?php

namespace App\Controller;

use App\Entity\CentroContact;
use App\Entity\UserSolicitationSampleDropyD;
use App\Form\CentroContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * @Route("/vitaminas")
 */
class CentroController extends Controller
{
    /**
     * @Route("/", name="centro")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $centro = new CentroContact();
        $em = $this->getDoctrine()->getManager();  

        $contacts = $em->getRepository('App:CentroContact')->findAll();
        $user_solicitation_sample = $em->getRepository('App:UserSolicitationSampleDropyD')->findAll();
        //dump($user_solicitation_sample); die();
        return $this->render('centro/index.html.twig', [
            'contacts' => $contacts,
            'user_solicitation_sample' => $user_solicitation_sample
        ]);
    }

    /**
     * @Route("/quiz", name="centro_quiz")
     */
    public function quiz(Request $request)
    { 
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository('App:QuizAll')->findOneBySlug('quiz-vitaminado-sera-que-voce-sabe-tudo-sobre-vitaminas');
        $questions = $em->getRepository('App:QuizAllQuestions')->findBy(['quiz_all' => $quiz]);
        
        return $this->render('centro/quiz.html.twig', [
            'questions' => $questions,
            'quiz' => $quiz
        ]);
    }

    /**
     * @Route("/solicitacao-amostra", name="solicitation_sample")
     */
    public function solicitationSampleDropyD(Request $request) 
    {
        return $this->redirectToRoute('home');
        die();
        
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }
        
        $user = $this->getUser();
        //dump($user); die();
        if ($request->request->has('d')) {
            $error = array();

            if (trim($request->request->get('zip_code'))) {
                $zip_code = str_replace([' ', '-'], '', $request->request->get('zip_code'));
                $user->setZipCode($zip_code);
            } else {
                $error['zip_code'] = 'O CEP é obrigatório.';
            }
            
            if (trim($request->request->get('address'))) {
                $user->setAddress($request->request->get('address'));
            } else {
                $error['address'] = 'O endereço é obrigatório.';
            }
            
            if (trim($request->request->get('number'))) {
                $user->setNumber($request->request->get('number'));
            } else {
                $error['number'] = 'O número é obrigatório.';
            }
            
            if (trim($request->request->get('complement'))) {
                $user->setComplement($request->request->get('complement'));
            } else {
                //$error['complement'] = 'O complemento é obrigatório.';
            }
            
            if (trim($request->request->get('neighborhood'))) {
                $user->setNeighborhood($request->request->get('neighborhood'));
            } else {
                $error['neighborhood'] = 'O bairro é obrigatório.';
            }
            
            if (trim($request->request->get('city'))) {
                $user->setCity($request->request->get('city'));
            } else {
                $error['city'] = 'A cidade é obrigatória.';
            }
            
            if (trim($request->request->get('state'))) {
                $user->setEstateAddress($request->request->get('state'));
            } else {
                $error['state'] = 'O estado é obrigatório.';
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }
            
            $em = $this->getDoctrine()->getManager();
            $user_solicitation_sample = $em->getRepository('App:UserSolicitationSampleDropyD')->findOneBy(['user' => $user]);

            if (!$user_solicitation_sample) {
                $user_solicitation_sample = new UserSolicitationSampleDropyD(); // cadastro 
                $user_solicitation_sample->setUser($user); 
                $user_solicitation_sample->setCreatedAt(new \DateTime());
                $user_solicitation_sample->setUpdatedAt(new \DateTime());  

                $em->persist($user_solicitation_sample);
                $em->flush();

                //update info user
                $em->persist($user);
                $em->flush();

                return new JsonResponse([
                    'status' => true,
                    'message' => '<a class="close-modal" onclick="core.hideMessage()">x</a>' .
                                 '<p>Informações recebidas!</p>' .
                                 '<p>Sua amostra chegará ao<br> endereço informado<br> em até 90 dias.'
                ]);
            }

            return new JsonResponse([
                'status' => true,
                'message' => '<a class="close-modal" onclick="core.hideMessage()">x</a>' .
                             '<br><br>' .
                             '<p>Você já solicitou a amostra. Aguarde o envio.</p><br>'
            ]);
        }

        return $this->render('centro/solicitation_sample.html.twig', []);
    }

   
}