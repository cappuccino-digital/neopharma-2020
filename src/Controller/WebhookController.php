<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
/**
 * @Route("/webhook", methods={"POST"})
 */
class WebhookController extends Controller
{
    /**
     * @Route("", name="webhook")
     */
    public function index(Request $request)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
        $email = isset($dataPost['email']) ? $dataPost['email'] : null;
        $courseId = isset($dataPost['id']) ? $dataPost['id'] : null;
        $accessKey = isset($dataPost['access_key']) ? $dataPost['access_key'] : null;
        $accessHash = isset($dataPost['hash']) ? $dataPost['hash'] : null;

        if(!$email || !$courseId || !$accessKey || !$accessHash){
            $email = $request->request->get('email');
            $courseId = $request->request->get('id');
            $accessKey = $request->request->get('access_key');
            $accessHash = $request->request->get('hash');
        }

        if($accessKey != 'racine2k19' && $accessHash != 'b707e8465652e1b8d18f4194ef4ee991'){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-001"
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('App:User')->findOneByEmail($email);

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-002"
            ]);
        }

        $course = $em->getRepository('App:Course')->findOneByCourseId($courseId);

        if(!$course){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-003"
            ]);
        }

        $userCourseRegister = $em->getRepository('App:UserCourse')->findOneBy(['course' => $course, 'user' => $user]);
       
        if(!$userCourseRegister){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-004"
            ]);
        }
        
        $userCourseRegister->setConclusionAt(new \DateTime());
        $em->persist($userCourseRegister);
        $em->flush();

        $em->getRepository('App:UserPoint')->setPointsToBadge($user, $course, 'aluno-dedicado', 'course');

        //Gamificação interação
        $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'concluir-um-curso', 'curso', $course->getId()); 

        //registrando os cursos que usuário já havia concluídos
        $user_courses = $em->getRepository('App:UserCourse')->findBy(['user' => $user->getId()]);
        if (count($user_courses) > 0) {
            $course_conclusion = 0;
            foreach ($user_courses as $user_course) {
                if ($user_course->getConclusionAt()) {
                    $courses = $user_course->getCourse();
                    
                    $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'concluir-um-curso', 'curso', $courses->getId()); //Gamificação interação
                }
            }
        }
        // END - Gamificação

        //interações de curso para o neo excelência
        // $points_type = $em->getRepository('App:PointsTypeExcelencia')->findOneBy(['cod_interaction' => $course->getCodInteraction(), 'active' => true]);

        // if ($points_type) {
        //     $user_interacao = $em->getRepository('App:UserPoint')
        //                         ->setUserPointsNeoExcelencia(
        //                             $user, 
        //                             $course,
        //                             $points_type->getSlug(), 
        //                             'curso', 
        //                             $this->getParameter('api_excelencia'),
        //                             $this->getParameter('admin_excelencia_user'),
        //                             $this->getParameter('admin_excelencia_pass')
        //                         );
        // }


        return new JsonResponse([
            'status' => true,
            'message' => "SUCSS-001"
        ]);

    }

     /**
     * @Route("/users", name="webhook-get-users")
     */
    public function users(Request $request)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
        $page = isset($dataPost['page']) ? $dataPost['page'] : null;
        $limit = isset($dataPost['limit']) ? $dataPost['limit'] : null;
        $accessKey = isset($dataPost['access_key']) ? $dataPost['access_key'] : null;
        $accessHash = isset($dataPost['hash']) ? $dataPost['hash'] : null;

        if(!$accessKey || !$accessHash || !$page || !$limit ){
            $accessKey = $request->request->get('access_key');
            $accessHash = $request->request->get('hash');
            $page = $request->request->has('page') ? $request->request->get('page') : 1;
            $limit = $request->request->has('limit') ? $request->request->get('limit') : 10;
        }

        if(!$accessKey || !$accessHash){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-001"
            ]);
        }

        if($accessKey != 'racine2k19' && $accessHash != 'b707e8465652e1b8d18f4194ef4ee991'){
            return new JsonResponse([
                'status' => false,
                'message' => "ERR-001"
            ]);
        }

        $limit > 50 ? $limit = 50 : '';

        $builder = $this
            ->getDoctrine()
            ->getRepository('App:UserCourse')
            ->createQueryBuilder('q');

        $userCourseRegister = $builder->getQuery()->getResult();

        $userCourseRegister = $this->get('knp_paginator')->paginate(
            $userCourseRegister,
            $page,
            $limit
        );

        $userResponse = [];

        foreach($userCourseRegister as $key=>$reg){
            $userResponse[$key]['nome'] = $reg->getUser()->getName();
            $userResponse[$key]['email'] = $reg->getUser()->getEmail();
            $userResponse[$key]['cpf'] = $reg->getUser()->getCpf();
            $userResponse[$key]['profissao'] = $reg->getUser()->getIsPharmaceutical() ? 'farmaceutico' : 'balconista';
            if ($reg->getUser()->getIsPharmaceutical()){
                $userResponse[$key]['crf'] = $reg->getUser()->getCrf();
                $userResponse[$key]['uf'] = $reg->getUser()->getState();
            }
        }

        return new JsonResponse([
            'status' => true,
            'message' => "SUCSS-001",
            'proxima_pagina' => $userCourseRegister->getPageCount() > $page ? $page + 1 : null,
            'total_paginas' => $userCourseRegister->getPageCount(),
            'users' => $userResponse
        ]);
    }
}
