<?php

namespace App\Controller;

use App\Entity\CorinthiansFluminenseCompanion;
use App\Entity\PreferentialChildren;
use App\Entity\UserCorinthiansFluminense;
use App\Entity\UserPreferential;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;


class CorinthiansFluminenseController extends Controller
{
    /**
     * @Route("/corinthians-x-fluminense", name="corinthians-fluminense")
     */
    public function index(Request $request, \Swift_Mailer $mailer) 
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }

        //remover essa redenrização somente na quinta para liberar o formulário
        //return $this->render('corinthians_fluminense/teaser.html.twig', []); die();

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_tickets_corinthians_all = [];

        //Gestor de loja
        if ($user->getIsPharmacy()) {
            return $this->redirectToRoute('home');
        } 
        
        if ($user->getIsPharmaceutical()) { //farmacêutico
            $user_tickets_corinthians_all = $em->getRepository('App:UserCorinthiansFluminense')->findBy(['is_pharmaceutical' => true]);
        } else if (!$user->getIsPharmaceutical()) { //balconista
            $user_tickets_corinthians_all = $em->getRepository('App:UserCorinthiansFluminense')->findBy(['is_pharmaceutical' => false]);
        } 
        
        //apenas 3 ingressos por perfil
        if (count($user_tickets_corinthians_all) >= 3) {
            return $this->redirectToRoute('corinthians-fluminense-result', ['result'=> 'ingressos-esgotados']);
        }

        //dump($user); die();
        if ($request->request->has('tc')) {
            $error = array();
            
            //dados pessoais
            $name = trim($request->request->get('name'));
            $zip_code = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address = trim($request->request->get('address'));
            $number = trim($request->request->get('number'));
            $complement = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city = trim($request->request->get('city'));
            $state = trim($request->request->get('state'));
            $cellphone = trim($request->request->get('cellphone'));
            $email = trim($request->request->get('email'));
            $birthday = trim($request->request->get('birthday'));
            $gender = trim($request->request->get('gender'));
            $educationalLevel = trim($request->request->get('educationalLevel'));
            //acompanhante
            $name_companion = trim($request->request->get('name_companion'));
            $cellphone_companion = trim($request->request->get('cellphone_companion'));
            $email_companion = trim($request->request->get('email_companion'));
            //preferencias
            $hobby = trim($request->request->get('hobby_other')) != '' ? $request->request->get('hobby_other') : trim($request->request->get('hobby'));
            $sport = trim($request->request->get('sport_other')) != '' ? $request->request->get('sport_other') : trim($request->request->get('sport'));
            $team_football = trim($request->request->get('team_football_other')) != '' ? $request->request->get('team_football_other') : trim($request->request->get('team_football'));
            $is_children = $request->request->get('is_children') == 1 ? true : false;
            $how_many = $request->request->get('how_many');
            $info_children = $request->request->get('info_children');

    
            if ((!$this->checkCity($city, $state)) || ($state != 'SP')) { 
                $url = $request->getSchemeAndHttpHost() . '/corinthians-x-fluminense/cidade-indisponivel';
                
                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            if ($state != 'SP') {
                $error['state'] = 'Estado não permitido.';
            } 

            if ($name) {
                $user->setName($request->request->get('name'));
            } else {
                $error['name'] = 'O nome é obrigatório.';
            }
    
            if ($gender) {
                $user->setGender($gender);
            } else {
                $error['gender'] = 'O Gênero é obrigatório.';
            }
                                                     
            if ($birthday != '') {
                $d = explode('/', $birthday);
                if (isset($d[0]) && isset($d[1]) && isset($d[2])) {
                    $birthday = "{$d[2]}-{$d[1]}-{$d[0]}";
    
                    $user->setBirthday(new \DateTime($birthday));
                }
            } else {
                $error['birthday'] = 'A Data de nascimento é obrigatório.';
            }

            if ($educationalLevel) {
                $user->setEducationalLevel($educationalLevel);
            } else {
                $error['educationalLevel'] = 'O Grau de escolaridade é obrigatório.';
            }

            if ($zip_code) {
                $user->setZipCode($zip_code);
            } else {
                $error['zip_code'] = 'O CEP é obrigatório.';
            }
            
            if ($address) {
                $user->setAddress($address);
            } else {
                $error['address'] = 'O endereço é obrigatório.';
            }
            
            if ($number) {
                $user->setNumber($number);
            } else {
                $error['number'] = 'O número é obrigatório.';
            }
            
            if ($complement) {
                $user->setComplement($complement);
            } else {
                //$error['complement'] = 'O complemento é obrigatório.';
            }
            
            if ($neighborhood) {
                $user->setNeighborhood($neighborhood);
            } else {
                $error['neighborhood'] = 'O bairro é obrigatório.';
            }
            
            if ($city) {
                $user->setCity($city);
            } else {
                $error['city'] = 'A cidade é obrigatória.';
            }
            
            if ($state) {
                $user->setEstateAddress($state);
            } else {
                $error['state'] = 'O estado é obrigatório.';
            }
           
            if ($cellphone) {
                if (strlen($cellphone) < 15) { //contando com as maskaras
                    $error['cellphone'] = "Celular inválido";
                } else {
                    $user->setCellphone($cellphone);
                }
            } else {
                $error['cellphone'] = 'O celular é obrigatório.';
            }

            if ($email) {
                $user->setEmail($email);
                if ($user->getIsPharmacy()) {
                     
                } else {
                    $user->setUsername($email);
                }
            } else {
                $error['email'] = 'O celular é obrigatório.';
            }

            //validação e-mail principal
            if (!$email) {
                $error['email'] = 'O e-mail é obrigatório';
            } else {

                if ($email != $user->getEmail()) {
                   
                    $user_check_email = $em->getRepository('App:User')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email or u.username = :username')
                            ->setParameter('email', $email)
                            ->setParameter('username', $email)
                            ->getQuery()
                            ->getResult();
                    
                    if ($user_check_email) {
                        $error['email'] = 'Email está sendo utilizado.';
                    }

                    $user_participant_check = $em->getRepository('App:UserCorinthiansFluminense')->createQueryBuilder('uc')
                                        ->innerJoin('App:User', 'u' , 'WITH' , 'uc.user = u.id')
                                        ->where('u.email = :email or u.username = :username')
                                        ->setParameter('email', $email)
                                        ->setParameter('username', $email)
                                        ->getQuery()
                                        ->getResult();

                    if ($user_participant_check) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                } else {
                    
                    $user_companion_check_email = $em->getRepository('App:CorinthiansFluminenseCompanion')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email')
                            ->setParameter('email', $email)
                            ->getQuery()
                            ->getResult();

                    if ($user_companion_check_email) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                }
                
                //validação e-mail
                if (!$this->validaEmail($email)) {
                    $error['email'] = 'E-mail inválido';
                }
            }

            //acompanhante
            if (!$name_companion) {           
                $error['name_companion'] = 'O nome é obrigatório.';
            }
            
            if (!$cellphone_companion) {
                $error['cellphone_companion'] = 'O celular é obrigatório.';
            } else {
                
                if (strlen($cellphone_companion) < 15) { //contando com as maskaras
                    $error['cellphone_companion'] = "Celular inválido";
                }
            }

            //preferencias
            if (!$hobby) {
                $error['hobby'] = 'O hobby é obrigatório';
            } 
            
            if (!$sport) {
                $error['sport'] = 'O esporte é obrigatório';
            }

            if (!$team_football) {
                $error['team_football'] = 'O time que você torce é obrigatório';
            }

            

            if (!$email_companion) {
                $error['email_companion'] = 'O e-mail é obrigatório.';
            } else {

                $is_check_email_companion = false;

                $user_check_corinthians_always_ready = $em->getRepository('App:UserCorinthiansFluminense')->findOneBy(['user' => $user]);

                if ($user_check_corinthians_always_ready) {
                    //dump($user_check_corinthians_always_ready); die();
                    $user_check_companion_corinthians_always_ready = $em->getRepository('App:CorinthiansFluminenseCompanion')->findOneBy(['user_corinthians_fluminense' => $user_check_corinthians_always_ready]);
                    
                    if (($email_companion != $user_check_companion_corinthians_always_ready->getEmail())) {
                        $is_check_email_companion = true;                       
                    }
                } else {
                    $is_check_email_companion = true;
                }

                if ($is_check_email_companion) {
                    $user_companion_check_email = $em->getRepository('App:CorinthiansFluminenseCompanion')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email')
                            ->setParameter('email', $email_companion)
                            ->getQuery()
                            ->getResult();

                    if ($user_companion_check_email) {
                        $error['email_companion'] = 'Email está sendo utilizado';
                    }

                    $user_participant_check = $em->getRepository('App:UserCorinthiansFluminense')->createQueryBuilder('uc')
                                            ->innerJoin('App:User', 'u' , 'WITH' , 'uc.user = u.id')
                                            ->where('u.email = :email or u.username = :username')
                                            ->setParameter('email', $email_companion)
                                            ->setParameter('username', $email_companion)
                                            ->getQuery()
                                            ->getResult();

                    if ($user_participant_check) {
                        $error['email_companion'] = 'Email está sendo utilizado';
                    }
                }


                //validação e-mail
                if (!$this->validaEmail($email_companion)) {
                    $error['email_companion'] = 'E-mail inválido';
                }
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }
            
            $em = $this->getDoctrine()->getManager();
            $user_tickets_corinthians = $em->getRepository('App:UserCorinthiansFluminense')->findOneBy(['user' => $user]);

            if (!$user_tickets_corinthians) {
                //cadastro do usuário para receber os ingressos
                $user_tickets_corinthians = new UserCorinthiansFluminense();  
                $user_tickets_corinthians->setUser($user); 

                if ($user->getIsPharmaceutical()) { //farmacêutico
                    $user_tickets_corinthians->setIsPharmaceutical(true); 
                } else if (!$user->getIsPharmaceutical()) { //balconista
                    $user_tickets_corinthians->setIsPharmaceutical(false); 
                } 
                
                $user_tickets_corinthians->setCreatedAt(new \DateTime());
                $user_tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($user_tickets_corinthians);
                $em->flush();

                //cadastro acompanhante
                $tickets_corinthians = new CorinthiansFluminenseCompanion(); 
                $tickets_corinthians->setUserCorinthiansFluminense($user_tickets_corinthians); 
                $tickets_corinthians->setName($name_companion); 
                $tickets_corinthians->setCellphone($cellphone_companion); 
                $tickets_corinthians->setEmail($email_companion); 
                $tickets_corinthians->setCreatedAt(new \DateTime());
                $tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($tickets_corinthians);
                $em->flush();

                //update info user
                $em->persist($user);
                $em->flush();


            //preferencias - insert info
                $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

                if (!$user_preferential) {
                    $user_preferential = new UserPreferential();
                }

                $user_preferential->setUser($user);
                $user_preferential->setHobby($hobby);
                $user_preferential->setSport($sport);
                $user_preferential->setTeamFootball($team_football);
                $user_preferential->setIsChildren($is_children);
                $user_preferential->setHowManyChildren($how_many);
                $user_preferential->setCreatedAt(new \DateTime());
                $user_preferential->setUpdatedAt(new \DateTime());

                $em->persist($user_preferential);
                $em->flush();

                //atualizando o time na tabela de usuário também
                $user->setTeamFootball($team_football);
                $em->persist($user);
                $em->flush();

                //se tem filhos
                if ($is_children == true && $how_many > 0) {
                    for ($i = 1; $i <= $how_many; $i++) {
                        $preferential_children = $em->getRepository('App:PreferentialChildren')->findOneBy(['user_preferential' => $user_preferential, 'children' => $i]);

                        if (!$preferential_children) {
                            $preferential_children = new PreferentialChildren();
                            $preferential_children->setUserPreferential($user_preferential);
                            $preferential_children->setCreatedAt(new \DateTime());
                        }
                        
                        //Outros campos
                        $team_football = isset($info_children['team_football']['other'][$i]) && $info_children['team_football']['other'][$i] != '' ? $info_children['team_football']['other'][$i] : $info_children['team_football'][$i];
                        $sport         = isset($info_children['sport']['other'][$i]) && $info_children['sport']['other'][$i] != '' ? $info_children['sport']['other'][$i] : $info_children['sport'][$i];
                        
                        //dump($team_football, $sport); die();                   
                        $preferential_children->setChildren($i);
                        $preferential_children->setTeamFootball($team_football);
                        $preferential_children->setSport($sport);
                        $preferential_children->setAge($info_children['age'][$i]);
                        $preferential_children->setUpdatedAt(new \DateTime());

                        $em->persist($preferential_children);
                        $em->flush();
                    }
                } else {
                    $preferential_children = $em->getRepository('App:PreferentialChildren')->findBy(['user_preferential' => $user_preferential]);
                    foreach ($preferential_children as $children) {
                        $em->remove($children);
                        $em->flush();
                    }
                }


            //--- END preferencias

                $emails = ['wellington.santos@cappuccinodigital.com.br', 'andre.lin@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br'];
                //$emails = ['wellington.santos@cappuccinodigital.com.br'];
                    
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo($emails);
              
                $message
                    ->setReplyTo($email)
                    ->setSubject('Portal Neo Pharma - Ingressos do jogo do Corinthians')
                    ->setBody(
                      'Nome: ' . $user->getName() . ', ' . 
                      'CEP: ' . $user->getZipCode() . ', ' . 
                      'Endereço: ' . $user->getAddress() . ', ' . 
                      'Número: ' . $user->getNumber() . ', ' . 
                      'Bairro: ' . $user->getNeighborhood() . ', ' . 
                      'Cidade: ' . $user->getCity() . ', ' . 
                      'Estado: ' . $user->getState() . ', ' . 
                      'Celular: ' . $user->getCellphone() . ', ' . 
                      'E-mail: ' . $user->getEmail() . ' '. 
                      ' Dados do Acompanhante '. ', ' . 
                      'Nome: ' . $name_companion . ', ' . 
                      'Celular: ' . $cellphone_companion . ', ' . 
                      'E-mail: ' . $email_companion 
                    );

                $response = $mailer->send($message);

                $url = $request->getSchemeAndHttpHost() . '/corinthians-x-fluminense/parabens';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            return new JsonResponse([
                'status' => false,
                'is_register' => true, 
                'message' => 'Você já fez o cadastro, aguarde as instruções que serão enviadas por e-mail'
            ]);
        }

        $corinthians_fluminense_companion = [];
        $user_corinthians_fluminense = $em->getRepository('App:UserCorinthiansFluminense')->findOneBy(['user' => $user]);
        if ($user_corinthians_fluminense) {
            $corinthians_fluminense_companion = $em->getRepository('App:CorinthiansFluminenseCompanion')->findOneBy(['user_corinthians_fluminense' => $user_corinthians_fluminense]);  
        }


        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

        $teams = [
            'América-MG',
            'Athletico',
            'Avaí',
            'Atlético-GO',
            'Atlético-MG',
            'Bahia',
            'Botafogo',
            'Bragantino',
            'Brasil de Pelotas',
            'Brusque',
            'Ceará',
            'Chapecoense',
            'Confiança',
            'Corinthians',
            'Curitiba',
            'CRB',
            'Cruzeiro',
            'CSA',
            'Cuiabá',
            'Fortaleza',
            'Flamengo',
            'Fluminense',
            'Goiás',
            'Grêmio',
            'Guarani',
            'Internacional',
            'Juventude',
            'Londrina',
            'Náutico',
            'Operário',
            'Palmeiras',
            'Ponte Preta',
            'Remo',
            'Sampaio Corrêa',
            'Santos',
            'São Paulo',
            'Sport',
            'Vasco',
            'Vila Nova',
            'Vitória',
            'Nenhum'
        ];
      
        return $this->render('corinthians_fluminense/index.html.twig', [
            'corinthians_fluminense_companion' => $corinthians_fluminense_companion,
            'user_preferential' => $user_preferential,
            'teams' => $teams
        ]);
    }

    /**
     * @Route("/corinthians-x-fluminense/{result}", name="corinthians-fluminense-result", defaults={"result"=null})
     */
    public function result(Request $request, $result)
    {
        //die('te');
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        //remover essa redenrização somente na quarta feira para liberar o formulário
        //return $this->redirectToRoute('corinthians-fluminense'); die();

        $array = ['parabens', 'ingressos-esgotados', 'cidade-indisponivel'];

        if (!in_array($result, $array)) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('corinthians_fluminense/result.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/corinthians-x-fluminense/pagina/teaser", name="corinthians-fluminense-teaser")
     */
    public function teaser(Request $request) 
    {
        return $this->render('corinthians_fluminense/teaser.html.twig', []);
    }

    private function checkCity($city, $state)
    {
        $array = [
            'Arujá',
            'Barueri',
            'Biritiba Mirim',
            'Caieiras',
            'Cajamar',
            'Carapicuíba',
            'Cotia',
            'Diadema',
            'Embu das Artes',
            'Embu-Guaçu',
            'Ferraz de Vasconcelos',
            'Francisco Morato',
            'Franco da Rocha',
            'Guararema',
            'Guarulhos',
            'Itapecerica da Serra',
            'Itapevi',
            'Itaquaquecetuba',
            'Jandira',
            'Juquitiba',
            'Mairiporã',
            'Mauá',
            'Mogi das Cruzes',
            'Osasco',
            'Pirapora do Bom Jesus',
            'Poá',
            'Ribeirão Pires',
            'Rio Grande da Serra',
            'Salesópolis',
            'Santa Isabel',
            'Santana de Parnaíba',
            'Santo André',
            'São Bernardo do Campo',
            'São Caetano do Sul',
            'São Lourenço da Serra',
            'São Paulo',
            'Suzano',
            'Taboão da Serra',
            'Vargem Grande Paulista',
        ];

        if ($state != 'SP') {
            return false;
        }

        if (!in_array($city, $array)) {
            return false;
        }

        return true;
    }

    private function validaEmail($mail){
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }
}
