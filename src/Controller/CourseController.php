<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
/**
 * @Route("/cursos")
 */
class CourseController extends Controller
{
    /**
     * @Route("", name="course_home")
     */
    public function home(Request $request)
    {
        throw $this->createNotFoundException('A página não foi encontrada');

        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $banners = $em->getRepository('App:Banner')->getBannerByTypeName('cursos');
        
        $builder = $em->getRepository('App:Course')->createQueryBuilder('q');
        //dump($request); die();
        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $builder->andWhere("q.isOnlyToClerk = true OR q.isPublic = true")->andWhere("q.isActive = true");
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                $builder->andWhere("q.isOnlyToPharmaceutical = true")->andWhere("q.isActive = true");
    
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                $builder->andWhere("q.isOnlyToClerk = true")->andWhere("q.isActive = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        
                $builder->andWhere("q.isPharmacy = true")->andWhere("q.isActive = true");
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')) {
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar o curso."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            } else if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.isActive = true");
            }
        }

       # $builder->andWhere("q.isActive = true");

        $builder->orderBy('q.createdAt','DESC');

        $courses = $builder->getQuery()->getResult();
       
        $courses = $this->get('knp_paginator')->paginate(
            $courses,
            $request->query->getInt('page', 1),
            5
        );
       
        return $this->render('course/index.html.twig', [
            'banners' => $banners,
            'courses' => $courses
        ]);
    }

    /**
     * @Route("/{slug}", name="course_detail")
     */
    public function detail($slug, Request $request)
    {
        throw $this->createNotFoundException('A página não foi encontrada');

        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('App:Course')->findOneBySlug($slug);
        
        if(!$course){
            return $this->redirectToRoute('course_home');
        }
        
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$course->getIsPublic()){
            return $this->redirectToRoute('course_home');
        } else {
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$course->getIsOnlyToPharmaceutical()){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                return $this->redirectToRoute('course_home');
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$course->getIsOnlyToClerk() || $this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$course->getIsOnlyToClerk()){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                
                return $this->redirectToRoute('course_home');
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->getUser()->getIsRegisterComplete() && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
                $this->addFlash("alert", "É necessário completar o cadastro para visualizar o curso."); 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            } 
        }

        //validação
        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        //LP inverno obrigatório estar logado
        $courses_lp_inverno = array(
            'caracteristicas-das-principais-classes-de-medicamentos-utilizadas-no-tratamento-da-asma-dpoc-e-rinite-alergica-brocodilatadores-corticosteroides-mucoliticos-e-descongestionantes',
            'orientacao-para-uso-de-produtos-que-requerem-cuidados-especiais-nasais-e-inalatorios-oculares-e-transdermicos',
            'reacoes-alergicas-e-atuacao-do-farmaceutico-na-sua-identificacao-prevencao-e-manejo'
        );
        
        if (in_array($slug, $courses_lp_inverno)) { 
            if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                /** Gravando a referencia do conteúdo */
                $url_reference = $request->getUri();
                $response = $this->redirectToRoute('home-login');
                $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
                return $response;
                /*** END  */
            }
        } 
        /* end lógica LP */
        
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContentView($course->getId(),'curso');

        $isSaved = false;
        $user = $this->getUser();
    
        if($user){
            $isFavorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $course->getId(), 'type' => 'course', 'user' => $user]);
            if($isFavorite){
                $isSaved = true;
            }
        }
        
        $course->setViews($course->getViews() == null ? 1 : ($course->getViews() + 1));
        $em->persist($course);
        $em->flush();

        return $this->render('course/detail.html.twig', [
            'course' => $course,
            'contentReferrer' => $contentReferrer,
            'isSaved' => $isSaved
        ]);
    }
}
