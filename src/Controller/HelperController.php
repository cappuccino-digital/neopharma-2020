<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Entity\ProductType;
use App\Entity\ProductSistemicClass;
use App\Entity\ProductClassifiationByDistribAndPresc;
use App\Entity\ProductAsset;
use App\Entity\ReferrerContent;
use DateTime;

/**
 * @Route("/helper")
 */
class HelperController extends Controller
{
    /**
     * @Route("/import-product-classes", name="helper-product-classes")
     */
    public function importProductClasses()
    {
        if($this->getParameter('APP_ENV') != 'dev'){
            return new JsonResponse([
                'status' => false,
                'message' => 'Nothing to do here'
            ]);
        }

        return new JsonResponse([
            'status' => false,
            'message' => 'Nothing to do here'
        ]);
        
        $file = $this->get('kernel')->getProjectDir() . '/public_html/uploads/tipos_produtos.csv';

        $csv = array_map('str_getcsv', file($file));
        $em = $this->getDoctrine()->getManager();


        foreach($csv as $key => $conf){
            r($key+1);
            if($key > 0){
                if(isset($conf[0])){
                    $name = trim($conf[0]);
                    $existCat = $em->getRepository('App:ProductCategory')->findByName($name);
                    if(!$existCat){
                        $existCat = new ProductCategory();
                        $existCat->setName($name);
                        $existCat->setIsActive(true);
                        $existCat->setCreatedAt(new \DateTime());
                        $existCat->setUpdatedAt(new \DateTime());
                        $em->persist($existCat);
                        $em->flush();
                    }
                }
                if(isset($conf[1])){
                    $name = trim($conf[1]);
                    $existCat = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->findByName($name);
                    if(!$existCat){
                        $existCat = new ProductClassifiationByDistribAndPresc();
                        $existCat->setName($name);
                        $existCat->setIsActive(true);
                        $existCat->setCreatedAt(new \DateTime());
                        $existCat->setUpdatedAt(new \DateTime());
                        $em->persist($existCat);
                        $em->flush();
                    }
                }
                if(isset($conf[2])){
                    $name = trim($conf[2]);
                    $existCat = $em->getRepository('App:ProductSistemicClass')->findByName($name);
                    if(!$existCat){
                        $existCat = new ProductSistemicClass();
                        $existCat->setName($name);
                        $existCat->setIsActive(true);
                        $existCat->setCreatedAt(new \DateTime());
                        $existCat->setUpdatedAt(new \DateTime());
                        $em->persist($existCat);
                        $em->flush();
                    }
                }
                if(isset($conf[3])){
                    $name = trim($conf[3]);
                    $existCat = $em->getRepository('App:ProductType')->findByName($name);
                    if(!$existCat){
                        $existCat = new ProductType();
                        $existCat->setName($name);
                        $existCat->setIsActive(true);
                        $existCat->setCreatedAt(new \DateTime());
                        $existCat->setUpdatedAt(new \DateTime());
                        $em->persist($existCat);
                        $em->flush();
                    }
                }
            }
        }

        return new JsonResponse([
            'status' => true,
            'message' => 'All done Captain'
        ]);

    }

    /**
     * @Route("/import-products", name="helper-products")
     */
    public function importProducts()
    {
        if($this->getParameter('APP_ENV') != 'dev'){
            return new JsonResponse([
                'status' => false,
                'message' => 'Nothing to do here'
            ]);
        }
        
        return new JsonResponse([
            'status' => false,
            'message' => 'Nothing to do here'
        ]);

        $file = $this->get('kernel')->getProjectDir() . '/public_html/uploads/protudos_new_201910041631.json';

        $csv = json_decode(file_get_contents($file), true);

        $em = $this->getDoctrine()->getManager();

        foreach($csv['protudos_new'] as $key => $prod){

            $manager = trim($prod['GERENTE']);
            $internalCode = trim($prod['COD INT']);
            $barcode = trim($prod['EAN']);
            $isActive = trim($prod['STATUS']);
            $name = trim($prod['NOME COMERCIAL']);
            $activeSubstance = trim($prod['PRINCÍPIO ATIVO']);
            $presentation = $prod['APRESENTAÇÃO'];
            $fullPresentation = $prod['APRESENTAÇÃO COMPLETA'];
            $sapName = trim($prod['NOME SAP']);
            $refMedicine = trim($prod['MED. REFERÊNCIA']);
            $indication = $prod['INDICAÇÃO'];
            $composition = $prod['COMPOSIÇÃO'];
            $dosage = $prod['POSOLOGIA'];
            $contraindication = $prod['CONTRA INDICAÇÃO'];
            $isPsicotropic = trim($prod['Psicotrópico']) == 'Sim' ? true : false;
            $anvisaRegistration = $prod['Nº Registro (ANVISA)'];

            $product_category = $prod['SIG'];
            $product_classifiation_by_distrib_and_presc = $prod['RX OU MIP'];
            $product_type = $prod['FORMA FARMACÊUTICA'];
            $sistemic_class = $prod['CLASSE'];

            $assetOne = $prod['FOTOS 660'];
            $assetTwo = $prod['FOTOS 660 2'];

            $existCat = $em->getRepository('App:ProductCategory')->findOneByName(trim($product_category));
            $existClass = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->findOneByName(trim($product_classifiation_by_distrib_and_presc));
            $existSistemic = $em->getRepository('App:ProductSistemicClass')->findOneByName(trim($sistemic_class));
            $existType = $em->getRepository('App:ProductType')->findOneByName(trim($product_type));

            $product = $em->getRepository('App:Product')->findOneByBarcode($barcode);

            if($product){
                continue;
            } else {
                $product = new Product();
            
                $product->setType($existType);
                $product->setClassification($existClass);
                $product->setSistemicClass($existSistemic);
                $product->setCategory($existCat);
    
    
                $product->setName($name);
                $product->setManager($manager);
                $product->setInternalCode($internalCode);
                $product->setPresentation($presentation);
                $product->setFullPresentation($fullPresentation);
                $product->setSapName($sapName);
                $product->setRefMedicine($refMedicine);
                $product->setBarcode($barcode);
                $product->setActiveSubstance($activeSubstance);
                $product->setAnvisaRegistration($anvisaRegistration);
                
                $product->setIndication(nl2br($indication));
                $product->setContraindication(nl2br($contraindication));
                $product->setDosage(nl2br($dosage));
                $product->setComposition(nl2br($composition));
                
                $product->setCreatedAt(new \DateTime());
                $product->setUpdatedAt(new \DateTime());
                $product->setIsOnlyToPharmaceutical(false);
                $product->setIsPublic(true);
    
                if($isActive == 'Ativo'){
                    $product->setIsHomeHighLight(false);
                    $product->setIsActive(true);
                } else if($isActive == 'Lançamento'){
                    $product->setIsHomeHighLight(true);
                    $product->setIsActive(true);
                } else {
                    $product->setIsHomeHighLight(false);
                    $product->setIsActive(true);
                }
    
                if($assetOne == '' && $assetOne == ''){
                    $product->setIsHomeHighLight(false);
                    $product->setIsActive(false);
                }
                
                $product->setIsPsicotropic($isPsicotropic);
                
                
                $fileDir = $this->get('kernel')->getProjectDir() . '/public_html/uploads/product/';
    
                $bullaFile = $prod['ARQUIVO'];
                $file = $this->get('kernel')->getProjectDir() . '/public_html/uploads/bulaFile/' . $bullaFile;
                
                if(is_file($file)){
                    file_put_contents($fileDir.$barcode.'.pdf', file_get_contents($file));
                    $product->setBullaFile('uploads/product/'.$barcode.'.pdf');
                } else {
                    $product->setBullaFile(null);
                }
    
                $em->persist($product);
                $em->flush($product);
    
                
    
                if($assetOne != ''){
                    $assetOneFile = $this->get('kernel')->getProjectDir() . '/public_html/uploads/produtosImagem/' . $assetOne . '.jpg';
                    
                    if(is_file($assetOneFile)){
                        $assetName = $barcode . '_' . $better_token = md5(uniqid(rand(), true)) .'.jpg';
                        file_put_contents($fileDir.$assetName, file_get_contents($assetOneFile));
                        
                        $asset = new ProductAsset();
                        $asset->setType('image');
                        $asset->setFile('uploads/product/' . $assetName );
                        $asset->setProduct($product);
                        $em->persist($asset);
                        $em->flush();
                    }
                }
    
                if($assetTwo != ''){
                    $assetTwoFile = $this->get('kernel')->getProjectDir() . '/public_html/uploads/produtosImagem/' . $assetTwo . '.jpg';
                    if(is_file($assetTwoFile)){
                        $assetName = $barcode . '_' . $better_token = md5(uniqid(rand(), true)) .'.jpg';
                        file_put_contents($fileDir.$assetName, file_get_contents($assetTwoFile));
                        
                        $asset = new ProductAsset();
                        $asset->setType('image');
                        $asset->setFile('uploads/product/' . $assetName );
                        $asset->setProduct($product);
                        $em->persist($asset);
                        $em->flush();
                    }
                }

                
            }
        }
        return new JsonResponse([
            'status' => true,
            'message' => 'All done Captain'
        ]);
    }

    /**
     * @Route("/create-products-other-presentations", name="helper-create-products-other-presentations")
     */
    public function createOtherPresentationRelations()
    {
        if($this->getParameter('APP_ENV') != 'dev'){
            return new JsonResponse([
                'status' => false,
                'message' => 'Nothing to do here'
            ]);
        }
        
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('App:Product')->findAll();


        foreach($products as $p){
            $otherProducts = $em->getRepository('App:Product')->findByName($p->getName());
            if(count($otherProducts) > 1){
                $otherPresentation = [];
                foreach($otherProducts as $other){
                    if($other->getId() != $p->getId()){
                        $otherPresentation[] = $other->getId();
                    }
                }
                $p->setOtherPresentation(json_encode($otherPresentation));
                $em->persist($p);
                $em->flush();
            }
        }

        return new JsonResponse([
            'status' => true,
            'message' => 'All done Captain'
        ]);

    }
}
