<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProductSistemicClass;

/**
 * @Route("/admin/product/sistemic-class")
 */
class AdminProductSistemicClassController extends Controller
{
    /**
     * @Route("", name="admin_product_sistemic_class_list")
     */
    public function productSistemicClassList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:ProductSistemicClass')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $sistemicClasses = $builder->getQuery()->getResult();

        $sistemicClasses = $this->get('knp_paginator')->paginate(
            $sistemicClasses,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_product_sistemic_class/list.html.twig', [
            'sistemicClasses' => $sistemicClasses
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_sistemic_class_new")
     */
    public function productSistemicClassNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_product_sistemic_class/new.html.twig', []);
        }

        $form = $request->request->get('form');


        $sistemicClass = new ProductSistemicClass();
        $sistemicClass->setName($form['name']);
        $sistemicClass->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $sistemicClass->setCreatedAt(new \DateTime());
        $sistemicClass->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($sistemicClass);
        $em->flush();

        return $this->redirectToRoute('admin_product_sistemic_class_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_product_sistemic_class_edit")
     */
    public function productSistemicClassEdit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sistemicClass = $em->getRepository('App:ProductSistemicClass')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_product_sistemic_class/edit.html.twig', [
                'sistemicClass' => $sistemicClass
            ]);
        }

        $form = $request->request->get('form');

        $sistemicClass->setName($form['name']);
        $sistemicClass->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $sistemicClass->setUpdatedAt(new \DateTime());
        
        $em->persist($sistemicClass);
        $em->flush();

        return $this->redirectToRoute('admin_product_sistemic_class_list');
    }
}
