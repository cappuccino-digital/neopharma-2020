<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\QuizScore;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * @Route("/quiz")
 */
class QuizController extends Controller
{
    /**
     * @Route("", name="quiz_home")
     */
    public function index(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
      
        $em = $this->getDoctrine()->getManager();
        $banners = $em->getRepository('App:Banner')->getBannerByTypeName('quiz');

        $builder = $em->getRepository('App:QuizAll')->createQueryBuilder('q');

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($request->query->has('q')) {
                $builder
                    ->where('q.name LIKE :query')
                    ->setParameter('query', '%' . $request->query->get('q') . '%');
            }
           
            if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCrf())) {
                    $this->addFlash("alert", "Por favor preencha o campo CRF.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                $builder->andWhere("q.is_pharmaceutical = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                //verificando usuários que não preencheram o campo obrigatório 
                if (!trim($this->getUser()->getCnpj())) {
                    $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                    return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
                }
                $builder->andWhere("q.is_clerk = true");
            } else if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
                $builder->andWhere("q.is_clerk = true");
            }
            
        } else {
            $builder->andWhere("q.is_public = true");
        }

        $builder->andWhere("q.is_active = true");
        
        $builder->orderBy('q.created_at','DESC');

        $quizes = $builder->getQuery()->getResult();

        // $quizes = $this->get('knp_paginator')->paginate(
        //     $quizes,
        //     $request->query->getInt('page', 1),
        //     5
        // );

        return $this->render('quiz/index.html.twig', [
            'quizes' => $quizes,
            'banners' => $banners
        ]);
    }

    /**
     * @Route("/{slug}", name="quiz_do")
     */
    public function quiz($slug, Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository('App:QuizAll')->findOneBySlug($slug);

        if (!$quiz) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        $question = $em->getRepository('App:QuizAllQuestions')->findBy(['quiz_all' => $quiz]);
       
        return $this->render('quiz/quiz.html.twig', [
            'quiz' => $quiz,
            'questions' => $question
        ]);
    }

    /**
     * @Route("/resultado/perguntas", name="quiz_result")
     */
    public function result(Request $request)
    {
        if ($request->request->has('q')) {
            $slug = $request->request->get('slug');
            $hits = $request->request->get('hits');
            $response = ['status' => false, 'message' => 'Resultado não encontrado.'];

            $em = $this->getDoctrine()->getManager();
            $quiz = $em->getRepository('App:QuizAll')->findOneBySlug($slug);
            $quiz_result = $em->getRepository('App:QuizAllRuleResult')->findBy(['quiz_all' => $quiz]);

            if ($quiz_result) { 
                foreach ($quiz_result as $q) {
                    if (($q->getMax() >= $hits) and ($q->getMin() <= $hits)) {
                        $response = ['status' => true, 'message' => $q->getDescription()];
                    }
                }
            }

            $user = $this->getUser();
            if ($user) {
                $em->getRepository('App:UserPoint')->setPointsToBadge($user, $quiz, 'fazedor-de-quiz', 'fazedor-do-novo-quiz');
                $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'responder-a-quiz', 'quiz', $quiz->getId()); //Gamificação interação
            }

            return new JsonResponse($response);
        }
    }
}
