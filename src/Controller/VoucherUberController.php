<?php

namespace App\Controller;

use App\Entity\UserPreferential;
use App\Entity\UserVoucherUber;
use App\Entity\PreferentialChildren;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;

class VoucherUberController extends Controller
{
    /**
     * @Route("/voucher", name="voucher_uber")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {   
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }

        return $this->redirectToRoute('voucher-uber-result', ['result'=> 'ingressos-indisponivel']);

        //remover essa redenrização somente na quinta para liberar o formulário
        //return $this->render('corinthians_fluminense/teaser.html.twig', []); die();

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_tickets_corinthians_all = [];

        //Gestor de loja e balconista não são permitido
        if ($user->getIsPharmacy() || !$user->getIsPharmaceutical()) {
            return $this->redirectToRoute('home');
        } 
        

        $user_tickets_corinthians_all = $em->getRepository('App:UserVoucherUber')->findAll();
        
        //apenas 1960 ingressos
        if (count($user_tickets_corinthians_all) >= 1960) {
            return $this->redirectToRoute('voucher-uber-result', ['result'=> 'ingressos-esgotados']);
        }

        //dump($user); die();
        if ($request->request->has('tc')) {
            $error = array();
            
            //dados pessoais
            $name = trim($request->request->get('name'));
            $zip_code = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address = trim($request->request->get('address'));
            $number = trim($request->request->get('number'));
            $complement = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city = trim($request->request->get('city'));
            $state = trim($request->request->get('state'));
            $cellphone = trim($request->request->get('cellphone'));
            $email = trim($request->request->get('email'));
            $birthday = trim($request->request->get('birthday'));
            $gender = trim($request->request->get('gender'));
            $educationalLevel = trim($request->request->get('educationalLevel'));
            $accept_term = trim($request->request->get('accept_term'));
            //dump($accept_term); die();
            //acompanhante
            $name_companion = trim($request->request->get('name_companion'));
            $cellphone_companion = trim($request->request->get('cellphone_companion'));
            $email_companion = trim($request->request->get('email_companion'));
            //preferencias
            $hobby = trim($request->request->get('hobby_other')) != '' ? $request->request->get('hobby_other') : trim($request->request->get('hobby'));
            $sport = trim($request->request->get('sport_other')) != '' ? $request->request->get('sport_other') : trim($request->request->get('sport'));
            $team_football = trim($request->request->get('team_football_other')) != '' ? $request->request->get('team_football_other') : trim($request->request->get('team_football'));
            $is_children = $request->request->get('is_children') == 1 ? true : false;
            $how_many = $request->request->get('how_many');
            $info_children = $request->request->get('info_children'); 

    
            // if ((!$this->checkCity($city, $state))) { 
            //     $url = $request->getSchemeAndHttpHost() . '/voucher/ingressos-indisponivel';
                
            //     return new JsonResponse([   
            //         'status' => true,
            //         'url' => $url,
            //     ]);
            // }

            // if ($state != 'SP') {
            //     $error['state'] = 'Estado não permitido.';
            // } 

            if ($name) {
                $user->setName($request->request->get('name'));
            } else {
                $error['name'] = 'O nome é obrigatório.';
            }
    
            if ($gender) {
                $user->setGender($gender);
            } else {
                $error['gender'] = 'O Gênero é obrigatório.';
            }
                                                     
            if ($birthday != '') {
                $d = explode('/', $birthday);
                if (isset($d[0]) && isset($d[1]) && isset($d[2])) {
                    $birthday = "{$d[2]}-{$d[1]}-{$d[0]}";
    
                    $user->setBirthday(new \DateTime($birthday));
                }
            } else {
                $error['birthday'] = 'A Data de nascimento é obrigatório.';
            }

            if ($educationalLevel) {
                $user->setEducationalLevel($educationalLevel);
            } else {
                $error['educationalLevel'] = 'O Grau de escolaridade é obrigatório.';
            }
           
            if ($zip_code) {
                $user->setZipCode($zip_code);
            } else {
                $error['zip_code'] = 'O CEP é obrigatório.';
            }
           
            if ($address) {
                $user->setAddress($address);
            } else {
                $error['address'] = 'O endereço é obrigatório.';
            }
            
            if ($number) {
                $user->setNumber($number);
            } else {
                $error['number'] = 'O número é obrigatório.';
            }
            
            if ($complement) {
                $user->setComplement($complement);
            } else {
                //$error['complement'] = 'O complemento é obrigatório.';
            }
            
            if ($neighborhood) {
                $user->setNeighborhood($neighborhood);
            } else {
                $error['neighborhood'] = 'O bairro é obrigatório.';
            }

            if ($accept_term) {
                //$user->setNeighborhood($neighborhood);
            } else {
                $error['accept_term'] = 'A leitura do termo é obrigatório.';
            }
            
            if ($city) {
                $user->setCity($city);
            } else {
                $error['city'] = 'A cidade é obrigatória.';
            }
            
            if ($state) {
                $user->setEstateAddress($state);
            } else {
                $error['state'] = 'O estado é obrigatório.';
            }
           
            if ($cellphone) {
                if (strlen($cellphone) < 15) { //contando com as maskaras
                    $error['cellphone'] = "Celular inválido";
                } else {
                    $user->setCellphone($cellphone);
                }
            } else {
                $error['cellphone'] = 'O celular é obrigatório.';
            }
         
            if ($email) {
                $user->setEmail($email);
                if ($user->getIsPharmacy()) {
                     
                } else {
                    //$user->setUsername($email);
                }
            } else {
                $error['email'] = 'O celular é obrigatório.';
            }
           
            //validação e-mail principal
            if (!$email) {
                $error['email'] = 'O e-mail é obrigatório';
            } else {

                if ($email != $user->getEmail()) {
                   
                    $user_check_email = $em->getRepository('App:User')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email or u.username = :username')
                            ->setParameter('email', $email)
                            ->setParameter('username', $email)
                            ->getQuery()
                            ->getResult();
                    
                    if ($user_check_email) {
                        $error['email'] = 'Email está sendo utilizado.';
                    }

                    $user_participant_check = $em->getRepository('App:UserVoucherUber')->createQueryBuilder('uc')
                                        ->innerJoin('App:User', 'u' , 'WITH' , 'uc.user = u.id')
                                        ->where('u.email = :email or u.username = :username')
                                        ->setParameter('email', $email)
                                        ->setParameter('username', $email)
                                        ->getQuery()
                                        ->getResult();

                    if ($user_participant_check) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                } else {
                    
                    $user_companion_check_email = $em->getRepository('App:CorinthiansFluminenseCompanion')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email')
                            ->setParameter('email', $email)
                            ->getQuery()
                            ->getResult();

                    if ($user_companion_check_email) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                }
                
                //validação e-mail
                if (!$this->validaEmail($email)) {
                    $error['email'] = 'E-mail inválido';
                }
            }
           
            //preferencias
            if (!$hobby) {
                $error['hobby'] = 'O hobby é obrigatório';
            } 
            
            if (!$sport) {
                $error['sport'] = 'O esporte é obrigatório';
            }

            if (!$team_football) {
                $error['team_football'] = 'O time que você torce é obrigatório';
            }
           

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }
          
            $em = $this->getDoctrine()->getManager();
            $user_tickets_corinthians = $em->getRepository('App:UserVoucherUber')->findOneBy(['user' => $user]);

            if (!$user_tickets_corinthians) {
                //cadastro do usuário para receber os ingressos
                $user_tickets_corinthians = new UserVoucherUber();  
                $user_tickets_corinthians->setUser($user); 

                if ($user->getIsPharmaceutical()) { //farmacêutico
                    $user_tickets_corinthians->setIsPharmaceutical(true); 
                } else if (!$user->getIsPharmaceutical()) { //balconista
                    $user_tickets_corinthians->setIsPharmaceutical(false); 
                } 

                $user_tickets_corinthians->setIsNewRegister(false); 
                
                $user_tickets_corinthians->setCreatedAt(new \DateTime());
                $user_tickets_corinthians->setUpdatedAt(new \DateTime());  
       
                $em->persist($user_tickets_corinthians);
                $em->flush();

                // //cadastro acompanhante
                // $tickets_corinthians = new CorinthiansFluminenseCompanion(); 
                // $tickets_corinthians->setUserCorinthiansFluminense($user_tickets_corinthians); 
                // $tickets_corinthians->setName($name_companion); 
                // $tickets_corinthians->setCellphone($cellphone_companion); 
                // $tickets_corinthians->setEmail($email_companion); 
                // $tickets_corinthians->setCreatedAt(new \DateTime());
                // $tickets_corinthians->setUpdatedAt(new \DateTime());  

                // $em->persist($tickets_corinthians);
                // $em->flush();
            
                //update info user
                $em->persist($user);
                $em->flush();


            //preferencias - insert info
                $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

                if (!$user_preferential) {
                    $user_preferential = new UserPreferential();
                }

                $user_preferential->setUser($user);
                $user_preferential->setHobby($hobby);
                $user_preferential->setSport($sport);
                $user_preferential->setTeamFootball($team_football);
                $user_preferential->setIsChildren($is_children);
                $user_preferential->setHowManyChildren($how_many);
                $user_preferential->setCreatedAt(new \DateTime());
                $user_preferential->setUpdatedAt(new \DateTime());

                $em->persist($user_preferential);
                $em->flush();

                //atualizando o time na tabela de usuário também
                $user->setTeamFootball($team_football);
                $em->persist($user);
                $em->flush();

                //se tem filhos
                if ($is_children == true && $how_many > 0) {
                    for ($i = 1; $i <= $how_many; $i++) {
                        $preferential_children = $em->getRepository('App:PreferentialChildren')->findOneBy(['user_preferential' => $user_preferential, 'children' => $i]);

                        if (!$preferential_children) {
                            $preferential_children = new PreferentialChildren();
                            $preferential_children->setUserPreferential($user_preferential);
                            $preferential_children->setCreatedAt(new \DateTime());
                        }
                        
                        //Outros campos
                        $team_football = isset($info_children['team_football']['other'][$i]) && $info_children['team_football']['other'][$i] != '' ? $info_children['team_football']['other'][$i] : $info_children['team_football'][$i];
                        $sport         = isset($info_children['sport']['other'][$i]) && $info_children['sport']['other'][$i] != '' ? $info_children['sport']['other'][$i] : $info_children['sport'][$i];
                        
                        //dump($team_football, $sport); die();                   
                        $preferential_children->setChildren($i);
                        $preferential_children->setTeamFootball($team_football);
                        $preferential_children->setSport($sport);
                        $preferential_children->setAge($info_children['age'][$i]);
                        $preferential_children->setUpdatedAt(new \DateTime());

                        $em->persist($preferential_children);
                        $em->flush();
                    }
                } else {
                    $preferential_children = $em->getRepository('App:PreferentialChildren')->findBy(['user_preferential' => $user_preferential]);
                    foreach ($preferential_children as $children) {
                        $em->remove($children);
                        $em->flush();
                    }
                }


            //--- END preferencias

                $emails = ['wellington.santos@cappuccinodigital.com.br', 'andre.lin@cappuccinodigital.com.br', 'caroline.veiga@cappuccinodigital.com.br'];
                //$emails = ['wellington.santos@cappuccinodigital.com.br'];
                    
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo($emails);
              
                $message
                    ->setReplyTo($email)
                    ->setSubject('Portal Neo Pharma - Voucher Uber')
                    ->setBody(
                      'Nome: ' . $user->getName() . ', ' . 
                      'CEP: ' . $user->getZipCode() . ', ' . 
                      'Endereço: ' . $user->getAddress() . ', ' . 
                      'Número: ' . $user->getNumber() . ', ' . 
                      'Bairro: ' . $user->getNeighborhood() . ', ' . 
                      'Cidade: ' . $user->getCity() . ', ' . 
                      'Estado: ' . $user->getState() . ', ' . 
                      'Celular: ' . $user->getCellphone() . ', ' . 
                      'E-mail: ' . $user->getEmail() . ' '                    
                    );

                $response = $mailer->send($message);

                $url = $request->getSchemeAndHttpHost() . '/voucher/parabens';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            return new JsonResponse([
                'status' => false,
                'is_register' => true, 
                'message' => 'Você já fez o cadastro, aguarde as instruções que serão enviadas por e-mail'
            ]);
        }

        $corinthians_fluminense_companion = [];
        $user_corinthians_fluminense = $em->getRepository('App:UserCorinthiansFluminense')->findOneBy(['user' => $user]);
        if ($user_corinthians_fluminense) {
            $corinthians_fluminense_companion = $em->getRepository('App:CorinthiansFluminenseCompanion')->findOneBy(['user_corinthians_fluminense' => $user_corinthians_fluminense]);  
        }


        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

        $teams = [
            'América-MG',
            'Athletico',
            'Avaí',
            'Atlético-GO',
            'Atlético-MG',
            'Bahia',
            'Botafogo',
            'Bragantino',
            'Brasil de Pelotas',
            'Brusque',
            'Ceará',
            'Chapecoense',
            'Confiança',
            'Corinthians',
            'Curitiba',
            'CRB',
            'Cruzeiro',
            'CSA',
            'Cuiabá',
            'Fortaleza',
            'Flamengo',
            'Fluminense',
            'Goiás',
            'Grêmio',
            'Guarani',
            'Internacional',
            'Juventude',
            'Londrina',
            'Náutico',
            'Operário',
            'Palmeiras',
            'Ponte Preta',
            'Remo',
            'Sampaio Corrêa',
            'Santos',
            'São Paulo',
            'Sport',
            'Vasco',
            'Vila Nova',
            'Vitória',
            'Nenhum'
        ];
        
        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

        return $this->render('voucher-uber/index.html.twig', [
            'controller_name' => 'VoucherUber',
            'teams' => $teams,
            'user_preferential' => $user_preferential,
        ]);
    }

    /**
     * @Route("/voucher/{result}", name="voucher-uber-result", defaults={"result"=null})
     */
    public function result(Request $request, $result)
    {
        //die('te');
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        //remover essa redenrização somente na quarta feira para liberar o formulário
        //return $this->redirectToRoute('corinthians-fluminense'); die();

        $array = ['parabens', 'ingressos-esgotados', 'ingressos-indisponivel'];

        if (!in_array($result, $array)) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('voucher-uber/result.html.twig', [
            'result' => $result
        ]);
    }

    
    private function checkCity($city, $state)
    {
        $array = [
            'anapolis',
            'angra dos reis',
            'aracaju',
            'aracatuba',
            'arapiraca',
            'bage',
            'barbacena',
            'barreiras',
            'bauru',
            'belem',
            'belo horizonte',
            'blumenau',
            'boa vista',
            'botucatu',
            'braganca paulista',
            'brasilia',
            'cabo frio',
            'cachoeiro de itapemirim',
            'campina grande',
            'campinas',
            'campo grande',
            'campos dos goytacazes',
            'caruaru',
            'cascavel',
            'caxias do sul',
            'chapeco',
            'colatina',
            'conselheiro lafaiete',
            'cornelio procopio',
            'criciuma',
            'cuiaba',
            'curitiba',
            'divinopolis',
            'dourados',
            'feira de santana',
            'florianopolis',
            'fortaleza',
            'foz do iguacu',
            'franca',
            'garanhuns',
            'goiania',
            'governador valadares',
            'guarapuava',
            'ijui',
            'imperatriz',
            'ipatinga',
            'itabira',
            'itabuna ',
            'ilheus',
            'itajai',
            'itajuba',
            'itaperuna',
            'itapipoca',
            'ivaipora',
            'jequie',
            'ji-parana',
            'joacaba',
            'joao pessoa',
            'joinville',
            'juazeiro do norte',
            'juiz de fora',
            'lages',
            'linhares',
            'londrina',
            'macae',
            'macapa',
            'maceio',
            'manaus',
            'maraba',
            'marilia',
            'maringa',
            'mogi guacu',
            'montes claros',
            'mossoro',
            'muriae',
            'natal',
            'nova friburgo',
            'palmas',
            'paranagua',
            'parauapebas',
            'parnaiba',
            'passo fundo',
            'patos',
            'patos de minas',
            'paulo afonso',
            'pelotas',
            'petrolina',
            'petropolis',
            'piracicaba',
            'pocos de caldas',
            'ponta grossa',
            'porto alegre',
            'porto seguro',
            'porto velho',
            'pouso alegre',
            'presidente prudente',
            'recife',
            'ribeirao preto',
            'rio branco',
            'rio de janeiro',
            'rio verde',
            'rondonopolis',
            'salvador',
            'santa cruz do capibaribe',
            'santa cruz do sul',
            'santa maria',
            'santarem',
            'santo angelo',
            'santo antonio de jesus',
            'santos',
            'sao bento do sul',
            'sao carlos',
            'sao jose do rio preto',
            'sao jose dos campos',
            'sao luis',
            'sao paulo',
            'sobral',
            'sorocaba',
            'teofilo otoni',
            'teresina',
            'tres rios',
            'tucurui',
            'uba',
            'uberaba',
            'uberlandia',
            'umuarama',
            'uruguaiana',
            'valenca',
            'varginha',
            'vitoria',
            'vitoria da conquista',
            'volta redonda',
        ];

        // if ($state != 'SP') {
        //     return false;
        // }
        $city = $this->clarCharSpecial(trim($city));
        $city = mb_strtolower($city, 'UTF-8');
        if (!in_array($city, $array)) {
            return false;
        }

        return true;
    }

    private function validaEmail($mail){
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }

    private function clarCharSpecial($string) {
        $caracteres_sem_acento = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Â'=>'Z', 'Â'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Å'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'Å'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
            'Ä'=>'a', 'î'=>'i', 'â'=>'a', 'È'=>'s', 'È'=>'t', 'Ä'=>'A', 'Î'=>'I', 'Â'=>'A', 'È'=>'S', 'È'=>'T',
        );
        $nova_string = strtr($string, $caracteres_sem_acento);
        return $nova_string;
    }

    /**
     * @Route("/user/voucher/cron", name="cron-voucher")
     */
    public function cronInactivity(Request $request, \Swift_Mailer $mailer)
    {
        if ($request->query->has('q')) {
            $em = $this->getDoctrine()->getManager();
            $userVouchersRepository = $em->getRepository('App:UserVoucherUber');
            $user_vouchers = $userVouchersRepository->findBy(['is_run_cron' => false]); //pegando todos os usuários que estão no prazo de 60 ou 90 dias sem acessar o portal 
            $total = $userVouchersRepository->findAll();
            $amount = $request->query->get('amount');

            if (count($user_vouchers) > 0) {
                foreach ($user_vouchers as $user_voucher) {
                        $voucher = $em->getRepository('App:Voucher')->findOneBy(['is_active' => true], [], 1);
                        
                        if ($user_voucher->getVoucher()) {
                            $voucher = $user_voucher->getVoucher();
                        } else {
                            //desativa o voucher
                            $voucher->setIsActive(false);

                            $em->persist($voucher);
                            $em->flush();

                            $user_voucher->setVoucher($voucher);
                        }

                        //marcado como já enviado
                        $user_voucher->setIsRunCron(true);
                        
                        $em->persist($user_voucher);
                        $em->flush();   

                        //$emails = ['wellington.santos@cappuccinodigital.com.br', 'andre.lin@cappuccinodigital.com.br', 'caroline.veiga@cappuccinodigital.com.br'];
                        $emails = ['wellington.santos@cappuccinodigital.com.br'];
                        // $emails = ['wellington.santos@cappuccinodigital.com.br', $user_voucher->getUser()->getEmail()];
                            
                        $logger = new Swift_Plugins_Loggers_ArrayLogger();
                        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                        
                        $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                            ->setTo($user_voucher->getUser()->getEmail());
                        // ->setTo('wellington.santos@cappuccinodigital.com.br');
                                             
                        $message
                            ->setReplyTo('wellington.santos@cappuccinodigital.com.br')
                            ->setSubject('Portal Neo Pharma - Voucher Uber')
                            ->setBody(
                                $this->renderView(
                                    'voucher-uber/email-cron.html.twig',
                                    ['url' => $voucher->getLink()]
                                ),
                                'text/html'
                            );
        
                        $mailer->send($message);                       
                                             
                        $pedding = $userVouchersRepository->findBy(['is_run_cron' => true]);
                        return new JsonResponse([
                            'status' => true,
                            'pending' => count($total) - count($pedding),
                            'total' => count($total),
                            'message' => 'Enviado com sucessso'
                        ]);  
                } //end foreach
            } else {
                $pedding = $userVouchersRepository->findBy(['is_run_cron' => true]); 
                $total = $userVouchersRepository->findAll();

                if (count($pedding) == count($total)) {
                    return new JsonResponse([
                        'status' => true,
                        'message' => 'Todos os voucher foram enviados com sucesso'
                    ]);  
                }

            } //end if 
        
        }//end if 
       
        return $this->render('voucher-uber/cron.html.twig', [
               
        ]);
        
    }

}
