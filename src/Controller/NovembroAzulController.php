<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class NovembroAzulController extends Controller
{
    /**
     * @Route("/novembro-azul", name="novembro_azul")
     */
    public function index()
    {   
        return $this->render('novembro-azul/index.html.twig', [
            'controller_name' => 'NovembroAzulController',
        ]);
    }

}