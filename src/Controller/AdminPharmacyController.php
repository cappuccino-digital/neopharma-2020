<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Pharmacy;

class AdminPharmacyController extends Controller
{
    /**
     * @Route("/admin/todas/farmacias", name="admin_pharmacy_full")
     */
    public function index(Request $request)
    {              
        set_time_limit(0);
       
        $em = $this->getDoctrine()->getManager();
        $pharmacies = $em->getRepository('App:Pharmacy')->findBy([], ['createdAt' => 'desc'], 50);
     
        $pharmacies = $this->get('knp_paginator')->paginate(
            $pharmacies,
            $request->query->getInt('page', 1),
            20
        );
      
        return $this->render('admin_pharmacy/index.html.twig', [
            'pharmacies' => $pharmacies,
        ]);
    } 

    /**
     * @Route("/admin/importar/farmacias", name="admin_pharmacy_import")
     */
    public function import(Request $request)
    {   
        if ($request->request->has('loadXml')) {
            set_time_limit(0);  

            $new_name = 'farmacias.xml';
            $base = $this->get('kernel')->getProjectDir() . '/public_html';
            $isFiles = "{$base}/uploads/farmacias/{$new_name}";
            
            if ($request->files->has('arquivo')) {
                $files = $request->files->get('arquivo');
                if (!file_exists($isFiles)) {
                    $files->move(
                        $this->getParameter('farmacias_directory'),
                        $new_name
                    );
                }
            } 

            $files = $isFiles; 
        
            $doc = new \DOMDocument(); 
            $doc->load(realpath($files));
            $doc->formatOutput = true; // se
            $thedocument = $doc->documentElement;
            $pharmaciesXml = $thedocument->getElementsByTagName('Farmacia');
     
            $i = 1;
            $number = $request->request->has('number') ? $request->request->get('number') : 10;
            $nodeToRemove = array();
            foreach ($pharmaciesXml as $key => $csv) {
                if ($i <= $number) {
                    if ($csv->getElementsByTagName("nm_pdv")->length == 0) {  //verificando se existe o elemento
                        $name = null;
                    } else {
                        $name = $csv->getElementsByTagName("nm_pdv")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("cnpj_pdv")->length == 0) {  //verificando se existe o elemento
                        $cnpj = null;
                    } else {
                        $cnpj = $csv->getElementsByTagName("cnpj_pdv")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("ENDERECO")->length == 0) {  //verificando se existe o elemento
                        $endereco = null;
                    } else {
                        $endereco = $csv->getElementsByTagName("ENDERECO")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_bairro")->length == 0) {  //verificando se existe o elemento
                        $bairro = null;
                    } else {
                        $bairro = $csv->getElementsByTagName("nm_bairro")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_cidade")->length == 0) {  //verificando se existe o elemento
                        $cidade = null;
                    } else {
                        $cidade = $csv->getElementsByTagName("nm_cidade")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_uf")->length == 0) {  //verificando se existe o elemento
                        $uf = null;
                    } else {
                        $uf = $csv->getElementsByTagName("nm_uf")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("cd_cep")->length == 0) {  //verificando se existe o elemento
                        $cep = null;
                    } else {
                        $cep = $csv->getElementsByTagName("cd_cep")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_canal")->length == 0) {  //verificando se existe o elemento
                        $canal = null;
                    } else {
                        $canal = $csv->getElementsByTagName("nm_canal")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_canal_genericos")->length == 0) {  //verificando se existe o elemento
                        $canal_genericos = ' ';
                    } else {
                        $canal_genericos = $csv->getElementsByTagName("nm_canal_genericos")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_canal_smart")->length == 0) {  //verificando se existe o elemento
                        $canal_smart = ' ';
                    } else {
                        $canal_smart = $csv->getElementsByTagName("nm_canal_smart")->item(0)->nodeValue;
                    }
                    
                    if ($csv->getElementsByTagName("nm_grupo_canal")->length == 0) {  //verificando se existe o elemento
                        $grupo_canal = '---';
                    } else {
                        $grupo_canal = $csv->getElementsByTagName("nm_grupo_canal")->item(0)->nodeValue;
                    }

                    if ($csv->getElementsByTagName("ESTA_ATIVO")->length == 0) {  //verificando se existe o elemento
                        $ativo = null;
                    } else {
                        $ativo = $csv->getElementsByTagName("ESTA_ATIVO")->item(0)->nodeValue;
                    }
                   // die('teste');
                    //dump($csv, $name, $cnpj, $endereco, $bairro, $cidade,  $uf, $cep, $canal, $canal_genericos, $ativo, $canal_smart, $grupo_canal); die(); 
                    $em = $this->getDoctrine()->getManager();
                    $pharmacies = $em->getRepository('App:Pharmacy')->findOneBy(['cnpj'=> $cnpj]);
                    
                        if (!$pharmacies) {
                            $pharmacy = new Pharmacy();
                            $pharmacy->setPdv($name);
                            $pharmacy->setCnpj($cnpj);
                            $pharmacy->setAddress($endereco);
                            $pharmacy->setNeighborhood($bairro);
                            $pharmacy->setCity($cidade);
                            $pharmacy->setState($uf);
                            $pharmacy->setZipCode($cep);
                            $pharmacy->setChannel($canal);
                            $pharmacy->setGenericChannel($canal_genericos);
                            $pharmacy->setSmartChannel($canal_smart);
                            $pharmacy->setIsActive(isset($ativo) == 'S' ? true : false);
                            $pharmacy->setGroupChannel($grupo_canal);
                            $pharmacy->setCreatedAt(new \DateTime());
                            $pharmacy->setUpdatedAt(new \DateTime());
                            
                            $em->persist($pharmacy);
                            $em->flush();
                        } else {
                            $pharmacies->setAddress($endereco);
                            $pharmacies->setNeighborhood($bairro);
                            $pharmacies->setCity($cidade);
                            $pharmacies->setState($uf);
                            $pharmacies->setZipCode($cep);
                            $pharmacies->setIsActive(isset($ativo) == 'S' ? true : false);
                            $pharmacies->setUpdatedAt(new \DateTime());

                            $em->persist($pharmacies);
                            $em->flush();
                        }    

                        $nodeToRemove[] = $csv;
                    }

                    if ($i == $number) break;

                    $i++;
                }
          
                if (count($nodeToRemove) > 0) {
                    foreach ($nodeToRemove as $node) {
                        $node->parentNode->removeChild($node);
                    }

                    $doc->save($files);

                    if ($pharmaciesXml->length != 0) {
                        return new JsonResponse([
                            'status' => true,
                            'amount' => $pharmaciesXml->length,
                            'message' => "Falta apenas ({$pharmaciesXml->length})"
                        ]);  
                    }
                }    
                
                unlink($files); //depois que cadastrar as farmacias excluir o arquivo XML do server

                return new JsonResponse([
                    'status' => true,
                    'amount' => $pharmaciesXml->length,
                    'message' => 'Farmácias cadastradas com sucessso'
                ]);  
        }

        return new JsonResponse([
            'status' => false,
            'message' => 'Arquivo XML não encontrado.'
        ]);  
    }

}
