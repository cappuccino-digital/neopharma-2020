<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\BlogPostType;

/**
 * @Route("/admin/blog/type")
 */
class AdminBlogTypeController extends Controller
{
    /**
     * @Route("", name="admin_blog_type_list")
     */
    public function postTypeList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:BlogPostType')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $types = $builder->getQuery()->getResult();

        $types = $this->get('knp_paginator')->paginate(
            $types,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_blog_type/list.html.twig', [
            'types' => $types
        ]);
    }

    /**
     * @Route("/novo", name="admin_blog_type_new")
     */
    public function postTypeNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_blog_type/new.html.twig', []);
        }

        $form = $request->request->get('form');

        $type = new BlogPostType();
        $type->setName($form['name']);
        $type->setIsActive(isset($form['isActive']) ? ($form['isActive'] == 'on' ? true : false ): false );
        $type->setCreatedAt(new \DateTime());
        $type->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($type);
        $em->flush();

        return $this->redirectToRoute('admin_blog_type_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_blog_type_edit")
     */
    public function postTypeEdit($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('App:BlogPostType')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_blog_type/edit.html.twig', [
                'type' => $type
            ]);
        }

        $form = $request->request->get('form');

        $type->setName($form['name']);
        $type->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $type->setUpdatedAt(new \DateTime());
        
        $em->persist($type);
        $em->flush();
        
        return $this->redirectToRoute('admin_blog_type_list');
    }
}
