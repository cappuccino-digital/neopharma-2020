<?php

namespace App\Controller;

use App\Entity\Gallery;
use App\Form\GalleryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/admin/galeria")
 */
class AdminGalleryController extends Controller
{
    /**
     * @Route("/", name="admin_gallery")
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $galleries = $em->getRepository('App:Gallery')->findAll();

        $galleries = $this->get('knp_paginator')->paginate(
            $galleries,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('admin_gallery/index.html.twig', [
            'galleries' => $galleries,
        ]);
    }

    /**
     * @Route("/novo", name="admin_gallery_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {


        $gallery = new Gallery();
        $form = $this->createForm(GalleryType::class, $gallery);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gallery = $form->getData();

            $imageFile = $form->get('file')->getData();

                if ($imageFile) {
                    if (filesize($imageFile) != false) {

                        if (filesize($imageFile) > 1048576) {

                            $this->addFlash("alert", "Imagem maior que o permitido.");

                            return $this->redirectToRoute('admin_gallery_new');
                        }
                    } else {

                        $this->addFlash("alert", "ERRO AO IMPORTAR ARQUIVO. POR FAVOR SELECIONE OUTRO.");

                        return $this->redirectToRoute('admin_gallery_new');

                        $fileName = $fileUploader->upload($imageFile, 'gallery');
                        $gallery->setFile($fileName);
                    }

                    $gallery->setCreatedAt(new \DateTime());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($gallery);
                    $em->flush();


                    $this->addFlash("alert", "Upload feito com sucesso.");


                    $this->addFlash("alert", "Upload feito com sucesso.");

                    return $this->redirectToRoute('admin_gallery');
                }

                return $this->redirectToRoute('admin_gallery');
            

            return $this->render('admin_gallery/new.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }
}
