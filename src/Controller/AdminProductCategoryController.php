<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProductCategory;

/**
 * @Route("/admin/product/category")
 */
class AdminProductCategoryController extends Controller
{
    /**
     * @Route("", name="admin_product_category_list")
     */
    public function productCategoryList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:ProductCategory')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $categories = $builder->getQuery()->getResult();

        $categories = $this->get('knp_paginator')->paginate(
            $categories,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_product_category/list.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_category_new")
     */
    public function productCategoryNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_product_category/new.html.twig', []);
        }

        $form = $request->request->get('form');


        $category = new ProductCategory();
        $category->setName($form['name']);
        $category->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $category->setCreatedAt(new \DateTime());
        $category->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return $this->redirectToRoute('admin_product_category_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_product_category_edit")
     */
    public function productCategoryEdit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('App:ProductCategory')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_product_category/edit.html.twig', [
                'category' => $category
            ]);
        }

        $form = $request->request->get('form');

        $category->setName($form['name']);
        $category->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $category->setUpdatedAt(new \DateTime());
        
        $em->persist($category);
        $em->flush();

        return $this->redirectToRoute('admin_product_category_list');
    }
}
