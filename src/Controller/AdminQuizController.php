<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Quiz;
use App\Entity\QuizQuestion;
use App\Entity\QuizAnswer;
use App\Entity\QuizResult;
use App\Entity\ReferrerContent;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin/quiz")
 */
class AdminQuizController extends Controller
{
    /**
     * @Route("", name="admin_quiz_list")
     */
    public function index(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Quiz')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->orWhere('q.description LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $quizzes = $builder->getQuery()->getResult();

        $paginator = $this->get('knp_paginator')->paginate(
            $quizzes,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_quiz/index.html.twig', [
            'quizzes' => $paginator,
        ]);
    }

    /**
     * @Route("/novo", name="admin_quiz_new")
     */
    public function adminQuizNew(Request $request)
    {

        if ($request->isMethod('GET')) {
            return $this->render('admin_quiz/new.html.twig', []);
        }
        $form = $request->request->get('form');
        $files = $request->files->all();

        $quizForm['text'] = $form['quiz'];
        $quizForm['file'] = $files['form']['quiz'];

        $questionForm['text'] = $form['question'];
        $questionForm['file'] = $files['form']['question'];

        $resultForm['text'] = $form['result'];
        $productReferred = $request->request->get('referrer');

        $quiz = $this->saveQuiz($quizForm);
        $this->saveQuestion($quiz, $questionForm);
        $this->saveResult($quiz, $resultForm);
        if (is_array($productReferred)) $this->saveReferredContent($quiz, $productReferred);

        return $this->redirectToRoute('admin_quiz_list');
    }

    /**
     * @Route("/{id}", name="admin_quiz_view")
     */
    public function adminQuizView($id)
    {
        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository('App:Quiz')->find($id);
        $questions = $em->getRepository('App:QuizQuestion')->findBy(['quiz' => $quiz], ['position' => 'ASC']);
        $results = $em->getRepository('App:QuizResult')->findBy(['quiz' => $quiz]);

        $questionArray = [];

        foreach ($questions as $key => $q) {
            $questionArray[$key]['id'] = $q->getId();
            $questionArray[$key]['question'] = $q->getQuestion();
            $questionArray[$key]['hasMultipleAnswers'] = $q->getHasMultipleAnswers();
            $questionArray[$key]['numberOfAnswers'] = $q->getNumberOfAnswers();
            $questionArray[$key]['position'] = $q->getPosition();
            foreach ($q->getAnswers() as $k => $a) {
                $questionArray[$key]['answers'][$k]['id'] = $a->getId();
                $questionArray[$key]['answers'][$k]['answer'] = $a->getAnswer();
                $questionArray[$key]['answers'][$k]['feedback'] = $a->getFeedback();
                $questionArray[$key]['answers'][$k]['value'] = $a->getValue();
                $questionArray[$key]['answers'][$k]['isCorrect'] = $a->getIsCorrect();
                $questionArray[$key]['answers'][$k]['image'] = $a->getImage();
                $questionArray[$key]['answers'][$k]['imageMobile'] = $a->getImageMobile();
            }
        }

        $resultsArray = [];

        foreach ($results as $key => $r) {
            $resultsArray[$key]['id'] = $r->getId();
            $resultsArray[$key]['finalMessage'] = $r->getFinalMessage();
            $resultsArray[$key]['minScore'] = $r->getMinScore();
            $resultsArray[$key]['maxScore'] = $r->getMaxScore();
        }

        return $this->render('admin_quiz/show.html.twig', [
            'quiz' => $quiz,
            'questions' => $questionArray,
            'results' => $resultsArray
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_quiz_edit")
     */
    public function adminQuizEdit(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $quiz =  $em->getRepository('App:Quiz')->find($id);
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContent($quiz->getId(), 'quiz');

        if ($request->isMethod('GET')) {
            return $this->render('admin_quiz/edit.html.twig', [
                'quiz' => $quiz,
                'contentReferrer' => $contentReferrer
            ]);
        }

        $form = $request->request->get('form');
        $files = $request->files->all();



        $quizForm['text'] = $form['quiz'];
        $quizForm['file'] = $files['form']['quiz'];

        $questionForm['text'] = $form['question'];
        $questionForm['file'] = $files['form']['question'];

        //r($questionForm);die;
        $resultForm['text'] = $form['result'];
        $productReferred = $request->request->get('referrer');

        $quiz = $this->saveQuiz($quizForm);
        $this->saveQuestion($quiz, $questionForm);
        $this->saveResult($quiz, $resultForm);
        if (is_array($productReferred)) $this->saveReferredContent($quiz, $productReferred);

        return $this->redirectToRoute('admin_quiz_list');
    }

    /**
     * @Route("/remove-question/{id}", name="admin_quiz_remove_question")
     */
    public function removeQuestion($id)
    {
        $em = $this->getDoctrine()->getManager();
        $question =  $em->getRepository('App:QuizQuestion')->find($id);

        if (!$question) {
            return new JsonResponse([
                'status' => true
            ]);
        }

        $em->remove($question);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    /**
     * @Route("/remove-answer/{id}", name="admin_quiz_remove_answer")
     */
    public function removeQuestionAnswer($id)
    {
        $em = $this->getDoctrine()->getManager();
        $answer =  $em->getRepository('App:QuizAnswer')->find($id);

        if (!$answer) {
            return new JsonResponse([
                'status' => true
            ]);
        }

        $em->remove($answer);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    private function saveReferredContent($quiz, $productReferred)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($productReferred as $referred) {
            $r = new ReferrerContent();
            $r->setMainContentId($quiz->getId());
            $r->setMainContentType('quiz');
            $r->setReferrerContentId((int)$referred['contentId']);
            $r->setReferrerContentType($referred['type']);
            $r->setCreatedAt(new \DateTime());
            $r->setUpdatedAt(new \DateTime());

            $em->persist($r);

            $rBack = new ReferrerContent();
            $rBack->setReferrerContentId($quiz->getId());
            $rBack->setReferrerContentType('quiz');
            $rBack->setMainContentId((int)$referred['contentId']);
            $rBack->setMainContentType($referred['type']);
            $rBack->setCreatedAt(new \DateTime());
            $rBack->setUpdatedAt(new \DateTime());

            $em->persist($rBack);
            $em->flush();
        }
    }

    private function saveQuiz($quizForm)
    {

        $em = $this->getDoctrine()->getManager();
        $isNew = false;
        if (isset($quizForm['text']['id'])) {
            $q = $em->getRepository('App:Quiz')->find($quizForm['text']['id']);
            $q->setUpdatedAt(new \DateTime());
        } else {
            $q = new Quiz();
            $q->setCreatedAt(new \DateTime());
            $q->setUpdatedAt(new \DateTime());
            $isNew = true;
        }

        $q->setTitle($quizForm['text']['title']);
        $q->setShortDescription($quizForm['text']['shortDescription']);
        $q->setDescription(html_entity_decode($quizForm['text']['description'], ENT_COMPAT, 'UTF-8'));
        $q->setIntroduction(html_entity_decode($quizForm['text']['introduction'], ENT_COMPAT, 'UTF-8'));
        $q->setIsOnlyToPharmaceutical(isset($quizForm['text']['isOnlyToPharmaceutical']) ? $quizForm['text']['isOnlyToPharmaceutical'] : false);
        $q->setIsActive(isset($quizForm['text']['isActive']) ? $quizForm['text']['isActive'] : false);
        $q->setIsPublic(isset($quizForm['text']['isPublic']) ? $quizForm['text']['isPublic'] : false);

        $q->setMetaTitle($quizForm['text']['metaTitle']);
        $q->setMetaDescription($quizForm['text']['metaDescription']);
        $q->setImageMetaTitle($quizForm['text']['imageMetaTitle']);
        $q->setImageMetaDescription($quizForm['text']['imageMetaDescription']);
        $q->setImageMobileMetaTitle($quizForm['text']['imageMobileMetaTitle']);
        $q->setImageMobileMetaDescription($quizForm['text']['imageMobileMetaDescription']);



        if (isset($quizForm['file']['image']) && $quizForm['file']['image'] !== null) {
            if (filesize($quizForm['file']['image']) != false) {

                if (filesize($quizForm['file']['image']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_quiz_new');
                }
                if ($q->getImage() != null && $q->getImage() != '') {
                    $this->removeFile($q->getImage());
                }
                $q->setImage($this->saveFile($quizForm['file']['image'], $q, 'quiz'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");

                return $this->redirectToRoute('admin_quiz_new');
            }

            if ($q->getImage() != null && $q->getImage() != '') {
                $this->removeFile($q->getImage());
            }
            $q->setImage($this->saveFile($quizForm['file']['image'], $q, 'quiz'));

        } else {
            if ($isNew) {
                $q->setImage('_dist/image/default_desktop.png');
            }
        }

        if (isset($quizForm['file']['imageMobile']) && $quizForm['file']['imageMobile'] !== null) {
            if (filesize($quizForm['file']['imageMobile']) != false) {

                if (filesize($quizForm['file']['imageMobile']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_quiz_new');
                }

                if ($q->getImageMobile() != null && $q->getImageMobile() != '') {
                    $this->removeFile($q->getImageMobile());
                }
                $q->setImageMobile($this->saveFile($quizForm['file']['imageMobile'], $q, 'quiz'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");

                return $this->redirectToRoute('admin_quiz_new');
            }

            if ($q->getImageMobile() != null && $q->getImageMobile() != '') {
                $this->removeFile($q->getImageMobile());
            }
        } else {
            if ($isNew) {
                $q->setImageMobile('_dist/image/default_mobile.png');
            }
        }


        if (isset($quizForm['file']['pdfFile']) && $quizForm['file']['pdfFile'] !== null) {
            if ($q->getPdfFile() != null && $q->getPdfFile() != '') {
                $this->removeFile($q->getPdfFile());
            }
            $q->setPdfFile($this->saveFile($quizForm['file']['pdfFile'], $q, 'quiz'));
        }

        $em->persist($q);
        $em->flush($q);

        return $q;
    }

    private function saveQuestion($quiz, $questionForm)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($questionForm['text'] as $key => $question) {

            if (isset($question['id'])) {
                $q = $em->getRepository('App:QuizQuestion')->find($question['id']);
                $q->setUpdatedAt(new \DateTime());
            } else {
                $q = new QuizQuestion();
                $q->setCreatedAt(new \DateTime());
                $q->setUpdatedAt(new \DateTime());
                $q->setQuiz($quiz);
            }

            $q->setPosition($question['position']);
            $q->setQuestion($question['question']);
            $q->setHasMultipleAnswers(isset($question['hasMultipleAnswers']) ? $question['hasMultipleAnswers'] : false);
            $q->setNumberOfAnswers($question['numberOfAnswers']);

            $em->persist($q);
            $em->flush($q);

            $answers = $question['answer'];
            $answersFile = $questionForm['file'][$key]['answer'];

            foreach ($answers as $i => $answer) {
                $questionIsNew = false;
                if (isset($answer['id'])) {
                    $a = $em->getRepository('App:QuizAnswer')->find($answer['id']);
                    $a->setUpdatedAt(new \DateTime());
                } else {
                    $a =  new QuizAnswer();
                    $a->setCreatedAt(new \DateTime());
                    $a->setUpdatedAt(new \DateTime());
                    $a->setQuestion($q);
                    $questionIsNew = true;
                }

                $a->setAnswer($answer['answer']);
                $a->setValue($answer['value']);
                $a->setFeedBack($answer['feedback']);
                $a->setImageMetaTitle($answer['imageMetaTitle']);
                $a->setImageMetaDescription($answer['imageMetaDescription']);
                $a->setImageMobileMetaTitle($answer['imageMobileMetaTitle']);
                $a->setImageMobileMetaDescription($answer['imageMobileMetaDescription']);
                $a->setIsCorrect(isset($answer['isCorrect']) ? $answer['isCorrect'] : false);

                $em->persist($a);
                $em->flush($a);

                if (isset($answersFile[$i]['image']) && $answersFile[$i]['image'] !== null) {
                    if ($a->getImage() != null && $a->getImage() != '') {
                        $this->removeFile($a->getImage());
                    }
                    $a->setImage($this->saveFile($answersFile[$i]['image'], $quiz, 'quiz'));
                }

                if (isset($answersFile[$i]['imageMobile']) && $answersFile[$i]['imageMobile'] !== null) {
                    if ($a->getImageMobile() != null && $a->getImageMobile() != '') {
                        $this->removeFile($a->getImageMobile());
                    }
                    $a->setImageMobile($this->saveFile($answersFile[$i]['imageMobile'], $quiz, 'quiz'));
                }

                $em->persist($a);
                $em->flush($a);
            }
        }

        return true;
    }

    private function saveResult($quiz, $resultForm)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($resultForm['text'] as $key => $result) {

            if (isset($result['id'])) {
                $r = $em->getRepository('App:QuizResult')->find($result['id']);
                $r->setUpdatedAt(new \DateTime());
            } else {
                $r = new QuizResult();
                $r->setQuiz($quiz);
                $r->setCreatedAt(new \DateTime());
                $r->setUpdatedAt(new \DateTime());
            }

            $r->setFinalMessage($result['finalMessage']);
            $r->setMinScore($result['minScore']);
            $r->setMaxScore($result['maxScore']);

            $em->persist($r);
            $em->flush($r);
        }

        return true;
    }

    private function saveFile($file, $quiz, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $quiz->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
