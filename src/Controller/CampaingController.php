<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/videos")
 */
class CampaingController extends Controller
{
    /**
     * @Route("", name="campanhas_home")
     */
    public function index(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        $em = $this->getDoctrine()->getManager();
        $builder = $em->getRepository('App:NeoTvVideo')->createQueryBuilder('p');
        
        $cid = $request->query->has('cid') ? $request->query->get('cid') : null;

        $builder->andWhere("p.isActive = true")->orderBy('p.createdAt','DESC');
        $videos = $builder->getQuery()->getResult();

        $user = $this->getUser();
        if ($user) { 
            if ($request->request->get('g')) {
                if ($request->request->has('i')) {
                    $movie = $em->getRepository('App:NeoTvVideo')->findOneBy(['id' => $request->request->get('i')]);
                    
                    $em->getRepository('App:UserPoint')->setPointsToBadge($user, $movie, 'assistir-a-um-video', 'video');
                    $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'assistir-a-um-video', 'video', $movie->getId()); //Gamificação interação
                    
                    return new JsonResponse([
                        'status' => true,
                        'message' => 'Interação registrada com sucesso'
                    ]); 
                }
             }
        }
        
        $response = $this->render('campaing/index.html.twig', [
            'videos' => $videos,
            'cid' => $cid
        ]);

        if ($request->query->get('p') == 'n-motivos') {
            // Set cookie value
            $response->headers->setCookie(new Cookie('n-motivos', true));
        }

        return $response;
    }
}
