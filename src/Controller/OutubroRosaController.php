<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class OutubroRosaController extends Controller
{
    /**
     * @Route("/outubro-rosa", name="outubro_rosa")
     */
    public function index(Request $request)
    {   
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        return $this->render('outubro-rosa/index.html.twig', [
            'controller_name' => 'OutubroRosaController',
        ]);
    }

}