<?php

namespace App\Controller;

use App\Entity\UserCourseEmblue;
use App\Service\EmBlueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserCourseController extends Controller
{
    /**
     * @Route("/cron-course/{when}", name="cron-course-start")
     */
    public function cronCourseStart(Request $request, $when)
    {
        if ($request->query->has('q')) {          
            $event = '';
            $date_start = '';
            $date_now = date('Y-m-d');
            $amount = $request->query->has('amount') ? $request->query->get('amount') : null;
            
            $em = $this->getDoctrine()->getManager();
            
            //eventos e data de start
            if ($when == 'start') {
                $event = 'curso-inicio';
                $date_start = $date_now;
            } else if ($when == '07day') {
                $event = 'curso-7dias';
                $date_start = date('Y-m-d', strtotime($date_now . ' -7 day'));
            } else if ($when == '30day') { 
                $event = 'curso-30dias';
                $date_start = date('Y-m-d', strtotime($date_now . ' -30 day'));
            } else if ($when == 'finish') { 
                $event = 'curso-concluido';
                $date_start = $date_now; 
            }
            
            //query
            if ($when == 'finish') {
                $user_courses  = $em->getRepository('App:UserCourse')->findByCourseFinish($date_start, $amount);
                $total_user_courses = $em->getRepository('App:UserCourse')->findByCourseFinish($date_start);
            } else {
                $user_courses  = $em->getRepository('App:UserCourse')->findByCourseStart($date_start, $amount); 
                $total_user_courses = $em->getRepository('App:UserCourse')->findByCourseStart($date_start);
            }
            //dump($total_user_courses, $user_courses); die();

            if (count($user_courses) > 0) {
                foreach ($user_courses as $user_course) {
                    $user = $em->getRepository('App:User')->findOneBy(['id' => $user_course['user_id']]);
                    $course = $em->getRepository('App:Course')->findOneBy(['id' => $user_course['course_id']]);
                    
                    if ($user) {
                        if ($when) { 
                            $user_course_emblue = $em->getRepository('App:UserCourseEmblue')->findOneBy(['user' => $user, 'course' => $course, 'type' => $event]);
                            //dump($user_course_emblue); die();
                            if (!$user_course_emblue) {
                                //gavando as requisições
                                $user_course_emblue = new UserCourseEmblue();
                                $user_course_emblue->setUser($user);
                                $user_course_emblue->setCourse($course);
                                $user_course_emblue->setEmail($user->getEmail());
                                $user_course_emblue->setType($event);

                                //enviando o contato para emBlue
                                $emBlue = new EmBlueService();
                                $contact = $emBlue->eventCourseStart($user, $user_course['course'], $event);
                                
                                if (isset($contact['status'])) {   
                                    $user_course_emblue->setIsSendEmblue($contact['status']);          
                                    if ($contact['status']) {                
                                        $user_course_emblue->setMessage($contact['message']);
                                    } else {
                                        $user_course_emblue->setMessage($contact['message']); 
                                    }  
                                    $user_course_emblue->setReturnApi($contact['result']);
                                }
                                
                                $user_course_emblue->setDateRunCron(new \DateTime());

                                $em->persist($user_course_emblue);
                                $em->flush();
                            }
                        }
                    }
                    
                                                                            
                } //end foreach

                return new JsonResponse([
                    'status' => true,
                    'pending' => count($total_user_courses) - $amount,
                    'total' => count($total_user_courses),
                    'message' => 'Enviado com sucessso'
                ]);  

            } else {
                if ((count($total_user_courses) > 0) && (count($total_user_courses) == $amount)) {
                    return new JsonResponse([
                        'status' => true,
                        'message' => 'Todos os contatos foram enviados com sucesso'
                    ]);  
                }

            } //end if 
    

            return new JsonResponse([
                'status' => false,
                'message' => 'Nenhum registro foi encontrado'
            ]);  
        
        }//end if 
       
        return $this->render('user_course/user_course_start.html.twig', [
               'when' => $when
        ]);
        
    }
}
