<?php

namespace App\Controller;

use App\Entity\TicketsCorinthiansCompanion;
use App\Entity\UserTicketsCorinthians;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;

class TicketsCorinthiansController extends Controller
{
    /**
     * @Route("/arena", name="tickets-corinthians")
     */
    public function index(Request $request, \Swift_Mailer $mailer) 
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
    
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //apenas 30 pessoas pode pedir  os ingressos
        $user_tickets_corinthians_all = $em->getRepository('App:UserTicketsCorinthians')->findAll();

        if (count($user_tickets_corinthians_all) >= 30) {
            return $this->redirectToRoute('tickets-corinthians-result', ['result'=> 'ingressos-esgotados']);
        }

        return $this->redirectToRoute('tickets-corinthians-result', ['result'=> 'ingressos-esgotados']);

        //dump($user); die();
        if ($request->request->has('tc')) {
            $error = array();

            $name = trim($request->request->get('name'));
            $zip_code = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address = trim($request->request->get('address'));
            $number = trim($request->request->get('number'));
            $complement = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city = trim($request->request->get('city'));
            $state = trim($request->request->get('state'));
            $cellphone = trim($request->request->get('cellphone'));
            $email = trim($request->request->get('email'));
            //acompanhante
            $name_companion = trim($request->request->get('name_companion'));
            $cellphone_companion = trim($request->request->get('cellphone_companion'));
            $email_companion = trim($request->request->get('email_companion'));
    
            if ((!$this->checkCity($city, $state)) || ($state != 'SP')) {
                $url = $request->getSchemeAndHttpHost() . '/arena/ingressos-indisponivel';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            if ($state != 'SP') {
                $error['state'] = 'Estado não permitido.';
            } 
            if ($name) {
                $user->setName($request->request->get('name'));
            } else {
                $error['name'] = 'O nome é obrigatório.';
            }

            if ($zip_code) {
                $user->setZipCode($zip_code);
            } else {
                $error['zip_code'] = 'O CEP é obrigatório.';
            }
            
            if ($address) {
                $user->setAddress($address);
            } else {
                $error['address'] = 'O endereço é obrigatório.';
            }
            
            if ($number) {
                $user->setNumber($number);
            } else {
                $error['number'] = 'O número é obrigatório.';
            }
            
            if ($complement) {
                $user->setComplement($complement);
            } else {
                //$error['complement'] = 'O complemento é obrigatório.';
            }
            
            if ($neighborhood) {
                $user->setNeighborhood($neighborhood);
            } else {
                $error['neighborhood'] = 'O bairro é obrigatório.';
            }
            
            if ($city) {
                $user->setCity($city);
            } else {
                $error['city'] = 'A cidade é obrigatória.';
            }
            
            if ($state) {
                $user->setEstateAddress($state);
            } else {
                $error['state'] = 'O estado é obrigatório.';
            }
           
            if ($cellphone) {
                $user->setCellphone($cellphone);
            } else {
                $error['cellphone'] = 'O celular é obrigatório.';
            }

            if ($email) {
                $user->setEmail($email);
            } else {
                $error['email'] = 'O celular é obrigatório.';
            }

            //validação e-mail
            if (!$email) {
                $error['email'] = 'O e-mail é obrigatório';
            } else {

                if (($email != $user->getEmail()) || $email != $user->getUsername()) {
                   
                    $user_check_email = $em->getRepository('App:User')
                            ->createQueryBuilder('u')
                            ->andWhere('u.email = :email or u.username = :username')
                            ->setParameter('email', $email)
                            ->setParameter('username', $email)
                            ->getQuery()
                            ->getResult();
                    
                    if ($user_check_email) {
                        $error['email'] = 'Email está sendo utilizado';
                    }
                } 
                //validação e-mail
                if (!$this->validaEmail($email)) {
                    $error['email'] = 'E-mail inválido';
                }
            }

            //acompanhante
            if (!$name_companion) {           
                $error['name_companion'] = 'O nome é obrigatório.';
            }
            
            if (!$cellphone_companion) {
                $error['cellphone_companion'] = 'O celular é obrigatório.';
            }

            if (!$email_companion) {
                $error['email_companion'] = 'O e-mail é obrigatório.';
            } else {
                //validação e-mail
                if (!$this->validaEmail($email_companion)) {
                    $error['email_companion'] = 'E-mail inválido';
                }
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $error,
                ]);
            }
            
            $em = $this->getDoctrine()->getManager();
            $user_tickets_corinthians = $em->getRepository('App:UserTicketsCorinthians')->findOneBy(['user' => $user]);

            if (!$user_tickets_corinthians) {
                //cadastro do usuário para receber os ingressos
                $user_tickets_corinthians = new UserTicketsCorinthians();  
                $user_tickets_corinthians->setUser($user); 
                $user_tickets_corinthians->setCreatedAt(new \DateTime());
                $user_tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($user_tickets_corinthians);
                $em->flush();

                //cadastro acompanhante
                $tickets_corinthians = new TicketsCorinthiansCompanion(); 
                $tickets_corinthians->setUserTicketsCorinthians($user_tickets_corinthians); 
                $tickets_corinthians->setName($name_companion); 
                $tickets_corinthians->setCellphone($cellphone_companion); 
                $tickets_corinthians->setEmail($email_companion); 
                $tickets_corinthians->setCreatedAt(new \DateTime());
                $tickets_corinthians->setUpdatedAt(new \DateTime());  

                $em->persist($tickets_corinthians);
                $em->flush();

                //update info user
                $em->persist($user);
                $em->flush();

                $emails = ['wellington.santos@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br', 'caroline.veiga@cappuccinodigital.com.br'];
                //$emails = ['wellington.santos@cappuccinodigital.com.br'];
                    
                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
                
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo($emails);
              
                $message
                    ->setReplyTo($email)
                    ->setSubject('Portal Neo Pharma - Ingressos do jogo do Corinthians')
                    ->setBody(
                      'Nome: ' . $user->getName() . ', ' . 
                      'CEP: ' . $user->getZipCode() . ', ' . 
                      'Endereço: ' . $user->getAddress() . ', ' . 
                      'Número: ' . $user->getNumber() . ', ' . 
                      'Bairro: ' . $user->getNeighborhood() . ', ' . 
                      'Cidade: ' . $user->getCity() . ', ' . 
                      'Estado: ' . $user->getState() . ', ' . 
                      'Celular: ' . $user->getCellphone() . ', ' . 
                      'E-mail: ' . $user->getEmail() . ' '. 
                      ' Dados do Acompanhante '. ', ' . 
                      'Nome: ' . $name_companion . ', ' . 
                      'Celular: ' . $cellphone_companion . ', ' . 
                      'E-mail: ' . $email_companion 
                    );

                $response = $mailer->send($message);

                $url = $request->getSchemeAndHttpHost() . '/arena/inscricao-realizada-com-sucesso';

                return new JsonResponse([   
                    'status' => true,
                    'url' => $url,
                ]);
            }

            return new JsonResponse([
                'status' => false,
                'is_register' => true, 
                'message' => 'Você já fez o cadastro, aguarde as instruções que serão enviadas por e-mail'
            ]);
        }

        $tickets_corinthians= [];
        $user_tickets_corinthians = $em->getRepository('App:UserTicketsCorinthians')->findOneBy(['user' => $user]);
        if ($user_tickets_corinthians) {
            $tickets_corinthians = $em->getRepository('App:TicketsCorinthiansCompanion')->findOneBy(['user_tickets_corinthians' => $user_tickets_corinthians]);  
        }
      
        return $this->render('tickets_corinthians/index.html.twig', [
            'tickets_corinthians' => $tickets_corinthians
        ]);
    }

    /**
     * @Route("/arena/{result}", name="tickets-corinthians-result", defaults={"result"=null})
     */
    public function result(Request $request, $result)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $array = ['inscricao-realizada-com-sucesso', 'ingressos-esgotados', 'ingressos-indisponivel'];

        if (!in_array($result, $array)) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('tickets_corinthians/result.html.twig', [
            'result' => $result
        ]);
    }

    private function checkCity($city, $state)
    {
        $array = [
            'Arujá',
            'Barueri',
            'Biritiba Mirim',
            'Caieiras',
            'Cajamar',
            'Carapicuíba',
            'Cotia',
            'Diadema',
            'Embu das Artes',
            'Embu-Guaçu',
            'Ferraz de Vasconcelos',
            'Francisco Morato',
            'Franco da Rocha',
            'Guararema',
            'Guarulhos',
            'Itapecerica da Serra',
            'Itapevi',
            'Itaquaquecetuba',
            'Jandira',
            'Juquitiba',
            'Mairiporã',
            'Mauá',
            'Mogi das Cruzes',
            'Osasco',
            'Pirapora do Bom Jesus',
            'Poá',
            'Ribeirão Pires',
            'Rio Grande da Serra',
            'Salesópolis',
            'Santa Isabel',
            'Santana de Parnaíba',
            'Santo André',
            'São Bernardo do Campo',
            'São Caetano do Sul',
            'São Lourenço da Serra',
            'São Paulo',
            'Suzano',
            'Taboão da Serra',
            'Vargem Grande Paulista'
        ];

        if ($state != 'SP') {
            return false;
        }

        if (!in_array($city, $array)) {
            return false;
        }

        return true;
    }

    private function validaEmail($mail){
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }
}
