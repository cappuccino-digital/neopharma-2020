<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\NeoPharmaService;
use App\Entity\Course;
use App\Entity\ReferrerContent;


/**
 * @Route("/admin/curso")
 */
class AdminCourseController extends Controller
{
    /**
     * @Route("", name="admin_course_list")
     */
    public function adminCourseList(Request $request)
    {

        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Course')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $courses = $builder->getQuery()->getResult();

        $courses = $this->get('knp_paginator')->paginate(
            $courses,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('admin_course/list.html.twig', [
            'courses' => $courses
        ]);
    }

    /**
     * @Route("/novo", name="admin_course_new")
     */
    public function adminCourseNew(Request $request)
    {

        if ($request->isMethod('GET')) {
            return $this->render('admin_course/new.html.twig', []);
        }

        $files = $request->files->get('course');
        $courseInfo = $request->request->get('course');


        $course = new Course();
        $course->setName($courseInfo['name']);
        $course->setShortDescription($courseInfo['shortDescription']);
        $course->setDescription(html_entity_decode($courseInfo['description'], ENT_COMPAT, 'UTF-8'));
        $course->setCourseId($courseInfo['courseId']);
        $course->setCodInteraction($courseInfo['cod_interaction']);
        $course->setCourseLink($courseInfo['courseLink']);
        $course->setVideo($courseInfo['video']);
        $course->setClasses($courseInfo['classes']);
        $course->setCreatedAt(new \DateTime());
        $course->setUpdatedAt(new \DateTime());
        $course->setIsOnlyToPharmaceutical(isset($courseInfo['isOnlyToPharmaceutical']) ? $courseInfo['isOnlyToPharmaceutical'] : false);
        $course->setIsOnlyToClerk(isset($courseInfo['isOnlyToClerk']) ? $courseInfo['isOnlyToClerk'] : false);
        $course->setIsActive(isset($courseInfo['isActive']) ? $courseInfo['isActive'] : false);
        $course->setIsPublic(isset($courseInfo['isPublic']) ? $courseInfo['isPublic'] : false);
        $course->setIsPharmacy(isset($courseInfo['isPharmacy']) ? $courseInfo['isPharmacy'] : false);

        $courseTimeArray = explode(':', $courseInfo['courseTime']);
        $courseTime = (isset($courseTimeArray[0]) ? $courseTimeArray[0] * 3600 : 0) +
            (isset($courseTimeArray[1]) ? $courseTimeArray[1] * 60 : 0) +
            (isset($courseTimeArray[2]) ? $courseTimeArray[2] : 0);

        $course->setCourseTime($courseTime);

        $course->setMetaTitle($courseInfo['metaTitle']);
        $course->setMetaDescription($courseInfo['metaDescription']);
        $course->setImageMetaTitle($courseInfo['imageMetaTitle']);
        $course->setImageMetaDescription($courseInfo['imageMetaDescription']);
        $course->setImageMobileMetaTitle($courseInfo['imageMobileMetaTitle']);
        $course->setImageMobileMetaDescription($courseInfo['imageMobileMetaDescription']);

        $em = $this->getDoctrine()->getManager();

        if (isset($files['image'])) {
            if (filesize(filesize($files['image'])) != false) {
                if (filesize($files['image']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_course_new');
                }
                $course->setImage($this->saveFile($files['image'], $course, 'course'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                return $this->redirectToRoute('admin_course_new');
            }
        } else {
            $course->setImage('_dist/image/default_desktop.png');
        }

        if (isset($files['imageMobile'])) {
            if (filesize(filesize($files['imageMobile'])) != false) {

                if (filesize($files['imageMobile']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_course_new');
                }
                $course->setImageMobile($this->saveFile($files['imageMobile'], $course, 'course'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                return $this->redirectToRoute('admin_course_new');
            }
        } else {
            $course->setImageMobile('_dist/image/default_mobile.png');
        }
        $em->persist($course);
        $em->flush();

        $productReferred = $request->request->get('referrer');
        if (is_array($productReferred)) $this->saveReferredContent($course, $productReferred);

        return $this->redirectToRoute('admin_course_list');
    }

    /**
     * @Route("/view/{id}", name="admin_course_view")
     */
    public function adminCourseView($id)
    {
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('App:Course')->find($id);
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContent($course->getId(), 'curso');
        return $this->render('admin_course/view.html.twig', [
            'course' => $course,
            'contentReferrer' => $contentReferrer
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_course_edit")
     */
    public function adminCourseEdit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('App:Course')->find($id);
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContent($course->getId(), 'curso');
        if ($request->isMethod('GET')) {
            return $this->render('admin_course/edit.html.twig', [
                'course' => $course,
                'contentReferrer' => $contentReferrer
            ]);
        }

        $files = $request->files->get('course');
        $courseInfo = $request->request->get('course');

        $course->setName($courseInfo['name']);

        $course->setShortDescription($courseInfo['shortDescription']);
        $course->setDescription(html_entity_decode($courseInfo['description'], ENT_COMPAT, 'UTF-8'));

        $courseTimeArray = explode(':', $courseInfo['courseTime']);
        $courseTime = (isset($courseTimeArray[0]) ? $courseTimeArray[0] * 3600 : 0) +
            (isset($courseTimeArray[1]) ? $courseTimeArray[1] * 60 : 0) +
            (isset($courseTimeArray[2]) ? $courseTimeArray[2] : 0);

        $course->setCourseTime($courseTime);

        $course->setCourseId($courseInfo['courseId']);
        $course->setCodInteraction($courseInfo['cod_interaction']);
        $course->setCourseLink($courseInfo['courseLink']);
        $course->setVideo($courseInfo['video']);
        $course->setClasses($courseInfo['classes']);
        $course->setUpdatedAt(new \DateTime());
        $course->setIsOnlyToPharmaceutical(isset($courseInfo['isOnlyToPharmaceutical']) ? $courseInfo['isOnlyToPharmaceutical'] : false);
        $course->setIsOnlyToClerk(isset($courseInfo['isOnlyToClerk']) ? $courseInfo['isOnlyToClerk'] : false);
        $course->setIsActive(isset($courseInfo['isActive']) ? $courseInfo['isActive'] : false);
        $course->setIsPublic(isset($courseInfo['isPublic']) ? $courseInfo['isPublic'] : false);
        $course->setIsPharmacy(isset($courseInfo['isPharmacy']) ? $courseInfo['isPharmacy'] : false);

        $course->setMetaTitle($courseInfo['metaTitle']);
        $course->setMetaDescription($courseInfo['metaDescription']);
        $course->setImageMetaTitle($courseInfo['imageMetaTitle']);
        $course->setImageMetaDescription($courseInfo['imageMetaDescription']);
        $course->setImageMobileMetaTitle($courseInfo['imageMobileMetaTitle']);
        $course->setImageMobileMetaDescription($courseInfo['imageMobileMetaDescription']);

        if (isset($files['image'])) {
            if ($course->getImage() != null && $course->getImage() != '') {
                $this->removeFile($course->getImage());
            }

            $course->setImage($this->saveFile($files['image'], $course, 'course'));
        }

        if (isset($files['imageMobile'])) {
            if ($course->getImageMobile() != null && $course->getImageMobile() != '') {
                $this->removeFile($course->getImageMobile());
            }

            $course->setImageMobile($this->saveFile($files['imageMobile'], $course, 'course'));
        }

        $em->persist($course);
        $em->flush();

        $productReferred = $request->request->get('referrer');
        if (is_array($productReferred)) $this->saveReferredContent($course, $productReferred);

        return $this->redirectToRoute('admin_course_list');
    }

    private function saveReferredContent($curso, $productReferred)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($productReferred as $referred) {
            $r = new ReferrerContent();
            $r->setMainContentId($curso->getId());
            $r->setMainContentType('curso');
            $r->setReferrerContentId((int)$referred['contentId']);
            $r->setReferrerContentType($referred['type']);
            $r->setCreatedAt(new \DateTime());
            $r->setUpdatedAt(new \DateTime());

            $em->persist($r);

            $rBack = new ReferrerContent();
            $rBack->setReferrerContentId($curso->getId());
            $rBack->setReferrerContentType('curso');
            $rBack->setMainContentId((int)$referred['contentId']);
            $rBack->setMainContentType($referred['type']);
            $rBack->setCreatedAt(new \DateTime());
            $rBack->setUpdatedAt(new \DateTime());

            $em->persist($rBack);
            $em->flush();
        }
    }

    private function saveFile($file, $post, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $post->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }




    /**
     * @Route("/racine/atualizar", name="admin_course_update_racine")
     */
    public function updateCourseRacine()
    {
        $apiNeoPharma     = new NeoPharmaService(
            $this->getParameter('ws_racine_url'),
            $this->getParameter('ws_racine_token')
        );

        $listCourseRacine = $apiNeoPharma->getListCourse();

        $em = $this->getDoctrine()->getManager();

        if (count($listCourseRacine) > 0) {

            foreach ($listCourseRacine as $key => $racineCourso) :
                $racine_id = $racineCourso->id != 1 ? $racineCourso->id : null;

                if (!empty($racine_id)) {
                    $course    = $em->getRepository('App:Course')->findOneBy(['courseId' => $racine_id]);

                    if (empty($course)) { //new curso
                        $new_course = new Course();
                        $new_course->setCourseId($racineCourso->id);
                        $new_course->setName($racineCourso->fullname);
                        $new_course->setShortDescription($racineCourso->displayname);
                        $new_course->setDescription($racineCourso->summary);
                        $new_course->setIsActive($racineCourso->visible == 1 ? true : false);
                        $new_course->setCreatedAt(new \DateTime());
                        $new_course->setUpdatedAt(new \DateTime());

                        $em->persist($new_course);
                        $em->flush();
                    } else { //update curso
                        $course->setName($racineCourso->fullname);
                        $course->setUpdatedAt(new \DateTime());
                        $course->setShortDescription($racineCourso->displayname);
                        $course->setDescription($racineCourso->summary);
                        $course->setIsActive($racineCourso->visible == 1 ? true : false);
                        $em->persist($course);
                        $em->flush();
                    }
                }

            endforeach;

            $this->addFlash("success", "Lista atualizada com os curso do Racine");
        } else {

            $this->addFlash("error", "Erro na atualização dos cursos do Racine");
        }

        return $this->redirectToRoute('admin_course_list');
    }
}
