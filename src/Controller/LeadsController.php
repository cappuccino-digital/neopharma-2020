<?php

namespace App\Controller;

use App\Entity\LeadEmblue;
use App\Entity\Leads;
use App\Service\EmBlueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class LeadsController extends Controller
{
    /**
     * @Route("/leads", name="lead-save")
     */
    public function save(Request $request)
    {
        $error = [];
        $em = $this->getDoctrine()->getManager();
        $leads = $em->getRepository('App:Leads')->findOneBy(['email' => $request->request->get('email')]);
       
        if (!$leads) {
            $leads = New Leads();
        }

        if (!trim($request->request->get('occupation'))) {
            $error['profile'] = 'O perfil é obrigatório.';
        }

        $p = '';
        if (($request->request->get('occupation') == 'balconista') || ($request->request->get('occupation') == 'farmaceutico')) {
            $p = 'bf';
            if (!trim($request->request->get('name'))) {
                $error['name'] = 'O nome é obrigatório.';
            }

            if (trim($request->request->get('email'))) {
                if (!$this->validEmail($request->request->get('email'))) {
                    $error['email'] = 'E-mail inválido.';
                }     
            } else {
                $error['email'] = 'O e-mail é obrigatório.';
            }
        } else if ($request->request->get('occupation') == 'gestor-de-loja') {
            $p = 'g';
            if (!trim($request->request->get('cnpj'))) {
                $error['cnpj'] = 'CNPJ é obrigatório.';
            }

            if (!trim($request->request->get('cpf'))) {
                $error['cpf'] = 'CPF é obrigatório.';
            }
        }

        if (count($error) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'error' => $error,
            ]);
        }

        if ($request->request->get('occupation') == 'balconista') {
            $role = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
        } else if ($request->request->get('occupation') == 'farmaceutico') {
            $role = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
        } else if ($request->request->get('occupation') == 'gestor-de-loja') { 
            $role = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACIA');
        }

        $leads->setIsFacebook(false);
        $leads->setIsGoogle(false);
        $leads->setName($request->request->get('name'));
        $leads->setEmail($request->request->get('email'));
        $leads->setIsNewsletter($request->request->has('newsletter') ? true : false);
        $leads->setRole($role);
        $leads->setCreatedAt(new \DateTime());

        $em->persist($leads);
        $em->flush();

        if ($request->request->has('newsletter')) { 
            /** API emBlue E-mail Regua */
            if ($leads->getIsNewsletter()) {
               
                $lead_emBlue = $em->getRepository('App:LeadEmblue')->findOneBy(['lead' => $leads]);
                if (!$lead_emBlue) {
                    $emBlue = new EmBlueService();
                    $contact = $emBlue->eventLeads($leads, false);

                    $lead_emBlue = new LeadEmblue();
                    $lead_emBlue->setCreatedAt(new \DateTime());
                    $lead_emBlue->setLead($leads);
                    $lead_emBlue->setEmail($leads->getEmail());
                    if ($contact['status']) {                
                        $lead_emBlue->setEvent($contact['result']);
                        $lead_emBlue->setMessage($contact['message']);
                    } else {
                        $lead_emBlue->setMessage($contact['message']);  
                    }
                    $lead_emBlue->setUpdatedAt(new \DateTime());
    
                    $em->persist($lead_emBlue);
                    $em->flush();
                }
            }  
            /** END */
        }

        $this->get('session')->set('lead_id', $leads->getId());
        $this->get('session')->set('p', $request->request->get('occupation'));
        
        if ($p == 'bf') { //Balconista e farmacêutico
            $this->get('session')->set('n', $leads->getName());
            $this->get('session')->set('m', $leads->getEmail());
        } else if ($p == 'g') { //Gestor de loja
            $this->get('session')->set('cf', $request->request->get('cpf'));
            $this->get('session')->set('cj', $request->request->get('cnpj'));
        }
            
        return new JsonResponse([
            'status' => true,
            'message' => 'Cadastro realizado com sucesso.'
        ]);
    }

    public function validEmail($email)
    {
        $Syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if (preg_match($Syntaxe, $email))
            return true;
        else
            return false;
    }
}
