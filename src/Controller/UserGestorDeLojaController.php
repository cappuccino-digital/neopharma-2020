<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\UserRegisterIncomplete;
use App\Entity\UserInvite;
use App\Entity\User;
use App\Entity\Contact;
use App\Entity\UserNewPassword;
use App\Entity\UserChangeProfile;
use App\Entity\UserCongressPharmaceutical;
use App\Entity\UserUsingSameCpf;
use App\Entity\UserStoreManagerResponsibleChange;
use App\Service\ExcelenciaSonoraService;
use Symfony\Component\HttpFoundation\Cookie;

use App\Service\EmBlueService;
use App\Entity\UserEmblue;

use DateTime;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;
class UserGestorDeLojaController extends Controller
{
    /**
     * @Route("/login/check-user/excelencia", name="login-excelencia")
     */
    public function loginExcelencia(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        if ($request->isMethod('GET')) {
            return $this->render('home/login.html.twig', [
                
            ]);
        }

        $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
        $password = $request->request->get('pass');
        
        if (!$cnpj || !$password) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Todos os campos devem ser preenchidos.', 
            ]);
        }
    
        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'isPharmacy' => true, 'isActive' => true]);

        if (!$user) { // não existe na nossa base
            $user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'isPharmacy' => true, 'isActive' => false, 'is_test' => true]); //Usuário de teste
            if (!$user) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Cadastro no perfil Gestor de Loja não encontrado.', 
                ]);
            }
        }

        $bool = ($encoder->isPasswordValid($user, $password)) ? true : false;
        
        if (!$bool) {
            return new JsonResponse([  //dados incorretos na nossa base 
                'status' => false,
                'message' => 'CNPJ ou Senha incorretos', 
            ]);
        }
      
        //logando o usuário
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setTokenApiExcelencia($this->generateToken());

        $em->persist($user);
        $em->flush();
        //end 

        //register login user
        $user_login = $em->getRepository('App:UserLogin')->saveLastLogin($user);

        $url = $request->getSchemeAndHttpHost();
        
        //SONORA enviar informações 
        $neopharma_excelencia = new ExcelenciaSonoraService(
            $this->getParameter('sonora_excelencia_secret'),
            $this->getParameter('sonora_excelencia_env')
        );
        $response = $neopharma_excelencia->impersonate($user);
        
        if ($response['status']) {
            $url = $response['url']; //redirecionar para o Excelência
        }
        // //end 

        if (!$user->getIsRegulation()) {
            $url = '/regulamento';   
        }

        return new JsonResponse([
            'status' => true,
            'url' => $url
        ]);
    }
    
    /**
     * @Route("/consult-gestor", name="home-consult-gestor")
     */
    public function consultGestor(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        if($request->isMethod('GET')){
            return $this->redirectToRoute('home');
        }
        
        if ($request->request->has('type')) {
            $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
            $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf'));
           
            if (!$cpf || !$cnpj) {
                return new JsonResponse([
                    'status' => false,
                    'message' => '',
                ]);
            }

            if (!$this->validaCNPJ($cnpj)) {
                return new JsonResponse(['status'=> false, 'message' => 'CNPJ inválido.']);
            }

            if (!$this->validaCPF($cpf)) {
                return new JsonResponse(['status'=> false, 'message' => 'CPF inválido.']);
            }
            
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'cpf' => $cpf]);
            $is_check_user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'isPharmacy' => true]);
            $is_check_cpf = $em->getRepository('App:User')->findOneBy(['cpf' => $cpf]);
            $pharmacies_elegible = $em->getRepository('App:PharmaciesElegible')->findOneBy(['cnpj' => $cnpj, 'is_active' => true]);
            //validação para farmacêutico sem CNPJ
            if ($is_check_cpf) {
                if ($is_check_cpf->getIsPharmacy() == true) {
                    //para gestor é permitido reutizar o cpf
                } else { 
                    if (!$user) {
                        return new JsonResponse(['status'=> false, 'message' => 'CPF está sendo usado em outro perfil.']);
                    }
                }
            }
            
            //verificando se a farmácia é elegivel
            if (!$pharmacies_elegible) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'CNPJ não faz parte do Neopharma Excelência.'
                ]);
            } 

            //CNPJ já cadastrado
            if ($is_check_user) {
                if ($is_check_user->getIsPharmacy() == true) {  
                    return new JsonResponse(['status'=> false, 'message' => 'CNPJ já cadastrado, vá para a tela de login.']);
                }
            }
            
            //CNPJ já cadastrado
            if ($user) {
                if ($user->getIsPharmacy() == true) {  
                    return new JsonResponse(['status'=> false, 'message' => 'CNPJ já cadastrado, vá para a tela de login.']);
                }
            } 

            //código faz parte da regra para permitir múltiplos CPF no mesmo CNPJ(valida apenas para Gestor de Loja)
            $code_multi_cpf = 'GL-002'; 
            if ($request->request->get('multi_cpf') != $code_multi_cpf) {
                if ($is_check_cpf) {
                    if ($is_check_cpf->getIsPharmacy() == true) {
                        return new JsonResponse(['status'=> true, 'message' => 'CPF já utilizado no cadastro de outra loja. Confirma a utilização do mesmo CPF para outra loja?', 'multi_cpf' => true, 'code' => $code_multi_cpf]);
                    }
                }
            }

            /**código faz parte da troca de perfil para gestor de loja */
            if ($user) {
                $user_change_profile = $em->getRepository('App:UserChangeProfile')->findOneBy(['user' => $user->getId()]);
            
                if ($user_change_profile) {
                    if ($user_change_profile->getIsChangeProfile()) {
                        return new JsonResponse(['status'=> false, 'message' => 'CNPJ já cadastrado, vá para a tela de login.']);
                    } else {
                        return new JsonResponse(['status'=> false, 'message' => 'Acesse o seu e-mail, está pendente a confirmação da migração do cadastro.']);
                    }
                } 
            }
            
            $code = 'GL-001'; //código de troca de perfil
            if ($request->request->get('change_profile') != $code) {
                if ($user) { 
                    return new JsonResponse(['status'=> true, 'message' => 'Seu CPF já consta em nosso banco de dados. Você deseja, mesmo assim, migrar o seu perfil para Gestor de Loja?', 'change_profile' => true, 'code' => $code]);
                }
            }
            /** end */

            if (
                ($pharmacies_elegible && !$user) || //condição 01: se o cara for elegivel e não tem cadastro na nossa base
                ($pharmacies_elegible && $request->request->get('change_profile') == $code) //condição 1.1: se o cara for elegivel, mais quer mudar de perfil para gestor de loja
            ){  
                
                $pharmacy_info = [
                    'status' => true,
                    'message' => 'CNPJ elegível.',
                    'box' => 'register',
                    'elegivel' => true,
                    'razao' => $pharmacies_elegible->getCompanyName(),
                    'uf' => $pharmacies_elegible->getUf(),
                    'cnpj' => $cnpj,
                    'cpf' => $cpf,
                    'email' => '',
                ];
                          
                //código faz parte da troca de perfil para gestor de loja
                if ($user) { 
                    $pharmacy_info['nome'] = $user->getName(); 
                    $pharmacy_info['email'] = $user->getEmail(); 
                    $pharmacy_info['change_profile'] = true; 
                }
                
                //código faz parte da regra de usar o mesmo CPF para múltiplos CNPJs
                if ($is_check_cpf) {
                    if ($is_check_cpf->getIsPharmacy() == true) {
                        $pharmacy_info['email'] = $is_check_cpf->getEmail(); 
                        $pharmacy_info['multi_cpf'] = true;
                    }
                }
                         
                return new JsonResponse($pharmacy_info);
                
            } else if ($pharmacies_elegible && $user) { //condição 02: se o cara for elegivel e tem cadastro na nossa base
                
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Usuário cadastrado em Neopharma.',
                    'elegivel' => true,
                    'box' => 'logged',
                    'url' => "{$request->getSchemeAndHttpHost()}/authentication/{$user->getTokenApiExcelencia()}",
                ]);
            } 

        }

        throw $this->createNotFoundException('A página não foi encontrada');
    }

    /**
     * @Route("/cadastro/gestor", name="register-gestor")
     */
    public function registerGestor(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $error = array();
       
        $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
        $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf'));
        
        
        
        $user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'cpf' => $cpf, 'isPharmacy' => true]);


        if (!$user) {
            $user = new User(); // cadastro 
            $user->setCreatedAt(new \DateTime());
        }

        /** troca de perfil para gestor */
        if ($request->request->get('change_profile') == 'GL-001') {
            $user = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'cpf' => $cpf]);
            $user = new UserChangeProfile();
        }
       
        /** end */
    
        if ($request->request->has('name') && trim($request->request->get('name'))) {
            $user->setName($request->request->get('name'));
        } else {
            $error['name'] = 'O nome é obrigatório.';
        }

        if ($request->request->has('mail') && trim($request->request->get('mail'))) {
            if ($this->validEmail($request->request->get('mail'))) {
                $user->setEmail($request->request->get('mail'));
            } else {
                $error['mail'] = 'E-mail inválido.';
            }   

            //verificando se o e-mail está sendo usado
            if (!$request->request->get('multi_cpf')) { 
                $is_exist_email = $em->getRepository('App:User')->findOneByEmail($request->request->get('mail'));

                if ($is_exist_email) {
                    if (!$request->request->get('change_profile') == 'GL-001') {
                        $error['mail'] = 'Email está sendo utilizado';
                    }
                } 
            } 
        } else {
            $error['mail'] = 'O e-mail é obrigatório.';
        }
        
        if ($cpf && trim($cpf)) {
            $user->setCpf($cpf);
        } else {
            $error['cpf'] = 'O CPF é obrigatório.';
        }

        if ($cnpj && trim($cnpj)) {
            $user->setCnpj($cnpj);
        } else {
            $error['cnpj'] = 'O CNPJ é obrigatório.';
        }

        if (empty($request->request->get('p'))) {
            $error['password'] = 'A senha é obrigatória.';
        }

        if (empty($request->request->get('pr'))) {
            $error['passwordRepeat'] = 'O campo repetir senha é obrigatório.';
        }

        if ($request->request->get('p') == $request->request->get('pr')) {
            $user->setSalt(md5(uniqid())); /* parte 2 execelência */
            $user->setPassword($encoder->encodePassword($user, $request->request->get('p')));
        } else {
            $error['password'] = 'O campo senha e repetir senha estão diferentes.';
        }

        /* news fields */
        if ($request->request->has('razao_social') && trim($request->request->get('razao_social'))) {
            $razao_social = str_replace('&', '&amp;', $request->request->get('razao_social'));
            $user->setCompanyName($razao_social);
        } else {
            $error['razao_social'] = 'A razão social é obrigatória.';
        }
        
        // if ($request->request->has('inscricao_estadual') && trim($request->request->get('inscricao_estadual'))) {
        //     $user->setStateRegistration($request->request->get('inscricao_estadual'));
        // } else {
        //     $error['inscricao_estadual'] = 'A inscrição estadual é obrigatório.';
        // }
        
        if ($request->request->has('name_fantasia') && trim($request->request->get('name_fantasia'))) {
            $name_fantasia = str_replace('&', '&amp;', $request->request->get('name_fantasia'));
            $user->setFantasyName($name_fantasia);
        } else {
            $error['name_fantasia'] = 'O nome fantasia é obrigatória.';
        }

        if ($request->request->has('cep') && trim($request->request->get('cep'))) {
            $cep = str_replace([' ', '-'], '', $request->request->get('cep'));
            $user->setZipCode($cep);
        } else {
            $error['cep'] = 'O CEP é obrigatório.';
        }
        
        if ($request->request->has('endereco') && trim($request->request->get('endereco'))) {
            $user->setAddress($request->request->get('endereco'));
        } else {
            $error['endereco'] = 'O endereço é obrigatório.';
        }
        
        if ($request->request->has('numero') && trim($request->request->get('numero'))) {
            $user->setNumber($request->request->get('numero'));
        } else {
            $error['numero'] = 'O número é obrigatório.';
        }
        
        if ($request->request->has('complemento') && trim($request->request->get('complemento'))) {
            $user->setComplement($request->request->get('complemento'));
        } else {
            //$error['complemento'] = 'O complemento é obrigatório.';
        }
        
        if ($request->request->has('bairro') && trim($request->request->get('bairro'))) {
            $user->setNeighborhood($request->request->get('bairro'));
        } else {
            $error['bairro'] = 'O bairro é obrigatório.';
        }
        
        if ($request->request->has('cidade') && trim($request->request->get('cidade'))) {
            $user->setCity($request->request->get('cidade'));
        } else {
            $error['cidade'] = 'A cidade é obrigatória.';
        }
        
        if ($request->request->has('uf') && trim($request->request->get('uf'))) {
            $user->setState($request->request->get('uf'));
        } else {
            $error['uf'] = 'O estado é obrigatório.';
        }

        if ($request->request->has('ddd_celular') && trim($request->request->get('ddd_celular'))) {
            if (strlen($request->request->get('ddd_celular')) < 2) {
                $error['ddd_celular'] = 'DDD inválido.';
            } else {
                $user->setDddCellphone($request->request->get('ddd_celular'));
            }
        } else {
            $error['ddd_celular'] = 'O DDD é obrigatório.';
        }
        
        if ($request->request->has('celular') && trim($request->request->get('celular'))) {
            if (strlen($request->request->get('celular')) < 10) {
                $error['celular'] = 'Número de celular inválido';
            } else {
                $user->setCellphone($request->request->get('celular'));
            }
        } else {
            $error['celular'] = 'O celular é obrigatório.';
        }
        
        if ($request->request->has('ponto_referencia') && trim($request->request->get('ponto_referencia'))) {
            $user->setReferencePoint($request->request->get('ponto_referencia'));
        } else {
            $error['ponto_referencia'] = 'O ponto de referência é obrigatório.';
        }   

        if ($request->request->has('whatsApp')) {
            $user->setIsNotificationWhatsapp($request->request->get('whatsApp'));
        }

        /* end */
        if ($request->request->get('term') == 0) {
            $error['term'] = 'A leitura do termo de uso é obrigatório.';
        }       
        
        if ($request->request->get('regulation') == 0) {
            $error['regulation'] = 'A leitura do regulamento é obrigatório.';
        }     

        if($request->files->has('avatar')){
            $fileName =  'user-'.date('i:s').'.jpg';
            $folder = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user/';
            $filePath = $folder . $fileName;
           
            if(!is_dir($folder)) {
                mkdir($folder, 0777, true);
            }

            copy($request->files->get('avatar')->getRealPath(), $filePath);
            $user->setAvatar('uploads/user/' . $fileName);
        }

        /*** campos novos sonora */
        $birthday = $request->request->get('birthday');
        // $gender = $request->request->get('gender');

        // if ($gender) {
        //     $user->setGender($gender);
        // }
                                                 
        if ($birthday != '') {
            $d = explode('/', $birthday);
            if(isset($d[0]) && isset($d[1]) && isset($d[2])){
                $birthday = "{$d[2]}-{$d[1]}-{$d[0]}";

                $user->setBirthday(new \DateTime($birthday));
            }
        } else {
            $error['birthday'] = 'A Data de nascimento é obrigatório.';
        }

        $user->setNewsletter($request->request->has('newsletter') ? $request->request->get('newsletter') : false);
        /*** end  */

        $team_football = trim($request->request->get('team_football'));
        $user->setTeamFootball($team_football);
       
        if (count($error) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'fields' => $error,
            ]);
        }

        /** troca de perfil para gestor */ 
        if ($request->request->get('change_profile') == 'GL-001') {
           $expiresAt = new \DateTime('+1 month');
           //$expiresAt->add(new \DateInterval('PT8H'));
        
           $user_chage_profile = $em->getRepository('App:User')->findOneBy(['cnpj' => $cnpj, 'cpf' => $cpf]);
           $user->setUser($user_chage_profile);
           $user->setIsSendEmail(false);
           $user->setIsChangeProfile(false);
           $user->setCreatedAt(new \DateTime());
           $user->setUpdatedAt(new \DateTime());
           $user->setTokenEmail($this->generateToken());          
           $user->setExpiresAt($expiresAt);
           $user->setIsValid(true);
        
            /** send e-mail */
            $logger = new Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
    
            if($this->getParameter('APP_ENV') == 'dev'){
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
            } else {
                $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
            }

            $url = $this->generateUrl('change-profile', array('token' => $user->getTokenEmail()), UrlGenerator::ABSOLUTE_URL);
           
            $message->setTo($user->getEmail())
            ->setSubject('Confirmação de migração de cadastro.')
            ->setBody(
                $this->renderView(
                    'email_mkt/change_profile.html.twig',
                    ['user' => $user, 'url' => $url]
                ),
                'text/html'
            );
    
            $response = $mailer->send($message);
    
            if($response == 1){
                $user->setIsSendEmail(true);
            } else {
                $user->setIsSendEmail(false);
            }
            /** end */

           $em->persist($user);
           $em->flush();

           return new JsonResponse([
                'status' => true,
                'message' => 'Confirme a troca do perfil no seu e-mail',
                'action' => 'change_profile',
                'url' => "{$request->getSchemeAndHttpHost()}/change/profile/{$user->getTokenEmail()}"
            ]);
        } 
        /** end */ 
        
        $role = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACIA');
        $user->addRole($role);
        $user->setIsPharmacy(true);
        $user->setIsRegulation(true); //regulamento
        $user->setCodExcelencia(0);     
        $user->setIsRegisterCompleteExcelencia(true);
        $user->setIsActive(true); 
        $user->setIsPharmaceutical(false);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());
        $user->setTokenApiExcelencia($this->generateToken()); //token gerado para API de excelencia       
                
        $em->persist($user);
        $em->flush();

        $user->setShareCode($this->generateShareCode($user)); /* parte 2 execelência */
        $em->persist($user);
        $em->flush();

        /** código faz parte da regra de múltiplos CPF para o mesmo CNPJ */ 
        if ($request->request->get('multi_cpf') == 'GL-002') {
            $user_using_same_cpfs = $em->getRepository('App:User')->findBy(['cpf' => $cpf, 'isPharmacy' => true]);

            if ($user_using_same_cpfs) {
                foreach ($user_using_same_cpfs as $u) {
                    $user_using_same = $em->getRepository('App:UserUsingSameCpf')->findOneBy(['cnpj' => $u->getCnpj(), 'cpf' => $u->getCpf()]);

                    if ($user_using_same) {
                        $user_using_same->setUpdatedAt(new \DateTime());
                    } else {
                        $user_using_same = new UserUsingSameCpf();

                        $user_using_same->setUser($u);
                        $user_using_same->setName($u->getName());
                        $user_using_same->setCnpj($u->getCnpj());
                        $user_using_same->setCpf($u->getCpf());
                        $user_using_same->setEmail($u->getEmail());
                        $user_using_same->setCreatedAt(new \DateTime());
                        $user_using_same->setUpdatedAt(new \DateTime());
                    }

                    $em->persist($user_using_same);
                    $em->flush();
                }
            }

        }

        /** logando o usuário  */
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
       
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setTokenApiExcelencia($this->generateToken());

        $em->persist($user);
        $em->flush();
        /** end */

        $url = $request->getSchemeAndHttpHost();

        //SONORA enviar informações 
        $neopharma_excelencia = new ExcelenciaSonoraService(
            $this->getParameter('sonora_excelencia_secret'),
            $this->getParameter('sonora_excelencia_env')
        );
        $response = $neopharma_excelencia->impersonate($user);

        if ($response['status']) {
            $url = $response['url']; //redirecionar para o Excelência
        }
        //end

        /** API emBlue */
        $emBlue = new EmBlueService();
        $leads  = $em->getRepository('App:Leads')->findOneBy(['email' => $user->getEmail()]);

        //removendo o contato do usuário da base
        if ($leads) {
            $lead_emblue = $em->getRepository('App:LeadEmblue')->findOneBy(['lead' => $leads]);

            if ($lead_emblue) {
                $em->remove($lead_emblue);
                $em->flush();
            }

            //avisando o emblue que o usuário deixou de ser um lead
            $contact = $emBlue->eventLeads($leads, true);

            //removendo o lead
            $em->remove($leads);
            $em->flush();
        }

        /** E-mail Boas Vindas*/
        if ($user->getNewsletter()) {
            //enviando o e-mail do usuário para emblue para disparar o e-mail de boas vinda
            $contact = $emBlue->eventWelcome($user);

            $user_emBlue = $em->getRepository('App:UserEmblue')->findOneBy(['user' => $user]);
            if (!$user_emBlue) {
                $user_emBlue = new UserEmblue();
                $user_emBlue->setCreatedAt(new \DateTime());
            }
            
            $user_emBlue->setUser($user);
            $user_emBlue->setEmail($user->getEmail());
            if ($contact['status']) {                
                $user_emBlue->setEvent($contact['result']);
                $user_emBlue->setMessage($contact['message']);
            } else {
                $user_emBlue->setMessage($contact['message']);  
            }
            $user_emBlue->setUpdatedAt(new \DateTime());

            $em->persist($user_emBlue);
            $em->flush();
        }  
        /** END */

        $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'cadastrar-se-na-plataforma', 'cadastro', null); //Gamificação interação
        $em->getRepository('App:UserPointGamification')->checkRegisterComplete($user); //Gamificação interação
        
        //Redirecionamento para a referência após o login
        $url_reference = $request->cookies->get('_referenceURL') ? $request->cookies->get('_referenceURL') : $this->redirectToRoute('home');
        
        $response = new JsonResponse([
            'status' => true,
            'message' => 'Cadastro realizado com sucesso.',
            'url' => $url_reference,
        ]);

        $response->headers->setCookie(new Cookie('_referenceURL', false));
        /** END */

        //cadastro que vieram da página do congresso  
        if ($request->cookies->get('_congressocrf') == true) {
            $response->headers->setCookie(new Cookie('_congressocrf', false));

            //verificando se o usuário já possui cadastro
            $user_congress_pharmaceutical = $em->getRepository('App:UserCongressPharmaceutical')->findOneBy(['user' => $user]);

            if (!$user_congress_pharmaceutical) {
                //cadastro no congresso
                $user_congress_pharmaceutical = new UserCongressPharmaceutical(); 
                $user_congress_pharmaceutical->setUser($user); 
                $user_congress_pharmaceutical->setIsRegistration(true); 
                $user_congress_pharmaceutical->setCreatedAt(new \DateTime());
                $user_congress_pharmaceutical->setUpdatedAt(new \DateTime());  

                $em->persist($user_congress_pharmaceutical);
                $em->flush();
            }
        }
        
        return $response;
    }

    /**
     * @Route("/authentication/{token}", name="gestor-authentication")
     */
    // public function authenticationToken(Request $request, $token)
    // {
    //     if (empty($token)) {
    //         throw $this->createNotFoundException('token inválido');
    //     }

    //     $em = $this->getDoctrine()->getManager();		
    //     $user = $em->getRepository('App:User')->findOneBy(['token_api_excelencia'=> $token]);

    //     if (empty($user)) {
    //         throw $this->createNotFoundException('token inválido');
    //     }
            
    //     $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
    //     $this->get('security.token_storage')->setToken($token);
    //     $this->get('session')->set('_security_main', serialize($token));
    //     $event = new InteractiveLoginEvent($request, $token);
       
    //     $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

    //     $user->setTokenApiExcelencia($this->generateToken());

    //     $em->persist($user);
    //     $em->flush();

    //     return $this->redirectToRoute('home');
    // }


    /**
     * @Route("/change/profile/{token}", name="load-change-profile-gestor")
     */
    public function saveChangeProvile(Request $request, $token)
    { 
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if(!$token){
            $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
            return $this->redirectToRoute('home');
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:UserChangeProfile')->findOneBy(['token_email' => $token]);
        
        if (!$user) {
            //die('teste 01');
            $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
        }
        
        $this->addFlash("change_profile", "Para concluir a migração de seu cadastro, acesse o e-mail enviado para o endereço utilizado em seu cadastro e clique no link de confirmação."); 
        
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/completar-cadastro/{paramenter}", name="home-register-complete", defaults={"paramenter"=null})
     */
    public function registerComplete(Request $request, $paramenter, UserPasswordEncoderInterface $encoder)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $user = $this->getUser();
        
        if ($user->getIsRegisterComplete()) {
            return $this->redirectToRoute('user-home');
        }

        if($request->isMethod('GET')){
            return $this->render('home/register-complete.html.twig', ['paramenter'=> $paramenter]);
        }
        
        $em = $this->getDoctrine()->getManager();	
        $form = $request->request->get('form');

        //validação 
        $errors = array();

        if (!trim($form['cpf'])) {
            $errors[] = 'O CPF é obrigatório';
        } 

        if($form['type'] == 'farmaceutico'){

            if (isset($form['crf']) && !trim($form['crf'])) {
                $errors[] = 'O CRF é obrigatório';
            } 

            if (!trim($form['state'])) {
                $errors[] = 'O estado é obrigatório';
            } 

        } else if($form['type'] == 'balconista'){
            if (isset($form['cnpj']) && !trim($form['cnpj'])) {
                $errors[] = 'O CNPJ é obrigatório';
            } 
        }
                
        if (count($errors) > 0 ) {
            $stringError = "<ul>";
            foreach ($errors as $key => $error) {
                    $stringError .= "<li>{$error}</li>";
            }
            $stringError .= "</ul>";

            $this->addFlash("error", "{$stringError}");

            if ($paramenter == 'incompleto') { 
                return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
            } else {
                return $this->redirectToRoute('home-register-complete');
            }
        }
         /** end validação */
        /** corrigindo cadastros que estão incompletos */
        $isIncomplete = false;
        if ($paramenter == 'incompleto'){
            $isIncomplete = true;
        } else if ($user->getCreatedAt()->format('Y-m-d') <= date('Y-m-d')) { 
            $isIncomplete = true;
        }

        if ($isIncomplete) { 
            $user_register = $em->getRepository('App:UserRegisterIncomplete')->findOneBy(['user' => $user, 'field' => 'cadastro-rede-social-incompleto']);
        
            if (!$user_register) {
                $user_register =  new UserRegisterIncomplete();
           
                $user_register->setUser($user);
                $user_register->setIsRegisterComplete(true);
                $user_register->setIsGestor($form['type'] == 'gestor' ? true : false);
                $user_register->setIsPharmaceutical($form['type'] == 'farmaceutico' ? true : false);
                $user_register->setIsClerk($form['type'] == 'balconista' ? true : false);
                $user_register->setIsSocialNetwork(true);
                $user_register->setField('cadastro-rede-social-incompleto');
                $user_register->setCreatedAt(new \DateTime());
                $em->persist($user_register);
                $em->flush();

                $this->addFlash("success", "Cadastro completado com sucesso.");
            }
        }
        /** end */

        $user->setIsRegisterComplete(true);
        $user->setCpf(str_replace(['.', ' ', '-'], '', $form['cpf']));
        $user->setNewsletter(isset($form['newsletter']) ? $form['newsletter'] : false);

        
        if($form['type'] == 'balconista'){
            $roleBalc = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
            $user->addRole($roleBalc);
            $user->setIsPharmaceutical(false);
            $user->setCnpj(str_replace(['.', ' ', '-', '/'], '',$form['cnpj']));
        } else if($form['type'] == 'farmaceutico'){
            $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
            $user->addRole($roleFarmac);
            $user->setIsPharmaceutical(true);
            $user->setCrf(str_replace(['.', ' ', '-', '/'], '',$form['crf']));
            $user->setState($form['state']);
        }

        //removendo o role de cadastro incompleto
        $roleUser = $em->getRepository('App:Role')->findOneByRole('ROLE_USER');
        $user->removeRole($roleUser);
               
        $user->setUpdatedAt(new \DateTime());
        $user->setShareCode($this->generateShareCode($user));
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if(isset($form['friendIndication']) && $form['friendIndication'] != ''){
            $invite = new UserInvite();
            $invite->setUser($user);
            $invite->setShareCode($form['friendIndication']);
            $em->persist($invite);
            $em->flush();

            $user->setInvite($invite);
            $em->persist($invite);
            $em->flush();

            $userPointer = $em->getRepository('App:User')->findOneByShareCode($form['friendIndication']);
            $em->getRepository('App:UserPoint')->setPointsToBadge($userPointer, $user, 'quem-indica', 'quem-indica');
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/regulamento", name="gestor-regulation")
     */
    public function regulation(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
      
        $user = $this->getUser();
        if ($user) {
            if ($request->request->has('e')) {
                if ($request->request->get('r') == 1) {
                    $em = $this->getDoctrine()->getManager();
                    $user->setIsRegulation(true);
                    $user->setLastLoginAt(new \DateTime());

                    $em->persist($user);
                    $em->flush();  
                    
                    $url = $request->getSchemeAndHttpHost();
        
                    //SONORA enviar informações 
                    $neopharma_excelencia = new ExcelenciaSonoraService(
                        $this->getParameter('sonora_excelencia_secret'),
                        $this->getParameter('sonora_excelencia_env')
                    );
                    $response = $neopharma_excelencia->impersonate($user);
            
                    if ($response['status']) {
                        $url = $response['url']; //redirecionar para o Excelência
                    }
                    //end 

                    return $this->redirect($url);
                }
            }
        }

        return $this->render('home/regulation.html.twig', [

        ]);
    }

    /**
     * @Route("/editar/gestor", name="editar-gestor")
     */
    public function editarGestor(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
       
        $error = array();
        $avatar = $request->files->get('avatar');

        $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
        $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf'));
        
        $user = $this->getUser(); //editar
       
        if ($request->request->has('name') && trim($request->request->get('name'))) {
            $user->setName($request->request->get('name'));
        } else {
            $error['name'] = 'O nome é obrigatório.';
        }

        if ($request->request->has('mail') && trim($request->request->get('mail'))) {
            if ($this->validEmail($request->request->get('mail'))) {
                $user->setEmail($request->request->get('mail'));
            } else {
                $error['mail'] = 'E-mail inválido.';
            }   
        } else {
            $error['mail'] = 'O e-mail é obrigatório.';
        }

        if ($cpf && trim($cpf)) {
            $user->setCpf($cpf);
        } else {
            $error['cpf'] = 'O CPF é obrigatório.';
        }

        if ($cnpj && trim($cnpj)) {
            $user->setCnpj($cnpj);
        } else {
            $error['cnpj'] = 'O CNPJ é obrigatório.';
        }

        if (trim($request->request->get('p'))) {
            if ($request->request->get('p') == $request->request->get('pr')) {
                $user->setSalt(md5(uniqid())); /* parte 2 execelência */
                $user->setPassword($encoder->encodePassword($user, $request->request->get('p')));
            } else {
                $error['password'] = 'O campo trocar senha e repetir senha estão diferentes.';
            }
        }else{
            $user->setPassword($user->getPassword());
        }

        /* news fields */
        if ($request->request->has('razao_social') && trim($request->request->get('razao_social'))) {
            $razao_social = str_replace('&', '&amp;', $request->request->get('razao_social'));
            $user->setCompanyName($razao_social);
        } else {
            $error['razao_social'] = 'A razão social é obrigatória.';
        }
        
        // if ($request->request->has('inscricao_estadual') && trim($request->request->get('inscricao_estadual'))) {
        //     $user->setStateRegistration($request->request->get('inscricao_estadual'));
        // } else {
        //     $error['inscricao_estadual'] = 'A inscrição estadual é obrigatório.';
        // }
        
        if ($request->request->has('name_fantasia') && trim($request->request->get('name_fantasia'))) {
            $name_fantasia = str_replace('&', '&amp;', $request->request->get('name_fantasia'));
            $user->setFantasyName($name_fantasia);
        } else {
            $error['name_fantasia'] = 'O nome fantasia é obrigatório.';
        }

        if ($request->request->has('cep') && trim($request->request->get('cep'))) {
            $cep = str_replace([' ', '-'], '', $request->request->get('cep'));
            $user->setZipCode($cep);
        } else {
            $error['cep'] = 'O CEP é obrigatório.';
        }
                
        if ($request->request->has('endereco') && trim($request->request->get('endereco'))) {
            $user->setAddress($request->request->get('endereco'));
        } else {
            $error['endereco'] = 'O endereço é obrigatório.';
        }
        
        if ($request->request->has('numero') && trim($request->request->get('numero'))) {
            $user->setNumber($request->request->get('numero'));
        } else {
            $error['numero'] = 'O número é obrigatório.';
        }
        
        if ($request->request->has('complemento') && trim($request->request->get('complemento'))) {
            $user->setComplement($request->request->get('complemento'));
        } else {
            //$error['complemento'] = 'O complemento é obrigatório.';
        }
        
        if ($request->request->has('bairro') && trim($request->request->get('bairro'))) {
            $user->setNeighborhood($request->request->get('bairro'));
        } else {
            $error['bairro'] = 'O bairro é obrigatório.';
        }
        
        if ($request->request->has('cidade') && trim($request->request->get('cidade'))) {
            $user->setCity($request->request->get('cidade'));
        } else {
            $error['cidade'] = 'A cidade é obrigatória.';
        }
        
        if ($request->request->has('uf') && trim($request->request->get('uf'))) {
            $user->setState($request->request->get('uf'));
        } else {
            $error['uf'] = 'O estado é obrigatório.';
        }

        if ($request->request->has('ddd_celular') && trim($request->request->get('ddd_celular'))) {
            if (strlen($request->request->get('ddd_celular')) < 2) {
                $error['ddd_celular'] = 'DDD inválido.';
            } else {
                $user->setDddCellphone($request->request->get('ddd_celular'));
            }
        } else {
            $error['ddd_celular'] = 'O DDD é obrigatório.';
        }
        
        if ($request->request->has('celular') && trim($request->request->get('celular'))) {
            if (strlen($request->request->get('celular')) < 10) {
                $error['celular'] = 'Número de celular inválido';
            } else {
                $user->setCellphone($request->request->get('celular'));
            }
        } else {
            $error['celular'] = 'O celular é obrigatório.';
        }
        
        if ($request->request->has('ponto_referencia') && trim($request->request->get('ponto_referencia'))) {
            $user->setReferencePoint($request->request->get('ponto_referencia'));
        } else {
            $error['ponto_referencia'] = 'O ponto de referência é obrigatório.';
        }   

        //dump($request->request->get('whatsApp')); die();

        if ($request->request->has('whatsApp')) {
            $user->setIsNotificationWhatsapp($request->request->get('whatsApp'));
        }
        /* end */
     
        if($request->files->has('avatar')){
            $fileName =  'user-'.$user->getId().'-'.date('s'). '.jpg';
            $folder = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user/';
            $filePath = $folder . $fileName;
           
            if(!is_dir($folder)) {
                mkdir($folder, 0777, true);
            }

            copy($request->files->get('avatar')->getRealPath(), $filePath);
            $user->setAvatar('uploads/user/' . $fileName);
        }

        
        /*** campos novos sonora */
        $birthday = $request->request->get('birthday');
        //$gender = $request->request->get('gender');

        // if ($gender) {
        //     $user->setGender($gender);
        // }
                                        
        if ($birthday != '') {
            $d = explode('/', $birthday);
            if(isset($d[0]) && isset($d[1]) && isset($d[2])){
                $birthday = "{$d[2]}-{$d[1]}-{$d[0]}";

                $user->setBirthday(new \DateTime($birthday));
            }
        } else {
            $error['birthday'] = 'A Data de nascimento é obrigatório.';
        }

        $user->setNewsletter($request->request->has('newsletter') ? $request->request->get('newsletter') : false);
        /*** end  */

        $team_football = trim($request->request->get('team_football'));
        $user->setTeamFootball($team_football);
       
        if (count($error) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'fields' => $error,
            ]);
        }
        
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());
        $user->setTokenApiExcelencia($this->generateToken()); //token gerado para API de excelencia
        $user->setCodExcelencia(0);
        $em->persist($user);
        $em->flush();    

        //logando o usuário 
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
       
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setTokenApiExcelencia($this->generateToken());

        $em->persist($user);
        $em->flush();
        //end 

        $em->getRepository('App:UserPointGamification')->checkRegisterComplete($user); //Gamificação interação

        //SONORA enviar informações 
        $neopharma_excelencia = new ExcelenciaSonoraService(
            $this->getParameter('sonora_excelencia_secret'),
            $this->getParameter('sonora_excelencia_env')
        );
       
        $neopharma_excelencia->impersonate($user);
        
        $url = $request->getSchemeAndHttpHost();

        return new JsonResponse([
            'status' => true,
            'message' => 'Atualizado com sucesso.',
            'url' => $url
        ]);
    }

    /**
     * @Route("/consulta/acesso", name="consulta-acesso-excelencia")
     */
    public function consultaAcessoExcelencia(Request $request)
    {
        $user = $this->getUser();

        //Usuário não encontrado
        if (!$user->getIsPharmacy()) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado.'
            ]);
        }

        //Aceitar o regulamento
        if (!$user->getIsRegulation()) {
            $url = '/regulamento';   
    
            return new JsonResponse([
                'status' => true,
                'message' => 'O regulamento é obrigatório.',
                'url' => $url
            ]);
        }

        //SONORA enviar informações 
        $neopharma_excelencia = new ExcelenciaSonoraService(
            $this->getParameter('sonora_excelencia_secret'),
            $this->getParameter('sonora_excelencia_env')
        );
        $response = $neopharma_excelencia->impersonate($user);

        if ($response['status']) {
            $url = $response['url']; //redirecionar para o Excelência

            return new JsonResponse([
                'status' => true,
                'message' => 'success.',
                'url' => $url
            ]);
        }
        //end 

        return new JsonResponse(
            $response
        );
    }

    /**
     * @Route("/alteracao-responsavel", name="user-responsible-update")
     */
    public function userResponsibleUpdate(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
           /** Gravando a referencia do conteúdo */
           $url_reference = $request->getUri();
           $response = $this->redirectToRoute('home-login');
           $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

           return $response;
           /*** END  */
        }
        
        $user = $this->getUser();

        if ($request->request->has('e')) {
            $em = $this->getDoctrine()->getManager();
            $error = array();
           
            $cnpj = str_replace(['.', ' ', '-', '/'], '', $request->request->get('cnpj'));
            $cpf = str_replace(['.', ' ', '-'], '', $request->request->get('cpf'));
            $name = $request->request->get('name');
            $email = $request->request->get('email');
                           
            if (!$user->getIsPharmacy()) {
                $error['message'] = 'Usuário não encontrado.';
            }
                   
            if (!$name) {
                $error['name'] = 'O nome é obrigatório.';
            }
    
            if ($email) {
                if ($this->validEmail($email)) {
                    if ($email == $user->getEmail()) {
                        $search_email = $em->getRepository('App:User')
                                    ->createQueryBuilder('u')
                                    ->where('u.email = :email or u.username = :username')
                                    ->andWhere('u.cpf = :cpf')
                                    ->setParameter('email', $email)
                                    ->setParameter('username', $email)
                                    ->setParameter('cpf', $cpf)
                                    ->getQuery()
                                    ->getResult();

                        if ($search_email) {
                            $is_gestor = false;

                            foreach ($search_email as $u) {
                                if (($u->getIsPharmacy() == true) && ($email != $u->getEmail())) {
                                    $is_gestor = true;
                                }
                            }

                            if (!$is_gestor) {
                                $error['email'] = 'Email está sendo utilizado';
                            }
                        }
                      
                    } else {
                        //E-mail validado pode liberar
                        //$user->setEmail($email);
                    }
                } else {
                    $error['email'] = 'E-mail inválido.';
                }   
            } else {
                $error['email'] = 'O e-mail é obrigatório.';
            }
            
            if (!$cpf) {
                $error['cpf'] = 'O CPF é obrigatório.';
            } else {
                $serch_cpf = $em->getRepository('App:User')->findOneByCpf(str_replace(['.', ' ', '-'], '', $cpf));
                if ($serch_cpf) {
                    $error['cpf'] = 'CPF está sendo utilizado.';
                } else if(!$this->validaCPF($cpf)) {
                    $error['cpf'] = 'CPF inválido.';
                }
            }
            
            $is_new_password = false;
            $actual_password = $request->request->get('actual_password');
            $new_password = $request->request->get('new_password');
            $repeat_password = $request->request->get('repeat_password');
    
            if ($new_password) {
                if (!$repeat_password) {
                    $error['repeat_password'] = 'A repetição da nova senha é obrigatória.';
                } else if (!$actual_password) {
                    $error['actual_password'] = 'A senha atual é obrigatória.';
                } else if ($new_password == $repeat_password) {
                    $bool = ($encoder->isPasswordValid($user, $actual_password)) ? true : false;
        
                    if (!$bool) {
                        $error['actual_password'] = 'A senha atual está incorreta.';
                    } else {
                        $is_new_password = true;
                    }

                } else {
                    $error['new_password'] = 'O campo trocar senha e repetir senha estão diferentes.';
                }
            } else {
                if (!$repeat_password) {
                    $error['repeat_password'] = 'A repetição da nova senha é obrigatória.';
                } 

                if (!$actual_password) {
                    $error['actual_password'] = 'A senha atual é obrigatória.';
                }

                $error['new_password'] = 'A nova senha é obrigatória.';
            }

            if (count($error) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'fields' => $error,
                ]);
            }

            //verificando se existe outros gestores de loja que esteja usando o mesmo CPF para outros CNPJ
            $user_using_mult_cpf = $em->getRepository('App:User')->findBy(['cpf' => $user->getCpf(), 'isPharmacy' => true]);
            $is_mult_register_cpf = false;
             
            if (count($user_using_mult_cpf) > 1) {
               
                foreach ($user_using_mult_cpf as $mult_cpf) {
                    if ($mult_cpf->getCnpj() != $user->getCnpj()) {
                        $is_mult_register_cpf = true;
                        break;
                    }
                }

                if ($is_mult_register_cpf) {
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'O CPF do cadastro atual é responsável por mais de uma loja. Para fazer essa alteração entre em contato com o SAC pelo e-mail: excelencia@portalneopharma.com.br',
                    ]);
                }
                
            }
            
            //com historico de migração de perfil
            $user_change_profile = $em->getRepository('App:UserChangeProfile')->findOneBy(['cpf' => $user->getCpf(), 'cnpj' => $user->getCnpj()]);

            if ($user_change_profile) {
             
                //cadastro novo para gestor de loja 
                $new_user = $em->getRepository('App:User')->findOneBy(['cnpj' => $user->getCnpj(), 'cpf' => $cpf, 'isPharmacy' => true]);

                if (!$new_user) {
                    $new_user = new User(); // cadastro 
                }
            
                $new_user->setName($name);
                $new_user->setUsername(null);
                $new_user->setEmail($email);
                $new_user->setCpf($cpf);
                $new_user->setCnpj($user->getCnpj());
                
                if ($is_new_password) {
                    $new_user->setSalt(md5(uniqid())); /* parte 2 execelência */
                    $new_user->setPassword($encoder->encodePassword($new_user, $request->request->get('new_password')));
                }

                $new_user->setCompanyName($user->getCompanyName()); 
                $new_user->setAvatar($user->getAvatar()); 
                $new_user->setFantasyName($user->getFantasyName());
                $new_user->setZipCode($user->getZipCode()); 
                $new_user->setAddress($user->getAddress()); 
                $new_user->setNumber($user->getNumber()); 
                $new_user->setComplement($user->getComplement()); 
                $new_user->setNeighborhood($user->getNeighborhood()); 
                $new_user->setCity($user->getCity()); 
                $new_user->setState($user->getState()); 
                $new_user->setDddCellphone($user->getDddCellphone()); 
                $new_user->setCellphone($user->getCellphone());
                $new_user->setReferencePoint($user->getReferencePoint()); 

                $is_whats = $user_change_profile->getIsNotificationWhatsapp() ? true : false;
                
                $new_user->setIsNotificationWhatsapp($is_whats); 
                //add role gestor de loja
                $role_gestor_loja = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACIA');
                $new_user->addRole($role_gestor_loja);
                $new_user->setIsPharmacy(true);
                $new_user->setIsActive(true);
                $new_user->setIsPharmaceutical(false);
                $new_user->setIsRegisterCompleteExcelencia(true);
                $new_user->setCodExcelencia(0);   
                $new_user->setTokenApiExcelencia($this->generateToken()); //token gerado para API de excelencia
                $new_user->setCreatedAt(new \DateTime());
                $new_user->setUpdatedAt(new \DateTime());
                $new_user->setLastLoginAt(new \DateTime());
                
                $em->persist($new_user);
                $em->flush();

                //SONORA enviar informações 
                $neopharma_excelencia = new ExcelenciaSonoraService(
                    $this->getParameter('sonora_excelencia_secret'),
                    $this->getParameter('sonora_excelencia_env')
                );
            
                $neopharma_excelencia->impersonate($new_user);

                //Gravando na tabela de controle de alteração de responsável 
                $user_responsible_change = $em->getRepository('App:UserStoreManagerResponsibleChange')->findOneBy(['cnpj' => $new_user->getCnpj(), 'cpf' => $new_user->getCpf()]);

                if (!$user_responsible_change) {
                    $user_responsible_change = new UserStoreManagerResponsibleChange(); // cadastro 
                } 

                $user_responsible_change->setUser($new_user);
                $user_responsible_change->setCnpj($new_user->getCnpj());
                $user_responsible_change->setCpf($new_user->getCpf());
                $user_responsible_change->setIsClerk(false);
                $user_responsible_change->setIsPharmaceutical(false);
                $user_responsible_change->setIsStoreManager(true);
                $user_responsible_change->setIsStoreManagerResponsible(true);
                $user_responsible_change->setCreatedAt(new \DateTime());

                $em->persist($user_responsible_change);
                $em->flush();

                //voltando o perfil do usuário para farmacêutico ou balconista

                $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
                                    
                $is_pharmaceutical = false;
                $is_clerk = false;
                if ($user->getRoles()->contains($roleFarmac)) {
                    $is_pharmaceutical = true;
                } else {
                    $is_clerk = true;
                }

                $user->setUsername($user->getEmail());
                $user->setEmail($user->getEmail());
                $user->setCompanyName(null); 
                $user->setFantasyName(null);
                $user->setZipCode(null); 
                $user->setAddress(null); 
                $user->setNumber(null); 
                $user->setComplement(null); 
                $user->setNeighborhood(null); 
                $user->setCity(null); 
                $user->setState(null); 
                $user->setDddCellphone(null); 
                $user->setCellphone(null);
                $user->setReferencePoint(null);
                $user->removeRole($role_gestor_loja);
                $user->setIsPharmacy(false);
                $user->setIsActive(true);
                $user->setIsPharmaceutical($is_pharmaceutical);
                $user->setIsRegisterCompleteExcelencia(false);
                $user->setCodExcelencia(null);   
                $user->setTokenApiExcelencia(null); //token gerado para API de excelencia
                $user->setUpdatedAt(new \DateTime());
                $user->setLastLoginAt(new \DateTime());
                
                $em->persist($user);
                $em->flush();

                //Gravando na tabela de controle de alteração de responsável 
                $user_responsible_change = $em->getRepository('App:UserStoreManagerResponsibleChange')->findOneBy(['cnpj' => $user->getCnpj(), 'cpf' => $user->getCpf()]);

                if (!$user_responsible_change) {
                    $user_responsible_change = new UserStoreManagerResponsibleChange(); // cadastro 
                }

                $user_responsible_change->setUser($user);
                $user_responsible_change->setCnpj($user->getCnpj());
                $user_responsible_change->setCpf($user->getCpf());
                $user_responsible_change->setIsClerk($is_clerk);
                $user_responsible_change->setIsPharmaceutical($is_pharmaceutical);
                $user_responsible_change->setIsStoreManager(false);
                $user_responsible_change->setIsStoreManagerResponsible(false);
                $user_responsible_change->setCreatedAt(new \DateTime());

                $em->persist($user_responsible_change);
                $em->flush();
                
                return new JsonResponse([
                    'status' => true,   
                    'message' => 'success.'
                ]);
            
            } else { 
                //sem historico de migração de perfil, apenas alterando as informações do cadastro atual
                $user->setName($name);
                $user->setUsername(null);
                $user->setEmail($email);
                $user->setCpf($cpf);
                $user->setCnpj($user->getCnpj());

                if ($is_new_password) {
                    $user->setSalt(md5(uniqid())); /* parte 2 execelência */
                    $user->setPassword($encoder->encodePassword($user, $request->request->get('new_password')));
                }

                $user->setLastLoginAt(new \DateTime());
                $user->setUpdatedAt(new \DateTime());  
                $em->persist($user);
                $em->flush();

                //Gravando na tabela de controle de alteração de responsável 
                $user_responsible_change = $em->getRepository('App:UserStoreManagerResponsibleChange')->findOneBy(['cnpj' => $user->getCnpj(), 'cpf' => $user->getCpf()]);

                if (!$user_responsible_change) {
                    $user_responsible_change = new UserStoreManagerResponsibleChange(); // cadastro 
                } 

                $user_responsible_change->setUser($user);
                $user_responsible_change->setCnpj($user->getCnpj());
                $user_responsible_change->setCpf($user->getCpf());
                $user_responsible_change->setIsClerk(false);
                $user_responsible_change->setIsPharmaceutical(false);
                $user_responsible_change->setIsStoreManager(true);
                $user_responsible_change->setIsStoreManagerResponsible(true);
                $user_responsible_change->setCreatedAt(new \DateTime());

                $em->persist($user_responsible_change);
                $em->flush();

                //SONORA enviar informações 
                $neopharma_excelencia = new ExcelenciaSonoraService(
                    $this->getParameter('sonora_excelencia_secret'),
                    $this->getParameter('sonora_excelencia_env')
                );
            
                $neopharma_excelencia->impersonate($user);

                return new JsonResponse([
                    'status' => true,   
                    'message' => 'success.'
                ]);
            }
           
        }
               
        return $this->render('user/user_responsible_update.html.twig', [
            'user' => $user
        ]);
    }

    

    private function generateShareCode($user)
    {
        $name = explode(' ', $user->getName());
        $better_token = substr(md5(uniqid(rand(), true)),rand(0,26),5);
        return strtolower($name[0] . $better_token);
    }

    private function validateCaptcha($recaptcha)
    {
        $validation = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcsdrUUAAAAAGIZc0gfunjTt-zLnRuyEmDKnefB&response=".$recaptcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        $response = json_decode($validation,true);
        return isset($response['success']) ? $response['success'] : false;
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    function validEmail($email)
    {
        $Syntaxe = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if (preg_match($Syntaxe, $email))
            return true;
        else
            return false;
    }

    public function validaCPF($cpf = null) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
    
            return true;
        }
    }

    public function validaCNPJ($cnpj = null) {
        // Verifica se um número foi informado
        if(empty($cnpj)) {
            return false;
        }
       // dump($cnpj); die();
        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cnpj) != 14) {
            return false;
        }
        
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' || 
            $cnpj == '11111111111111' || 
            $cnpj == '22222222222222' || 
            $cnpj == '33333333333333' || 
            $cnpj == '44444444444444' || 
            $cnpj == '55555555555555' || 
            $cnpj == '66666666666666' || 
            $cnpj == '77777777777777' || 
            $cnpj == '88888888888888' || 
            $cnpj == '99999999999999') {
            return false;
            
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
         
            $j = 5;
            $k = 6;
            $soma1 = 0;
            $soma2 = 0;
    
            for ($i = 0; $i < 13; $i++) {
    
                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;
                
                $soma2 += ($cnpj{$i} * $k);
    
                if ($i < 12) {
                    $soma1 += ($cnpj{$i} * $j);
                }
    
                $k--;
                $j--;
    
            }
    
            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;
    
            return (($cnpj{12} == $digito1) and ($cnpj{13} == $digito2));
         
        }
    }
}
