<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/rank")
 */
class RankController extends Controller
{
    /**
     * @Route("", name="rank_home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();	
        
        $badges = $em->getRepository('App:Badge')->findBy(['isActive' => true]);

        $rank = [];

        foreach($badges as $key=>$badge){
            $rank[$key]['badge'] = $badge;
            $rank[$key]['rank'] = $em->getRepository('App:UserPoint')->getUserRankByBadge($badge);
        }

        return $this->render('rank/index.html.twig', [
            'rank' => $rank
        ]);
    }
}
