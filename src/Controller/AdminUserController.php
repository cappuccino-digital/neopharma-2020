<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;


use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * @Route("/admin/user")
 */
class AdminUserController extends Controller
{
    /**
     * @Route("", name="admin_user_list")
     */
    public function index(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:User')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query OR q.username LIKE :query OR q.name LIKE :query OR q.email LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $users = $builder->getQuery()->getResult();

        $users = $this->get('knp_paginator')->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_user/list.html.twig', [
            'users' => $users
        ]);
    }

        /**
     * @Route("/novo", name="admin_user_new")
     */
    public function adminUserNew(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
        ->add('name', TextType::class, array('required' => true, 'label' => 'Nome'))
        ->add('email', TextType::class, array('required' => true, 'label' => 'Email'))
        ->add('username', TextType::class, array('required' => true, 'label' => 'Username'))
        ->add('avatar', FileType::class, array('required' => false, 'label' => 'Avatar'))
        ->add('crf', TextType::class, array('required' => false, 'label' => 'CRF'))
        ->add('cpf', TextType::class, array('required' => false, 'label' => 'CPF'))
        ->add('cnpj', TextType::class, array('required' => false, 'label' => 'CNPJ da Farmácia'))
        ->add('state', ChoiceType::class, array(
            'required' => false, 
            'label' => 'Estado',
            'choices' => [
                'Acre' => 'AC',
                'Alagoas' => 'AL',
                'Amapá' => 'AP',
                'Amazonas' => 'AM',
                'Bahia ' => 'BA',
                'Ceará' => 'CE',
                'Distrito Federal' => 'DF',
                'Espírito Santo' => 'ES',
                'Goiás' => 'GO',
                'Maranhão' => 'MA',
                'Mato Grosso' => 'MT',
                'Mato Grosso do Sul' => 'MS',
                'Minas Gerais' => 'MG',
                'Pará' => 'PA',
                'Paraíba' => 'PB',
                'Paraná' => 'PR',
                'Pernambuco' => 'PE',
                'Piauí' => 'PI',
                'Rio de Janeiro' => 'RJ',
                'Rio Grande do Norte' => 'RN',
                'Rio Grande do Sul' => 'RS',
                'Rondônia' => 'RO',
                'Roraima' => 'RR',
                'Santa Catarina' => 'SC',
                'São Paulo' => 'SP',
                'Sergipe' => 'SE',
                'Tocantins' => 'TO'
            ]))
        ->add('city', TextType::class, array('required' => false, 'label' => 'Cidade'))
        ->add('roles', EntityType::class, array(
                'class' => 'App:Role',
                'choice_label' => 'role',
                'label' => 'Select a Role',
                'multiple' => true,
            ))
        ->add('gender', ChoiceType::class, array(
            'required' => false, 
            'label' => 'Sexo',
            'choices' => [
                'Masculino' => 'male',
                'Feminino' => 'female',
            ]))
        ->add('password', PasswordType ::class, array('label' => 'Password'))
        ->add('isActive', CheckboxType::class, array('label' => 'Ativo'))
        ->add('isPharmaceutical', CheckboxType::class, array('label' => 'Farmacêutico'))
        ->add('save', SubmitType::class, array('attr' => array('class' => 'save')))
        ->getForm();

        if($request->isMethod('GET')){
            return $this->render('admin_user/new.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $form->handleRequest($request);

        if($form->isValid()) {    
            $em = $this->getDoctrine()->getManager();
            $user->setCreatedAt(new \Datetime());
            $user->setUpdatedAt(new \Datetime());
            
            if($user->getAvatar()){
                r($user->getAvatar());
                $folder = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user';

                if(!is_dir($folder)){
                    mkdir($folder, 777);
                }
                $fileName = $user->getUsername().'.jpg';                
                
                copy($user->getAvatar(), $folder .'/'. $fileName);

                $user->setAvatar('uploads/user/'.$fileName);
            } else {
                $user->setAvatar('uploads/user/profile.png');
            }
            
            if($user->getSalt() == '' || $user->getSalt() == null){
                $salt = md5(uniqid());
                $user->setSalt($salt);
            }

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_user_list');
        }
        
        return $this->render('admin_user/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_user_edit")
     */
    public function adminUserEdit(Request $request, $id, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($id);
        $oldAvatar = $user->getAvatar();
        $oldPassword = $user->getPassword();

        $form = $this->createFormBuilder($user)
        ->add('name', TextType::class, array('required' => true, 'label' => 'Nome'))
        ->add('email', TextType::class, array('required' => true, 'label' => 'Email'))
        ->add('username', TextType::class, array('required' => true, 'label' => 'Username'))
        ->add('avatar', FileType::class, array('required' => false, 'label' => 'Avatar', 'data_class' => null))
        ->add('crf', TextType::class, array('required' => false, 'label' => 'CRF'))
        ->add('cpf', TextType::class, array('required' => false, 'label' => 'CPF'))
        ->add('cnpj', TextType::class, array('required' => false, 'label' => 'CNPJ da Farmácia'))
        ->add('state', ChoiceType::class, array(
            'required' => false, 
            'label' => 'Estado',
            'choices' => [
                'Acre' => 'AC',
                'Alagoas' => 'AL',
                'Amapá' => 'AP',
                'Amazonas' => 'AM',
                'Bahia ' => 'BA',
                'Ceará' => 'CE',
                'Distrito Federal' => 'DF',
                'Espírito Santo' => 'ES',
                'Goiás' => 'GO',
                'Maranhão' => 'MA',
                'Mato Grosso' => 'MT',
                'Mato Grosso do Sul' => 'MS',
                'Minas Gerais' => 'MG',
                'Pará' => 'PA',
                'Paraíba' => 'PB',
                'Paraná' => 'PR',
                'Pernambuco' => 'PE',
                'Piauí' => 'PI',
                'Rio de Janeiro' => 'RJ',
                'Rio Grande do Norte' => 'RN',
                'Rio Grande do Sul' => 'RS',
                'Rondônia' => 'RO',
                'Roraima' => 'RR',
                'Santa Catarina' => 'SC',
                'São Paulo' => 'SP',
                'Sergipe' => 'SE',
                'Tocantins' => 'TO'
            ]))
        ->add('city', TextType::class, array('required' => false, 'label' => 'Cidade'))
        ->add('gender', ChoiceType::class, array(
            'required' => false, 
            'label' => 'Sexo',
            'choices' => [
                'Masculino' => 'male',
                'Feminino' => 'female',
            ]
            ))
        ->add('roles', EntityType::class, array(
                'class' => 'App:Role',
                'choice_label' => 'role',
                'label' => 'Selecione o Acesso',
                'multiple' => true,
            ))
        ->add('isActive', CheckboxType::class, array('required' => false,'label' => 'Ativo'))
        ->add('isPharmaceutical', CheckboxType::class, array('required' => false,'label' => 'Farmacêutico'))
        ->add('password', PasswordType ::class, array('required' => false,'label' => 'Novo Password'))
        ->add('save', SubmitType::class, array('attr' => array('class' => 'save')))
        ->getForm();

        if($request->isMethod('GET')){
            return $this->render('admin_user/edit.html.twig', [
                'form' => $form->createView(),
                'user' => $user
            ]);
        }

        $form->handleRequest($request);
        
        if($form->isValid()) {    
            $em = $this->getDoctrine()->getManager();
            $user->setUpdatedAt(new \Datetime());

            if($user->getAvatar()){
                $folder = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user';

                if(!is_dir($folder)){
                    mkdir($folder, 777);
                }

                $fileName = $user->getUsername().'.jpg';                
                
                copy($user->getAvatar(), $folder .'/'. $fileName);

                $user->setAvatar('uploads/user/'.$fileName);
            } else {
                $user->setAvatar($oldAvatar);
            }
            
            if($user->getSalt() == '' || $user->getSalt() == null){
                $salt = md5(uniqid());
                $user->setSalt($salt);
            }

            if($user->getPassword() != $oldPassword){
                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            } else {
                $user->setPassword($oldPassword);
            }

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_user_list');
        }
        
        return $this->render('admin_user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);

    }

    private function removeFile($fileName)
    {
        if($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png'){
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName; 
        if(is_file($filePath)) unlink($filePath);
    }
}
