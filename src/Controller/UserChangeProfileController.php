<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Service\ExcelenciaSonoraService;

class UserChangeProfileController extends Controller
{
    /**
     * @Route("/change-profile", name="change-profile")
     */
    public function changeProfile(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $token = $request->query->get('token');
        
        if(!$token){
            $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
            return $this->redirectToRoute('home');
        }
        
        $em = $this->getDoctrine()->getManager();
        $user_change_profile = $em->getRepository('App:UserChangeProfile')->findOneBy(['token_email' => $token]);

        // if ($user_change_profile) {
        //     if ($user_change_profile->getIsChangeProfile()) {
        //         $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
        //         return $this->redirectToRoute('home');
        //     }
        // } 

        if (!$user_change_profile) {
            $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
            return $this->redirectToRoute('home');
        }

        if (!$user_change_profile->getIsValid()) {
            $this->addFlash("alert", "Entre em contato conosco, este link expirou.");
            return $this->redirectToRoute('home');
        }

        $user = $em->getRepository('App:User')->findOneBy(['id' => $user_change_profile->getUser()]);

        if (!$user) {
            $this->addFlash("error", "Não identificamos o seu cadastro, por favor entre em contato.");
            return $this->redirectToRoute('home');
        }

        $is_whats = 0;
        if ($user_change_profile->getIsNotificationWhatsapp()) {
            $is_whats = 1;
        }
                
        $user->setName($user_change_profile->getName()); //$request->request->set('name', $user_change_profile->getName());
        $user->setUsername(null);
        $user->setEmail($user_change_profile->getEmail()); //$request->request->set('mail', $user_change_profile->getEmail());
        $user->setCpf($user_change_profile->getCpf());//$request->request->set('cpf', $cpf); 
        $user->setCnpj($user_change_profile->getCnpj()); //$request->request->set('cnpj', $cnpj);
        $user->setSalt($user_change_profile->getSalt()); /* parte 2 execelência */
        $user->setPassword($user_change_profile->getPassword());
        $user->setCompanyName($user_change_profile->getCompanyName()); //$request->request->set('razao_social', $user_change_profile->getCompanyName());
        $user->setAvatar($user_change_profile->getAvatar()); //$request->request->set('inscricao_estadual', $user_change_profile->getStateRegistration());
        $user->setFantasyName($user_change_profile->getFantasyName());//$request->request->set('name_fantasia', $user_change_profile->getFantasyName());
        $user->setZipCode($user_change_profile->getZipCode()); //$request->request->set('cep', $cep);
        $user->setAddress($user_change_profile->getAddress()); //$request->request->set('endereco', $user_change_profile->getAddress());
        $user->setNumber($user_change_profile->getNumber()); //$request->request->set('numero', $user_change_profile->getNumber());
        $user->setComplement($user_change_profile->getComplement()); //$request->request->set('complemento', $user_change_profile->getComplement());
        $user->setNeighborhood($user_change_profile->getNeighborhood()); //($request->request->set('bairro', $user_change_profile->getNeighborhood());
        $user->setCity($user_change_profile->getCity()); //$request->request->set('cidade', $user_change_profile->getCity());
        $user->setState($user_change_profile->getState()); //$request->request->set('uf', $user_change_profile->getState()); 
        $user->setDddCellphone($user_change_profile->getDddCellphone()); //$request->request->set('ddd_celular', $user_change_profile->getDddCellphone());
        $user->setCellphone($user_change_profile->getCellphone()); //$request->request->set('celular', $user_change_profile->getCellphone())
        $user->setReferencePoint($user_change_profile->getReferencePoint()); //$request->request->set('ponto_referencia', $user_change_profile->getReferencePoint());
        $user->setIsNotificationWhatsapp($is_whats); //$request->request->set('whatsApp', $user_change_profile->getIsNotificationWhatsapp());
        $user->setBirthday($user_change_profile->getBirthday()); //$request->request->set('whatsApp', $user_change_profile->getIsNotificationWhatsapp());
        $user->setNewsletter($user_change_profile->getNewsletter()); //$request->request->set('whatsApp', $user_change_profile->getIsNotificationWhatsapp());
        $user->setTeamFootball($user_change_profile->getTeamFootball());

        $request->request->set('name', $user_change_profile->getName());
        $request->request->set('mail', $user_change_profile->getEmail());
        $request->request->set('cpf', $user_change_profile->getCpf()); 
        $request->request->set('cnpj', $user_change_profile->getCnpj());
        $request->request->set('razao_social', $user_change_profile->getCompanyName());
        //$request->request->set('inscricao_estadual', $user_change_profile->getStateRegistration());
        $request->request->set('name_fantasia', $user_change_profile->getFantasyName());
        $request->request->set('cep', $user_change_profile->getZipCode());
        $request->request->set('endereco', $user_change_profile->getAddress());
        $request->request->set('numero', $user_change_profile->getNumber());
        $request->request->set('complemento', $user_change_profile->getComplement());
        $request->request->set('bairro', $user_change_profile->getNeighborhood());
        $request->request->set('cidade', $user_change_profile->getCity());
        $request->request->set('uf', $user_change_profile->getState()); 
        $request->request->set('ddd_celular', $user_change_profile->getDddCellphone());
        $request->request->set('celular', $user_change_profile->getCellphone());
        $request->request->set('ponto_referencia', $user_change_profile->getReferencePoint());
        $request->request->set('whatsApp', $is_whats);
        $request->request->set('birthday', $user_change_profile->getBirthday());
        $request->request->set('newsletter', $user_change_profile->getNewsletter());
        $request->request->get('team_football', $user_change_profile->getTeamFootball());
        
        //removendo o role
        // foreach($user->getRoles() as $roles) {
        //     $user->removeRole($roles);
        // } 

        //add role gestor de loja
        $role = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACIA');
        $user->addRole($role);
        $user->setIsPharmacy(true);
        $user->setIsActive(true);
        $user->setIsPharmaceutical(false);
        $user->setIsRegisterCompleteExcelencia(true);
        $user->setCodExcelencia(0);   
        //$user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());

        $now = new \DateTime();
        if($user_change_profile->getExpiresAt()->format('Y-m-d H:i:s') <= $now->format('Y-m-d H:i:s')) {
            $user_change_profile->setIsValid(false);
            $user_change_profile->setTokenEmail($this->generateToken());
        }

        $user_change_profile->setIsChangeProfile(true);
        $em->persist($user_change_profile);
        $em->flush();

        $user->setTokenApiExcelencia($this->generateToken()); //token gerado para API de excelencia 
        $user->setUpdatedAt(new \DateTime());  
        $em->persist($user);
        $em->flush();


        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
       
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        //SONORA enviar informações 
        $neopharma_excelencia = new ExcelenciaSonoraService(
            $this->getParameter('sonora_excelencia_secret'),
            $this->getParameter('sonora_excelencia_env')
        );
        $response = $neopharma_excelencia->impersonate($user);

        if ($response['status']) {
            return $this->redirect($response['url']); //redirecionar para o Excelência
        }
        //end 

        $this->addFlash("success", "Perfil alterado com sucesso.");    

        return $this->redirectToRoute('home');
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
