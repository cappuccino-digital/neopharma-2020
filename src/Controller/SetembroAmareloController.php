<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class SetembroAmareloController extends Controller
{
    /**
     * @Route("/setembro-amarelo", name="setembro_amarelo")
     */
    public function index()
    {   
        return $this->render('setembro-amarelo/index.html.twig', [
            'controller_name' => 'SetembroAmareloController',
        ]);
    }

}