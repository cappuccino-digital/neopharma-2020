<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;
use App\Entity\UserFacebookData;
use App\Entity\UserGoogleData;
use App\Entity\UserInvite;
use App\Entity\Contact;
use App\Entity\UserNewPassword;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;


/**
 * @Route("/app")
 */
class AppApiController extends Controller
{
    /**
     * @Route("/get-product", name="app-get-product")
     */
    public function getProduct(Request $request)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
        $accessToken = isset($dataPost['at']) ? $dataPost['at'] : null;
        $barcode = isset($dataPost['barcode']) ? $dataPost['barcode'] : null;

        if(!$barcode || !$accessToken){
            $accessToken = $request->request->get('at');
            $barcode = $request->request->get('barcode');
        }

        if(!$barcode || !$accessToken){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneBy(['isActive' => true, 'appAccessToken' => $accessToken]);

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => "Algo deu errado com seu usuário. Saia do aplicativo e faça o login novamente"
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App:Product')->findOneByBarcode($barcode);
        
        if(!$product){
            return new JsonResponse([
                'status' => false,
                'message' => 'Produto inexistente',
            ]);
        }
        
        $productArray = [];

        $productArray['slug'] = $product->getSlug();
        $productArray['name'] = $product->getName();
        $productArray['barcode'] = $product->getBarCode();
        $productArray['activeSubstance'] = $product->getActiveSubstance();
        $productArray['anvisaRegistration'] = $product->getAnvisaRegistration();
        $productArray['fullPresentation'] = $product->getFullPresentation();
        $productArray['classification'] = $product->getClassification()->getName();
        $productArray['sistemicClass'] = $product->getSistemicClass()->getName();
        $productArray['refMedicine'] = $product->getRefMedicine();
        $productArray['isPsicotropic'] = $product->getIsPsicotropic() ? 'Sim' : 'Não';
        $productArray['indication'] = $product->getIndication();
        $productArray['contraindication'] = $product->getContraindication();
        $productArray['composition'] = $product->getComposition();
        $productArray['dosage'] = $product->getDosage();
        $productArray['bullaFile'] = $product->getBullaFile();
        $productArray['assets'] = [];

        foreach($product->getAssets() as $key=>$asset ){
            $productArray['assets'][$key]['file'] = $asset->getFile(); 
            $productArray['assets'][$key]['type'] = $asset->getType(); 
        }

        $otherPresentations = $em->getRepository('App:Product')->getProductOtherPresentation($product->getOtherPresentation());

        $prodOtherPres = [];

        foreach($otherPresentations as $k => $p){
            $prodOtherPres[$k]['name'] = $p->getName();
            $prodOtherPres[$k]['shortDescription'] = $p->getShortDescription();
            $prodOtherPres[$k]['fullPresentation'] = $p->getFullPresentation();
            $prodOtherPres[$k]['barcode'] = $p->getBarcode();

            foreach($p->getAssets() as $asset){
                if($asset->getType() == 'image'){
                    $prodOtherPres[$k]['image'] = $asset->getFile();
                    break;
                }
            }

            if(!isset($prodOtherPres[$k]['image'])){
                $prodOtherPres[$k]['image'] = $p->getBannerMobile();
            }
        }
        
        return new JsonResponse([
            'status' => true,
            'message' => 'OK',
            'product' => $productArray,
            'otherPresentations' => $prodOtherPres
        ]);
    }

    /**
     * @Route("/list-products", name="app-list-product")
     */
    public function listProducts(Request $request)
    {
        
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $page = isset($dataPost['p']) ? $dataPost['p'] : 1;
        $accessToken = isset($dataPost['at']) ? $dataPost['at'] : null;
        $filter = isset($dataPost['filter']) ? $dataPost['filter'] : null;
        $query = isset($dataPost['q']) ? $dataPost['q'] : null;
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneBy(['isActive' => true, 'appAccessToken' => $accessToken]);

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => "Algo deu errado com seu usuário. Saia do aplicativo e faça o login novamente"
            ]);
        }

        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Product')
            ->createQueryBuilder('q');
        
        if($filter && $filter != 'all'){
            $builder->innerJoin('App:ProductCategory', 'pc' , 'WITH' , 'q.category = pc.id')
                ->andWhere('pc.slug = :filter')
                ->setParameter('filter', $filter);
        } 

        if($query && $query != ''){
            $builder->andWhere('q.name LIKE :query OR q.anvisaRegistration LIKE :query OR q.activeSubstance LIKE :query OR q.shortDescription LIKE :query')
            ->setParameter('query', "%{$query}%");
        }

        if($user->getIsPharmaceutical() != true){
            $builder->andWhere('q.isOnlyToPharmaceutical != true');
        }

        $builder->andWhere('q.isActive = true');
        $builder->orderBy('q.name','ASC');

        $products = $builder->getQuery()->getResult();

        $products = $this->get('knp_paginator')->paginate(
            $products,
            $page,
            10
        );

        $prodResp = [];

        foreach($products as $k => $p){
            $prodResp[$k]['name'] = $p->getName();
            $prodResp[$k]['shortDescription'] = $p->getShortDescription();
            $prodResp[$k]['fullPresentation'] = $p->getFullPresentation();
            $prodResp[$k]['barcode'] = $p->getBarcode();

            foreach($p->getAssets() as $asset){
                if($asset->getType() == 'image'){
                    $prodResp[$k]['image'] = $asset->getFile();
                    break;
                }
            }

            if(!isset($prodResp[$k]['image'])){
                $prodResp[$k]['image'] = $p->getBannerMobile();
            }
        }

        
        $categories = $em->getRepository('App:ProductCategory')->findBy(['isActive' => true]);

        $catArray = [];
        $catArray[0]['name'] = 'Todos';
        $catArray[0]['id'] = 0;
        $catArray[0]['slug'] = 'all';

        foreach($categories as $k=>$cat){
            $catArray[$k+1]['name'] = $cat->getName();
            $catArray[$k+1]['id'] = $cat->getId();
            $catArray[$k+1]['slug'] = $cat->getSlug();
        }

        return new JsonResponse([
            'status' => true,
            'products' => $prodResp,
            'nextPage' => $page + 1,
            'hasNextPage' => $products->getPageCount() > $page ? true : false,
            'filters' => $catArray,
            'filterUsed' => $filter
        ]);
    }

    /**
     * @Route("/login", name="app-login")
     */
    public function doLogin(Request $request, UserPasswordEncoderInterface $encoder)
    {
        //dump($request); die();
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $username = isset($dataPost['u']) ? $dataPost['u'] : null;
        $pass = isset($dataPost['p']) ? $dataPost['p'] : null;

        if(!$username || !$pass){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['username' => $username, 'isActive' => true]);
        
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado'
            ]);
        }

        $bool = ($encoder->isPasswordValid($user, $pass)) ? true : false;

        if(!$bool){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário ou senha incorreta'
            ]);
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setAppAccessToken(md5($token));
        $em->persist($user);
        $em->flush();

        $userInfo = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'cpf' => $user->getCpf(),
            'crf' => $user->getCrf(),
            'cnpj' => $user->getCnpj(),
            'state' => $user->getState(),
            'isActive' => $user->getIsActive(),
            'isPharmaceutical' => $user->getIsPharmaceutical(),
            'appAccessToken' => $user->getAppAccessToken(),
            'newsletter' => $user->getNewsletter(),
            'hasSocialLogin' => $user->getFacebookData() ? true : ($user->getGoogleData() ? true : false)
        ];

        $em->getRepository('App:UserPoint')->setPointsToBadge($user, $user, 'downloader', 'downloader');

        return new JsonResponse([
            'status' => true,
            'user' => $userInfo
        ]);

    }

    /**
     * @Route("/social-media-login", name="app-login-social-media")
     */
    public function socialMediaLogin(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);

        if(!$data){
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-LG-001. Tente novamente mais tarde'
            ]);    
        }

        $type = isset($data['type']) ? $data['type'] : null;

        if(!$type){
            return new JsonResponse([
                'status' => false,
                'message' => 'Ocorreu um erro: ERR:SM-LG-002. Tente novamente mais tarde'
            ]);    
        }
        
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();	

        if($type == 'facebook'){
            if($user){
                
                $facebookData = $em->getRepository('App:UserFacebookData')->findOneByUser($user);

                if($facebookData){
                    return new JsonResponse([
                        'status' => true,
                        'isLogged' => true
                    ]);
                }

                $facebookData = $em->getRepository('App:UserFacebookData')->findOneByEmail($data['userInfo']['email']);
            
                if($facebookData){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'Esta conta esta atrelada a outro usuário'
                    ]);
                } 

                $facebookData = new UserFacebookData();
                $facebookData->setUser($user);
                $facebookData->setName($data['userInfo']['name']);
                $facebookData->setEmail($data['userInfo']['email']);
                $facebookData->setAccessToken(isset($data['authResponse']['accessToken']) ? $data['authResponse']['accessToken'] : null);
                $facebookData->setExpiresIn(isset($data['authResponse']['expiresIn']) ? $data['authResponse']['expiresIn'] : null);
                $facebookData->setSignedRequest(isset($data['authResponse']['signedRequest']) ? $data['authResponse']['signedRequest'] : null);
                $facebookData->setDataAccessExpirationTime(isset($data['authResponse']['data_access_expiration_time']) ? $data['authResponse']['data_access_expiration_time'] : null);
                $facebookData->setFacebookId(isset($data['authResponse']['userID']) ? $data['authResponse']['userID'] : null);

                $em->persist($facebookData);
                $em->flush();

                $user->setIsFacebookConnected(true);
                $user->setFacebookData($facebookData);
                $user->setUpdatedAt(new \DateTime());

                $em->persist($user);
                $em->flush();

                return new JsonResponse([
                    'status' => true,
                    'isLogged' => true
                ]);

            } else {

                $user = $em->getRepository('App:User')->findOneByEmail($data['userInfo']['email']);

                if(!$user){
                    $user = $em->getRepository('App:User')->findOneByFacebookEmail($data['userInfo']['email']);
                    if(!$user){
                        $user = new User();
                        $user->setSalt(md5(uniqid()));
                        $user->setPassword($encoder->encodePassword($user, $data['authResponse']['userID']));
                        $user->setName($data['userInfo']['name']);
                        $user->setUsername($data['userInfo']['email']);
                        
                        $user->setIsActive(false);
                        $user->setEmail($data['userInfo']['email']);
                        $user->setCreatedAt(new \DateTime());
                        $user->setUpdatedAt(new \DateTime());
                        $user->setLastLoginAt(new \DateTime());
                        $user->setIsFacebookConnected(true);
                        $user->setIsRegisterComplete(false);

                        $em->persist($user);
                        $em->flush();

                        $urlPicture = "https://graph.facebook.com/{$data['authResponse']['userID']}/picture?width=300&height=300&access_token={$data['authResponse']['accessToken']}";
                        $user->setAvatar($this->saveAvatar($user, $urlPicture));

                        $em->persist($user);
                        $em->flush();
                    }
                }
                $facebookData = $em->getRepository('App:UserFacebookData')->findOneByUser($user);
                
                if(!$facebookData){
                    $facebookData = new UserFacebookData();
                }

                $facebookData->setUser($user);
                $facebookData->setName($data['userInfo']['name']);
                $facebookData->setEmail($data['userInfo']['email']);
                $facebookData->setAccessToken(isset($data['authResponse']['accessToken']) ? $data['authResponse']['accessToken'] : null);
                $facebookData->setExpiresIn(isset($data['authResponse']['expiresIn']) ? $data['authResponse']['expiresIn'] : null);
                $facebookData->setSignedRequest(isset($data['authResponse']['signedRequest']) ? $data['authResponse']['signedRequest'] : null);
                $facebookData->setDataAccessExpirationTime(isset($data['authResponse']['data_access_expiration_time']) ? $data['authResponse']['data_access_expiration_time'] : null);
                $facebookData->setFacebookId(isset($data['authResponse']['userID']) ? $data['authResponse']['userID'] : null);

                $em->persist($facebookData);
                $em->flush();

                $user->setIsFacebookConnected(true);
                $user->setFacebookData($facebookData);
                $user->setUpdatedAt(new \DateTime());
                
                $em->persist($user);
                $em->flush();
            }
        } else {
            if($user){
                $googleData = $em->getRepository('App:UserGoogleData')->findOneByUser($user);

                if($googleData){
                    return new JsonResponse([
                        'status' => true,
                        'isLogged' => true
                    ]);    
                }

                $googleData = $em->getRepository('App:UserGoogleData')->findOneByEmail($data['userInfo']['email']);

                if($googleData){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'Esta conta esta atrelada a outro usuário'
                    ]);   
                }

                $googleData = new UserGoogleData();
                $googleData->setUser($user);
                $googleData->setName($data['userInfo']['name']);
                $googleData->setEmail($data['userInfo']['email']);
                $googleData->setGoogleId($data['userInfo']['id']);

                $em->persist($googleData);
                $em->flush();

                $user->setIsGoogleConnected(true);
                $user->setGoogleData($googleData);
                $user->setUpdatedAt(new \DateTime());

                $em->persist($user);
                $em->flush();
                
                return new JsonResponse([
                    'status' => true,
                    'isLogged' => true
                ]);
            } else {
                $user = $em->getRepository('App:User')->findOneByEmail($data['userInfo']['email']);

                if(!$user){
                    $user = $em->getRepository('App:User')->findOneByGoogleEmail($data['userInfo']['email']);
                    if(!$user){
                        $user = new User();
                        $user->setSalt(md5(uniqid()));
                        $user->setPassword($encoder->encodePassword($user, $data['userInfo']['uid']));
                        $user->setName($data['userInfo']['displayName']);
                        $user->setUsername($data['userInfo']['email']);
                        $user->setIsActive(false);
                        $user->setEmail($data['userInfo']['email']);
                        $user->setCreatedAt(new \DateTime());
                        $user->setUpdatedAt(new \DateTime());
                        $user->setLastLoginAt(new \DateTime());
                        $user->setIsGoogleConnected(true);
                        $user->setIsRegisterComplete(false);

                        $em->persist($user);
                        $em->flush();

                        $urlPicture = $data['userInfo']['photoURL'];
                        $user->setAvatar($this->saveAvatar($user, $urlPicture));

                        $em->persist($user);
                        $em->flush();
                    }
                }

                $googleData = $em->getRepository('App:UserGoogleData')->findOneByUser($user);
                
                if(!$googleData){
                    $googleData = new UserGoogleData();
                }

                $googleData->setUser($user);
                $googleData->setName($data['userInfo']['displayName']);
                $googleData->setEmail($data['userInfo']['email']);
                $googleData->setGoogleId($data['userInfo']['uid']);

                $em->persist($googleData);
                $em->flush();

                $user->setIsGoogleConnected(true);
                $user->setGoogleData($googleData);
                $user->setUpdatedAt(new \DateTime());

                $em->persist($user);
                $em->flush();
            }
        }

        if($user->getIsRegisterComplete()){
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            $user->setIsActive(true);
            $user->setAppAccessToken(md5($token));
            $em->persist($user);
            $em->flush();

            $userInfo = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'cpf' => $user->getCpf(),
                'crf' => $user->getCrf(),
                'cnpj' => $user->getCnpj(),
                'state' => $user->getState(),
                'isActive' => $user->getIsActive(),
                'isPharmaceutical' => $user->getIsPharmaceutical(),
                'appAccessToken' => $user->getAppAccessToken(),
                'newsletter' => $user->getNewsletter(),
                'hasSocialLogin' => $user->getFacebookData() ? true : ($user->getGoogleData() ? true : false)
            ];
    
            $em->getRepository('App:UserPoint')->setPointsToBadge($user, $user, 'downloader', 'downloader');

            return new JsonResponse([
                'status' => true,
                'user' => $userInfo,
                'shouldComplete' => false
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
                'id' => $user->getId(),
                'shouldComplete' => true
            ]);
        }
    }

    /**
     * @Route("/register", name="app-register")
     */
    public function doRegister(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
      
        $name = isset($dataPost['n']) ? trim($dataPost['n']) : null;
        $email = isset($dataPost['e']) ? trim($dataPost['e']) : null;
        $password = isset($dataPost['p']) ? trim($dataPost['p']) : null;
        $isPharmaceutical = isset($dataPost['i']) ? $dataPost['i'] : null;
        $crf = isset($dataPost['cr']) ? trim($dataPost['cr']) : null;
        $cpf = isset($dataPost['cp']) ? trim($dataPost['cp']) : null;
        $cnpj = isset($dataPost['cn']) ? trim($dataPost['cn']) : null;
        $newsletter = isset($dataPost['news']) ? $dataPost['news'] : null;
        $friendCode = isset($dataPost['fc']) ? $dataPost['fc'] : null;
        $id = isset($dataPost['id']) ? $dataPost['id'] : null;
        $state = isset($dataPost['st']) ? $dataPost['st'] : null;
        
        if(
            !$name ||
            !$email ||
            !$password ||
            $isPharmaceutical === null
        ){
            if ($isPharmaceutical) {
                if (!$crf) {
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'Paramêtros não encontrados F01'
                    ]);
                }
            }

            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados 01'
            ]);
        }

        if(!$isPharmaceutical && (!$cpf || !$cnpj)){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados 02'
            ]);
        }
       
        $em = $this->getDoctrine()->getManager();		

        if($id){
            $user = $em->getRepository('App:User')->find($id);
            if(!$user){
                $user = new User();    
            }
        } else {
            $user = new User();
        }
        
        $user->setName($name);
        $user->setEmail($email);
        $user->setUsername($email);

        $salt = md5(uniqid());
        $user->setSalt($salt);
        $user->setPassword($encoder->encodePassword($user, $password));

        $user->setIsPharmaceutical($isPharmaceutical);
        $user->setCrf(str_replace(['.', ' ', '-', '/'], '', $crf));
        $user->setCpf(str_replace(['.', ' ', '-', '/'], '', $cpf));
        $user->setCnpj(str_replace(['.', ' ', '-', '/'], '', $cnpj));
        
        $user->setNewsletter($newsletter == null ? false : $newsletter);
        $user->setIsActive(true);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());
        $user->setState($state);
        
        $em->persist($user);
        $em->flush();

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        if($isPharmaceutical){
            $role = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
            $user->addRole($role);
        } else {
            $role = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
            $user->addRole($role);
        }

        $user->setIsRegisterComplete(true);
        $user->setAppAccessToken(md5($token));
        $em->persist($user);
        $em->flush();

        $user->setShareCode($this->generateShareCode($user));
        $em->persist($user);
        $em->flush();

        $userInfo = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'cpf' => $user->getCpf(),
            'crf' => $user->getCrf(),
            'cnpj' => $user->getCnpj(),
            'isActive' => $user->getIsActive(),
            'state' => $user->getState(),
            'isPharmaceutical' => $user->getIsPharmaceutical(),
            'appAccessToken' => $user->getAppAccessToken(),
            'newsletter' => $user->getNewsletter(),
            'hasSocialLogin' => $user->getFacebookData() ? true : ($user->getGoogleData() ? true : false)
        ];

        if($friendCode != null && $friendCode != ''){
            $invite = new UserInvite();
            $invite->setUser($user);
            $invite->setShareCode($friendCode);
            $em->persist($invite);
            $em->flush();

            $user->setInvite($invite);
            $em->persist($user);
            $em->flush();

            $userPointer = $em->getRepository('App:User')->findOneByShareCode($friendCode);
            $em->getRepository('App:UserPoint')->setPointsToBadge($userPointer, $user, 'quem-indica', 'quem-indica');
        }

        $em->getRepository('App:UserPoint')->setPointsToBadge($user, $user, 'downloader', 'downloader');

        return new JsonResponse([
            'status' => true,
            'user' => $userInfo
        ]);

    }

    /**
     * @Route("/update-register", name="app-update-register")
     */
    public function doRegisterUpdate(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $id = isset($dataPost['id']) ? $dataPost['id'] : null;
        $name = isset($dataPost['n']) ? $dataPost['n'] : null;
        $email = isset($dataPost['e']) ? $dataPost['e'] : null;
        $isPharmaceutical = isset($dataPost['i']) ? $dataPost['i'] : null;
        $crf = isset($dataPost['cr']) ? $dataPost['cr'] : null;
        $cpf = isset($dataPost['cp']) ? $dataPost['cp'] : null;
        $cnpj = isset($dataPost['cn']) ? $dataPost['cn'] : null;
        $newsletter = isset($dataPost['news']) ? $dataPost['news'] : null;
        $appAccessToken = isset($dataPost['at']) ? $dataPost['at'] : null;
        $state = isset($dataPost['st']) ? $dataPost['st'] : null;

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->find($id);
        
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado'
            ]);
        }

        if($user->getAppAccessToken() != $appAccessToken){
            return new JsonResponse([
                'status' => false,
                'message' => 'Token inválido'
            ]);
        }

        $user->setName($name);
        $user->setIsPharmaceutical($isPharmaceutical);
        $user->setState($state);

        if($user->getEmail() != $email){
            $userWithEmail = $em->getRepository('App:User')->findOneByEmail($email);

            if($userWithEmail != $user){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Email já está sendo utilizado'
                ]);
            }

            $user->setEmail($email);
            $user->setUsername($email);
        }

        if($user->getCpf() != $cpf){
            $userWithCPF = $em->getRepository('App:User')->findOneByCpf($cpf);

            if($userWithCPF != $user){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'CPF já está sendo utilizado'
                ]);
            }

            $user->setCpf(str_replace(['.', ' ', '-', '/'], '', $cpf));
        }

        if($isPharmaceutical){
            if($user->getCrf() != $crf && $user->getState() != ''){
                $userWithCrf = $em->getRepository('App:User')->findOneBy(['crf' => str_replace(['.', ' ', '-', '/'], '', $crf), 'state' => $user->getState()]);
                if($userWithCrf && $userWithCrf != $user){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'CRF já está sendo utilizado'
                    ]);
                }
                $user->setCrf(str_replace(['.', ' ', '-', '/'], '', $crf));
            } else if($user->getState() == '') {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Para cadastrar um CRF você precisa selecionar um Estado'
                ]);
            }
        }

        if(!$isPharmaceutical){
            if($user->getCnpj() != $cnpj){
                $pharmacy = $em->getRepository('App:Pharmacy')->findOneByCnpj(str_replace(['.', ' ', '-', '/'], '', $crf));
                if(!$pharmacy){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'Farmácia nâo encontrada.'
                    ]);
                }
                $user->setCnpj(str_replace(['.', ' ', '-', '/'], '', $cnpj));
            }
        }

        $user->setNewsletter($newsletter == null ? false : $newsletter);
        $user->setIsActive(true);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());

        $roleFarma = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
        $roleBalcs = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');

        if($isPharmaceutical == true){
            $user->removeRole($roleBalcs);
            $user->addRole($roleFarma);
        } else if($isPharmaceutical === false) {
            $user->removeRole($roleFarma);
            $user->addRole($roleBalcs);
        }

        $em->persist($user);
        $em->flush();

        $userInfo = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'cpf' => $user->getCpf(),
            'crf' => $user->getCrf(),
            'cnpj' => $user->getCnpj(),
            'isActive' => $user->getIsActive(),
            'isPharmaceutical' => $user->getIsPharmaceutical(),
            'appAccessToken' => $user->getAppAccessToken(),
            'newsletter' => $user->getNewsletter(),
            'state' => $user->getState(),
            'hasSocialLogin' => $user->getFacebookData() ? true : ($user->getGoogleData() ? true : false)
        ];

        return new JsonResponse([
            'status' => true,
            'user' => $userInfo
        ]);

    }

    /**
     * @Route("/change-password", name="app-change-password")
     */
    public function changePassword(Request $request,UserPasswordEncoderInterface $encoder){
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $id = isset($dataPost['id']) ? $dataPost['id'] : null;
        $appAccessToken = isset($dataPost['appAccessToken']) ? $dataPost['appAccessToken'] : null;
        $passwordOld = isset($dataPost['po']) ? $dataPost['po'] : null;
        $passwordNew = isset($dataPost['pn']) ? $dataPost['pn'] : null;


        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['id' => $id, 'appAccessToken' => $appAccessToken]);
        
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado'
            ]);
        }

        $bool = ($encoder->isPasswordValid($user, $passwordOld)) ? true : false;

        if(!$bool){
            return new JsonResponse([
                'status' => false,
                'message' => 'Senha incorreta'
            ]);
        }
        
        $user->setPassword($encoder->encodePassword($user, $passwordNew));
        $em->persist($user);
        $em->flush();

        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/check-email", name="app-check-email")
     */
    public function checkEmail(Request $request){
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $email = isset($dataPost['e']) ? $dataPost['e'] : null;

        if(!$email){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneByEmail($email);

        if($user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Email está sendo utilizado'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    /**
     * @Route("/check-cpf-crf", name="app-check-cpf-crf")
     */
    public function checkUsername(Request $request){
        
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
       
        $crf = isset($dataPost['r']) ? $dataPost['r'] : null;
        $cpf = isset($dataPost['p']) ? $dataPost['p'] : null;
        $cnpj = isset($dataPost['c']) ? $dataPost['c'] : null;
        $type = isset($dataPost['t']) ? $dataPost['t'] : null;
        $state = isset($dataPost['s']) ? $dataPost['s'] : null;

        if(!$type){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados type'
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $user = null;

        if($type == 'r'){
            if(!$crf || !$state){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Paramêtros não encontrados crf or state'
                ]);
            }

            if($cpf && $cpf != ''){
                $user = $em->getRepository('App:User')->findOneByCpf($cpf);
                if($user){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'CPF já está sendo utilizado'
                    ]);
                }
            } else {
                $user = $em->getRepository('App:User')->findOneBy(['crf' => $crf, 'state' => $state]);
                if($user){
                    return new JsonResponse([
                        'status' => false,
                        'message' => 'CRF já está sendo utilizado'
                    ]);
                }
            }
        } else if($type == 'p'){

            if(!$cpf || !$cnpj){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Paramêtros não encontrados cpf or cnpj'
                ]);
            }
            $user = $em->getRepository('App:User')->findOneByCpf($cpf);
            if($user){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'CPF já está sendo utilizado'
                ]);
            }
            
            $pharmacy = $em->getRepository('App:Pharmacy')->findOneByCnpj(str_replace(['.', ' ', '-', '/'], '', $cnpj));
            if(!$pharmacy){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Farmácia não encontrada'
                ]);
            }

        }
        
        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/check-cnpj", name="app-check-cnpj")
     */
    public function checkCnpj(Request $request)
    {
        
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
       
        $cnpj = isset($dataPost['c']) ? $dataPost['c'] : null;

        if(!$cnpj){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }
        
        $em = $this->getDoctrine()->getManager();
        $pharmacy = $em->getRepository('App:Pharmacy')->findOneByCnpj(str_replace(['.', ' ', '-', '/'], '', $cnpj));

        if(!$pharmacy){
            return new JsonResponse([
                'status' => false,
                'message' => 'Farmácia não encontrada'
            ]);
        } else {
            return new JsonResponse([
                'status' => true,
            ]);
        }
    }

    /**
     * @Route("/contact", name="app-contact")
     */
    public function sendContact(Request $request, \Swift_Mailer $mailer)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
       
        $name = isset($dataPost['name']) ? $dataPost['name'] : null;
        $subject = isset($dataPost['subject']) ? $dataPost['subject'] : null;
        $email = isset($dataPost['email']) ? $dataPost['email'] : null;
        $message = isset($dataPost['message']) ? $dataPost['message'] : null;

        if(!$name || !$subject || !$email || !$message ){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados'
            ]);
        }

        if($name == '' || $subject == '' || $email == '' || $message == ''){
            return new JsonResponse([
                'status' => false,
                'message' => 'Todos os campos são obrigatórios'
            ]);
        }

        $logger = new Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

        if($this->getParameter('APP_ENV') == 'dev'){
            $message = (new \Swift_Message())->setFrom(['cappuccinodeveloper@gmail.com'])
                ->setTo(['rodrigo.taira@gmail.com', 'lucapcastro@gmail.com','wellington.santos@cappuccinodigital.com.br', 'ana.morales@cappuccinodigital.com.br']);
        } else {
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br'])
                ->setTo(['falecom@hypera.com.br']);
        }

        $message
        ->setReplyTo($dataPost['email'])
        ->setSubject('Neo Scan - ' . $dataPost['subject'])
        ->setBody(
            $this->renderView(
                'home/contact-email.html.twig',
                ['form' => $dataPost]
            ),
            'text/html'
        );

        $response = $mailer->send($message);

        $contact = new Contact();
        $contact->setName($dataPost['name']);
        $contact->setEmail($dataPost['email']);
        $contact->setSubject($dataPost['subject']);
        $contact->setMessage($dataPost['message']);
        $contact->setCreatedAt(new \DateTime());
        
        if($response == 1){
            $contact->setWasSent(true);
        } else {
            $contact->setWasSent(false);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();

        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/remember-password", name="app-remember-password")
     */
    public function rememberPassword(Request $request, \Swift_Mailer $mailer, UserPasswordEncoderInterface $encoder)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);
        $email = isset($dataPost['email']) ? $dataPost['email'] : null;
       
        if (!$email) {
            return new JsonResponse([
                'status' => false,
                'message' => 'E-mail não encontrado.'
            ]); 
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneByEmail($email);
       
        if(!$user){
            return new JsonResponse([
                'status' => true,
                'message' => 'Um email foi enviado com sua nova senha'
            ]);
        }
        
        $token = sha1($user->getId().$user->getCreatedAt()->getTimestamp().$user->getSalt().$user->getPassword()).dechex(time()).dechex($user->getId());
        
        $expiresAt = new \DateTime();
        $expiresAt->add(new \DateInterval('PT8H'));
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneByUser($user);

        if(!$newPassword){
            $newPassword = new UserNewPassword();
        }
        
        $newPassword->setExpiresAt($expiresAt);
        $newPassword->setToken($token);
        $newPassword->setIsValid(true);
        $newPassword->setUser($user);

        $em->persist($newPassword);
        $em->flush();

        
        $logger = new Swift_Plugins_Loggers_ArrayLogger();
        
        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

        if($this->getParameter('APP_ENV') == 'dev'){
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
        } else {
            $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
        }


        $message->setTo($user->getEmail())
        ->setSubject('Recuperar Senha')
        ->setBody(
            $this->renderView(
                'home/renew-password.html.twig',
                ['newPassword' => $newPassword, 'user' => $user]
            ),
            'text/html'
        );

        $response = $mailer->send($message);
       
        if($response == 1){
            return new JsonResponse([
                'status' => true,
                'message' => 'Um email foi enviado com sua nova senha'
            ]);
        }

        return new JsonResponse([
            'status' => false,
            'message' => 'Algo deu errado, tente novamente mais tarde'
        ]);
    }

    private function generateShareCode($user)
    {
        $name = explode(' ', $user->getName());
        $better_token = substr(md5(uniqid(rand(), true)),rand(0,26),5);
        return strtolower($name[0] . $better_token);
    }

    private function saveAvatar($user, $url)
    {
        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/user')) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/user', 777);
        }

        $fileName =  $user->getId() . '.jpg';
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/user/' . $fileName;

        file_put_contents($filePath, file_get_contents($url));

        return 'uploads/user/' . $fileName;
        
    }
}
