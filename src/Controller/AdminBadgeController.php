<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Badge;
use App\Entity\BadgeLevel;

/**
 * @Route("/admin/badge")
 */
class AdminBadgeController extends Controller
{
    /**
     * @Route("", name="admin_badge_list")
     */
    public function adminBadgeList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Badge')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $badges = $builder->getQuery()->getResult();

        $badges = $this->get('knp_paginator')->paginate(
            $badges,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_badge/list.html.twig', [
            'badges' => $badges
        ]);
    }

    /**
     * @Route("/novo", name="admin_badge_new")
     */
    public function adminBagdeNew(Request $request)
    {

        if ($request->isMethod('GET')) {
            return $this->render('admin_badge/new.html.twig', []);
        }

        $badgeInfo['info'] = $request->request->get('badge');
        $badgeInfo['files'] = $request->files->get('badge');

        $levelInfo['info'] = $request->request->get('level');
        $levelInfo['files'] = $request->files->get('level');

        $badge = $this->saveBadge($badgeInfo);
        $this->saveBadgeLevel($badge, $levelInfo);

        return $this->redirectToRoute('admin_badge_list');
    }

    /**
     * @Route("/{id}", name="admin_badge_view")
     */
    public function adminBagdeView($id)
    {
        $em = $this->getDoctrine()->getManager();
        $badge = $em->getRepository('App:Badge')->find($id);

        return $this->render('admin_badge/view.html.twig', [
            'badge' => $badge,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_badge_edit")
     */
    public function adminBagdeEdit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $badge = $em->getRepository('App:Badge')->find($id);

        if ($request->isMethod('GET')) {
            return $this->render('admin_badge/edit.html.twig', [
                'badge' => $badge,
            ]);
        }

        $badgeInfo['info'] = $request->request->get('badge');
        $badgeInfo['files'] = $request->files->get('badge');

        $levelInfo['info'] = $request->request->get('level');
        $levelInfo['files'] = $request->files->get('level');

        $badge = $this->saveBadge($badgeInfo);
        $this->saveBadgeLevel($badge, $levelInfo);

        return $this->redirectToRoute('admin_badge_list');
    }

    /**
     * @Route("/remove-level/{id}", name="admin_badge_remove_level")
     */
    public function removeLevel($id)
    {
        $em = $this->getDoctrine()->getManager();
        $level =  $em->getRepository('App:BadgeLevel')->find($id);

        if (!$level) {
            return new JsonResponse([
                'status' => true
            ]);
        }

        if ($level->getImage() != null && $level->getImage() != '') {
            $this->removeFile($level->getImage());
        }

        if ($level->getImageMobile() != null && $level->getImageMobile() != '') {
            $this->removeFile($level->getImageMobile());
        }

        $em->remove($level);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    private function saveBadge($badgeInfo)
    {


        $em = $this->getDoctrine()->getManager();
        if (isset($badgeInfo['info']['id'])) {
            $badge = $em->getRepository('App:Badge')->find($badgeInfo['info']['id']);
        } else {
            $badge = new Badge();
            $badge->setCreatedAt(new \DateTime());
        }


        $badge->setName($badgeInfo['info']['name']);
        $badge->setShortDescription($badgeInfo['info']['shortDescription']);
        $badge->setDescription(html_entity_decode($badgeInfo['info']['description'], ENT_COMPAT, 'UTF-8'));
        $badge->setUpdatedAt(new \DateTime());
        $badge->setIsActive(isset($badgeInfo['info']['isActive']) ? $badgeInfo['info']['isActive'] : false);
        $badge->setIsOnlyToPharmaceutical(isset($badgeInfo['info']['isOnlyToPharmaceutical']) ? $badgeInfo['info']['isOnlyToPharmaceutical'] : false);
        $badge->setGivenPointPharmaceutical($badgeInfo['info']['givenPointPharmaceutical']);
        $badge->setGivenPointClerk($badgeInfo['info']['givenPointClerk']);

        $badge->setMetaTitle($badgeInfo['info']['metaTitle']);
        $badge->setMetaDescription($badgeInfo['info']['metaDescription']);
        $badge->setImageMetaTitle($badgeInfo['info']['imageMetaTitle']);
        $badge->setImageMetaDescription($badgeInfo['info']['imageMetaDescription']);
        $badge->setImageMobileMetaTitle($badgeInfo['info']['imageMobileMetaTitle']);
        $badge->setImageMobileMetaDescription($badgeInfo['info']['imageMobileMetaDescription']);

        $em = $this->getDoctrine()->getManager();

        if (!isset($badgeInfo['info']['id'])) {
            if (isset($badgeInfo['files']['image'])) {
                if (filesize($badgeInfo['files']['image']) != false) {
                    if (filesize($badgeInfo['files']['image']) > 1048576) {

                        $this->addFlash("alert", "Imagem maior que o permitido.");

                        return $this->redirectToRoute('admin_badge_new');
                    }

                    $badge->setImage($this->saveFile($badgeInfo['files']['image'], $badge, 'badge'));
                } else {

                    $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");

                    return $this->redirectToRoute('admin_badge_new');
                }
            } else {
                $badge->setImage('_dist/image/default_desktop.png');
            }

            if (isset($badgeInfo['files']['imageMobile'])) {
                if (filesize($badgeInfo['files']['imageMobile']) != false) {

                    if (filesize($badgeInfo['files']['imageMobile']) > 1048576) {

                        $this->addFlash("alert", "Imagem maior que o permitido.");

                        return $this->redirectToRoute('admin_badge_new');
                    }
                    $badge->setImageMobile($this->saveFile($badgeInfo['files']['imageMobile'], $badge, 'badge'));
                } else {


                    $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                    return $this->redirectToRoute('admin_badge_new');
                }
            } else {
                $badge->setImageMobile('_dist/image/default_mobile.png');
            }
        } else {
            if (isset($badgeInfo['files']['image'])) {
                if ($badge->getImage() != null && $badge->getImage() != '') {
                    $this->removeFile($badge->getImage());
                }
                $badge->setImage($this->saveFile($badgeInfo['files']['image'], $badge, 'badge'));
            }

            if (isset($badgeInfo['files']['imageMobile'])) {
                if ($badge->getImageMobile() != null && $badge->getImageMobile() != '') {
                    $this->removeFile($badge->getImageMobile());
                }
                $badge->setImageMobile($this->saveFile($badgeInfo['files']['imageMobile'], $badge, 'badge'));
            }
        }

        $em->persist($badge);
        $em->flush();

        return $badge;
    }

    private function saveBadgeLevel($badge, $badgeLevels)
    {
        if (!is_array($badgeLevels) || $badgeLevels['info'] == null) {
            return false;
        }

        $em = $this->getDoctrine()->getManager();

        foreach ($badgeLevels['info'] as $key => $level) {
            $info = $level;
            $file = isset($badgeLevels['files'][$key]) ? $badgeLevels['files'][$key] : null;

            if (isset($info['id'])) {
                $badgeLevel = $em->getRepository('App:BadgeLevel')->find($info['id']);
            } else {
                $badgeLevel = new BadgeLevel();
            }

            $badgeLevel->setName($info['name']);
            $badgeLevel->setMinPointPharmaceutical($info['minPointPharmaceutical']);
            $badgeLevel->setMinPointClerk($info['minPointClerk']);
            $badgeLevel->setImageMetaTitle($info['imageMetaTitle']);
            $badgeLevel->setImageMetaDescription($info['imageMetaDescription']);
            $badgeLevel->setImageMobileMetaTitle($info['imageMobileMetaTitle']);
            $badgeLevel->setImageMobileMetaDescription($info['imageMobileMetaDescription']);

            $badgeLevel->setBadge($badge);

            if ($file) {
                if (!isset($info['id'])) {
                    if (isset($file['image'])) {
                        $badgeLevel->setImage($this->saveFile($file['image'], $badge, 'badge'));
                    } else {
                        $badgeLevel->setImage('_dist/image/default_desktop.png');
                    }

                    if (isset($file['imageMobile'])) {
                        $badgeLevel->setImageMobile($this->saveFile($file['imageMobile'], $badge, 'badge'));
                    } else {
                        $badgeLevel->setImageMobile('_dist/image/default_mobile.png');
                    }
                } else {
                    if (isset($file['image'])) {
                        if ($badgeLevel->getImage() != null && $badgeLevel->getImage() != '') {
                            $this->removeFile($badgeLevel->getImage());
                        }
                        $badgeLevel->setImage($this->saveFile($file['image'], $badge, 'badge'));
                    }

                    if (isset($file['imageMobile'])) {
                        if ($badgeLevel->getImageMobile() != null && $badgeLevel->getImageMobile() != '') {
                            $this->removeFile($badgeLevel->getImageMobile());
                        }
                        $badgeLevel->setImageMobile($this->saveFile($file['imageMobile'], $badge, 'badge'));
                    }
                }
            }

            $em->persist($badgeLevel);
            $em->flush();
        }

        return true;
    }

    private function saveFile($file, $post, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $post->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
