<?php

namespace App\Controller;

use App\Entity\PromoThirtyDayMovementMovies;
use App\Entity\PromoThirtyDayMovementUserMovie;
use App\Entity\UserPointPromoThirtyDayMovement;
use App\Entity\UserPromoThirtyDayMovement;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Cookie;

class PromoThirtyDayMovementController extends Controller
{
    /**
     * @Route("/30dias/pontos", name="promo_thirty_day_movement")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('q') == 'search') {
            //dump($request->request->get('q')); die();
            $code = trim($request->request->get('cp'));
            //dump($code); die();
            $thirty_day = $em->getRepository('App:PromoThirtyDayMovement')->findOneBy(['code' => $code]);
         
            if (!$thirty_day) {
                return new JsonResponse(['status' => false, 'message' => 'Código não encontrado.']);  
            }
          
            $points = $em->getRepository('App:UserInvite')->findBy(['shareCode' => $code]);
            //dump($points); die();
           
            return new JsonResponse([
                'status' => true,
                'points' => count($points) > 0 ? count($points) . ' pontos' : 0,
            ]);
        }

        return $this->render('promo_thirty_day_movement/index.html.twig', [
            'controller_name' => '$points',
        ]);
    }

    /**
     * @Route("/desafio-30-dias-de-movimento/cadastro", name="promo_thirty_day_movement_register")
     */
     public function register(Request $request)
     {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $this->getUser()->getId()]);
        
        if ($is_register_promo) {
            return $this->redirectToRoute('promo_thirty_day_movement_landpage');
        }

        return $this->render('promo_thirty_day_movement/register.html.twig', [
            //'controller_name' => '$points',
        ]);
     }

    /**
     * @Route("/desafio-30-dias-de-movimento/participar", name="promo_thirty_day_movement_create")
     */
    public function create(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        if($request->isMethod('GET')){
            return $this->render('home/login.html.twig', [
                
            ]);
        }

        $errors = [];

        if (empty($request->request->get('u'))) {
            $errors['alert'] = 'Para participar da promoção precisa estar logado.';
        }

        if (empty($request->request->get('whatsapp'))) {
            $errors['whatsapp'] = 'O preenchimento do Whatsapp é obrigatório​';
        }

        if (empty($request->request->get('instagram'))) {
            $errors['instagram'] = 'O preenchimento do Instragram é obrigatório​';
        }

        if (empty($request->request->get('regulament'))) {
            $errors['regulament'] = 'A leitura e aceite do regulamento são obrigatórios​';
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($request->request->get('u')); 
        
        if (!$user) {
            $errors['alert'] = 'Para participar da promoção precisa estar logado.';
        }
  
        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'errors' => $errors
            ]);
        }

        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $request->request->get('u')]);

        if ($is_register_promo) {
            return new JsonResponse([
                'status' => true,
                'message' => 'Você está cadastrado na promoção de 30 dias de movimento.'
            ]);
        }

        //marca 10 ponto por se cadastrar na promoção 
        $user_thirty_day = new UserPromoThirtyDayMovement();
        $user_thirty_day->setUser($user);
        $user_thirty_day->setWhatsapp($request->request->get('whatsapp'));
        $user_thirty_day->setInstagram($request->request->get('instagram'));
        $user_thirty_day->setIsRegulament($request->request->get('regulament'));
        $user_thirty_day->setCreatedAt(new DateTime());
        $user_thirty_day->setUpdatedAt(new DateTime());

        $em->persist($user_thirty_day);
        $em->flush();

        $user_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $request->request->get('u')]); //query para pegar id do user da promo para relacionar com a table de pontos abaixo
        $user_point_thirty_day = new UserPointPromoThirtyDayMovement();
        $user_point_thirty_day->setUserPromoThirtyDayMovement($user_register_promo); //verificar se está cadastrado para marcar ponto
        $user_point_thirty_day->setUser($user);
        $user_point_thirty_day->setPoint(10);
        $user_point_thirty_day->setPointType('cadastro-promo-30-dias-de-movimento');
        $user_point_thirty_day->setPointTypeDescription('Usuário cadastrado na promoção de 30 dias de movimento');
        $user_point_thirty_day->setIsPharmaceutical($user->getIsPharmaceutical() == 1 ? true : false);
        $user_point_thirty_day->setIsPharmaceuticalContent($user->getIsPharmaceutical() == 0 ? true : false);
        $user_point_thirty_day->setIsMonthMatch(false);
        $user_point_thirty_day->setCreatedAt(new DateTime());
        $user_point_thirty_day->setUpdatedAt(new DateTime());

        $em->persist($user_point_thirty_day);
        $em->flush();

        //marca 05 ponto por se cadastrar até a data de 01 de março de 2020
        if (date('Y-m-d') <= date('Y-m-d', strtotime('2020-03-01'))) { 
            $user_point_thirty_day = new UserPointPromoThirtyDayMovement(); 
            $user_point_thirty_day->setUserPromoThirtyDayMovement($user_register_promo); //verificar se está cadastrado para marcar ponto
            $user_point_thirty_day->setUser($user);
            $user_point_thirty_day->setPoint(5); //usuário cadastrado antes de 05 de março de 2020
            $user_point_thirty_day->setPointType('inscricao-antes-de-01-marco-2020');
            $user_point_thirty_day->setPointTypeDescription('Usuário cadastrado antes de 01 de março de 2020');
            $user_point_thirty_day->setIsPharmaceutical($user->getIsPharmaceutical() == 1 ? true : false);
            $user_point_thirty_day->setIsPharmaceuticalContent($user->getIsPharmaceutical() == 0 ? true : false);
            $user_point_thirty_day->setIsMonthMatch(true);
            $user_point_thirty_day->setCreatedAt(new DateTime());
            $user_point_thirty_day->setUpdatedAt(new DateTime());

            $em->persist($user_point_thirty_day);
            $em->flush();
        }

        //marca 05 ponto por indicação de amigo 
        if ($user->getInvite()) {
            $userInvide = $em->getRepository('App:User')->findOneby(['shareCode' => $user->getInvite()->getShareCode()]);

            if ($userInvide) {
                $user_register_promo_invide = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $userInvide->getId()]);
        
                if ($user_register_promo_invide) {
                    $user_point_thirty_day = new UserPointPromoThirtyDayMovement(); 
                    $user_point_thirty_day->setUserPromoThirtyDayMovement($user_register_promo_invide); 
                    $user_point_thirty_day->setUser($userInvide);
                    $user_point_thirty_day->setPoint(5);
                    $user_point_thirty_day->setPointType('indicacao-de-amigo');
                    $user_point_thirty_day->setPointTypeDescription('Usuário indicou amigo para se cadastrar na plataforma e na campanha');
                    $user_point_thirty_day->setIsPharmaceutical($userInvide->getIsPharmaceutical() == 1 ? true : false);
                    $user_point_thirty_day->setIsPharmaceuticalContent($userInvide->getIsPharmaceutical() == 0 ? true : false);
                    $user_point_thirty_day->setIsMonthMatch(false);
                    $user_point_thirty_day->setCreatedAt(new DateTime());
                    $user_point_thirty_day->setUpdatedAt(new DateTime());

                    $em->persist($user_point_thirty_day);
                    $em->flush();
                }
            }
        }
        //fim indicação de amigo

        $is_user_point = $em->getRepository('App:UserPointPromoThirtyDayMovement')->findBy(['user_promo_thirty_day_movement' => $user_register_promo->getId()]);
     
        if ($user_register_promo && $is_user_point) {
            return new JsonResponse([
                'status' => true,
                'message' => 'Cadastrado com sucesso.'
            ]);
        }

        $errors['alert'] = 'Ops! tente novamente mais tarde.';

        return new JsonResponse([
            'status' => false,
            'message' => 'Error',
            'errors' => $errors
        ]); 
      
    }

    /**
     * @Route("/desafio-30-dias-de-movimento", name="promo_thirty_day_movement_landpage")
     */
    public function show(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $this->getUser()->getId()]);

        if ($is_register_promo) {
            return $this->render('promo_thirty_day_movement/show.html.twig', [
            //'controller_name' => '$points',
            ]);
        }

        /** Gravando a referencia do conteúdo */
        $url_reference = $request->getUri();
        $response = $this->redirectToRoute('home-login');
        $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

        return $response;
        /*** END  */
    }

    /**
     * @Route("/desafio-30-dias-de-movimento/regulamento", name="promo_thirty_day_movement_regulation")
     */
    public function regulation(Request $request)
    {
        return $this->render('promo_thirty_day_movement/regulation.html.twig', [
            //'controller_name' => '$points',
        ]);
    }

    /**
     * @Route("/desafio-30-dias-de-movimento/generate-point-movie", name="promo_thirty_day_movement_generate_point_movie")
     */
    public function generatePointMovie(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        if($request->isMethod('GET')){
            return $this->render('home/login.html.twig', [
                
            ]);
        }
       
        $movie_id = $request->request->has('movie') ? $request->request->get('movie') : null;
        $user_id = $request->request->has('u') ? $request->request->get('u') : null;

        $errors = array();
        
        if (!$user_id) {
            $errors[] = 'Para participar da promoção precisa estar logado.';
        }

        if (!$movie_id) {
            $errors[] = 'É necessario assistir o vídeo para marca ponto.';
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($user_id); 
        
        if (!$user) {
            $errors[] = 'Usuário não encontrado.';
        }

        $user_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $user->getId()]);// verifico se está participando da promoção 

        if (!$user_register_promo) {
            $errors[] = 'Para marca ponto precisa estar participando da promoção.';
        }

        $movie = $em->getRepository('App:PromoThirtyDayMovementMovies')->findOneBy(['link' => $movie_id]);

        if (!$movie) {
            $errors[] = 'Vídeo não encontrado.';
        }
        
        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'errors' => $errors
            ]);
        }

        $user_movie = $em->getRepository('App:PromoThirtyDayMovementUserMovie')->findBy(['User' => $user, 'PromoThirtyDayMovementMovies' => $movie]);
       
        if (!$user_movie) {
            $user_point_thirty_day = new UserPointPromoThirtyDayMovement(); 
            $user_point_thirty_day->setUserPromoThirtyDayMovement($user_register_promo); //verificar se está cadastrado para marcar ponto
            $user_point_thirty_day->setUser($user);
            $user_point_thirty_day->setPoint(2);
            $user_point_thirty_day->setPointType('assistiu-video');
            $user_point_thirty_day->setPointTypeDescription('Usuário assistiu o vídeo de: ' . $movie->getTitle());
            $user_point_thirty_day->setIsPharmaceutical($user->getIsPharmaceutical() == 1 ? true : false);
            $user_point_thirty_day->setIsPharmaceuticalContent($user->getIsPharmaceutical() == 0 ? true : false);
            $user_point_thirty_day->setIsMonthMatch(false);
            $user_point_thirty_day->setCreatedAt(new DateTime());
            $user_point_thirty_day->setUpdatedAt(new DateTime());

            $em->persist($user_point_thirty_day);
            $em->flush();

            $user_movie = new PromoThirtyDayMovementUserMovie();
            $user_movie->setUserPointPromoThirtyDayMovement($user_point_thirty_day);
            $user_movie->setUser($user);
            $user_movie->setPromoThirtyDayMovementMovies($movie);
            $user_movie->setCreatedAt(new DateTime());
            $user_movie->setUpdatedAt(new DateTime());

            $em->persist($user_movie);
            $em->flush();
        }

        return new JsonResponse([
            'status' => true,
            'message' => 'Cadastrado com sucesso.'
        ]);  
    }


    /**
     * @Route("/desafio-30-dias-de-movimento/videos", name="promo_thirty_day_movement_videos")
     */
    public function videos(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $this->getUser()->getId()]);

        if (!$is_register_promo) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $query = $this->getDoctrine()
                ->getRepository('App:PromoThirtyDayMovementMovies')
                ->createQueryBuilder('m')
                ->getQuery();

        $movies = $query->getResult(Query::HYDRATE_ARRAY);
                 
        return $this->render('promo_thirty_day_movement/videos.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * @Route("/desafio-30-dias-de-movimento/ranking-balconista-e-farmaceutico", name="promo_thirty_day_movement_ranking_bf")
     */
    public function rankingClerkPharmaceutical(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $this->getUser()->getId()]);

        if (!$is_register_promo) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        //$users = $em->getRepository('App:PromoThirtyDayMovement')->findRankingFaramaceuticoBalconista();
        $ranking = $em->getRepository('App:PromoThirtyDayRankingClerkPharmaceutical')->createQueryBuilder('r')
                    ->select('r')
                    ->where('r.points > 0')
                    ->getQuery()
                    ->getResult();

        // Obtain a list of columns
        foreach ($ranking as $key => $row) {
            $points[$key]  = $row->getPoints();
            $data[$key] = $row->getCreatedAt();
        }

        array_multisort($points, SORT_DESC, $data, SORT_ASC, $ranking);

        return $this->render('promo_thirty_day_movement/ranking.html.twig', [
            'ranking' => $ranking,
        ]);
    }

    /**
     * @Route("/desafio-30-dias-de-movimento/atualiza-pontos-ranking-balconista-e-farmaceutico", name="promo_thirty_day_movement_update_ranking_bf")
     */
    public function updateRankingClerkPharmaceutical(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App:PromoThirtyDayMovement')->findRankingFaramaceuticoBalconista();

        if (!$users)  {
            return new JsonResponse([
                'status' => false,
                'message' => 'Ops! os pontos não foram atualizados'
            ]);
        }

        return new JsonResponse([
            'status' => true,
            'message' => 'Pontos atualizados com sucesso.'
        ]);
    }


    // /**
    //  * @Route("/desafio-30-dias-de-movimento/ranking-vendedores", name="promo_thirty_day_movement_rank")
    //  */
    // public function rank(Request $request)
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     $business = $em->getRepository('App:PromoThirtyDayMovement')->findRank();

    //     $time = date('d-m-Y s');
    //     $filename = "ranking-vendedores-{$time}.xls"; // File Name
    //     // Download file
    //     header("Content-Disposition: attachment; filename=\"$filename\"");
    //     header("Content-Type: application/vnd.ms-excel");

    //     //$flag = false;
    //     foreach ($business as $b) {
    //         // if (!$flag) {
    //         //     echo implode("\t", array_keys($row)) . "\r\n";
    //         //     $flag = true;
    //         // }
    //         echo implode("\t", array_values($b)) . "\r\n";
    //     }
    //    // dump($business); die();
    //     return new JsonResponse([
    //         'status' => true,
    //         'message' => 'Relatório gerado com sucesso.'
    //     ]);
    // }

    /**
     * @Route("/desafio-30-dias-de-movimento/export-ranking-balconista-e-farmaceutico", name="promo_thirty_day_movement_rank_bal_far")
     */
    public function rankBalFarma(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $business = $em->getRepository('App:PromoThirtyDayMovement')->findRankBalFarma();

        $time = date('d-m-Y s');
        $filename = "ranking-balconista-e-farmaceutico-{$time}.xls"; // File Name
        // Download file
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        //$flag = false;
        foreach ($business as $b) {
            // if (!$flag) {
            //     echo implode("\t", array_keys($row)) . "\r\n";
            //     $flag = true;
            // }
            echo implode("\t", array_values($b)) . "\r\n";
        }
       // dump($business); die();
        return new JsonResponse([
            'status' => true,
            'message' => 'Relatório gerado com sucesso.'
        ]);
    }

    // /**
    //  * @Route("/desafio-30-dias-de-movimento/atualiza-pontos-vendedores", name="promo_thirty_day_movement_point_update_vendas")
    //  */
    // public function atualizaPointVendas(Request $request)
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     $business = $em->getRepository('App:PromoThirtyDayMovement')->findRank();

    //     if (!$business)  {
    //         return new JsonResponse([
    //             'status' => false,
    //             'message' => 'Ops! os pontos não foram atualizados'
    //         ]);
    //     }

    //     return new JsonResponse([
    //         'status' => true,
    //         'message' => 'Pontos atualizados com sucesso.'
    //     ]);
    // } 

    /**
     * @Route("/desafio-30-dias-de-movimento/atualiza-pontos-vendedores", name="promo_thirty_day_movement_point_update_vendas")
     */
    public function cronUpdateVendedores(Request $request)
    {
        if ($request->query->get('q')) {
            $em = $this->getDoctrine()->getManager();
            $em->getRepository('App:PromoThirtyDayMovement')->findRank();

            $total = $em->getRepository('App:PromoThirtyDayMovement')->findAll();
            $send = $em->getRepository('App:PromoThirtyDayMovement')->vendedoresAtualizados();
           
            if (count($total) == $send) {
                $response = array(
                    'status' => true,
                    'message' => 'Todos os vendedores tiveram as suas pontuação atualizadas com sucesso.'
                );
            } else {
                $response = array(
                    'status' => true,
                    'quantity_sent' => $send,
                    'number' => $total,
                    'message' => 'OK',
                );
            }

            return new JsonResponse($response);
        }

        return $this->render('promo_thirty_day_movement/cron-upadate-vendedores.html.twig');
    }
    
    /**
     * @Route("/desafio-30-dias-de-movimento/ganhadores-da-semana", name="promo_thirty_day_movement_winners_week")
     */
    public function winnersWeek(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $is_register_promo = $em->getRepository('App:UserPromoThirtyDayMovement')->findOneBy(['user' => $this->getUser()->getId()]);

        if (!$is_register_promo) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }
        
        return $this->render('promo_thirty_day_movement/winners-week.html.twig');
    }



   
}
