<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Banner;

/**
 * @Route("/admin/banner")
 */
class AdminBannerController extends Controller
{
    /**
     * @Route("", name="admin_banner_list")
     */
    public function list(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:Banner')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $banners = $builder->getQuery()->getResult();

        $banners = $this->get('knp_paginator')->paginate(
            $banners,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_banner/list.html.twig', [
            'banners' => $banners
        ]);
    }

    /**
     * @Route("/novo", name="admin_banner_new")
     */
    public function new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:BannerType')->findBy(['isActive' => true]);

        if ($request->isMethod('GET')) {
            return $this->render('admin_banner/new.html.twig', [
                'types' => $types
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $files = $request->files->get('banner');
        $bannerInfo = $request->request->get('banner');

        $banner = new Banner();
        $banner->setTitle($bannerInfo['title']);
        $banner->setDescription($bannerInfo['description']);
        $banner->setIsActive(isset($bannerInfo['isActive']) ? true : false);
        $banner->setIsLogged(isset($bannerInfo['isLogged']) ? true : false);
        $banner->setIsPromo(isset($bannerInfo['isPromo']) ? true : false);
        $banner->setLink($bannerInfo['link']);
        $banner->setGaEvent($bannerInfo['ga_event']);
        $banner->setPosition($bannerInfo['position']);
        $banner->setCreatedAt(new \DateTime());
        $banner->setUpdatedAt(new \DateTime());

        $type = $em->getRepository('App:BannerType')->find($bannerInfo['type']);

        $banner->setType($type);

        $em->persist($banner);
        $em->flush();


        if (isset($files['image'])) {
            if (filesize($files['image']) != false) {
           
                if (filesize($files['image']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_banner_new');
                }
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR ARQUIVO. POR FAVOR SELECIONE OUTRO.");

                return $this->redirectToRoute('admin_banner_new');
            }

            $banner->setImage($this->saveFile($files['image'], $banner, 'banner'));
        } else {
            $banner->setImage('_dist/image/default_desktop.png');
        }

        if (isset($files['imageMobile'])) {
            if (filesize($files['imageMobile']) != false) {

                if (filesize($files['imageMobile']) > 1048576) {
                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_banner_new');
                }
            } else {
                $this->addFlash("alert", "ERRO AO IMPORTAR ARQUIVO. POR FAVOR SELECIONE OUTRO.");

                return $this->redirectToRoute('admin_banner_new');
            }

            $banner->setImageMobile($this->saveFile($files['imageMobile'], $banner, 'banner'));
        } else {
            $banner->setImageMobile('_dist/image/default_mobile.png');
        }

        $em->persist($banner);
        $em->flush();
    
        return $this->redirectToRoute('admin_banner_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_banner_edit")
     */
    public function edit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:BannerType')->findBy(['isActive' => true]);
        $banner = $em->getRepository('App:Banner')->find($id);

        if ($request->isMethod('GET')) {
            return $this->render('admin_banner/edit.html.twig', [
                'types' => $types,
                'banner' => $banner
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $files = $request->files->get('banner');
        $bannerInfo = $request->request->get('banner');

        $banner->setTitle($bannerInfo['title']);
        $banner->setDescription($bannerInfo['description']);
        $banner->setIsActive(isset($bannerInfo['isActive']) ? true : false);
        $banner->setIsLogged(isset($bannerInfo['isLogged']) ? true : false);
        $banner->setIsPromo(isset($bannerInfo['isPromo']) ? true : false);
        $banner->setLink($bannerInfo['link']);
        $banner->setGaEvent($bannerInfo['ga_event']);
        $banner->setPosition($bannerInfo['position']);
        $banner->setUpdatedAt(new \DateTime());

        $type = $em->getRepository('App:BannerType')->find($bannerInfo['type']);

        $banner->setType($type);

        if (isset($files['image'])) {

            if ($banner->getImage() != null && $banner->getImage() != '') {
                $this->removeFile($banner->getImage());
            }

            $banner->setImage($this->saveFile($files['image'], $banner, 'banner'));
        }

        if (isset($files['imageMobile'])) {
            if ($banner->getImageMobile() != null && $banner->getImageMobile() != '') {
                $this->removeFile($banner->getImageMobile());
            }

            $banner->setImageMobile($this->saveFile($files['imageMobile'], $banner, 'banner'));
        }

        $em->persist($banner);
        $em->flush();

        return $this->redirectToRoute('admin_banner_list');
    }

    private function saveFile($file, $post, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $post->getType()->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
