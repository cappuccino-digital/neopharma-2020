<?php

namespace App\Controller;

use App\Entity\MedalsGamification;
use App\Form\MedalsGamificationType;
use App\Repository\MedalsGamificationRepository;
use App\Service\FileUploader;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/medals/gamification")
 */
class AdminMedalsGamificationController extends Controller
{
    /**
     * @Route("/", name="admin_medals_gamification_index", methods={"GET"})
     */
    public function index(MedalsGamificationRepository $medalsGamificationRepository): Response
    {
        return $this->render('admin_medals_gamification/index.html.twig', [
            'medals_gamifications' => $medalsGamificationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_medals_gamification_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $medalsGamification = new MedalsGamification();
        $form = $this->createForm(MedalsGamificationType::class, $medalsGamification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $medalsGamification->setCreatedAt(new DateTime());
            $medalsGamification->setUpdatedAt(new DateTime());
            
            $entityManager->persist($medalsGamification);
            $entityManager->flush();

            return $this->redirectToRoute('admin_medals_gamification_index');
        }

        return $this->render('admin_medals_gamification/new.html.twig', [
            'medals_gamification' => $medalsGamification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_medals_gamification_show", methods={"GET"})
     */
    public function show(MedalsGamification $medalsGamification): Response
    {
        return $this->render('admin_medals_gamification/show.html.twig', [
            'medals_gamification' => $medalsGamification,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_medals_gamification_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MedalsGamification $medalsGamification, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(MedalsGamificationType::class, $medalsGamification);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        $image_old = $medalsGamification->getIcon();

        if ($form->isSubmitted() && $form->isValid()) {
           
            $imageFile = $form->get('icon')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'icon');
                $medalsGamification->setIcon($fileName);
            } else {
                $medalsGamification->setIcon($image_old);
            }

            $medalsGamification->setUpdatedAt(new DateTime());
          
            $entityManager->persist($medalsGamification);
            $entityManager->flush();

            return $this->redirectToRoute('admin_medals_gamification_index');
        }

        return $this->render('admin_medals_gamification/edit.html.twig', [
            'medals_gamification' => $medalsGamification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_medals_gamification_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MedalsGamification $medalsGamification): Response
    {
        if ($this->isCsrfTokenValid('delete'.$medalsGamification->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($medalsGamification);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_medals_gamification_index');
    }
}
