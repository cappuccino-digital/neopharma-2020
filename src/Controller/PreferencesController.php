<?php

namespace App\Controller;

use App\Entity\PreferentialChildren;
use App\Entity\UserPreferential;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ExcelenciaSonoraService;
use Symfony\Component\HttpFoundation\Cookie;
/**
 * @Route("/preferencias")
 */
class PreferencesController extends Controller
{
    /**
     * @Route("/", name="user-preferences")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));

            return $response;
            /*** END  */
        }
        
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

        $teams = [
            'América-MG',
            'Athletico',
            'Avaí',
            'Atlético-GO',
            'Atlético-MG',
            'Bahia',
            'Botafogo',
            'Bragantino',
            'Brasil de Pelotas',
            'Brusque',
            'Ceará',
            'Chapecoense',
            'Confiança',
            'Corinthians',
            'Curitiba',
            'CRB',
            'Cruzeiro',
            'CSA',
            'Cuiabá',
            'Fortaleza',
            'Flamengo',
            'Fluminense',
            'Goiás',
            'Grêmio',
            'Guarani',
            'Internacional',
            'Juventude',
            'Londrina',
            'Náutico',
            'Operário',
            'Palmeiras',
            'Ponte Preta',
            'Remo',
            'Sampaio Corrêa',
            'Santos',
            'São Paulo',
            'Sport',
            'Vasco',
            'Vila Nova',
            'Vitória',
            'Nenhum'
        ];

        return $this->render('preferences/index.html.twig', [
            'user_preferential' => $user_preferential,
            'teams' => $teams
        ]);
    }

    /**
     * @Route("/save", name="user-save-preferences")
     */
    public function save(Request $request): Response
    {
        $errors = array();

        $user = $this->getUser();

        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => "Faça o login novamente"
            ]);
        }

        $hobby = trim($request->request->get('hobby_other')) != '' ? $request->request->get('hobby_other') : trim($request->request->get('hobby'));
        $sport = trim($request->request->get('sport_other')) != '' ? $request->request->get('sport_other') : trim($request->request->get('sport'));
        $team_football = trim($request->request->get('team_football_other')) != '' ? $request->request->get('team_football_other') : trim($request->request->get('team_football'));
        $is_children = $request->request->get('is_children') == 1 ? true : false;
        $how_many = $request->request->get('how_many');
        $info_children = $request->request->get('info_children');
        
        //dump($request);
        //die(); 

        // if (!$hobby) {
        //     $errors['hobby'] = 'O hobby é obrigatório';
        // } 
        
        // if (!$sport) {
        //     $errors['sport'] = 'O esporte é obrigatório';
        // }

        // if (count($errors) > 0) {
        //     return new JsonResponse([
        //         'status' => false,
        //         'message' => 'Campos obrigatórios',
        //         'error' => $errors,
        //     ]);
        // }
        
        $em = $this->getDoctrine()->getManager();
        $user_preferential = $em->getRepository('App:UserPreferential')->findOneBy(['user' => $user]);

        if (!$user_preferential) {
            $user_preferential = new UserPreferential();
        }

        $user_preferential->setUser($user);
        $user_preferential->setHobby($hobby);
        $user_preferential->setSport($sport);
        $user_preferential->setTeamFootball($team_football);
        $user_preferential->setIsChildren($is_children);
        $user_preferential->setHowManyChildren($how_many);
        $user_preferential->setCreatedAt(new \DateTime());
        $user_preferential->setUpdatedAt(new \DateTime());

        $em->persist($user_preferential);
        $em->flush();

        //atualizando o time na tabela de usuário também
        $user->setTeamFootball($team_football);
        $em->persist($user);
        $em->flush();

        //se tem filhos
        if ($is_children == true && $how_many > 0) {
            for ($i = 1; $i <= $how_many; $i++) {
                $preferential_children = $em->getRepository('App:PreferentialChildren')->findOneBy(['user_preferential' => $user_preferential, 'children' => $i]);

                if (!$preferential_children) {
                    $preferential_children = new PreferentialChildren();
                    $preferential_children->setUserPreferential($user_preferential);
                    $preferential_children->setCreatedAt(new \DateTime());
                }
                
                //Outros campos
                $team_football = isset($info_children['team_football']['other'][$i]) != '' ? $info_children['team_football']['other'][$i] : $info_children['team_football'][$i];
                $sport =  isset($info_children['sport']['other'][$i]) != '' ? $info_children['sport']['other'][$i] : $info_children['sport'][$i];
                                       
                $preferential_children->setChildren($i);
                $preferential_children->setTeamFootball($team_football);
                $preferential_children->setSport($sport);
                $preferential_children->setAge($info_children['age'][$i]);
                $preferential_children->setUpdatedAt(new \DateTime());

                $em->persist($preferential_children);
                $em->flush();
            }
        } else {
            $preferential_children = $em->getRepository('App:PreferentialChildren')->findBy(['user_preferential' => $user_preferential]);
            foreach ($preferential_children as $children) {
                $em->remove($children);
                $em->flush();
            }
        }
        
        $em->getRepository('App:UserPointGamification')->checkRegisterComplete($user); //Gamificação interação
        
        $url = $request->getSchemeAndHttpHost();
        
        if ($user->getIsPharmacy()) {
            //SONORA enviar informações 
            $neopharma_excelencia = new ExcelenciaSonoraService(
                $this->getParameter('sonora_excelencia_secret'),
                $this->getParameter('sonora_excelencia_env')
            );
            $response = $neopharma_excelencia->impersonate($user);
            
            if ($response['status']) {
                $url = $response['url']; //redirecionar para o Excelência
            }
            //end 
        }

        return new JsonResponse([
            'status' => true,
            'url' => $url
        ]);
    }
}
