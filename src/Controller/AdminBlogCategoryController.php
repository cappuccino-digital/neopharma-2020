<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BlogPostCategory;

/**
 * @Route("/admin/blog/category")
 */
class AdminBlogCategoryController extends Controller
{
    /**
     * @Route("", name="admin_blog_category_list")
     */
    public function postCategoryList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:BlogPostCategory')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $categories = $builder->getQuery()->getResult();

        $categories = $this->get('knp_paginator')->paginate(
            $categories,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_blog_category/list.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/novo", name="admin_blog_category_new")
     */
    public function postCategoryNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_blog_category/new.html.twig', []);
        }

        $form = $request->request->get('form');


        $category = new BlogPostCategory();
        $category->setName($form['name']);
        $category->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $category->setCreatedAt(new \DateTime());
        $category->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return $this->redirectToRoute('admin_blog_category_list');
    }

    /**
     * @Route("/edit/{id}", name="admin_blog_category_edit")
     */
    public function postCategoryEdit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('App:BlogPostCategory')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_blog_category/edit.html.twig', [
                'category' => $category
            ]);
        }

        $form = $request->request->get('form');

        $category->setName($form['name']);
        $category->setIsActive(isset($form['isActive']) ? $form['isActive'] : false );
        $category->setUpdatedAt(new \DateTime());
        
        $em->persist($category);
        $em->flush();

        return $this->redirectToRoute('admin_blog_category_list');
    }
}
