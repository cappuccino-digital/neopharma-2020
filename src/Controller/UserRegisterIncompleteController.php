<?php

namespace App\Controller;

use App\Service\NeoPharmaService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swift_Plugins_LoggerPlugin;
use Swift_Plugins_Loggers_ArrayLogger;
class UserRegisterIncompleteController extends Controller
{
    /**
     * @Route("/usuarios/register/incomplete", name="admin_register_incomplete")
     */
    public function index(\Swift_Mailer $mailer)
    {
        set_time_limit(360);
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App:User')->findAll();

        $users_array = array();
        foreach ($users as $key => $user) :
            if (count($user->getRoles()) > 0) {
                foreach ($user->getRoles() as $role) {
                    if ($role->getRole() == 'ROLE_USER' && !$user->getIsRegisterComplete()) {
                        $users_array[] = ['id' => $user->getId(), 'name' => $user->getName(),'email' => $user->getEmail()];
                    } else if ($role->getRole() == 'ROLE_BALCAO' && !trim($user->getCnpj())) {
                        $users_array[] = ['id' => $user->getId(), 'name' => $user->getName(),'email' => $user->getEmail()];
                    } else if ($role->getRole() == 'ROLE_FARMACO' && !trim($user->getCrf())) {
                        $users_array[] = ['id' => $user->getId(), 'name' => $user->getName(),'email' => $user->getEmail()];
                    }
                }
            }
        endforeach;

        $email_test = array(
               ['id' => 1, 'name' => 'Wellington Santos', 'email' => 'wellington.santos@cappuccinodigital.com.br'],
               ['id' => 2, 'name' => 'Ana Morales', 'email' => 'ana.morales@cappuccinodigital.com.br'],
               ['id' => 3, 'name' => 'Caroline Veiga', 'email' => 'caroline.veiga@cappuccinodigital.com.br'],
               ['id' => 4, 'name' => 'Leonardo Bonavina', 'email' => 'Leonardo.Bonavina@cappuccinodigital.com.br'],
            );

        $isSendEmail = false;
        //if (count($users_array) > 0) {
        if (count($email_test) > 0) {
            //foreach ($users_array as $key => $user) {  //disparar e-mail para pessoas com cadastros incompletos
            foreach ($email_test as $key => $useriss) :  //disparar e-mail para pessoas com cadastros incompletos
                /** send e-mail */

                $logger = new Swift_Plugins_Loggers_ArrayLogger();
                $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
        
                if($this->getParameter('APP_ENV') == 'dev'){
                    $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
                } else {
                    $message = (new \Swift_Message())->setFrom(['nao-responda@portalneopharma.com.br']);
                }
                               
                $message->setTo($useriss['email'])
                ->setSubject('Complete o cadastro')
                ->setBody(
                    $this->renderView(
                        'email_mkt/complete-cadastro.html.twig',
                        ['user' => $user]
                    ),
                    'text/html'
                );

                $mailer->send($message);
                //$response = $mailer->send($message);
                    // if($response == 1){
                    //     $isSendEmail = true;
                    //    // $user->setIsSendEmail(true);
                    // } else {
                    //     $isSendEmail = false;
                    //     //$user->setIsSendEmail(false);
                    // }
                    
                /** end */      
            endforeach;
            
        }
     
        // return new JsonResponse([
        //     'status' => true,
        //     //'message' => "({count($users_array)}) e-mails enviados com sucesso!"
        //     'message' => "E-mails enviados com sucesso!"
        // ]);

        return $this->render('admin_register_incomplete/index.html.twig', [
            'controller_name' => 'AdminRegisterIncompleteController',
        ]);
    }
    // /**
    //  * @Route("/usuarios/registros-incomplete", name="user_register_incomplete")
    //  */
    // public function index()
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     $users = $em->getRepository('App:User')->findAll();
        
    //     $user_cpf_invalids = [];
    //     foreach ($users as $user) {
    //         if (trim($user->getCpf())) {
    //             if (!$this->validaCPF($user->getCpf())) {
    //                 $user_cpf_invalids[] = array(
    //                     'user_id' => $user->getId(),
    //                     'name' => $user->getName(),
    //                     'cpf' => $user->getCpf()
    //                 );
    //                 //dump($user_cpf_invalids); die();
    //             }
    //         }
    //     }   
        
    //     $time = date('d-m-Y s');
    //     $filename = "usuarios-cpfs-invalidos-{$time}.xls"; // File Name
    //     // Download file
    //     header("Content-Disposition: attachment; filename=\"$filename\"");
    //     header("Content-Type: application/vnd.ms-excel");

    //     if (count($user_cpf_invalids) > 0) {
    //         foreach ($user_cpf_invalids as $user) {
    //             //dump($user); die();
    //             echo implode("\t", array_values($user)) . "\r\n";
    //         }
    //     }
    //     //dump($errors); die();

    //     return  new JsonResponse([
    //         'status' => true
    //     ]);
    // }

    // /**
    //  * @Route("/racine/cursos/lista", name="cursos-racine-lista")
    //  */
    // public function coursesRacine()
    // {
    //     $apiNeoPharma     = new NeoPharmaService(
    //         $this->getParameter('ws_racine_url'),
    //         $this->getParameter('ws_racine_token')
    //     );

    //     $listCourseRacine = $apiNeoPharma->getListCourse();
    //     //dump($listCourseRacine); die();

    //     $em = $this->getDoctrine()->getManager();
    //     $courses = array();
    //     if (count($listCourseRacine) > 0) {

    //         foreach ($listCourseRacine as $key => $racineCourso) :
    //             $courses[] = array(
    //                 'curso_racine_id' => $racineCourso->id,
    //                 'name' => $racineCourso->fullname,
    //                 'short_description' => $racineCourso->displayname,
    //                 'description' => $racineCourso->summary,
    //                 'active' => $racineCourso->visible == 1 ? 'Sim' : 'Não'
    //             );
            
    //         endforeach;
    //     }

    //     if (count($courses) > 0) {
    //         foreach ($courses as $key => $c) {
                
    //         }
    //     }

    //     return  new JsonResponse([
    //         'status' => true
    //     ]);
        
    // }

    // public function validaCPF($cpf = null) {

    //     // Verifica se um número foi informado
    //     if(empty($cpf)) {
    //         return false;
    //     }
    
    //     // Elimina possivel mascara
    //     $cpf = preg_replace("/[^0-9]/", "", $cpf);
    //     $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
    //     // Verifica se o numero de digitos informados é igual a 11 
    //     if (strlen($cpf) != 11) {
    //         return false;
    //     }
    //     // Verifica se nenhuma das sequências invalidas abaixo 
    //     // foi digitada. Caso afirmativo, retorna falso
    //     else if ($cpf == '00000000000' || 
    //         $cpf == '11111111111' || 
    //         $cpf == '22222222222' || 
    //         $cpf == '33333333333' || 
    //         $cpf == '44444444444' || 
    //         $cpf == '55555555555' || 
    //         $cpf == '66666666666' || 
    //         $cpf == '77777777777' || 
    //         $cpf == '88888888888' || 
    //         $cpf == '99999999999') {
    //         return false;
    //      // Calcula os digitos verificadores para verificar se o
    //      // CPF é válido
    //      } else {   
            
    //         for ($t = 9; $t < 11; $t++) {
                
    //             for ($d = 0, $c = 0; $c < $t; $c++) {
    //                 $d += $cpf{$c} * (($t + 1) - $c);
    //             }
    //             $d = ((10 * $d) % 11) % 10;
    //             if ($cpf{$c} != $d) {
    //                 return false;
    //             }
    //         }
    
    //         return true;
    //     }
    // }

}
