<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProductClassifiationByDistribAndPresc;

/**
 * @Route("/admin/product/classification")
 */
class AdminProductClassificationController extends Controller
{
    /**
     * @Route("", name="admin_product_classification_list")
     */
    public function index(Request $request)
    {
        $builder = $this
        ->getDoctrine()
        ->getRepository('App:ProductClassifiationByDistribAndPresc')
        ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.name LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt','DESC');

        $classifications = $builder->getQuery()->getResult();

        $classifications = $this->get('knp_paginator')->paginate(
            $classifications,
            $request->query->getInt('page', 1),
            10
        );       

        return $this->render('admin_product_classification/list.html.twig', [
            'classifications' => $classifications
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_classification_new")
     */
    public function productTypeNew(Request $request)
    {
        if($request->isMethod('GET')){
            return $this->render('admin_product_classification/new.html.twig', []);
        }

        $form = $request->request->get('form');

        $classification = new ProductClassifiationByDistribAndPresc();
        $classification->setName($form['name']);
        $classification->setIsActive(isset($form['isActive']) ? ($form['isActive'] == 'on' ? true : false ): false );
        $classification->setCreatedAt(new \DateTime());
        $classification->setUpdatedAt(new \DateTime());


        $em = $this->getDoctrine()->getManager();
        $em->persist($classification);
        $em->flush();

        return $this->redirectToRoute('admin_product_classification_list');
    }
    
    /**
     * @Route("/edit/{id}", name="admin_product_classification_edit")
     */
    public function productTypeEdit($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $classification = $em->getRepository('App:ProductClassifiationByDistribAndPresc')->find($id);

        if($request->isMethod('GET')){
            return $this->render('admin_product_classification/edit.html.twig', [
                'classification' => $classification
            ]);
        }
        $form = $request->request->get('form');

        $classification->setName($form['name']);
        $classification->setIsActive(isset($form['isActive']) ? ($form['isActive'] == 'on' ? true : false ): false );
        $classification->setUpdatedAt(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($classification);
        $em->flush();

        return $this->redirectToRoute('admin_product_classification_list');
    }
}
