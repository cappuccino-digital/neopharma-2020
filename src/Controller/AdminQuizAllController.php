<?php

namespace App\Controller;

use App\Entity\QuizAll;
use App\Entity\QuizAllRuleResult;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/admin/quiz-novo")
 */
class AdminQuizAllController extends Controller
{
    /**
     * @Route("/", name="admin_quiz_all")
     */
    public function index(Request $request): Response
    {
        $builder = $this
        ->getDoctrine()
        ->getRepository('App:QuizAll')
        ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        //$builder->orderBy('q.createdAt','DESC');
        $quizzes = $builder->getQuery()->getResult();

        $paginator = $this->get('knp_paginator')->paginate(
            $quizzes,
            $request->query->getInt('page', 1),
            10
        );
    
        return $this->render('admin_quiz_all/index.html.twig', [
            'quizAll' => $paginator,
        ]);
    }

    /**
     * @Route("/cadastro", name="admin_quiz_all_new")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {        
        $em = $this->getDoctrine()->getManager();
        //$quiz = $em->getRepository('App:QuizAll')->findOneBy(['id' => $id]);

        if($request->isMethod('GET')){
            return $this->render('admin_quiz_all/new.html.twig', []);
        }

        $form = $request->request->get('form');            
        $name = $request->request->get('name');
        $descripton = $request->request->get('descripton');
        $image_alt = $request->request->get('image_alt');
        $is_active = $request->request->has('is_active') ? true : false;
        $is_public = $request->request->has('is_public') ? true : false;
        $is_pharmaceutical = $request->request->has('is_pharmaceutical') ? true : false;
        $is_clerk = $request->request->has('is_clerk') ? true : false;
        $is_pharmacy = $request->request->has('is_pharmacy') ? true : false;
        $slug = $this->slugify($name);

        $quiz = new QuizAll();
        $quiz->setName($name);
        $quiz->setSlug($slug);
        $quiz->setDescription($descripton);
        $quiz->setImageAlt($image_alt);
        $quiz->setIsActive($is_active);
        $quiz->setIsPublic($is_public);
        $quiz->setIsPharmaceutical($is_pharmaceutical);
        $quiz->setIsClerk($is_clerk);
        $quiz->setIsStoreManager($is_pharmacy);
        $quiz->setUpdatedAt(new \DateTime());
        $quiz->setCreatedAt(new \DateTime());

        $image = $request->request->has('image') ? $request->request->get('image') : null;
        if ($image) {
            $fileNameDesk = $fileUploader->upload($image, 'quiz_all');
            $quiz->setImage($fileNameDesk);  
        }

        $em->persist($quiz);
        $em->flush();

        foreach ($form as $key => $q) { 
            $quiz_result = new QuizAllRuleResult();    
            $quiz_result->setQuizAll($quiz);
            $quiz_result->setDescription($q['description']);
            $quiz_result->setMin($q['min']);
            $quiz_result->setMax($q['max']);
            $quiz_result->getUpdatedAt(new \DateTime());
            $quiz_result->setCreatedAt(new \DateTime());

            $em->persist($quiz_result);
            $em->flush();
        }
           
        return $this->redirectToRoute('admin_quiz_all');
    }

    /**
     * @Route("/editar/{id}", name="admin_quiz_all_edit")
     */
    public function edit(Request $request, $id, FileUploader $fileUploader): Response
    {
        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository('App:QuizAll')->findOneBy(['id' => $id]);

        if($request->isMethod('GET')){
            return $this->render('admin_quiz_all/edit.html.twig', [
                'quiz' => $quiz
            ]);
        }

        $form = $request->request->get('form');            
        $name = $request->request->get('name');
        $descripton = $request->request->get('descripton');
        $image = $request->request->get('image');
        $image_alt = $request->request->get('image_alt');
        $is_active = $request->request->has('is_active') ? true : false;
        $is_public = $request->request->has('is_public') ? true : false;
        $is_pharmaceutical = $request->request->has('is_pharmaceutical') ? true : false;
        $is_clerk = $request->request->has('is_clerk') ? true : false;
        $is_pharmacy = $request->request->has('is_pharmacy') ? true : false;
        $slug = $this->slugify($name);

        $quiz->setName($name);
        $quiz->setSlug($slug);
        $quiz->setDescription($descripton);
        $quiz->setImageAlt($image_alt);
        $quiz->setIsActive($is_active);
        $quiz->setIsPublic($is_public);
        $quiz->setIsPharmaceutical($is_pharmaceutical);
        $quiz->setIsClerk($is_clerk);
        $quiz->setIsStoreManager($is_pharmacy);
        $quiz->setUpdatedAt(new \DateTime());

        $image = $request->files->has('image') ? $request->files->get('image') : null;
        if ($image) {
            $fileNameDesk = $fileUploader->upload($image, 'quiz_all');
            $quiz->setImage($fileNameDesk);  
        }
       
        $em->persist($quiz);
        $em->flush();

        foreach ($form as $key => $q) { 
            if (isset($q['result_id'])) {
                $quiz_result = $em->getRepository('App:QuizAllRuleResult')->findOneBy(['id' => $q['result_id']]);
            } else {               
                $quiz_result = new QuizAllRuleResult();
                $quiz_result->setCreatedAt(new \DateTime());
            }
    
            $quiz_result->setQuizAll($quiz);
            $quiz_result->setDescription($q['description']);
            $quiz_result->setMin($q['min']);
            $quiz_result->setMax($q['max']);
            $quiz_result->getUpdatedAt(new \DateTime());

            $em->persist($quiz_result);
            $em->flush();
        }
           
        return $this->redirectToRoute('admin_quiz_all');
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
