<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\SurveyQuestionResponse;

class SurveyQuestionController extends Controller
{
    /**
     * @Route("/enquete", name="survey_question")
     */
    public function index(Request $request)
    {
        if($request->isMethod('GET')) {
            return $this->redirectToRoute('home');
        }

        if($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $question = $em->getRepository('App:SurveyQuestion')->findOneBy(['id' => $request->request->get('q')]);
          
            $survey_question = new SurveyQuestionResponse();
            
            $response = false;
            if ($request->request->get('r') == 'sim') $response = true;
            
            $survey_question->setUser($this->getUser());
            $survey_question->setQuestion($question);
            $survey_question->setIsYes($response);
            $survey_question->setCreatedAt(new \DateTime());
           
            $em->persist($survey_question);
            $em->flush();

            $this->addFlash("success", "Resposta enviada com sucesso.");
            return $this->redirectToRoute('home');
        }

        return $this->redirectToRoute('home');
    }
}
