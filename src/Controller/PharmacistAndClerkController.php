<?php

namespace App\Controller;

use App\Entity\Leads;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Cookie;
use App\Entity\UserRegisterIncomplete;
use App\Entity\UserInvite;
use App\Entity\User;
use App\Entity\UserCongressPharmaceutical;
use App\Entity\UserEmblue;
use App\Entity\UserVoucherUber;
use App\Service\EmBlueService;


class PharmacistAndClerkController extends Controller
{
    /**
     * @Route("/cadastro/etapa-01", name="home-register")
     */
    public function registerStepOne(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $leads = [];
        $em = $this->getDoctrine()->getManager();
                
        if ($request->isMethod('GET')) {
            //cadastro para gestor de loja
            if ($this->get('session')->get('p') == 'gestor-de-loja') {
                return $this->render('home/register-gestor-loja.html.twig');    
            }
             
            $email = $this->get('session')->get('m') != false ? $this->get('session')->get('m') : null;
            if ($email) {
                $leads = $em->getRepository('App:Leads')->findOneByEmail($email);
            }
            //formulário para balconista e farmacêutico
            return $this->render('pharmacist_and_clerk/register-step-01.html.twig', ['leads' => $leads]);            
        }
              
        $occupation = trim($request->request->get('occupation'));
        $lead_id = trim($request->request->get('lead'));
        $name = trim($request->request->get('name'));
        $email = trim($request->request->get('email'));
        $gender = trim($request->request->get('gender'));
        $birthday = trim($request->request->get('birthday'));

        $userCheckEmail = $em->getRepository('App:User')
                             ->createQueryBuilder('u')
                             ->andWhere('u.email = :email or u.username = :username')
                             ->setParameter('email', $email)
                             ->setParameter('username', $email)
                             ->getQuery()
                             ->getResult();
        //validação 
        $errors = array();        
        $user = $em->getRepository('App:User')->findOneByEmail($email);
        
        $is_register_incomplete = false;
        if ($userCheckEmail) {            
            if ($user) {
                if ((!$user->getIsRegisterComplete() && $user->getFacebookData()) || (!$user->getIsRegisterComplete() && $user->getGoogleData())) {
                    $is_register_incomplete = true;
                } else {
                    $is_register_incomplete = false;
                }
            } else {
                if ($user->getUsername() == $email) {
                    $errors['email'] = "Email está sendo utilizado";
                } else {
                    $is_register_incomplete = false;
                }
            }
        
            if (!$is_register_incomplete) {
                $errors['email'] = "Email está sendo utilizado";
            } 
        }

        if (!$occupation) {
            $errors['occupation'] = 'O perfil é obrigatório';
        } 

        if (!$name) {
            $errors['name'] = 'O nome é obrigatório';
        } 
        
        if (!$email) {
            $errors['email'] = 'O e-mail é obrigatório';
        }
                 
        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'error' => $errors,
            ]);
        }
        /** end validação */
        $leads = $em->getRepository('App:Leads')->findOneById($lead_id);
        //dump($leads); die();
        if (!$leads) {
            $leads = new Leads();
            $leads->setIsFacebook(false);
            $leads->setIsGoogle(false);
            $leads->setIsNewsletter(false);
            $leads->setCreatedAt(new \DateTime());
        }

        $leads->setName($name);
        $leads->setEmail($email);
        $leads->setGender($gender);
    
        if ($birthday) {
            //$start = new \DateTime($birthday);  
            //$date_format = $start->format('Y-m-d 00:00:00');
            $leads->setBirthday($birthday);
        }

        //ROLES
        $roleBalc = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
        $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');

        if ($occupation == 'balconista') {
            $leads->setRole($roleBalc);
        } else if($occupation == 'farmaceutico'){
            $leads->setRole($roleFarmac);
        }
        
        $leads->setStep('STEP-ONE');
        $leads->setUpdatedAt(new \DateTime());
               
        $em->persist($leads);
        $em->flush();

        //atualizando as sessões após a primeira etapa do cadastro
        $this->get('session')->set('lead_id', $leads->getId());
        $this->get('session')->set('p', $occupation);
        $this->get('session')->set('n', $leads->getName());
        $this->get('session')->set('m', $leads->getEmail());

        $url = $this->generateUrl('home-register-two');
        
        $response = new JsonResponse([
            'status' => true,
            'url' => $url
        ]);
        
        return $response;
    }  

    /**
     * @Route("/cadastro/etapa-02", name="home-register-two")
     */
    public function registerStepTwo(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $leads = [];
        $lead_id = $this->get('session')->get('lead_id') != false ? $this->get('session')->get('lead_id') : null;

        //se o ID do Lead não existir será direcionado para a primeira etapa do cadastro
        if (!$lead_id) {
            return $this->redirectToRoute('home-register'); 
        }

        $em = $this->getDoctrine()->getManager();
        $leads = $em->getRepository('App:Leads')->findOneById($lead_id);
      
        //se o lead não existir na base será direcionado para a primeira etapa do cadastro
        if (!$leads) {
            return $this->redirectToRoute('home-register'); 
        }

        //se o usuário não tiver preenchido a etapa 2 será direcionado para a primeira etapa do cadastro
        if (($leads->getStep() != 'STEP-ONE') && ($leads->getStep() != 'STEP-TWO')) {
            return $this->redirectToRoute('home-register'); 
        }

        if ($request->isMethod('GET')) {                                     
            //formulário para balconista e farmacêutico
            return $this->render('pharmacist_and_clerk/register-step-02.html.twig', ['leads' => $leads]);            
        }

        //validação 
        $errors = array();
        $cellphone = trim($request->request->get('phone'));
        $estate_address = trim($request->request->get('estate_address'));
        $city = trim($request->request->get('city'));
        $educationalLevel = trim($request->request->get('educationalLevel'));
        $state = trim($request->request->get('state'));
        $crf = trim($request->request->get('crf'));
        $cpf = str_replace(['.', ' ', '-'], '', trim($request->request->get('cpf')));
        $cnpj = str_replace(['.', ' ', '-', '/'], '', trim($request->request->get('cnpj')));

        if (!$cellphone) {
            $errors['phone'] = 'O Celular é obrigatório';
        } else if (strlen($cellphone) < 11) {
            $errors['phone'] = 'Celular inválido';
        }

        $user_check_cpf = $em->getRepository('App:User')->findOneByCpf($cpf); 

        if (!$cpf) {
            $errors['cpf'] = 'O CPF é obrigatório';
        } else if(!$this->validaCPF($cpf)) {
            $errors['cpf'] = 'CPF inválido';
        }else if ($user_check_cpf) {
            $errors['cpf'] = 'CPF está sendo utilizado';
        }

        if ($leads->getRole()->getName() == 'Farmacêutico') {
            $crf_check = $em->getRepository('App:User')->findOneBy(['crf' => str_replace(['.', ' ', '-', '/'], '', $crf), 'state' => $state]);

            if (!$crf) {
                $errors['crf'] = 'O CRF é obrigatório';
            } 

            if (!$state) {
                $errors['state'] = 'O estado é obrigatório';
            } 

            //verificando se o CRF está sendo utilizado por outra pessoa
            if ($crf_check) {
                $errors['crf'] = 'CRF está sendo utilizado';
            }

            if ($cnpj) {
                $pharmacy = $em->getRepository('App:Pharmacy')->findOneByCnpj($cnpj);

                if (!$pharmacy) {
                    $errors['cnpj'] = 'Farmácia não encontrada';
                } else if(!$this->validaCNPJ($cnpj)) {
                    $errors['cnpj'] = 'CNPJ inválido';
                }
            }

        } else if ($leads->getRole()->getName() == 'Balconista') { 
            if (!$cnpj) {
                $errors['cnpj'] = 'O CNPJ é obrigatório';
            } else {
                $pharmacy = $em->getRepository('App:Pharmacy')->findOneByCnpj($cnpj);

                if (!$pharmacy) {
                    $errors['cnpj'] = 'Farmácia não encontrada';
                } else if(!$this->validaCNPJ($cnpj)) {
                    $errors['cnpj'] = 'CNPJ inválido';
                }
            }
        }        

        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'error' => $errors,
            ]);
        }

        $leads->setCrf($crf); //farmacêutico
        $leads->setState($state); //farmacêutico
        $leads->setCellphone($cellphone);
        $leads->setCnpj($cnpj);
        $leads->setCpf($cpf);
        $leads->setEducationalLevel($educationalLevel);
        $leads->setCity($city);
        $leads->setEstateAddress($estate_address);
        $leads->setStep('STEP-TWO');
        $leads->setUpdatedAt(new \DateTime());
               
        $em->persist($leads);
        $em->flush();

        $url = $this->generateUrl('home-register-three');
        
        $response = new JsonResponse([
            'status' => true,
            'url' => $url
        ]);
        
        return $response;

    }

    /**
     * @Route("/cadastro/etapa-03", name="home-register-three")
     */
    public function registerStepThree(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $leads = [];
        $lead_id = $this->get('session')->get('lead_id') != false ? $this->get('session')->get('lead_id') : null;

        //se o ID do Lead não existir será direcionado para a primeira etapa do cadastro
        if (!$lead_id) {
            return $this->redirectToRoute('home-register'); 
        }

        $em = $this->getDoctrine()->getManager();
        $leads = $em->getRepository('App:Leads')->findOneById($lead_id);
        
        //se o lead não existir na base será direcionado para a primeira etapa do cadastro
        if (!$leads) {
            return $this->redirectToRoute('home-register'); 
        }

        //se o usuário não tiver preenchido a etapa 2 será direcionado para a primeira etapa do cadastro
        if ($leads->getStep() != 'STEP-TWO') {
            return $this->redirectToRoute('home-register'); 
        }

        if ($request->isMethod('GET')) {                                     
            return $this->render('pharmacist_and_clerk/register-step-03.html.twig', ['leads' => $leads]);            
        }
        
        //validação 
        $errors = array();
        $secret = '6LcmcocaAAAAABukpYSV0DP_DFkCvXPAWfmNwEsV';  // check if reCaptcha has been validated by Google     
        $captchaId = $request->request->get('g-recaptcha-response'); /** REcaptcha */
        $responseCaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captchaId)); //send recaptcha

        $friendIndication = trim($request->request->get('friendIndication'));
        $password         = trim($request->request->get('password'));
        $passwordRepeat   = trim($request->request->get('passwordRepeat'));
        $term             = $request->request->has('term') ? trim($request->request->get('term')) : null;
        $newsletter       = trim($request->request->get('newsletter'));
        //dump($term); die();
        //carregando as informações do lead para salvar na tabela de usuário
        $occupation       = $leads->getRole()->getName();
        $name             = $leads->getName();
        $email            = $leads->getEmail();
        $cellphone        = $leads->getCellphone();
        $cpf              = $leads->getCpf();
        $cnpj             = $leads->getCnpj();
        $gender           = $leads->getGender();
        $educationalLevel = $leads->getEducationalLevel();
        $city             = $leads->getCity();
        $estate_address   = $leads->getEstateAddress();
        $birthday         = $leads->getBirthday();
        $crf              = $leads->getCrf();
        $state            = $leads->getState();

        $user = $em->getRepository('App:User')->findOneByEmail($email);

        $userCheckEmail = $em->getRepository('App:User')
            ->createQueryBuilder('u')
            ->andWhere('u.email = :email or u.username = :username')
            ->setParameter('email', $email)
            ->setParameter('username', $email)
            ->getQuery()
            ->getResult();
            
        
        if ($responseCaptcha->success == true) {
                         
        } else {
           $errors['recaptcha'] = "Selecione o recaptcha é obrigatório";
        }

        if (!$password) {
            $errors['password'] = 'A senha é obrigatória';
        }

        if (!$passwordRepeat) {
            $errors['passwordRepeat'] = 'O campo repetir senha é obrigatório.';
        }

        if (!$term) {
            $errors['term'] = 'A leitura do termo de uso é obrigatório.';
        }

        if ($password != $passwordRepeat) {
            $errors['passwordRepeat'] = 'O campo senha e repetir senha estão diferentes.';
        } 

        $is_register_incomplete = false;
        if ($userCheckEmail) {            
            if ($user) {
                if ((!$user->getIsRegisterComplete() && $user->getFacebookData()) || (!$user->getIsRegisterComplete() && $user->getGoogleData())) {
                    $is_register_incomplete = true;
                } else {
                    $is_register_incomplete = false;
                }
            } else {
                if ($user->getUsername() == $email) {
                    $errors['email'] = "E-mail está sendo utilizado";
                } else {
                    $is_register_incomplete = false;
                }
            }
        
            if (!$is_register_incomplete) {
                $errors['email'] = "E-mail está sendo utilizado";
            } 
        }
         
        if (count($errors) > 0) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Campos obrigatórios',
                'error' => $errors,
            ]);
        }

        /** end validação */
        if (!$user) {
            $user = new User();
        }
        
        //set user 
        $user->setName($name);
        $user->setEmail($email);
        $user->setCellphone($cellphone);
        $user->setUsername($email);
        $user->setIsRegisterComplete(true);
        $user->setCpf($cpf);
        $user->setNewsletter($newsletter ? $newsletter : false);
        //$user->setTeamFootball(null);
        $user->setSalt(md5(uniqid()));
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setGender($gender);
        $user->setEducationalLevel($educationalLevel);
        $user->setCity($city);
        $user->setEstateAddress($estate_address);

        if($birthday != ''){
            $d = explode('/', $birthday);
            if(isset($d[0]) && isset($d[1]) && isset($d[2])){
                $birthday = "{$d[2]}-{$d[1]}-{$d[0]}";
                $user->setBirthday(new \DateTime($birthday));
            }
        }

        if ($occupation == 'Balconista') {
            $roleBalc = $em->getRepository('App:Role')->findOneByRole('ROLE_BALCAO');
            $user->addRole($roleBalc);
            $user->setIsPharmaceutical(false);
            $user->setCnpj($cnpj);
        } else if($occupation == 'Farmacêutico'){
            $roleFarmac = $em->getRepository('App:Role')->findOneByRole('ROLE_FARMACO');
            $user->addRole($roleFarmac);
            $user->setIsPharmaceutical(true);
            $user->setCnpj($cnpj);
            $user->setCrf($crf);
            $user->setState($state);
        }
        
        $user->setIsActive(true);
        //$user->setEstateAddress(null);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setLastLoginAt(new \DateTime());
         
        $em->persist($user);
        $em->flush();

        $user->setShareCode($this->generateShareCode($user));
        $em->persist($user);
        $em->flush();
        
        if ($friendIndication) {
            $invite = new UserInvite();
            $invite->setUser($user);
            $invite->setShareCode($friendIndication);
            $em->persist($invite);
            $em->flush();
             
            $user->setInvite($invite);
            $em->persist($invite);
            $em->flush();

           /** Regra para incentivo de vendas */
           $cod_indication = $friendIndication;

           if ($cod_indication) {
               $em->getRepository('App:PromoThirtyDayMovement')->savePoints($user, $cod_indication, 'cadastro', null);
           }
           /** end */
        }
        
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

       //verificando cadastro incompleto
       if ($is_register_incomplete) {
           $user_register = $em->getRepository('App:UserRegisterIncomplete')->findOneBy(['user' => $user]);

           if (!$user_register) {
               $user_register =  new UserRegisterIncomplete();
           
               $user_register->setUser($user);
               $user_register->setIsRegisterComplete(true);
               $user_register->setIsGestor($occupation == 'gestor' ? true : false);
               $user_register->setIsPharmaceutical($occupation == 'farmaceutico' ? true : false);
               $user_register->setIsClerk($occupation == 'balconista' ? true : false);
               $user_register->setIsSocialNetwork(false);
               $user_register->setField('cadastro-rede-social-incompleto');
               $user_register->setCreatedAt(new \DateTime());
               $em->persist($user_register);
               $em->flush();

               //removendo o role de usuário incompleto
               $role_user = $em->getRepository('App:Role')->findOneByRole('ROLE_USER');
               
               $user->removeRole($role_user);
              
               $em->persist($user);
               $em->flush();

           }
       }

       /** API emBlue */
       $emBlue = new EmBlueService();
       $leads  = $em->getRepository('App:Leads')->findOneBy(['email' => $user->getEmail()]);

       //removendo o contato do usuário da base
       if ($leads) {
           $lead_emblue = $em->getRepository('App:LeadEmblue')->findOneBy(['lead' => $leads]);

           if ($lead_emblue) {
               $em->remove($lead_emblue);
               $em->flush();
           }

           //avisando o emblue que o usuário deixou de ser um lead
           $contact = $emBlue->eventLeads($leads, true);

           //removendo o lead
           $em->remove($leads);
           $em->flush();
       }

       /** E-mail Boas Vindas*/
       if ($user->getNewsletter()) {
           //enviando o e-mail do usuário para emblue para disparar o e-mail de boas vinda
           $contact = $emBlue->eventWelcome($user);

           $user_emBlue = $em->getRepository('App:UserEmblue')->findOneBy(['user' => $user]);
           if (!$user_emBlue) {
               $user_emBlue = new UserEmblue();
               $user_emBlue->setCreatedAt(new \DateTime());
           }
           
           $user_emBlue->setUser($user);
           $user_emBlue->setEmail($user->getEmail());
           if ($contact['status']) {                
               $user_emBlue->setEvent($contact['result']);
               $user_emBlue->setMessage($contact['message']);
           } else {
               $user_emBlue->setMessage($contact['message']);  
           }
           $user_emBlue->setUpdatedAt(new \DateTime());

           $em->persist($user_emBlue);
           $em->flush();
       }  
       /** END */

       $em->getRepository('App:UserPointGamification')->setInterationGamification($user, 'cadastrar-se-na-plataforma', 'cadastro', null); //Gamificação interação

       //Redirecionamento para a referência após o login
       $url_reference = $request->cookies->get('_referenceURL') ? $request->cookies->get('_referenceURL') : $this->generateUrl('home');

       $response = new JsonResponse([
           'status' => true,
           'message' => 'Cadastro realizado com sucesso.',
           'url' => $url_reference,
       ]);

       $response->headers->setCookie(new Cookie('_referenceURL', false));
       /** END */

       //cadastro que vieram da página do congresso  
       if ($request->cookies->get('_congressocrf') == true) {
           $response->headers->setCookie(new Cookie('_congressocrf', false));
           
           //verificando se o usuário já possui cadastro
           $user_congress_pharmaceutical = $em->getRepository('App:UserCongressPharmaceutical')->findOneBy(['user' => $user]);

           if (!$user_congress_pharmaceutical) {
               //cadastro no congresso
               $user_congress_pharmaceutical = new UserCongressPharmaceutical(); 
               $user_congress_pharmaceutical->setUser($user); 
               $user_congress_pharmaceutical->setIsRegistration(true); 
               $user_congress_pharmaceutical->setCreatedAt(new \DateTime());
               $user_congress_pharmaceutical->setUpdatedAt(new \DateTime());  

               $em->persist($user_congress_pharmaceutical);
               $em->flush();
           }
       }
        
        //Campanha Farmacêutico para receber voucher UBER 
        if ($user->getIsPharmaceutical()) {            
            $user_tickets_corinthians_all = $em->getRepository('App:UserVoucherUber')->findAll();
        
            //apenas 1960 ingressos
            if (count($user_tickets_corinthians_all) <= 1960) {
            
                //cadastro do usuário para receber os ingressos
                $user_tickets_corinthians = new UserVoucherUber();  
                $user_tickets_corinthians->setUser($user); 
                $user_tickets_corinthians->setIsPharmaceutical(true); 
                $user_tickets_corinthians->setIsNewRegister(true); 
                $user_tickets_corinthians->setCreatedAt(new \DateTime());
                $user_tickets_corinthians->setUpdatedAt(new \DateTime());  
    
                $em->persist($user_tickets_corinthians);
                $em->flush();
            }
        }
        /*** END */


        //removendo as sessões após usuário finalizar o cadastro
        $this->get('session')->remove('lead_id');
        $this->get('session')->remove('p');
        $this->get('session')->remove('n');
        $this->get('session')->remove('m');
       
       return $response;

    }

    private function validaCNPJ($cnpj = null) {
        // Verifica se um número foi informado
        if(empty($cnpj)) {
            return false;
        }
       // dump($cnpj); die();
        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cnpj) != 14) {
            return false;
        }
        
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' || 
            $cnpj == '11111111111111' || 
            $cnpj == '22222222222222' || 
            $cnpj == '33333333333333' || 
            $cnpj == '44444444444444' || 
            $cnpj == '55555555555555' || 
            $cnpj == '66666666666666' || 
            $cnpj == '77777777777777' || 
            $cnpj == '88888888888888' || 
            $cnpj == '99999999999999') {
            return false;
            
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
         
            $j = 5;
            $k = 6;
            $soma1 = 0;
            $soma2 = 0;
    
            for ($i = 0; $i < 13; $i++) {
    
                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;
                
                $soma2 += ($cnpj{$i} * $k);
    
                if ($i < 12) {
                    $soma1 += ($cnpj{$i} * $j);
                }
    
                $k--;
                $j--;
    
            }
    
            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;
    
            return (($cnpj{12} == $digito1) and ($cnpj{13} == $digito2));
         
        }
    }

    private function validaCPF($cpf = null) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
    
            return true;
        }
    }

    private function generateShareCode($user)
    {
        $name = explode(' ', $user->getName());
        $better_token = substr(md5(uniqid(rand(), true)),rand(0,26),5);
        return strtolower($name[0] . $better_token);
    }

    private function checkCity($city, $state)
    {
        $array = [
            'anapolis',
            'angra dos reis',
            'aracaju',
            'aracatuba',
            'arapiraca',
            'bage',
            'barbacena',
            'barreiras',
            'bauru',
            'belem',
            'belo horizonte',
            'blumenau',
            'boa vista',
            'botucatu',
            'braganca paulista',
            'brasilia',
            'cabo frio',
            'cachoeiro de itapemirim',
            'campina grande',
            'campinas',
            'campo grande',
            'campos dos goytacazes',
            'caruaru',
            'cascavel',
            'caxias do sul',
            'chapeco',
            'colatina',
            'conselheiro lafaiete',
            'cornelio procopio',
            'criciuma',
            'cuiaba',
            'curitiba',
            'divinopolis',
            'dourados',
            'feira de santana',
            'florianopolis',
            'fortaleza',
            'foz do iguacu',
            'franca',
            'garanhuns',
            'goiania',
            'governador valadares',
            'guarapuava',
            'ijui',
            'imperatriz',
            'ipatinga',
            'itabira',
            'itabuna ',
            'ilheus',
            'itajai',
            'itajuba',
            'itaperuna',
            'itapipoca',
            'ivaipora',
            'jequie',
            'ji-parana',
            'joacaba',
            'joao pessoa',
            'joinville',
            'juazeiro do norte',
            'juiz de fora',
            'lages',
            'linhares',
            'londrina',
            'macae',
            'macapa',
            'maceio',
            'manaus',
            'maraba',
            'marilia',
            'maringa',
            'mogi guacu',
            'montes claros',
            'mossoro',
            'muriae',
            'natal',
            'nova friburgo',
            'palmas',
            'paranagua',
            'parauapebas',
            'parnaiba',
            'passo fundo',
            'patos',
            'patos de minas',
            'paulo afonso',
            'pelotas',
            'petrolina',
            'petropolis',
            'piracicaba',
            'pocos de caldas',
            'ponta grossa',
            'porto alegre',
            'porto seguro',
            'porto velho',
            'pouso alegre',
            'presidente prudente',
            'recife',
            'ribeirao preto',
            'rio branco',
            'rio de janeiro',
            'rio verde',
            'rondonopolis',
            'salvador',
            'santa cruz do capibaribe',
            'santa cruz do sul',
            'santa maria',
            'santarem',
            'santo angelo',
            'santo antonio de jesus',
            'santos',
            'sao bento do sul',
            'sao carlos',
            'sao jose do rio preto',
            'sao jose dos campos',
            'sao luis',
            'sao paulo',
            'sobral',
            'sorocaba',
            'teofilo otoni',
            'teresina',
            'tres rios',
            'tucurui',
            'uba',
            'uberaba',
            'uberlandia',
            'umuarama',
            'uruguaiana',
            'valenca',
            'varginha',
            'vitoria',
            'vitoria da conquista',
            'volta redonda',
        ];

        // if ($state != 'SP') {
        //     return false;
        // }
        $city = $this->clarCharSpecial(trim($city));
        $city = mb_strtolower($city, 'UTF-8');
        if (!in_array($city, $array)) {
            return false;
        }

        return true;
    }

    private function clarCharSpecial($string) {
        $caracteres_sem_acento = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Â'=>'Z', 'Â'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Å'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'Å'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
            'Ä'=>'a', 'î'=>'i', 'â'=>'a', 'È'=>'s', 'È'=>'t', 'Ä'=>'A', 'Î'=>'I', 'Â'=>'A', 'È'=>'S', 'È'=>'T',
        );
        $nova_string = strtr($string, $caracteres_sem_acento);
        return $nova_string;
    }

 
}
