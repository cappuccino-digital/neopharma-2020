<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api/excelencia")
 */
class ApiExcelenciaController extends Controller {

    /**
     * @Route("/requestToken", name="api-login-neoexcelencia", methods={"POST"})
     */
    public function requestToken(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = json_decode($request->getContent(), true);

        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        
        if(!$username || !$password){
            return new JsonResponse([
                'status' => false,
                'message' => 'Paramêtros não encontrados.'
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneByUsername($username);
        
        if(!$user){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado.'
            ]);
        }

        $bool = ($encoder->isPasswordValid($user, $password)) ? true : false;

        if(!$bool){
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário ou senha incorreta.'
            ]);
        }

        return new JsonResponse([
            'status' => true,
            'token' => $user->getTokenApiExcelencia()
        ]);
    }

    /**
     * @Route("/login", name="api-login-user", methods={"POST"})
    */
    public function login(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $tokenUser = isset($dataPost['token']) ? $dataPost['token'] : null;
        $cpf       = isset($dataPost['cpf']) ? $dataPost['cpf']: null; 
        $cnpj      = isset($dataPost['cnpj']) ? $dataPost['cnpj'] : null;
        $page      = isset($dataPost['page']) ? $dataPost['page'] : null;

        if(!$tokenUser || !$cpf || !$cnpj || !$page){ 
            $tokenUser = $request->request->has('token') ? $request->request->get('token'): $request->query->get('token');
            $cpf       = $request->request->has('cpf') ? $request->request->get('cpf'): $request->query->get('cpf'); 
            $cnpj      = $request->request->has('cnpj') ? $request->request->get('cnpj'): $request->query->get('cnpj'); 
            $page      = $request->request->has('page') ? $request->request->get('page'): $request->query->get('page'); 
        }
       
        if (!$tokenUser) {
            return new JsonResponse([
                'status' => false,
                'message' => 'O token é obrigatório.'
            ]);
        }

        if(!$cpf || !$cnpj){
            return new JsonResponse([
                'status' => false,
                'message' => 'O CPF e CNPJ são obrigatório.'
            ]);
        }
        
        //verificando se o token é do perfil excelencia
        $em = $this->getDoctrine()->getManager();		
        $userTokenExcelencia = $em->getRepository('App:User')->findOneBy(['token_api_excelencia'=> $tokenUser]);
        $roleExcelencia      = $em->getRepository('App:Role')->findOneBy(['role'=> 'ROLE_EXCELENCIA']);
       // dump($userTokenExcelencia); die();
        if (!$userTokenExcelencia) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Token inválido.'
            ]);
        }
        
        $userExcelencia = $em->getRepository('App:User')->getConsultUserExcelencia(
                                                            $userTokenExcelencia->getId(), 
                                                            $roleExcelencia->getId());
        if (!$userExcelencia) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Usuário não encontrado.'
            ]);
        }

        //verificando user
        $user = $em->getRepository('App:User')->findOneBy(['cpf'=> $cpf, 'cnpj' => $cnpj, 'isPharmacy' => true]);

        if (!$user) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CPF ou CNPJ inválido.'
            ]);
        }

        //gerando novo token para user master excelencia
        $userTokenExcelencia->setTokenApiExcelencia($this->generateToken());
        $userTokenExcelencia->setUpdatedAt(new \DateTime());

        $em->persist($userTokenExcelencia);
        $em->flush();

        if ($page) { //página de direcionamento
            $page = '?page='.$page;
        }

        return new JsonResponse([
            'status' => true,
            'url' => "{$request->getSchemeAndHttpHost()}/api/excelencia/authentication/{$user->getTokenApiExcelencia()}{$page}"
        ]);
        
    }

    /**
     * @Route("/authentication/{token}", name="api-authentication-token")
     */
    public function authenticationToken(Request $request, $token)
    {
        if (empty($token)) {
            throw $this->createNotFoundException('token inválido');
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['token_api_excelencia'=> $token, 'isPharmacy' => true]);

        if (empty($user)) {
            throw $this->createNotFoundException('token inválido');
        }
            
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
       
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $user->setTokenApiExcelencia($this->generateToken());

        $em->persist($user);
        $em->flush();

        if ($request->query->has('page') && $request->query->get('page') == 'meus-dados') {
            return $this->redirectToRoute('user-home');
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/updateUser", name="api-updateUser", methods={"POST"})
     */
    public function updateUser(request $request)
    {
        $postdata = file_get_contents("php://input");
        $dataPost = json_decode($postdata, true);

        $tokenUser = isset($dataPost['token']) ? $dataPost['token'] : null;
        $email     = isset($dataPost['email']) ? $dataPost['email']: null; 
        $cnpj      = isset($dataPost['cnpj']) ? $dataPost['cnpj'] : null;
        $cpf       = isset($dataPost['cpf']) ? $dataPost['cpf'] : null; 

        if (!$tokenUser || !$email || !$cnpj || !$cpf) { 
            $tokenUser = $request->request->has('token') ? $request->request->get('token') : null;
            $email      = $request->request->has('email') ? $request->request->get('email') : null;
            $cnpj      = $request->request->has('cnpj') ? $request->request->get('cnpj') : null;  
            $cpf       = $request->request->has('cpf') ? $request->request->get('cpf') : null; 
        }

        if (!$tokenUser) {
            return new JsonResponse([
                'status' => false,
                'message' => 'O Token é obrigatório.'
            ]);
        }

        if(!$cnpj){
            return new JsonResponse([
                'status' => false,
                'message' => 'O CNPJ é obrigatório.'
            ]);
        }

        if(!$cpf){
            return new JsonResponse([
                'status' => false,
                'message' => 'O CPF é obrigatório.'
            ]);
        }
        
        if(!$email){
            return new JsonResponse([
                'status' => false,
                'message' => 'O E-mail é obrigatório.'
            ]);
        }
        
        //verificando se o token é do perfil excelencia
        $em = $this->getDoctrine()->getManager();		
        $userTokenExcelencia = $em->getRepository('App:User')->findOneBy(['token_api_excelencia'=> $tokenUser]);
        $roleExcelencia      = $em->getRepository('App:Role')->findOneBy(['role'=> 'ROLE_EXCELENCIA']);

        if (!$userTokenExcelencia) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Token inválido.'
            ]);
        }
        
        $userExcelencia = $em->getRepository('App:User')->getConsultUserExcelencia(
                                                            $userTokenExcelencia->getId(), 
                                                            $roleExcelencia->getId());
         if (!$userExcelencia) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Token inválido.'
            ]);
        }

        //verificando user
        $user = $em->getRepository('App:User')->findOneBy(['email'=> $email, 'cnpj' => $cnpj, 'isPharmacy' => true]);

        if (!$user) {
            return new JsonResponse([
                'status' => false,
                'message' => 'CNPJ ou E-mail inválido.'
            ]);
        }

        $user->setCpf($cpf); //alterando CPF
        $user->setUpdatedAt(new \DateTime());

        $em->persist($user);
        $em->flush();

        //gerando novo token para user master excelencia
        $userTokenExcelencia->setTokenApiExcelencia($this->generateToken());
        $userTokenExcelencia->setUpdatedAt(new \DateTime());

        $em->persist($userTokenExcelencia);
        $em->flush();
        
        return new JsonResponse([
            'status' => true,
            'message' => "CPF atualizado com sucesso."
        ]);
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    

}