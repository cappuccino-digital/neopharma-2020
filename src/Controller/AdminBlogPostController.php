<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\BlogPost;
use App\Entity\ReferrerContent;

/**
 * @Route("/admin/blog/post")
 */
class AdminBlogPostController extends Controller
{

    /**
     * @Route("", name="admin_blog_post_list")
     */
    public function postList(Request $request)
    {
        $builder = $this
            ->getDoctrine()
            ->getRepository('App:BlogPost')
            ->createQueryBuilder('q');

        if ($request->query->has('q')) {
            $builder
                ->where('q.title LIKE :query')
                ->setParameter('query', '%' . $request->query->get('q') . '%');
        }

        $builder->orderBy('q.createdAt', 'DESC');

        $posts = $builder->getQuery()->getResult();

        $posts = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin_blog_post/list.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/novo", name="admin_blog_post_new")
     */
    public function postNew(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:BlogPostType')->findBy(['isActive' => true]);
        $categories = $em->getRepository('App:BlogPostCategory')->findBy(['isActive' => true]);

        if ($request->isMethod('GET')) {
            return $this->render('admin_blog_post/new.html.twig', [
                'types' => $types,
                'categories' => $categories
            ]);
        }

        $files = $request->files->get('post');
        $post = $request->request->get('post');

        $np = new BlogPost();
        $np->setTitle($post['title']);
        $np->setShortDescription($post['shortDescription']);
        $np->setDescription(html_entity_decode($post['description'], ENT_COMPAT, 'UTF-8'));
        $np->setPost(html_entity_decode($post['post'], ENT_COMPAT, 'UTF-8'));
        $np->setIsActive(isset($post['isActive']) ? $post['isActive'] : false);
        $np->setIsOnlyToPharmaceutical(isset($post['isOnlyToPharmaceutical']) ? $post['isOnlyToPharmaceutical'] : false);
        $np->setIsOnlyToClerk(isset($post['isOnlyToClerk']) ? $post['isOnlyToClerk'] : false);
        $np->setIsPublic(isset($post['isPublic']) ? $post['isPublic'] : false);
        $np->setTags($post['tags']);
        $np->setMetaTitle($post['metaTitle']);
        $np->setMetaDescription($post['metaDescription']);
        $np->setImageMetaTitle($post['imageMetaTitle']);
        $np->setImageMetaDescription($post['imageMetaDescription']);
        $np->setImageMobileMetaTitle($post['imageMobileMetaTitle']);
        $np->setImageMobileMetaDescription($post['imageMobileMetaDescription']);

        $readingTimeArray = explode(':', $post['readingTime']);
        $readingTime = (isset($readingTimeArray[0]) ? $readingTimeArray[0] * 3600 : 0) +
            (isset($readingTimeArray[1]) ? $readingTimeArray[1] * 60 : 0) +
            (isset($readingTimeArray[2]) ? $readingTimeArray[2] : 0);
        $np->setReadingTime($readingTime);

        $np->setCreatedAt(new \DateTime());
        $np->setUpdatedAt(new \DateTime());

        $type = $em->getRepository('App:BlogPostType')->find($post['type']);
        $category = $em->getRepository('App:BlogPostCategory')->find($post['category']);

        $np->setCategory($category);
        $np->setType($type);

        if (isset($files['podcastFile'])) {
            $np->setPodcastFile($this->saveFile($files['podcastFile'], $np, 'blog'));
        }

        if (isset($files['videoFile'])) {
            $np->setvideoFile($this->saveFile($files['videoFile'], $np, 'blog'));
        }

        if (isset($files['image'])) {
            if (filesize($files['image']) != false) {

                if (filesize($files['image']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_blog_post_new');
                }
                $np->setImage($this->saveFile($files['image'], $np, 'blog'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                return $this->redirectToRoute('admin_blog_post_new');
            }
        } else {
            $np->setImage('_dist/image/default_desktop.png');
        }

        if (isset($files['imageMobile'])) {
            if (filesize($files['imageMobile']) != false) {

                if (filesize($files['imageMobile']) > 1048576) {

                    $this->addFlash("alert", "Imagem maior que o permitido.");

                    return $this->redirectToRoute('admin_blog_post_new');
                }
                $np->setImageMobile($this->saveFile($files['imageMobile'], $np, 'blog'));
            } else {

                $this->addFlash("alert", "ERRO AO IMPORTAR IMAGEM. SELECIONE OUTRO ARQUIVO.");
                return $this->redirectToRoute('admin_blog_post_new');
            }

            $np->setImageMobile($this->saveFile($files['imageMobile'], $np, 'blog'));

        } else {
            $np->setImageMobile('_dist/image/default_mobile.png');
        }
        $em->persist($np);
        $em->flush();

        $productReferred = $request->request->get('referrer');
        if (is_array($productReferred)) $this->saveReferredContent($np, $productReferred);

        return $this->redirectToRoute('admin_blog_post_list');
    }

    /**
     * @Route("/{id}", name="admin_blog_post_view")
     */
    public function postView(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $np = $em->getRepository('App:BlogPost')->find($id);
        return $this->render('admin_blog_post/view.html.twig', [
            'post' => $np
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_blog_post_edit")
     */
    public function postEdit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $types = $em->getRepository('App:BlogPostType')->findBy(['isActive' => true]);
        $categories = $em->getRepository('App:BlogPostCategory')->findBy(['isActive' => true]);
        $np = $em->getRepository('App:BlogPost')->find($id);
        $contentReferrer = $em->getRepository('App:ReferrerContent')->getReferrerContent($np->getId(), 'post');

        if ($request->isMethod('GET')) {
            return $this->render('admin_blog_post/edit.html.twig', [
                'types' => $types,
                'categories' => $categories,
                'np' => $np,
                'contentReferrer' => $contentReferrer
            ]);
        }

        $files = $request->files->get('post');
        $post = $request->request->get('post');

        $np->setTitle($post['title']);
        $np->setShortDescription($post['shortDescription']);
        $np->setDescription(html_entity_decode($post['description'], ENT_COMPAT, 'UTF-8'));
        $np->setPost(html_entity_decode($post['post'], ENT_COMPAT, 'UTF-8'));
        $np->setIsActive(isset($post['isActive']) ? $post['isActive'] : false);
        $np->setIsOnlyToPharmaceutical(isset($post['isOnlyToPharmaceutical']) ? $post['isOnlyToPharmaceutical'] : false);
        $np->setIsOnlyToClerk(isset($post['isOnlyToClerk']) ? $post['isOnlyToClerk'] : false);
        $np->setIsPublic(isset($post['isPublic']) ? $post['isPublic'] : false);
        $np->setTags($post['tags']);

        $np->setMetaTitle($post['metaTitle']);
        $np->setMetaDescription($post['metaDescription']);
        $np->setImageMetaTitle($post['imageMetaTitle']);
        $np->setImageMetaDescription($post['imageMetaDescription']);
        $np->setImageMobileMetaTitle($post['imageMobileMetaTitle']);
        $np->setImageMobileMetaDescription($post['imageMobileMetaDescription']);

        $readingTimeArray = explode(':', $post['readingTime']);
        $readingTime = (isset($readingTimeArray[0]) ? $readingTimeArray[0] * 3600 : 0) +
            (isset($readingTimeArray[1]) ? $readingTimeArray[1] * 60 : 0) +
            (isset($readingTimeArray[2]) ? $readingTimeArray[2] : 0);
        $np->setReadingTime($readingTime);
        $np->setUpdatedAt(new \DateTime());

        $type = $em->getRepository('App:BlogPostType')->find($post['type']);
        $category = $em->getRepository('App:BlogPostCategory')->find($post['category']);

        $np->setCategory($category);
        $np->setType($type);

        $em->persist($np);
        $em->flush();

        if (isset($files['podcastFile'])) {
            if ($np->getPodcastFile() != null && $np->getPodcastFile() != '') {
                $this->removeFile($np->getPodcastFile());
            }
            if ($np->getVideoFile() != null && $np->getVideoFile() != '') {
                $this->removeFile($np->getVideoFile());
                $np->setvideoFile(null);
            }
            $np->setPodcastFile($this->saveFile($files['podcastFile'], $np, 'blog'));
        }

        if (isset($files['videoFile'])) {
            if ($np->getVideoFile() != null && $np->getVideoFile() != '') {
                $this->removeFile($np->getVideoFile());
            }
            if ($np->getPodcastFile() != null && $np->getPodcastFile() != '') {
                $this->removeFile($np->getPodcastFile());
                $np->setPodcastFile(null);
            }
            $np->setvideoFile($this->saveFile($files['videoFile'], $np, 'blog'));
        }

        if (isset($files['image'])) {
            if ($np->getImage() != null && $np->getImage() != '') {
                $this->removeFile($np->getImage());
            }

            $np->setImage($this->saveFile($files['image'], $np, 'blog'));
        }

        if (isset($files['imageMobile'])) {
            if ($np->getImageMobile() != null && $np->getImageMobile() != '') {
                $this->removeFile($np->getImageMobile());
            }

            $np->setImageMobile($this->saveFile($files['imageMobile'], $np, 'blog'));
        }

        $em->persist($np);
        $em->flush();

        $productReferred = $request->request->get('referrer');
        if (is_array($productReferred)) $this->saveReferredContent($np, $productReferred);

        return $this->redirectToRoute('admin_blog_post_list');
    }

    private function saveReferredContent($post, $productReferred)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($productReferred as $referred) {
            $r = new ReferrerContent();
            $r->setMainContentId($post->getId());
            $r->setMainContentType('post');
            $r->setReferrerContentId((int)$referred['contentId']);
            $r->setReferrerContentType($referred['type']);
            $r->setCreatedAt(new \DateTime());
            $r->setUpdatedAt(new \DateTime());

            $em->persist($r);

            $rBack = new ReferrerContent();
            $rBack->setReferrerContentId($post->getId());
            $rBack->setReferrerContentType('post');
            $rBack->setMainContentId((int)$referred['contentId']);
            $rBack->setMainContentType($referred['type']);
            $rBack->setCreatedAt(new \DateTime());
            $rBack->setUpdatedAt(new \DateTime());

            $em->persist($rBack);
            $em->flush();
        }
    }

    private function saveFile($file, $post, $directory)
    {
        $date = new \DateTime();

        if (!is_dir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory)) {
            mkdir($this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory, 777);
        }

        $better_token = md5(uniqid(rand(), true));

        $fileName =  $this->slugify($better_token . '_' . $post->getSlug() . '_' . $date->getTimestamp() . '_' .  str_replace(' ', '_', $file->getClientOriginalName()));
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/uploads/' . $directory . '/' . $fileName;

        copy($file->getRealPath(), $filePath);

        return 'uploads/' . $directory . '/' . $fileName;
    }

    private function removeFile($fileName)
    {
        if ($fileName == '_dist/image/default_desktoobject.png' || $fileName == '_dist/image/img/default_mobile.png') {
            return false;
        }
        $filePath = $this->get('kernel')->getProjectDir() . '/public_html/' . $fileName;
        if (is_file($filePath)) unlink($filePath);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
