<?php

namespace App\Controller;

use App\Entity\UserCourse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\NeoPharmaService;
use App\Entity\UserRacine;
use Psr\Log\LoggerInterface;

/**
 * @Route("/racine/api")
 */
class RacineApiController extends Controller
{
    /**
     * @Route("/racine/api/curso/{courseId}", name="racine_api_subscribe")
     */
    public function subscribeUserToCourse(Request $request, $courseId)
    {
        //Pego o usuario logado
        $user = $this->getUser();

        // valido o usuario
        // if(!$user){
        //     return new JsonResponse([
        //         'status' => false,
        //         'message' => 'Usuário nâo cadastrado não encontrados'
        //     ]);
        // }

        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('App:Course')->findOneByCourseId($courseId);
        
        /** Regra para incentivo de vendas */
        $cod_indication = $request->request->get('c');

        if ($cod_indication) {
            $em->getRepository('App:PromoThirtyDayMovement')->savePoints($user, $cod_indication, 'curso', $course);
        }
        /** end */

        // valido o curso
        if (!$course) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Curso não encontrado'
            ]);
        }

        //checo se o curso for para farmaceuticos e se ele tem este role
        if ($course->getIsOnlyToPharmaceutical()) {
            if(!$this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && $course->getIsOnlyToClerk() == false){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Curso apenas para Farmacêuticos'
                ]);
            }
        }
        
        //checo se o curso for para balconistas e se ele tem este role
        if ($course->getIsOnlyToClerk()) {
            if(!$this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && $course->getIsOnlyToPharmaceutical() == false){
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Curso apenas para Balconistas'
                ]);
            }
        }
        
        //Pegar o user logado, fazer cadastro, fazer login, inscrever no curso e redirecionar para o curso usando a Api 
        $apiNeoPharma   = new NeoPharmaService(
            $this->getParameter('ws_racine_url'),
            $this->getParameter('ws_racine_token')
        );

        $userRacine = $apiNeoPharma->getUser($user, $courseId);
      
        if ($userRacine['status']) {
            $this->saveRacine($user, $userRacine['userRacineId'], $courseId); // salvar na tabela racine
        } 

        return new JsonResponse($userRacine);
    }

    public function saveRacine($user, $recineId, $courseId)
    {
        $em = $this->getDoctrine()->getManager();
        $racine = $em->getRepository('App:UserRacine')->findOneBy(['user' => $user->getId()]);
        $course = $em->getRepository('App:Course')->findOneByCourseId($courseId);
        
        //salvar na tabela de usuário do racine
        if (empty($racine)) {
            $racine = new UserRacine(); 
            $racine->setUser($user);
            $racine->setRacineId($recineId);
            $racine->setCreatedAt(new \DateTime());
            $racine->setUpdatedAt(new \DateTime());

            $em->persist($racine);
            $em->flush();
        }

        // salvando na tabela de curso de usuario racine
        if (!empty($course)) {
            $getUserCourseRacine = $em->getRepository('App:UserCourse')->findOneBy(['user' => $user->getId(), 'course' => $course->getId()]);
    
            if (empty($getUserCourseRacine)) {
                $userCourse = new UserCourse();
                $userCourse->setUser($user);
                $userCourse->setCourse($course);
                $userCourse->setSubscribedAt(new \DateTime());
                $userCourse->setAccessToken($this->getParameter('ws_racine_token'));

                $em->persist($userCourse);
                $em->flush();
            }
        }

        return true;
    }
}
