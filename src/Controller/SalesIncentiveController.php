<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SalesIncentiveController extends Controller
{
    /**
     * @Route("/incentivo-2021", name="incentivo")
     */
    public function incentivo(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('q') == 'search') {
            //dump($request->request->get('q')); die();
            $code = trim($request->request->get('cp'));
            //dump($code); die();
            $incentivo_vendas = $em->getRepository('App:PromoThirtyDayMovement')->findOneBy(['code' => $code, 'is_incentivo_vendas' => true]);
         
            if (!$incentivo_vendas) {
                return new JsonResponse(['status' => false, 'message' => 'Código não encontrado.']);  
            }
          
            $points = $incentivo_vendas->getPoints();
                       
            return new JsonResponse([
                'status' => true,
                'points' => $points > 0 ? $points . ' pontos' : 0,
                //'points' => count($points) > 0 ? count($points) . ' pontos' : 0,
            ]);
        }

        return $this->render('sales_incentive/index.html.twig', [

        ]);
    }
}
