<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/doralgina")
 */
class DoralginaController extends Controller
{
    /**
     * @Route("/", name="doralgina")
     */
    public function index(Request $request)
    {   
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }

        return $this->render('doralgina/index.html.twig', [
            'controller_name' => 'DoralginaController',
        ]);
    }

    /**
     * @Route("/quiz", name="doralgina_quiz")
     */
    public function quiz(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }
        return $this->render('doralgina/quiz.html.twig');
    }

    /**
     * @Route("/resposta/{slug}", name="doralgina_response", defaults={"slug"=null})
     */
    public function result(Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('home-login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_FARMACO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório
            if (!trim($this->getUser()->getCrf())) {
                $this->addFlash("alert", "Por favor preencha o campo CRF.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_BALCAO') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            //verificando usuários que não preencheram o campo obrigatório 
            if (!trim($this->getUser()->getCnpj())) {
                $this->addFlash("alert", "Por favor preencha o campo CNPJ.");
                return $this->redirectToRoute('user-home', ['paramenter'=>'cadastro-incompleto']);
            }
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_FARMACIA')){
            $this->addFlash("alert", "É necessário completar o cadastro para visualizar o conteúdo do site.");  
            return $this->redirectToRoute('home-register-complete', ['paramenter'=> 'incompleto']);
        }
        
        return $this->render('doralgina/result.html.twig');
    }
}
