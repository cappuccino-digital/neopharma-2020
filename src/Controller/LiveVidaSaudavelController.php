<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class LiveVidaSaudavelController extends Controller
{
    /**
     * @Route("/live-saude-mental", name="live_vida_saudavel")
     */
    public function index()
    {
        return $this->render('live-vida-saudavel/index.html.twig', [
            'controller_name' => 'LiveVidaSaudavelController',
        ]);
    }
}
