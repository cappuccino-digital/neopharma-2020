<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DocumentarioController extends Controller
{
    /**
     * @Route("/documentario", name="documentario")
     */
    public function index(Request $request)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password') || $request->query->has('r')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        return $this->render('documentario/index.html.twig', [
            'controller_name' => 'DocumentarioController',
        ]);
    }
}
