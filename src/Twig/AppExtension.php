<?php 

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Doctrine\ORM\EntityManagerInterface;

class AppExtension extends AbstractExtension
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * GetProvinceExtension constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('contentRelacion', [$this, 'contentRelacion']),
            new TwigFunction('calcPercentageSurvey', [$this, 'calcPercentageSurvey']),
            new TwigFunction('categoryBlog', [$this, 'categoryBlog']),
            new TwigFunction('qtdSurvey', [$this, 'qtdSurvey']),
            new TwigFunction('notificationGamification', [$this, 'notificationGamification']),
            new TwigFunction('userPositionGamification', [$this, 'userPositionGamification']),
            new TwigFunction('pagesSpecial', [$this, 'pagesSpecial']),
            new TwigFunction('pagesSpecialContents', [$this, 'pagesSpecialContents']),
            new TwigFunction('userPointTotalGamefication', [$this, 'userPointTotalGamefication']),
        ];
    }

    public function contentRelacion($type, $content)
    {
        $contents = $this->em->getRepository('App:ReferrerContent')->findBy(['mainContentType' => $type, 'mainContentId' => $content->getId()], ['createdAt'=> 'DESC']); 
        
        $item = [];
        foreach ($contents as $content) {
            if ($content) {
                //dump($content->getReferrerContentId()); die(); 
                if ($type == 'curso') {
                    $item[] = $this->em->getRepository('App:Course')->findOneBy(['id' => $content->getReferrerContentId()]); 
                    //dump($item); die();
                }
            }
        }
        
        return $item;
    }

    public function calcPercentageSurvey($user)
    {
        //dump($user); die();

        $em = $this->em;
        $question = $em->getRepository('App:SurveyQuestion')->findOneBy(['is_active' => true], ['created_at'=>'DESC']);

        if ($question) {
            //dump($question); die();
            $no = $em->getRepository('App:SurveyQuestionResponse')->createQueryBuilder('q')
                    ->select('count(q.id)')
                    ->where('q.is_yes = false')
                    ->andWhere('q.question = :question')
                    ->setParameter('question', $question)
                    ->getQuery()
                    ->getSingleScalarResult();

            $yes = $em->getRepository('App:SurveyQuestionResponse')->createQueryBuilder('q')
                    ->select('count(q.id)')
                    ->where('q.is_yes = true')
                    ->andWhere('q.question = :question')
                    ->setParameter('question', $question)
                    ->getQuery()
                    ->getSingleScalarResult();
            //die('teste');

            #$question = $em->getRepository('App:SurveyQuestion')->findOneBy([], ['created_at'=>'DESC']);
            $participate = $this->em->getRepository('App:SurveyQuestionResponse')->findOneBy(['user' => $user, 'question' => $question]);
            
            //verificando se o usuário participou da enquete
            $is_participate = false;
            if ($participate) {
                $is_participate = true;
            }
            
            //verificando as respostas (sim ou não) quem teve mais votos 
            if ($yes == $no) {
                $qtd = $no;
                $message = $question->getResponseEmpate();
            }else if ($yes > $no) {
                $qtd = $yes;
                $message = $question->getResponseYes();
            } else {
                $qtd = $no;
                $message = $question->getResponseNo();
            }
            
            //checando o total de votos 
            $total = $this->em->getRepository('App:SurveyQuestionResponse')->createQueryBuilder('q')
                ->select('count(q.id)')
                ->where('q.question = :question')
                ->setParameter('question', $question)
                ->getQuery()
                ->getSingleScalarResult();
            //dump($total); die();
            //calculando a porcentagem
            if ($total > 0) {
                $percentage = ($qtd * 100) / $total; 
            } else {
                $percentage = 0;
            }

           $response = ['is_participate' => $is_participate, 'percentage' => intval($percentage), 'message' => $message];
        } else {
            $response = ['is_participate' => false, 'percentage' => 0, 'message' => ''];
        }

        //dump($response); die();
        return $response;
    }

    public function qtdSurvey($question)
    {
        $em = $this->em;
        $question = $em->getRepository('App:SurveyQuestion')->findOneById($question);
        $response = ['status' => false, 'yes' => 0, 'no' => 0];

        if ($question) {
            $no = $em->getRepository('App:SurveyQuestionResponse')->createQueryBuilder('q')
                    ->select('count(q.id)')
                    ->where('q.is_yes = false')
                    ->andWhere('q.question = :question')
                    ->setParameter('question', $question)
                    ->getQuery()
                    ->getSingleScalarResult();

            $yes = $em->getRepository('App:SurveyQuestionResponse')->createQueryBuilder('q')
                    ->select('count(q.id)')
                    ->where('q.is_yes = true')
                    ->andWhere('q.question = :question')
                    ->setParameter('question', $question)
                    ->getQuery()
                    ->getSingleScalarResult();

            $response = ['status' => true, 'yes' => $yes, 'no' => $no];
        }

        return $response;
    }

    public function categoryBlog($slug)
    {   
        $category = '';

        if ($slug == 'materias') {
            $category = '<i class="icon-materia"></i> MATÉRIA';
        } else if ($slug == 'video') {
            $category = '<i class="icon-video"></i> VÍDEO';
        } else if ($slug == 'podcast') {
            $category = '<i class="icon-podcast"></i> PODCAST';
        } else if ($slug == 'quiz') {
            $category = '<i class="icon-quiz"></i> QUIZ';
        } 
        return $category;
    }

    public function notificationGamification($user)
    {  
        $em = $this->em;
        $user_points = $em->getRepository('App:UserPointGamification')->findOneBy(['user' => $user, 'is_mission' => true], ['id' => 'DESC']);
        $user_level = $em->getRepository('App:UserLevelGamification')->findOneBy(['user' => $user], ['id' => 'DESC']);
       
        $array_content['is_mission'] = false;
        $array_content['is_mission_show_popup'] = false;
        
        $array_content['is_level'] = false;
        $array_content['is_level_show_popup'] = false;

        if ($user_points) {
            if ($user_points->getIsMission()) {
                $user_medals = $em->getRepository('App:UserMedalsGamification')->findOneBy(['user' => $user_points->getUser()], ['id' => 'DESC']);
                
                $array_content['is_mission_show_popup'] = $user_points->getIsShowPopup();

                if ($user_medals) {
                    $array_content['is_mission'] = true;
                    $array_content['point_id'] = $user_points->getId();
                    
                    $array_content['interation_name'] = $user_points->getInteration()->getName();
                    $array_content['interation_point'] = $user_points->getInteration()->getPoint();
                    $array_content['medal_name'] = $user_medals->getMedal()->getName();
                    $array_content['medal_slug'] = $user_medals->getMedal()->getSlug();
                    $array_content['medal_icon'] = $user_medals->getMedal()->getIcon();   
                }   
            }
        }

        if ($user_level) {
            $level = $em->getRepository('App:LevelGamification')->findOneBy(['id' => $user_level->getLevel()]);

            $array_content['is_level'] = true;
            $array_content['id_level'] = $user_level->getId();
            $array_content['name_level'] = $level->getName();
            $array_content['slug_level'] = $level->getSlug();
            $array_content['is_level_show_popup'] = $user_level->getIsShowPopup();

        }

        return $array_content;
    }

    public function userPositionGamification($search_key, $search_value, $user)
    {
        return $this->em->getRepository('App:UserPointGamification')->userPositionGamification($search_key, $search_value, $user);
    }

    public function pagesSpecial()
    {
        return $this->em->getRepository('App:SpecialCampaign')->findBy(['is_active' => true, 'is_menu' => true]);
    }

    public function pagesSpecialContents($landing_page)
    {
        return $this->em->getRepository('App:SpecialCampaign')->findOneBy(['slug' => $landing_page, 'is_active' => true]);
    }

    public function userPointTotalGamefication($user)
    {  
        return $this->em->getRepository('App:UserPointTotalGamification')->findOneBy(['user' => $user]);
    }
}