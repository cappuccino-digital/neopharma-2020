// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    messagingSenderId: "585804593058",
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();


messaging.setBackgroundMessageHandler(function(payload) {	
    var notificationTitle = payload.data.title;
    var notificationOptions = {
        body: payload.data.message,
        icon: 'https://portalneopharma.com.br/_dist/image/icon.png',
        collapse_key: false
    };
  	return self.registration.showNotification(notificationTitle,notificationOptions);
});