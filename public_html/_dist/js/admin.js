$("document").ready(function(){
    hideLoading();

    $(".menu-father").on("click", function(){

        var menuId = "#" + $(this).attr("id") + "-menu";
        console.log(menuId);

        if( $(menuId).hasClass("opened") ){
            $(menuId).slideUp('slow', function(){
                $(this).removeClass("opened");
            });
        } else {
            $(".treeview-menu.opened").slideUp('slow', function(){
                $(".treeview-menu").removeClass("opened");
            });
            
            $(menuId).slideDown('slow', function(){
                $(this).addClass("opened");
                $(this).css("display","flex");
            });
        }
    });

    $(".select-content").select2({
        theme: "classic"
    });

    $(".hold-menu").on("click", function(){
        if($(".hold-menu").hasClass("sidebar-hide")){
            $(".sidebar").show("slow", function(){
                $(".hold-menu").removeClass("sidebar-hide");
                $(".admin-content").css("width", "calc(100% - 250px)");
                $(".hold-menu").html('<i class="fas fa-times"></i>');
                if(root == 'admin_home'){
                    chartMonthLead.flush();
                    chartGender.flush();
                    chartStateBalconista.flush();
                    chartStateFarmaceutico.flush();
                    chartStateLifetimeFarmaco.flush();
                    chartLifetimeFarmaco.flush();
                    chartStateLifetimeBalconista.flush();
                    chartGenderLifetimeBalconista.flush();
                    chartPostByMonthGraph.flush();
                    chartPostByType.flush();
                    chartPostByCategory.flush();
                    chartCourseStatistic.flush();
                }
            });
        } else {
            $(".sidebar").hide("slow", function(){
                $(".hold-menu").addClass("sidebar-hide");
                $(".admin-content").css("width", "100%");
                $(".hold-menu").html('<i class="fas fa-bars"></i>');
                if(root == 'admin_home'){
                    chartMonthLead.flush();
                    chartGender.flush();
                    chartStateBalconista.flush();
                    chartStateFarmaceutico.flush();
                    chartStateLifetimeFarmaco.flush();
                    chartLifetimeFarmaco.flush();
                    chartStateLifetimeBalconista.flush();
                    chartGenderLifetimeBalconista.flush();
                    chartPostByMonthGraph.flush();
                    chartPostByType.flush();
                    chartPostByCategory.flush();
                    chartCourseStatistic.flush();
                }
            });
        }
    });

    $(".remove-referrer-register").on("click", function(){
        var canRemove = confirm("Deseja realmente remover este conteúdo?");

        if(canRemove){
            showLoading();
            var parent = $(this).parent();
            var id = $(this).data("id");
            var ajaxUrl = removeReferredContentUrl;
            $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "json",
                data: {
                    id : id
                },
                success: function(response) {
                    if (response.status) {
                        alert("Referencia removida com sucesso!");
                        parent.remove();
                    } else {
                        alert(response.message);
                    }
                    hideLoading();
                }
            });
        }
    });

    $("#add-referrer").on("click", function () {
        
        var newReferred = '<div class="form-group"><label for="asset-type">Tipo de Conteudo</label><select required class="selectReferrerType" name="referrer[0][type]"><option value="">Selecione o tipo de conteúdo</option><option value="post">Post</option><option value="produto">Produto</option><option value="curso">Curso</option><option value="quiz">Quiz</option></select></div><div class="form-group"><label for="asset-type">Conteudo</label><select required class="select-content" name="referrer[0][contentId]"></select></div><i class="fa fa-times remove-referrer" aria-hidden="true"></i>';
        var lastResult = $(".hold-referrer-content").last();
        var id = lastResult.data("id");

        if(id == undefined){
            id = 0;
        }
        var findString = "referrer\\[0\\]";
        var replaceString = "referrer[" + (id + 1) + "]";

        var re = new RegExp(findString, "g");
        var newhtml = newReferred.replace(re, replaceString);

        var newResult = '<div class="hold-referrer-content form-group-double" data-id="' + (id + 1) + '">' + newhtml + '</div>';
        $("#hold-referrer").append(newResult);

        $(".select-content").select2({theme: "classic"});

        $(".selectReferrerType").unbind("change").on("change", function () {
            var thisSelect = $(this);
            showLoading();
            $.ajax({
                url: referredContentUrl,
                type: "POST",
                dataType: "json",
                data: {
                    type: thisSelect.val()
                },
                success: function (response) {
                    if (response.status) {
                        thisSelect.parent(".form-group").siblings(".form-group").children(".select-content").html("");
                        for (aux = 0; aux < response.values.length; aux ++) {
                            var value = response.values[aux];
                            thisSelect.parent(".form-group").siblings(".form-group").children(".select-content").append('<option value="' + value.value + '">' + value.label + '</option>');
                        }
                    }
                    hideLoading();
                }
            });
        });

        $(".remove-referrer").unbind("click").on("click", function(){
            $(this).parent().remove();
        });

        
    });


    CKEDITOR.on('instanceReady', function () {
        $.each(CKEDITOR.instances, function (instance) {
            CKEDITOR.instances[instance].document.on("keyup", CK_jQ);
            CKEDITOR.instances[instance].document.on("paste", CK_jQ);
            CKEDITOR.instances[instance].document.on("keypress", CK_jQ);
            CKEDITOR.instances[instance].document.on("blur", CK_jQ);
            CKEDITOR.instances[instance].document.on("change", CK_jQ);
        });
    });

    function CK_jQ() {
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        }
    }

    
});

function showLoading(){
    $('.lightbox').css("display","flex");
}

function hideLoading(){
    $('.lightbox').css("display","none");
}