$(document).ready(function(){
    var header = $('header');

    // Compactando o Meny superior no scroll
    $(window).scroll(function () {
        compactHeader();
    });

    $('.nav .dropdown').on('click', function(){
        $(this).find('.submenu').toggleClass('open');
    });

    $('.name-user').on('click', function(){
        $(this).parent().find('.user-drop-dowm').toggleClass('open');
    });

    function openMenu(){
        $( '#menu-mobile' ).unbind('click').on('click' , function(e){
            e.preventDefault();

            if($('#menu-mobile').hasClass('navOpen')){
                $('#menu-mobile').removeClass('navOpen');
                $('.menu').removeClass('open-menu');
                $('.avatar-menu').removeClass('open');
                $('.search-global').removeClass('open');
            } else{
                $('#menu-mobile').addClass('navOpen');
                $('.menu').addClass('open-menu');
                $('.avatar-menu').addClass('open');
                $('.search-global').addClass('open');
            }
        });
    }

    openMenu();


    function compactHeader() {
        if ($(window).scrollTop() > header.outerHeight()) {
            header.addClass('compact');
            $('body').addClass('compact');
        } else {
            header.removeClass('compact');
            $('body').removeClass('compact');
        }
    }

    if($("#slide-products .slides").length) {
        $("#slide-products .slides").owlCarousel({
            autoplay: true,
            autoplayHoverPause: true,
            autoplayTimeout: 120000,
            center: true,
            margin: 0,
            loop: true,
            responsiveClass: true,
            nav: false,
            items: 1
        }).on('changed.owl.carousel', function(e) {
            $(this).parent().find('.slide-icon').fadeOut(3000);
        });

        $("#slide-products .owl-dot").each(function(x) {
            var x = x + 1;
            $(this).attr("id", "slide-products-dot-"+x);
        });
    }


    function openModal(){
        $( '[data-modal]' ).unbind('click').on('click' , function(e){
            e.preventDefault();
            var openModal = $(this).attr("data-modal");
            $('body').addClass('open-modal');
            $('.modal-default').removeClass('open-modal');
            $('#'+openModal).addClass('open-modal');
        });
    }

    function closeModal(){
        $( '.close-modal' ).unbind('click').on('click' , function(e){
            e.preventDefault();

            if($('body, .modal-default').hasClass('open-modal')){
                $('body, .modal-default').removeClass('open-modal');
            }
        });
    }

    $( '.popup-gamefication .close-pop' ).unbind('click').on('click' , function(e){
        e.preventDefault();
        $(this).parent().parent().fadeOut();
        closeNotification()        
    });

    openModal();
    closeModal();
    if(root == 'home'){
        fire.init();
    }


    
});