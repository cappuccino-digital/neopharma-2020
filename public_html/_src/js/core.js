var core = core || {};
var messageLogin = "Você precisa estar logado na plataforma.";
core = {
    init: function(){
        core.startGamePoints();
        core.startSocialMediaLogin();
        core.startRegister();
    },
    startGamePoints: function(){
        $(".rate-object").on("click", function(){
            if(userIsLogged == 1){
                core.saveScore($(this).data('ajax'));
            } else {
                $(this).prop('checked', false);
                $(this).siblings(".rate-object").each(function(){
                    $(this).prop('checked', false);
                    if($(this).hasClass('scored')) $(this).prop('checked', true);
                });
                core.showMessage("login", messageLogin);
            }
        });

        $('.check-if-is-logged').unbind("click").on("click", function(e){
            var href = $(this).attr('href');
            $('#login-route').val(href);
            //console.log('HREF ' + $(this).attr('href'));
            if(userIsLogged == 0){
                core.showMessage("login", messageLogin);
                e.preventDefault();
                return false;
            } 
        });

        $(".add-favorite").on("click", function(){
            if(userIsLogged == 1){
                core.addFavorite($(this).data('ajax'));
            } else {
                core.showMessage("login", messageLogin);
            }
        });

        $(".share-linkedin, .share-facebook, .share-whatsapp, .share-email").unbind("click").on("click", function(){
            if(userIsLogged == 1){
                var ajaxUrl = $(this).data("ajaxurl");
                core.shareContent(ajaxUrl);
            }
        });
    },
    startSocialMediaLogin: function(){
        gapi.load('auth2', function(){
            auth2 = gapi.auth2.init({
                client_id: '175399297069-iqkk9g6q4pruogt6h9cqsl5jsqorm97b.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
            });

            auth2.attachClickHandler(document.querySelector('.googleLoginButton'), {},
                function(googleUser) {
                    onSignIn(googleUser);
                }, function(error) {
                    
                }
            );
        });

        $(".facebookLoginButton").unbind('click').on("click", function(){
            FB.login(function(response){
                var dataConnect = response;
                if (dataConnect.status === 'connected') {
                    FB.api(
                        "/" + dataConnect.authResponse.userID + "/",{'fields': 'name, email, id'},
                        function (userdata) {
                            if (userdata && !userdata.error) {
                                dataConnect.userInfo = userdata;
                                dataConnect.type = 'facebook';
                                core.loginUserSocialMedia(dataConnect);
                            }
                        }
                    );
                } else {
                    
                }
                
            }, {scope: 'public_profile,email'});
        });
        
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();

            var dataConnect = {
                'userInfo' : {'id' : profile.getId(), 'name' : profile.getName(), 'avatar':profile.getImageUrl(),  'email':profile.getEmail()},
                'type' : 'google'
            };

            core.loginUserSocialMedia(dataConnect);
            
        }       
    },
    startRegister: function(){
        $("#select-register-gestor").unbind("Click").on("click", function(e){
            $(".is-gestor").css("display" , "block");
            $(".is-not-gestor").css("display" , "none");
        });

       // $("#register-cnpj").prop("required", true);//perfil balconista adicionando obrigatóridade no campo CNPJ

        $("#select-register-balc").unbind("Click").on("click", function(e){
            $(".is-gestor").css("display" , "none");
            $(".is-not-gestor").css("display" , "block");
            $("#register-crf-hold").css("display", "none");
            $("#register-crf").prop("required", false);
            $("#register-state-hold").css("display", "none");
            $("#label-register-state").html("Estado ");
            $("#register-state").prop("required", false);
            $("#register-cnpj-hold").css("display", "block");
            $("#register-cnpj-farmaceutico-hold").css("display", "none");
            $("#label-for-cnpj").html('CNPJ da farmácia * <span class="error cnpj-error" id="cnpj-error"></span>');
            $("#register-cnpj").prop("required", true);
            $("#user-cpf").prop("required", true);
            $("#register-cpf").prop("required", true);
            $("#label-for-cpf").html('CPF * <span class="error" id="cpf-error">');
        });

        $("#select-register-farmac").unbind("Click").on("click", function(e){
            $(".is-gestor").css("display" , "none");
            $(".is-not-gestor").css("display" , "block");
            $("#register-crf-hold").css("display", "block");
            $("#register-crf").prop("required", true);
            $("#register-state-hold").css("display", "block");
            $("#label-register-state").html("Estado *");
            $("#register-cnpj-farmaceutico-hold").css("display", "block");
            
            $("#label-for-cnpj").html('CNPJ da farmácia <span class="error cnpj-error" id="cnpj-error"></span>');
            $("#register-state").prop("required", true);
            $("#register-cnpj-hold").css("display", "block");
            $("#register-cnpj").prop("required", false);
            $("#user-cpf").prop("required", false);
            $("#register-cpf").prop("required", true);
            $("#label-for-cpf").html('CPF * <span class="error" id="cpf-error">');
        });          
    },
    loginUserSocialMedia: function(data){
        $.ajax({
            url: ajaxUrlLoginSocialMedia,
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                if (response.status) {
                    if (response.hasOwnProperty('isLogged')) {
                        window.location.reload();
                    } else {
                        window.location = register + '?l=' + response.lead;
                    }
                    // if(response.shouldComplete){
                    //     window.location = completeRegister;
                    // } else {
                    //     window.location.reload();
                    // }
                } else {
                    core.showMessage("error", response.message);
                }
            }
        });
    },
    addFavorite: function(ajaxUrl){
        $.ajax({
            url: ajaxUrl,
            type: "POST",
            dataType: "json",
            success: function (response) {
                if (response.status) {
                   if(response.isActive){
                        $(".add-favorite").addClass('saved');
                   } else {
                        $(".add-favorite").removeClass('saved');
                   }
                } else {
                    core.showMessage("login", response.message);
                }
            }
        });
    },
    saveScore: function(ajaxUrl){
        $.ajax({
            url: ajaxUrl,
            type: "POST",
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    $(".rate-object").each(function(){
                        $(this).prop('checked', false);
                        $(this).removeClass('scored');
                        if(parseInt($(this).val()) <= parseFloat(response.avgScore)){
                            $(this).prop('checked', true);
                            $(this).addClass('scored');
                        }
                    });
                } else {
                    core.showMessage("login", response.message);
                }
            }
        });
    },
    showMessage: function(type, message){
        $("#lightbox-message").html("");
        //alert(type , message, this);
        if(type == 'login'){
            $("#lightbox-message").html(message);
            $("#lightbox-login-form").css("display","block");
            $("#login-form-lightbox").unbind('submit').on('submit',function(e){
                $("#login-form-lightbox").unbind('submit');
                $("#lightbox-message").html("Entrando na plataforma, aguarde.");
                e.preventDefault();
                core.login(e);
            });
        } else {
            $("#lightbox-message").html(message);
            $("#lightbox-login-form").css("display","none");
        }
        
        $('body').addClass('open-modal');
        $('.modal-default').addClass('open-modal');
    },
    hideMessage: function(){
        if($('body, .modal-default').hasClass('open-modal')){
            $('body, .modal-default').removeClass('open-modal');
        }
    },
    login: function(e){
        var username = $("#login-form-lightbox-username").val();
        var password = $("#login-form-lightbox-password").val();
        var route = $('#login-route').val();

        if(username == "" || password == ""){
            $("#lightbox-message").html("Todos os campos são obrigatórios.");
            $("#login-form-lightbox").unbind('submit').on('submit',function(e){
                $("#login-form-lightbox").unbind('submit');
                $("#lightbox-message").html("Entrando na plataforma, aguarde.");
                e.preventDefault();
                core.login(e);
            });
        }

        $.ajax({
            url: ajaxUrlLogin,
            type: "POST",
            dataType: "json",
            data: {
                'u': username,
                'p': password
            },
            success: function (response) {
                if (response.status) {
                    $("#lightbox-message").html("Login executado com sucesso.");
                    setTimeout(function(){
                        if (route) {
                            window.location = route;
                        } else {
                            window.location.reload();
                        }
                    },2000);
                } else {
                    $("#lightbox-message").html(response.message);
                    $("#login-form-lightbox").unbind('submit').on('submit',function(e){
                        $("#login-form-lightbox").unbind('submit');
                        $("#lightbox-message").html("Entrando na plataforma, aguarde.");
                        e.preventDefault();
                        core.login(e);
                    });
                }
            }
        });

    },
    shareContent: function(ajaxUrl){
        $.ajax({
            url: ajaxUrl,
            type: "POST",
            dataType: "json",
            success: function (response) {
                if (response.status) {
                } else {
                }
            }
        });
    },
    setQuizDone: function(ajaxUrl){
        $.ajax({
            url: ajaxUrl,
            type: "POST",
            dataType: "json",
            success: function (response) {
                if (response.status) {
                } else {
                }
            }
        });
    }
};

$("document").ready(function(){
    core.init();
});